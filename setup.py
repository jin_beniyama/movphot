#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup
from glob import glob
from os.path import basename
from os.path import splitext



if __name__ == "__main__":

  # Read README.md
  with open("README.md", "r") as f:
    long_description = f.read()

  author = "Jin BENIYAMA"
  email = "beniyama@ioa.s.u-tokyo.ac.jp"

  # from https://pypi.org/classifiers/
  classifiers = [
    "Development Status :: 1 - Planning",
    "Programming Language :: Python :: 3.7",
    "License :: OSI Approved :: MIT License"
    ]
  
  setup(
    name="movphot",
    version="0.0.1",
    author=author,
    author_email=email,
    maintainer=author,
    maintainer_email=email,
    url="https://bitbucket.org/jin_beniyama/movphot/src/master/",
    # Upload scripts in 'src' as 'movphot'
    packages=["movphot", "movphot.scripts"],
    # Define condole scripts
    entry_points = {
      "console_scripts": ["phot_color = movphot.scripts.phot_color:main"
                          ]
    },
    install_requires = ["astropy", "astroquery"],
    description="moving object photometry",
    long_description = long_description,
    long_description_content_type="text/markdown"
    )
