#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Determine Color Term (CT) using stars in all frames
by setting an offset to 0 in each axis.
Input csv has to contain at least 2-bands photometric results,
created with derive_color.py.
The filename is like below.
  3200_colall_N100_hoge.csv"

Large color (col_inst or col_cat) points are removed in output figures.
(a value err_th is used to remove objects 
by both catalog mantide and instrumental magnitude)

Definitions of CT and Z
------------------------------
  CT (color term):
    slope of color_cat (m_cat,1-m_cat,2) vs m_inst,1 - m_cat,1
  Z (magnitude zero point):
    intercept of color_cat (m_cat,1-m_cat,2) vs m_inst,1 - m_cat,1

CTs are common values for an instrument. (outputs of this script)
Zs could differ even in during the same night.


Output Examples
---------------
(saved in plotmag)
2021TY14_CTstatus_N4_gri.png



Keys
----
magnitude: g_inst, gerr_inst, g_ccor, gerr_ccor, g_wmean, g_wstd
           (g_mean, g_std, g_std_phot, g_SD, g_SE)
color    : g_r, g_rerr, g_r_wmean, g_r_wstd
           (instrumental colors are not used in this script)
CTG/CTI:
CT/Z:



Summary of Procedures
---------------------
(p1, p2, ... are usefull search words.)
  p1  Read photometric csv
  p2  Obtain common objID from input DataFrame
  p3  Remove red comparison stars from df 
  p4  Remove blue comparison stars from df 
  p5  Remove large magnitude error comparison stars from df
  p6  Remove comparison stars from df by magnitude
  p7  Calculate CTs (and Zs temporally, not final values)
  p8  Save CTs
"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from movphot.common import log10err, checknan
from movphot.visualization import myfigure_ncol
from movphot.prepro import (
    clean_photres, calc_d_from_edge, calc_CT, plot_CT, plot_CTfit, 
    calc_Z, plot_Z, band4CT, band4CTG, calc_objmag, calc_meanmag,
    calc_simplemeancolor, plot_objmag, calc_resmag, plot_res)


if __name__ == "__main__":
    parser = ap(description="Derive object magnitude with Color Term")
    parser.add_argument(
        "csv", type=str, help="photometry_result.csv")
    parser.add_argument(
        "colcsv", type=str, help="csv contains object colors")
    parser.add_argument(
        "obj", type=str, help="object name")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--inst", type=str, default=None, help="instrumental name")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--catmag_min", type=float, default=13,
        help="minimum magnitude")
    parser.add_argument(
        "--catmag_max", type=float, default=20,
        help="maximum magnitude")
    parser.add_argument(
        "--catmagerr_max", type=float, default=0.1, 
        help="maximum catalog magnitude error")
    parser.add_argument(
        "--eflag_max", type=float, default=1, 
        help="maximum eflag value for both object and comparison stars")
    parser.add_argument(
        "--gr_max", type=float, default=1.1, 
        help="maximum g_r value for comparison stars")
    parser.add_argument(
        "--gr_min", type=float, default=-0.5, 
        help="minimum g_r value for comparison stars")
    parser.add_argument(
        "--ri_min", type=float, default=0, 
        help="minimum g_r value for comparison stars")
    parser.add_argument(
        "--ri_max", type=float, default=0.8, 
        help="maximum g_r value for comparison stars")
    parser.add_argument(
        "--cntmfrac_max", type=float, default=0.05, 
        help="maximum contamination fraction")
    parser.add_argument(
        "--dedge_min", type=int, default=30, 
        help="maximum distance from the edge")
    parser.add_argument(
        "--nx", type=int, default=2160, help="number of pixels along x axis")
    parser.add_argument(
        "--ny", type=int, default=1280, help="number of pixels along y axis")
    parser.add_argument(
        "--key_x", type=str, default="x", help="keyword for x")
    parser.add_argument(
        "--key_y", type=str, default="y", help="keyword for y")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, 
        help="output messages")
    parser.add_argument(
        "--Nobj_min", type=int, default=10, 
        help="minimum object numbers in a frame to be desired")
    parser.add_argument(
        "--Ncut", type=int, default=1, 
        help="Cut dataframe to check CT status.")
    parser.add_argument(
        "--target", nargs=5, default=[0, 0, 0, 0, 0], 
        help="Search object using x0, x1, y0, y1 in CTplot")
    parser.add_argument(
        "--ycolor", type=int, default=1, 
        help="Number of colors to plot CT residual histograms depends on y")
    parser.add_argument(
        "--colormap", action="store_true", default=False,
        help="Plot CT fitting with color map like Fig. 7 in Jackson+2021")
    args = parser.parse_args()


    # Set output directory
    outdir = "plotmag"
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    # Parse arguments
    # nx and ny are used to remove objects close to edge region
    if args.inst == "TriCCS":
        nx, ny = 2160, 1280
    else:
        nx, ny = args.nx, args.ny
    bands = args.bands

    # Do not remove objects with extreme colors
    target         = args.obj
    gr_min, gr_max = args.gr_min, args.gr_max
    ri_min, ri_max = args.ri_min, args.ri_max
    catmag_min     = args.catmag_min
    catmag_max     = args.catmag_max
    catmagerr_max  = args.catmagerr_max
    cntmfrac_max   = args.cntmfrac_max
    dedge_min      = args.dedge_min
    key_x, key_y   = args.key_x, args.key_y
    # Selection criteria of reference stars
    Nobj_min    = args.Nobj_min
    # Selection criteria of the target
    eflag_max      = args.eflag_max
    verbose        = args.verbose
    ycolor         = args.ycolor
    colormap       = args.colormap
    if colormap:
        cmstr = "cm"
    else:
        cmstr = ""
    # Number of bands
    N_band = len(bands)
    Ncut = args.Ncut
    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]
    # For output files
    catmag_max_str = "f".join(str(catmag_max).split("."))
    catmag_min_str = "f".join(str(catmag_min).split("."))
    eflag_max_str = "f".join(str(eflag_max).split("."))

    filename_tail = (
        f"{band_part}_magmin{catmag_min_str}magmax{catmag_max_str}Nobjmin"
        f"{Nobj_min}eflagth{eflag_max_str}dedge{dedge_min}")

    print("\nArguments")
    print(f"Extreme colors gr color          = {gr_min}--{gr_max}")
    print(f"               ri color          = {ri_min}--{ri_max}")
    print(f"               catalog mag       > {catmag_min}")
    print(f"               catalog mag       < {catmag_max}")
    print(f"               catalog mag error < {catmagerr_max}")
    print(f"               distance to edge  > {dedge_min}")
    print(f"               key_x, key_y      = {key_x}, {key_y}")
    print(f"               Nobj_min          = {Nobj_min}")
    print(f"               eflag             < {eflag_max}")


    print("\n================================================================")
    print("p1 Read photometric csv")
    print("==================================================================")
    df = pd.read_csv(args.csv, sep=" ")
    print(f"  N_df = {len(df)} (original)")


    print("\n================================================================")
    print("p2 Clean the reference stars")
    print("==================================================================")
    # only gr_min, gr_max
    df, objID = clean_photres(
        df, bands, key_x, key_y, gr_min, gr_max, ri_min, ri_max, 
        catmag_min, catmag_max, catmagerr_max, verbose)

    
    print("\n================================================================")
    print("p3 Calculate distance from the edge in each frame")
    print("==================================================================")
    # Suppress PerformanceWarning
    df = df.copy()
    for b in bands:
        for obj in objID:
            df.loc[:,f"dedge_{obj}_{b}"] = calc_d_from_edge(
                df[f"{key_x}_{obj}_{b}"], df[f"{key_y}_{obj}_{b}"], 
                nx, ny)


    print("\n================================================================")
    print("p4 Calculate time in second and save for periodic analysis")
    print("==================================================================")
    for b in bands:
        # For periodic analysis 
        df.loc[:, f"t_sec_obs_{b}"] = df[f"t_mjd_{b}"]*24*3600.
        # Set the first time to zero
        df[f"t_sec_obs_{b}"] -= df.at[0, f"t_sec_obs_{b}"]


    print("\n================================================================")
    print("p5 Calculate and plot CTs")
    print("==================================================================")
    # Cut and replot CT for the sake of the clarity
    df_cut_list = []
    for n in range(Ncut):
        # Extract df
        N0 = int(len(df)/Ncut*n)
        N1 = int(len(df)/Ncut*(n+1))
        # if Ncut !=0, Create separate plot
        df_temp = df[N0:N1]
    
        # axes_raw : 
        #   mag_cat - mag_inst vs. catalog color with raw fit
        # axes_shift : 
        #   mag_cat - mag_inst vs. catalog color with weighted fit
        # axes_res : 
        #   residuals (MC model with shift - obs.)
        fig, axes_raw, axes_shift, axes_CT, axes_res = myfigure_ncol(
            len(bands), 3, True)
        df_CT_list = []
        axes_raw[0].set_title("Without shift")
        axes_shift[0].set_title("Shifted to a mean of zero")
        for idx,band in enumerate(bands):
            # Search target only in selected band
            if band == args.target[4]:
                # target has [x0, x1, y0, y1, bands]
                target_info = [float(x) for x in args.target[0:4]]
            else:
                target_info = None
            
            ax_raw = axes_raw[idx]
            ax_shift = axes_shift[idx]
            ax_CT = axes_CT[idx]
            ax_res = axes_res[idx]
            # Calculate CTs 
            #   Always save y_coord as well 
            #   to investigate y coordinate dependence
            CT_df_list = calc_CT(
                df_temp, objID, band, bands, Nobj_min, 
                eflag_max, dedge_min, cntmfrac_max, target_info)
            # Plot without shift
            #   Plot residuals using different colors if ycolor > 1
            #   to investigate y coordinate dependence
            _ = plot_CTfit(
                CT_df_list, band, bands, fig, ax_raw, shift=False, 
                ycolor=ycolor, ny=ny, colormap=colormap)
            # Plot with shift
            df_CT = plot_CTfit(
                CT_df_list, band, bands, fig, ax_shift, ax_res=ax_res, 
                shift=True, ycolor=ycolor, ny=ny, colormap=colormap)
            # Time-series CT
            plot_CT(
                CT_df_list, band, bands, ax_CT)

            df_CT_list.append(df_CT)
        out =  (
            f"{args.obj}_CTstatus{n+1}_N{len(df_temp)}"
            f"_{filename_tail}{cmstr}.png")
        out = os.path.join(outdir, out)
        plt.savefig(out, dpi=200)
        plt.close()
        df_CT = pd.concat(df_CT_list, axis=1)
        # Add cut df
        df_cut_list.append(df_CT)
    df_CT = pd.concat(df_cut_list, axis=0)

    # Quit if N_band == 2
    if N_band==2:
        sys.exit()

    # Only plot and finish analyses when Ncut > 1
    if len(df_CT) > 1:
        print("Only plot and finish analyses when Ncut > 1")
        sys.exit()

    checknan(df)

    # For reference 
    print("\n================================================================")
    print("p6 Save CTs")
    print("==================================================================")
    out =  f"{args.obj}_CT_N{len(df)}_{filename_tail}.csv"
    df_CT.to_csv(out, sep=" ", index=False)

  
    print("\n================================================================")
    print("p7 Calculate instrumental magnitude of the target")
    print("==================================================================")
    for band in bands:
        # To avoid RuntimeWarning: divide by zero encountered in log10 when f=0
        s_maginst = [
            -2.5*np.log10(f) if f > 0 else 0 for f in df[f"flux_{band}"]]
        s_maginsterr = 2.5*log10err(
            df[f"flux_{band}"], df[f"fluxerr_{band}"])
        df.insert(0, f"{band}_inst", s_maginst)
        df.insert(0, f"{band}err_inst", s_maginsterr) 
    checknan(df)

    print("\n================================================================")
    print("p8 Calculate and plot Z of each frame")
    print("==================================================================")
    # axes_Z : 
    #     CT corrected Z light curves
    fig, axes_Z = myfigure_ncol(n_band=len(bands), n_col=1)
    dy_list = []
    df_Z_list = []
    for idx, band in enumerate(bands):

        ax_Z = axes_Z[idx]
        # ToDo CTGerr useless?
        CT = df_CT.at[0, f"{band}_CT_MC"]
        CTerr = df_CT.at[0, f"{band}_CTerr_MC"]
        #print(f"    CT = {CT:.4f}+-{CTerr:.4f}")

        ax_Z.set_ylabel("Z")
        if idx==(len(bands)-1):
            ax_Z.set_xlabel("Seconds")

        # Calculate Zs 
        df_Z = calc_Z(
            df, objID, band, bands, CT, CTerr, 
            Nobj_min, eflag_max, dedge_min, cntmfrac_max)
        # Plot Zs
        plot_Z(df_Z, band, bands, ax_Z)

        # Break the process when N_band==2
        if N_band==2:
            sys.exit()

        # Delta y to scale ylim
        ymin, ymax = ax_Z.get_ylim()
        dy_list.append(ymax-ymin)
        df_Z_list.append(df_Z)

    df_Z = pd.concat(df_Z_list, axis=1)

    # Set y scale
    dy = np.max(dy_list)
    for idx, band in enumerate(bands):
        ax_Z = axes_Z[idx]
        ymin, ymax = ax_Z.get_ylim()
        ymean = np.mean([ymin, ymax])
        ax_Z.set_ylim([ymean-0.5*dy, ymean+0.5*dy])

    band_str = "".join(bands)
    out =  f"{args.obj}_Zstatus_N{len(df)}_{filename_tail}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    plt.close()
    checknan(df)

    
    # Sometimes "RuntimeError("Optimal parameters not found: " + errmsg)"
    # happens
    try:
        print("\n================================================================")
        print("p9 Mag residuals of field stars")
        print("==================================================================")
        # Create 
        # axes_res : 
        #     unnormalized residual (mag_cat - mag_est)
        # axes_res_norm : 
        #     normalized residual (mag_cat - mag_est)/sigma_est
        fig, axes_res, axes_res_norm = myfigure_ncol(n_band=len(bands), n_col=2)
        xmin_list, xmax_list, ymax_list = [], [], []
        for idx, band in enumerate(bands):
            ax_res = axes_res[idx]
            ax_res_norm = axes_res_norm[idx]
            ax_res.set_ylabel("N")

            # ToDo CTGerr useless?
            CT = df_CT.at[0, f"{band}_CT_MC"]
            CTerr = df_CT.at[0, f"{band}_CTerr_MC"]
            #print(f"    CT = {CT:.4f}+-{CTerr:.4f}")


            if idx==(len(bands)-1):
                ax_res.set_xlabel("Residuals")
                ax_res_norm.set_xlabel("Normalized residuals")

            # Calculate mag residuals of field stars
            # i.e., g_res, g_reserr etc.
            df_res = calc_resmag(
                df, objID, band, bands, CT, CTerr, df_Z,
                Nobj_min, eflag_max, dedge_min, cntmfrac_max, verbose=False)
            # Plot unnormalized residuals
            plot_res(
                df_res, band, bands, ax_res, mag=True)
            # Plot normalized residuals
            plot_res(
                df_res, band, bands, ax_res_norm, norm=True, mag=True)
            # Break the process when N_band==2
            if N_band==2:
                sys.exit()

            # To scale xlim
            xmin, xmax = ax_res.get_xlim()
            xmin_list.append(xmin)
            xmax_list.append(xmax)
            # To scale ylim
            ymin, ymax = ax_res.get_ylim()
            ymax_list.append(ymax)

        # Set x scale
        xmin_common = np.min(xmin_list)
        xmax_common = np.max(xmax_list)
        # Set y scale
        ymax_common = np.max(ymax_list)
        for idx, band in enumerate(bands):
            ax_res = axes_res[idx]
            ax_res.set_xlim([xmin_common, xmax_common])
            ax_res.set_ylim([0, ymax_common])

        band_str = "".join(bands)
        out =  f"{args.obj}_resmag_N{len(df)}_{filename_tail}.png"
        out = os.path.join(outdir, out)
        plt.savefig(out, dpi=200)
        plt.close()
        checknan(df)
    except:
        print("Skip magres plot due to an error.")
        pass


    print("\n================================================================")
    print("p10 Remove CT=0 data ")
    print("==================================================================")
    for b in bands:
        df_CT0 = df_CT[df_CT[f"{b}_CT_MC"] == 0]
        if len(df_CT0) > 0:
            print(f"  Remove N={len(df_CT0)} ({b}-band CT = 0)")
            idx_rm = df_CT0.index.tolist()
            df     = df[~df.index.isin(idx_rm)]
            df_CT = df_CT[~df_CT.index.isin(idx_rm)]
    checknan(df)


    print("\n================================================================")
    print("p10 Calculate object magnitude of each frame")
    print("==================================================================")
    # df          : CT, objinfo
    # df_Z        : Z
    # df_objcolor : objcolor 
    
    # Read colors of an object
    # Recognize index !
    df_col = pd.read_csv(args.colcsv, sep=" ", index_col=0)
    # Calculate mean color of an object
    # key : 
    for idx, band in enumerate(bands):
        df_col = calc_simplemeancolor(df_col, band, bands)
    checknan(df)
    
    # Use good frames to match dimensions with df_col

    idx_good_col = df_col.index.tolist()
    df = df[df.index.isin(idx_good_col)]
    df_Z = df_Z[df_Z.index.isin(idx_good_col)]
    assert len(df)==len(df_col), "Not the name dimension! df and df_col"
    assert len(df)==len(df_Z), "Not the name dimension! df and df_Z"

    for idx, band in enumerate(bands):
        # Calculate g_ccor and gerr_ccor etc.
        df = calc_objmag(df, df_Z, df_col, band, bands)
    checknan(df)


    print("\n================================================================")
    print("p11 Clean the target")
    print("==================================================================")
    # Select object by eflag
    N_bef = len(df)
    for b in bands:
        df = df[df[f"eflag_{b}"] < eflag_max]
    df = df.reset_index(drop=True)
    print(f"  N_df = {len(df)} (after eflag cleaning)")
    
    # Select object by cntm
    for j,band in enumerate(bands):
        df = df[df[f"cntmfrac_{band}"] < cntmfrac_max]
    df = df.reset_index(drop=True)
    print(f"  N_df = {len(df)} (after cntm cleaning)")
    checknan(df)

    # Useless ?
    print("\n================================================================")
    print("p12 Calculate mean magnitude (after cleaning)")
    print("==================================================================")
    for idx, band in enumerate(bands):
        df = calc_meanmag(df, band)

    print("\n================================================================")
    print("p13 Plot object magnitude of each frame")
    print("==================================================================")
    # 1. instrumental cmag
    #    ex) g_inst, gerr_inst
    # 2. CT, Z corrected color 
    #    from color_inst = color_cat*CTG + CTI
    #    color_cat = (color_inst - CTI)/CTG
    #    ex) g_ccor, gerr_ccor ?
    fig, axes_inst, axes_cor = myfigure_ncol(n_band=len(bands), n_col=2)
    dy_list = []
    for idx, band in enumerate(bands):

        ax_inst = axes_inst[idx]
        ax_cor = axes_cor[idx]

        if idx==0:
            ax_inst.set_title("Instrumental color")
            ax_cor.set_title("CT/Z corrected color")
        if idx==(len(bands)-1):
            ax_inst.set_xlabel("Time [sec]")
            ax_cor.set_xlabel("Time [sec]")

        plot_objmag(df, band, bands, ax_inst, ax_cor)
        ax_inst.set_ylabel("Magnitude [mag]")
        ax_inst.legend()
        ax_cor.legend()

        # Delta y to scale ylim
        ymin_inst, ymax_inst = ax_inst.get_ylim()
        ymin_cor, ymax_cor = ax_cor.get_ylim()
        dy_inst = ymax_inst - ymin_inst
        dy_cor = ymax_cor - ymin_cor
        dy_list.append(np.max([dy_inst, dy_cor]))

        # Break this process when CTGs==1 and N_band==2
        if N_band==2:
            break

    # Set y scale
    dy = np.max(dy_list)

    for idx, band in enumerate(bands):
        ax_inst = axes_inst[idx]
        ymin_inst, ymax_inst = ax_inst.get_ylim()
        ymean_inst = np.mean([ymin_inst, ymax_inst])
        ax_inst.set_ylim([ymean_inst+0.5*dy, ymean_inst-0.5*dy])

        ax_cor = axes_cor[idx]
        ymin_cor, ymax_cor = ax_cor.get_ylim()
        ymean_cor = np.mean([ymin_cor, ymax_cor])
        ax_cor.set_ylim([ymean_cor+0.5*dy, ymean_cor-0.5*dy])

    out =  f"{args.obj}_cormaglc_N{len(df)}_{filename_tail}.png"
    out = os.path.join(outdir, out)
    fig.savefig(out, dpi=200)
    checknan(df)


    print("\n================================================================")
    print("p14 Save photometric result csv")
    print("==================================================================")
    df["obj"] = target
    out =  f"{args.obj}_magall_N{len(df)}_{filename_tail}.csv"
    df.to_csv(out, sep=" ", index=False)
