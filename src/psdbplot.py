#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot object info. in psdb.
Ex) g-r vs. r-i to know reddening 
"""
from argparse import ArgumentParser as ap
import os
import sys
import sqlite3
from contextlib import closing
import time
from astroquery.mast import Catalogs
from astropy.coordinates import SkyCoord
from astropy import units as u
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt



if __name__=="__main__":
    parser = ap(description="handling Pan-STARRS database test")
    parser.add_argument(
        "csv", type=str, 
        help="csv name saved by psdb.py")
    parser.add_argument(
        "--color", action="store_true", default=None, 
        help="Plot g-r vs. r-i")
    parser.add_argument(
        "--magmax", default=17., type=float, 
        help="maximum g magnitude")
    args = parser.parse_args()

    df = pd.read_csv(args.csv,  sep=" ")

    
    if args.color:
        fig = plt.figure(figsize=(8,6))
        ax = fig.add_subplot()
        ax.set_xlabel("g-r")
        ax.set_ylabel("r-i")

        
        # Plot with different size depending on the g-band magnitude
        s0 = 500
        s_list = [300, 200, 100, 50, 30]

        for idx,m0 in enumerate([14, 15 ,16, 17, 18]):
            df_temp = df[(df["gMeanPSFMag"] > m0) & (df["gMeanPSFMag"] < m0+1)]
            col1 = df_temp["gMeanPSFMag"] - df_temp["rMeanPSFMag"]
            col2 = df_temp["rMeanPSFMag"] - df_temp["iMeanPSFMag"]
            size = s0/(idx+1)
            size = s_list[idx]
            ax.scatter(
                col1, col2, s=size, facecolor="", edgecolor="black", label=f"{m0} <m < {m0+1} N={len(col1)}")
            ax.legend()
            plt.savefig("gr_ri.png", dpi=200)


