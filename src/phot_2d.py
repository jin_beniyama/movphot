#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Do Photometory for arbitrary x,y coordinates.
"""
import os
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import sep
import astropy.io.fits as fits

import matplotlib.pyplot as plt
from scipy.stats import sigmaclip

from calcerror import diverr
import movphot as mp
from movphot.common import get_filename, nowstr
from movphot.photfunc import obtain_winpos, calc_time_shift, create_saturation_mask
from movphot.visualization import mycolor, add_circle_with_radius

plt.rcParams["font.family"] = "Arial"


if __name__ == "__main__":
    parser = ap(description="Point photometry")
    parser.add_argument(
        "fits", type=str, nargs="*",
        help="2-d fits file (Tomo-e or TriCCS)")
    parser.add_argument(
        "-x", type=int, nargs="*", 
        help="x in pixel coordinate")
    parser.add_argument(
        "-y", type=int, nargs="*", 
        help="y in pixel coordinate")
    parser.add_argument(
        "--zr", type=int, nargs=2,  
        help="z range")
    parser.add_argument(
        "--inst", type=str, required=True, 
        choices=["TriCCS", "tomoe"],
        help="used instrument")
    parser.add_argument(
        "-r", "--radius", type=float, default=10, 
        help="aperture radius in unit of pixel")
    parser.add_argument(
        "--ann_gap", type=float, default=2, 
        help="Gap between radius and annulus in pix")
    parser.add_argument(
        "--ann_width", type=float, default=3, 
        help="Width of annulus in pix")
    parser.add_argument(
        "--phottype",  type=str, default="app",
        help='app, ann (faint object), or iso (bright)')
    parser.add_argument(
        "--key_x", type=str, default="x", 
        help="keyworf of x")
    parser.add_argument(
        "--key_y", type=str, default="y", 
        help="keyworf of y")
    parser.add_argument(
        "--nowinpos", action="store_true", default=False,
        help="Do not use a winpos as an aperture location (for faint objects)")
    parser.add_argument(
        "--satumask", action="store_true", default=False,
        help="Use saturation mask to remove background")
    parser.add_argument(
        "--r_dilation", type=float, default=10,
        help="Dilation radius")
    parser.add_argument(
        "--out", type=str, default=None, 
         help="output csv file")
    args = parser.parse_args()
    
    # Parse arguments
    radius    = args.radius
    ann_gap   = args.ann_gap
    ann_width = args.ann_width
    inst   = args.inst

    param_app = dict(
        radius=radius, ann_gap=ann_gap, ann_width=ann_width)
    param_iso = dict(
        r_disk=0, epsilon=0, sigma=0, minarea=0)

    if args.nowinpos:
        winpos = False
    else:
        winpos = True

    phottype = args.phottype
    key_x, key_y = args.key_x, args.key_y

    # Movphot Class
    # Target name == None
    # refmagmin, refmagmax are all None
    ph = mp.Movphot(
        inst, None, None, None, radius, param_app, param_iso)

    now = nowstr()
    if args.satumask:
        satumask = "_wsatumask"
    else:
        satumask = "_wosatumask"
    outdir = f"{now}_phot2d{satumask}"
    if not os.path.isdir(outdir):
        os.makedirs(outdir, exist_ok=True)

    df_list = []
    # Open fits
    for idx_fits,fi in enumerate(args.fits):
        src = fits.open(fi)[0]
        hdr = src.header
        # Obtain starting time (self.t_utc, self.t_mjd, and self.t_jd) in hdr
        ph.obtain_time_from_hdr(hdr)
        image = src.data.byteswap().newbyteorder()
        ny, nx = image.shape
        ph.nx = nx
        ph.ny = ny
        print(f"Shape of the input fits (nx, ny) = ({nx}, {ny})")
       
        if args.satumask:
            mask = create_saturation_mask(image, ph.satu_count, args.r_dilation)
        else:
            mask = None

        # Single exposure time
        t_frame = hdr[ph.hdr_kwd["exp"]]

        # Backgroung calculation
        if phottype == "app":
            image = ph.remove_background(image, mask)
            print(f"BG mean: {ph.bg_level:.2f} rms: {ph.bg_rms:.2f}")

        elif phottype == "ann":
            # For sep.extract, we need to remove background.
            image_bgsub = ph.remove_background(image, mask)
            print(f"BG mean: {ph.bg_level:.2f} rms: {ph.bg_rms:.2f}")

        # Do Photometry
        x_obj, y_obj = args.x[idx_fits], args.y[idx_fits]
        # Winpos for plot
        x_obj, y_obj, _ = obtain_winpos(
            image, [x_obj], [y_obj], [radius], nx, ny)

        df_obj = ph.phot_loc(
            image, hdr, phottype, x_obj, y_obj, winpos)

        _, fluxerr, _ = sep.sum_circle(
            img, [xc], [yc], r=radius, err=bgerr_pix, gain=None)

        # 3. Aperture flux with local background subtraction with gain = None (or gain = gain)
        flux, _, _ = sep.sum_circle(
            img, [xc], [yc], r=radius, bkgann=bkgann, err=bgerr_pix, gain=None)

        # A. Background levels in annuli
        fann, _, _ = sep.sum_circann(
            img, [xc], [yc], rin, rout, err=None, gain=None)
        fann = float(fann)
        fann_list.append(fann)

        flux, fluxerr = float(flux), float(fluxerr)
        
        # Just to check =======================================================
        #S1 = (3.1415*(radius)**2)
        #S2 = (3.1415*(radius+1)**2)
        #bgerr_exp1 = S1**0.5*bgerr_pix
        #bgerr_exp2 = S2**0.5*bgerr_pix
        #print(f"area           = {S1:.2f} - {S2:.2f}")
        #print(f"Expected bgerr = {bgerr_exp1:.2f} -- {bgerr_exp2:.2f}")
        #print(f"Measured bgerr = {fluxerr:.2f}")
        # Just to check =======================================================

        # 4. Add poisson error of the target by hand
        Perr_target = (flux*gain)**0.5/gain
        fluxerr = (fluxerr**2 + Perr_target**2)**0.5



        # Update time with time shift
        # Time is not common
        ts = calc_time_shift((nx, ny), x_obj[0], y_obj[0])
        print(f"Time shift = {ts}")
        ph.t_jd = ph.t_jd + (t_frame + ts)/3600./24.

        df_obj["nframe"] = idx_fits+1
        ph.add_photometry_info(df_obj)
        df_list.append(df_obj)

    df = pd.concat(df_list)
    # Remove dummy columns
    df = df.drop(columns="t_utc")
    df = df.drop(columns="t_mjd")
    df = df.reset_index(drop=True)

    out = "photres.txt"
    out = os.path.join(outdir, out)
    df.to_csv(out, sep=" ", index=False)


    # Plot lcs ================================================================
    fig = plt.figure(figsize=(20, 20))
    
    # Last image
    ax_img = fig.add_axes([0.1, 0.55, 0.80, 0.4])
    ax_img.set_xlabel("x")
    ax_img.set_ylabel("y")
    ax_img.set_xlim([0, ph.nx])
    ax_img.set_ylim([0, ph.ny])
    sigma = 5
    _, vmin, vmax = sigmaclip(image, sigma, sigma)
    ax_img.imshow(image, cmap='gray', vmin=vmin, vmax=vmax)

    color = mycolor[0]
    ls    = "dashed"
    label = f"rad={radius}"
    df_temp = df[df["nframe"]==0]
    add_circle_with_radius(
        ax_img, df_temp, key_x, key_y, radius, color, ls, label)

    # Raw lcs
    ax1 = fig.add_axes([0.15, 0.28, 0.80, 0.22])
    ax1.axes.xaxis.set_visible(False)
    ax1.set_ylabel("Raw Flux [ADU]")
    col = mycolor[0]
    # Extract object 
    ax1.errorbar(
        df["t_jd"], df[f"flux"], df[f"fluxerr"],
        marker=None, color=col, ls="None")
    ax1.scatter(
        df["t_jd"], df[f"flux"], label=f"Raw", 
        ec=col, facecolor="None", s=300)
    
    ax1.legend()

    out = "lc.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    # Plot lcs ================================================================
