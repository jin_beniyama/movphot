#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Check flat fielding with stanard reduction.
"""
from argparse import ArgumentParser as ap
import os
import numpy as np
import astropy.io.fits as fits


def main(args):
  """
  This is the main function.

  Parameters
  ----------
  args : argparse.Namespace
    Arguments passed from the command-line as defined below.
  """
  assert (args.width%2)==0, "Width should be even number"
  width = args.width
  rad = width/2.
  # Searched range x-49 to x+50 in total 100 pix
  x0 = int(args.x - rad)
  x1 = int(args.x + rad)
  y0 = int(args.y - rad)
  y1 = int(args.y + rad)
  print(f"x0, x1 = {x0}, {x1}")
  print(f"y0, y1 = {y0}, {y1}")
  
  # Read object frame
  hdu_obj = fits.open(args.obj)
  hdr = hdu_obj[0].header

  # Read dark frame
  hdu_dark = fits.open(args.dark)
  src_dark = hdu_dark[0]
  dark = src_dark.data
  assert len(dark.shape)==2, "Dark should be 2-d fits."
  
  # Read flat frame
  hdu_flat = fits.open(args.flat)
  src_flat = hdu_flat[0]
  flat = src_flat.data
  assert len(flat.shape)==2, "Flat should be 2-d fits."

  

  
  # Do dark subtraction
  print(f"median count (before dark subtraction) {np.median(hdu_obj[0].data):.1f}")
  hdu_obj[0].data = hdu_obj[0].data - dark
  print(f"median count (after dark subtraction) {np.median(hdu_obj[0].data):.1f}")

  # Cut and calculate std after dark
  data_raw = hdu_obj[0].data[y0:y1, x0:x1]
  data_raw_list = data_raw.flatten()
  raw_mean, raw_std = np.mean(data_raw), np.std(data_raw)
  per_raw = raw_std/raw_mean*100.

  # Do flat-field correction
  hdu_obj[0].data = hdu_obj[0].data/flat

  # Cut and calculate std after flat
  data_flat = hdu_obj[0].data[y0:y1, x0:x1]
  data_flat_list = data_flat.flatten()
  flat_mean, flat_std = np.mean(data_flat), np.std(data_flat)
  per_flat = flat_std/flat_mean*100.
  
  
  # Plot histograms
  import matplotlib.pyplot as plt  
  fig = plt.figure(figsize=(10, 8))
  ax1 = fig.add_axes([0.15, 0.2, 0.3, 0.65])
  ax2 = fig.add_axes([0.6, 0.2, 0.3, 0.65])
  ax1.set_title("Raw")
  ax2.set_title("Raw/Flat")
  ax1.set_xlabel("Count [ADU]")
  ax2.set_xlabel("Count [ADU]")
  ax1.set_ylabel("N")
  ax2.set_ylabel("N")
  
  label_raw  = (
      f"(Mean, Std) = ({raw_mean:.2f}, {raw_std:.2f})\n"
      f"Std/Mean = {per_raw:.1f} %"
      )
  label_flat = (
      f"(Mean, Std) = ({flat_mean:.2f}, {flat_std:.2f})\n"
      f"Std/Mean = {per_flat:.1f} %"
      )
  bins = 100
  ax1.hist(data_raw_list, bins=bins, label=label_raw)
  ax2.hist(data_flat_list, bins=bins, label=label_flat)
  
  ax1.legend()
  ax2.legend()
  out = "checkflat_hist.png"
  plt.savefig(out)
 


if __name__ == "__main__":
  parser = ap(description="Check flat-fielding")
  parser.add_argument(
    "obj", type=str,
    help="a raw fits image")
  parser.add_argument(
    "dark", type=str,
    help="a raw dark frame")
  parser.add_argument(
    "flat", type=str, 
    help="a raw flat frame")
  parser.add_argument(
    "x", type=float,
    help="central x")
  parser.add_argument(
    "y", type=float, 
    help="central y")
  parser.add_argument(
    "--width", type=float, default=100,
    help="width of pixels")
  args = parser.parse_args()

  main(args)
