#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Check background information (simple).
"""
import os
import numpy as np
from argparse import ArgumentParser as ap
import datetime
import sep
import astropy.io.fits as fits
from remove_background import remove_background2d
from movphot.photfunc import calc_FWHM_fits, create_saturation_mask
import matplotlib.pyplot as plt
import mpl_toolkits.axes_grid1
import movphot as mp
from scipy.stats import sigmaclip


if __name__ == "__main__":
    parser = ap(description="Plot sky background (simple).")
    parser.add_argument(
        "fits", type=str,  
        help="input fits file")
    parser.add_argument(
        "--inst", type=str, required=True, 
        choices=["TriCCS", "murikabushi", "wfgs2", "MSI"],
        help="used instrument (for gain)")
    parser.add_argument(
        "--xr", type=int, nargs=2, default=None, 
         help="x range")
    parser.add_argument(
        "--yr", type=int, nargs=2, default=None, 
        help="y range")
    parser.add_argument(
        "--bgr", type=float, nargs=2, default=None, 
        help="bg range")
    parser.add_argument(
        "--bgsub", action="store_true", default=False,
        help='use header BGCOUNT, BGSTDDEV for background estimation if False')
    parser.add_argument(
        "--width", type=int, default=30, help="photometry width near x, y")
    parser.add_argument(
      "--sigma", type=int, default=5, 
      help="photometry threshold is bkgerr times sigma")
    parser.add_argument(
      "--fwhmout", type=str, default=None, 
      help="output png file")
    parser.add_argument(
      "--center", nargs=2, type=int, default=[50, 50], 
      help="Center coordinate of the 100x100 plot")
    parser.add_argument(
      "--out", type=str, default=None, 
      help="output csv file")
    parser.add_argument(
      "--bw", type=int, default=64, 
      help="box width and height for bgsubtraction")
    parser.add_argument(
      "--fw", type=int, default=3, 
      help="median filter width and height for bgsubtraction")
    parser.add_argument(
      "--target", nargs=2, type=float, default=None, 
      help="Coordinates of the target")
    parser.add_argument(
      "--outdir", type=str, default="bgplot", 
      help="Output directory")
    parser.add_argument(
        "--savefits", action="store_true", default=False,
        help='Save fits of bg and bgsubtracted image')
    parser.add_argument(
        "--bgfits", type=str, default="bg.fits",
        help='Filename of bg image')
    parser.add_argument(
        "--bgsubfits", type=str, default="bgsub.fits",
        help='Filename of bg subtracted image')
    args = parser.parse_args()
  
    # fits source  (HDU0, header + image data)
    src = fits.open(args.fits)[0]
    # fits header
    hdr = src.header
    # 2-d image
    image = src.data.byteswap().newbyteorder()

    # Movphot Class
    ph = mp.Movphot(args.inst)
    satumask = create_saturation_mask(image, ph.satu_count)
    N_mask = len(satumask[satumask==True])
    print(f"N_mask = {N_mask}")

    # BG subtraction
    bkg = sep.Background(
        image, mask=satumask, bw=args.bw, bh=args.bw, fw=args.fw, fh=args.fw)
    bg = bkg.back()
    image = image - bg

    # Cut image
    if args.xr:
        xmin, xmax = args.xr
        ymin, ymax = args.yr
        image = image[int(ymin):int(ymax), int(xmin):int(xmax)]
        image  = image.copy(order='C')

        bg = bg[int(ymin):int(ymax), int(xmin):int(xmax)]
        bg  = bg.copy(order='C')

    ny, nx = image.shape

    ## Cosmic ray
    #if args.inst=="TriCCS":
    #    from astroscrappy import detect_cosmics
    #    cleantype = "maskmed"
    #    verbose   = False

    #    mask = np.zeros((ny, nx), dtype=np.bool_)
    #    mask[241, 263] = 1

    #    rays_map, image = detect_cosmics(
    #        image, inmask=mask, satlevel=10000)



    # Plot background image
    fig = plt.figure(figsize=(12, int(12*ny/nx)))
    ax = fig.add_axes([0.1, 0.10, 0.8, 0.8])
    if args.bgr:
        from matplotlib.colors import Normalize
        bgmin, bgmax = args.bgr
        cbar = ax.imshow(bg, cmap='gray', vmin=bgmin, vmax=bgmax)
        fig.colorbar(cbar, ax=ax)
        cbar.set_clim(bgmin, bgmax)

    else:
        cbar = ax.imshow(bg, cmap='gray')
        fig.colorbar(cbar, ax=ax)
    ax.set_xlim([0, nx])
    ax.set_ylim([0, ny])
    ax.legend().get_frame().set_alpha(1.0)
    ax.invert_yaxis()
    ax.legend()
    plt.tight_layout()
    if args.out:
        out = args.out
    else:
        out = "bg.png"
    plt.savefig(out, dpi=200)
    plt.close()

    if args.savefits:
        src.data = bg
        src.writeto(args.bgfits, overwrite=True)
        src.data = image
        src.writeto(args.bgsubfits, overwrite=True)


    assert False, 1

    # Plot src image after 5-sigma clipping 
    sigma = 5
    _, vmin, vmax = sigmaclip(image, sigma, sigma)
    ny, nx = image.shape

    fig = plt.figure(figsize=(12, int(22*ny/nx)))
    ax = fig.add_axes([0.1, 0.10, 0.8, 0.8])

    cbar = ax.imshow(image, cmap='gray', vmin=vmin, vmax=vmax)
    fig.colorbar(cbar, ax=ax)
     
    if args.target:
        from matplotlib.collections import PatchCollection
        from matplotlib.patches import Circle
        x, y = args.target
        rad = 30
        for ax in [ax1, ax2]:
            ax.add_collection(PatchCollection(
              [Circle((x,y), rad)],
              color="red", ls="dashed", lw=1, facecolor="None", label=None)
            )

    ax.imshow(satumask, alpha=0.2)
    ax.set_xlim([0, nx])
    ax.set_ylim([0, ny])
    ax.legend().get_frame().set_alpha(1.0)
    ax.invert_yaxis()
    plt.tight_layout()
    if args.out:
        out = args.out
    else:
        out = "bgsub.png"
    plt.savefig(out, dpi=200)
    plt.close()

