#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Check photometory of reference stars on Catalog mag. vs. Instrumental mag. plot.

Example
-------
> phot_status_magmag.py photres_ref_magerr0f05dedge100eflag1.txt --bands g r z
"""
from argparse import ArgumentParser as ap
import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  


def plot_magmag(df, bands=["g", "r", "i"], out=None):
    """
    Plot cat. mag. vs. inst. mag. for 3 bands

    Parameters
    ----------
    df : pandas.DataFrame
        input dataframe
    bands : array-like
        three bands
    """
    fig = plt.figure(figsize=(20, 8))
    ax1 = fig.add_axes([0.10, 0.10, 0.25, 0.70])
    ax2 = fig.add_axes([0.40, 0.10, 0.25, 0.70])
    ax3 = fig.add_axes([0.70, 0.10, 0.25, 0.70])

    axes = [ax1, ax2, ax3]
    ax2.set_xlabel("Catalog magnitude")
    ax1.set_ylabel("Instrumental magnitude")
    obj = list(set(df.objID.tolist()))

    for idx_b, b in enumerate(bands):
        ax = axes[idx_b]
        for x in obj:
            df_x = df[(df["objID"]==x) & (df["band"]==b)]
            if len(df_x)==0:
                continue
            m_cat =  df_x[f"{b}MeanPSFMag"].values.tolist()
            m_inst =  -2.5*np.log10(df_x["flux"].values.tolist())
            ax.scatter(m_cat, m_inst, marker="x", color="black", s=1)

            # Mean
            cat_mean = np.mean(m_cat)
            inst_mean = np.mean(m_inst)
            ax.scatter(cat_mean, inst_mean, s=100, marker="o")

        xmin, xmax= ax.get_xlim()
        print(f"xmin, xmax = {xmin:.1f}, {xmax:.1f}")
        xmean = (xmin+xmax)*0.5
        ymin, ymax= ax.get_ylim()
        print(f"ymin, ymax = {ymin:.1f}, {ymax:.1f}")
        ymean = (ymin+ymax)*0.5
        w_x, w_y = abs(xmax - xmin), abs(ymax - ymin)
        w = np.max([w_x, w_y])*0.5
        xmin, xmax = xmean-w, xmean+w
        ymin, ymax = ymean-w, ymean+w
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])

        # Plot lines for reference
        x = np.arange(xmin, xmax, 0.1)
        const = ymin-xmin
        y = x+const
        ax.plot(x, y, label="y=x")
        ax.plot(x, y+0.1, label="y=x+0.1")
        ax.plot(x, y-0.1, label="y=x-0.1")
        ax.set_title(f"{b}-band")
        ax.legend()
    if out:
        plt.savefig(out)


if __name__ == "__main__":
    parser = ap(description="Check photometry status on mag. vs. mag. plot.")
    parser.add_argument(
        "ref", type=str,
        help="Photometric result of reference stars")
    parser.add_argument(
        "--bands", type=str, nargs=3, default=["g", "r", "i"],
        help="bands")
    parser.add_argument(
        "--out", type=str, default="photostatus_magmag.jpg", 
         help="Output file")
    args = parser.parse_args()
    
    # Read photometric results
    df_ref = pd.read_csv(args.ref, sep=" ")
    plot_magmag(df_ref, args.bands, args.out)
