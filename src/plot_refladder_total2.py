#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot instrumental magnitude lightcurves and 
corrected lightcurves with reference ladder technique.
("2" in plot_refladder_total2.py means for photometry result in new format.)

mag (from mag_red_phase) and magerr are useful columns.
There are 3 types of uncertainties:
    1. magerr (common magnitude, for x << 0.1)
    2. magerr0 (error for faint magnitude, for x >= 0.1)
       With flux, fluxerr, 
           magerr0 = |-2.5log10(flux) - (-2.5*log10(flux-fluxerr))|
                   = 2.5*log10(flux/(flux-fluxerr))
    3. magerr1 (error for bright magnitude, for x >= 0.1)
       With flux, fluxerr, 
           magerr1 = -2.5log10(flux) - (-2.5*log10(flux+fluxerr))
                   = -2.5*log10(flux/(flux+fluxerr))

ToDo
----
Calculation of magerr0 when flux-fluxerr < 0.

Outputs
-------
maginst.png
foldmaginst.png 
magtrend.png
magcorobj.png
magcorref.png
"""
import os 
import sys
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 

from movphot.common import diverr,  log10err
from movphot.visualization import (
    myfigure_ncol, mycolor, mymark, color_from_band, myfigure_long1col, colmark)
from movphot.prepro import refladder2, refladder3
# Private module !!
from aspect_data import (
    add_aspect_data, time_correction, mag_correction, phase_correction)


if __name__ == "__main__":
    parser = ap(description="Create relative lightcurves")
    parser.add_argument(
        "res_tar", type=str, 
        help="photometric results of a target")
    parser.add_argument(
        "res_ref", type=str, 
        help="photometric results of reference stars")
    parser.add_argument(
        "--target", type=str, 
        help="target name (necessary for periodic analysis)")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--JD0", type=float,  default=None,
        help="Standard of Julian Day")
    parser.add_argument(
        "--rotP", type=float,  default=None,
        help="Rotational period in hour")
    parser.add_argument(
        "--sec", action="store_true", default=False,
        help="Whether input period in sec")
    parser.add_argument(
        "--Nframe_min", type=int, default=10, 
        help="minimum frame numbers to be desired")
    parser.add_argument(
        "--loc", type=str, default="371",
        help="location of the observatory (MPC code)")
    parser.add_argument(
        "--stype", default="C", choices=["C", "S", "X", "D", "V"],
        help="spectral type(ex. C/S/X/D/V) for phase correction")
    parser.add_argument(
        "-G", type=float, default=0.15,
        help="slope parameter G for phase correction")
    parser.add_argument(
        "--fr", nargs=2, default=None, type=int,
        help="frame range")
    parser.add_argument(
        "--outdir", type=str, default=None,
        help="output directory")
    parser.add_argument(
        "--overwrite", action="store_true", default=False,
        help="Overwrite the output directory")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False,
        help="Print detail messages")
    args = parser.parse_args()


    # Selection criteria of reference stars
    Nframe_min    = args.Nframe_min
    # Target name
    if args.target is None:
        target        = "Target"
    else:
        target        = args.target

    # Rotational period in hour
    if args.sec:
        rotP_s         = args.rotP
        rotP_h         = rotP_s/3600.
    else:
        rotP_s         = None
        rotP_h         = args.rotP

    if args.rotP:
        if args.sec:
            rotP_list = str(rotP_s).split(".")
            str_P = "f".join(rotP_list)
            rotP_str = f"_rotP{str_P}s"
        else:
            rotP_list = str(rotP_h).split(".")
            str_P = "f".join(rotP_list)
            rotP_str = f"_rotP{str_P}h"
    else:
        rotP_str = ""

    if args.fr:
        fmin, fmax = args.fr
        fr_str = f"_f{fmin}to{fmax}"
    else:
        fr_str = ""
    
    # Not t_jd_ltcor since ref stars cannot be corrected
    key_t = "t_jd"
    # For final output 
    key_t_obj = "t_jd_ltcor"

    bands = args.bands
    # Number of bands
    N_band = len(bands)
    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]

    # FILE NAME OF OBJECT
    filename_obj = args.res_tar.split("/")[-1].split(".")[0]
    filename_ref = args.res_ref.split("/")[-1].split(".")[0]
    
    # outdir
    outdir_parent = "refladder"
    os.makedirs(outdir_parent, exist_ok=True)

    if args.outdir:
        outdir_child = args.outdir
    else:
        outdir_child = (
            f"{filename_obj}_{filename_ref}_rotP{rotP_str}_"
            f"Nframemin{Nframe_min}{fr_str}"
            )
    outdir = os.path.join(outdir_parent, outdir_child)
    if args.overwrite:
        os.makedirs(outdir, exist_ok=True)
    else:
        os.makedirs(outdir, exist_ok=False)
    

    print("\n================================================================")
    print("p1 Read photometric results")
    print("==================================================================")
    df_obj = pd.read_csv(args.res_tar, sep=" ")
    df_ref = pd.read_csv(args.res_ref, sep=" ")
    print(f"  N_df_ref = {len(df_ref)} (original)")
    print(f"  N_df_obj = {len(df_obj)} (original)")
    
    
    # Some detections have negative flux when observing faint objects.
    # Is it ok?
    SNR_min = 1
    low_snr = df_obj[df_obj["flux"]/df_obj["fluxerr"] < SNR_min]
    if len(low_snr) > 0:
        print(f"  Low SNR N={len(low_snr)} removed by hand.")
        df_obj = df_obj[df_obj["flux"]/df_obj["fluxerr"] > SNR_min]
        df_obj = df_obj.reset_index(drop=True)
    low_snr = df_ref[df_ref["flux"]/df_ref["fluxerr"] < SNR_min]
    if len(low_snr) > 0:
        print(f"  Low SNR N={len(low_snr)} removed by hand.")
        df_ref = df_ref[df_ref["flux"]/df_ref["fluxerr"] > SNR_min]
        df_ref = df_ref.reset_index(drop=True)
   
    # Calculate instrumental magnitude
    try:
        df_obj["maginst"]    = -2.5*np.log10(df_obj["flux"])
        df_obj["maginsterr"] = 2.5*log10err(df_obj["flux"], df_obj["fluxerr"])
        df_ref["maginst"]    = -2.5*np.log10(df_ref["flux"])
        df_ref["maginsterr"] = 2.5*log10err(df_ref["flux"], df_ref["fluxerr"])
    except RuntimeWarning as e:
        print(f"{e}")
        print(f"Finish the script. Check whether negative flux exists.")
        sys.exit()

    # Select data by frame
    if args.fr is not None:
        fmin, fmax = args.fr
        df_obj = df_obj[
            (df_obj["nframe"] >= fmin)
            & (df_obj["nframe"] <= fmax)]
        df_ref = df_ref[
            (df_ref["nframe"] >= fmin)
            & (df_ref["nframe"] <= fmax)]
        df_obj = df_obj.reset_index(drop=True)
        df_ref = df_ref.reset_index(drop=True)
        fitslist = df_obj[df_obj["band"]==b1]["fits"].values.tolist()
        print(f"Fits name ({b1}-band): {fitslist}")



    objid_all = list(set(df_ref["objID"].values.tolist()))

    #Number of reference stars
    N_ref = len(objid_all)

    nframe_list = list(set(df_obj["nframe"].values.tolist()))
    Nframe = len(nframe_list)


    # Correct time and brightness with JD0 (time zero point)
    df_obj["obj"] = target
    # Obtain aspect data
    # All col (mag, t_sec_obs, t_sec_ltcor, red_mag, red_mag_phase) have suffix 
    # r (asteroid-Sun), delta (asteroid-observer), alpha, PAB
    # Use t_jd_{band}
    assert np.min(df_obj[key_t])!=np.max(df_obj[key_t]), "Bad input."

    df_obj = add_aspect_data(df_obj, args.loc)
    # Do time correction (t_sec_obj to t_sec_ltcor)
    df_obj = time_correction(df_obj)
   
    # For periodic analysis 
    df_obj.loc[:, f"t_sec_obs"] = df_obj[key_t]*24*3600.
    # Set the first time to zero
    df_obj[f"t_sec_obs"] -= df_obj.at[0, f"t_sec_obs"]

    # Time standard
    if args.JD0:
        JD0 = args.JD0
    else:
        JD0 = np.min(df_obj[key_t])

    

    # At first, Remove with Nframe_min
    for b in bands:
        df_b = df_ref[df_ref["band"]==b]
        objid_band = list(set(df_b["objID"].values.tolist()))
        for x in objid_band:
            df_temp = df_b[df_b["objID"]==x]
            nframe_temp = list(set(df_temp["nframe"].values.tolist()))
            n_det = len(nframe_temp)
            if n_det < Nframe_min:
                # Remove object
                print(
                    f"Do not use {x} due to lack of the detectinos N={n_det}")
                df_ref = df_ref[~((df_ref["band"]==b) & (df_ref["objID"]==x))]

    objid_all = list(set(df_ref["objID"].values.tolist()))

    print("\n================================================================")
    print("p2 Plot instrumental lightcurve (uncorrected)")
    print("==================================================================")
    fig, axes = myfigure_ncol(3, 1)
    for idx_b, b in enumerate(bands):
        ax = axes[idx_b]
        
        # Obj
        color = color_from_band(b)
        df_obj_b = df_obj[df_obj["band"]==b]
        Ndet = len(df_obj_b)
        ax.errorbar(
            df_obj_b[key_t]-JD0, df_obj_b["maginst"], df_obj_b["maginsterr"],
            marker=None, color=color, ls="None")
        ax.scatter(
            df_obj_b[key_t]-JD0, df_obj_b["maginst"], label=f"{target} {b}-band N={Ndet}",
            ec=color, facecolor="None")

        ax.legend()

    w_list = []
    for ax in axes:
        ymin, ymax = ax.get_ylim()
        w = ymax - ymin
        w_list.append(w)
    w_max = np.max(w_list)
    for ax in axes:
        ymin, ymax = ax.get_ylim()
        c = np.mean([ymin, ymax])
        ax.set_ylim([c+w_max*0.5, c-w_max*0.5])

    # Temporally for 2010XC15 on 12/24
    #axes[0].set_ylim([-12.0, -12.5])
    #axes[1].set_ylim([-11.7, -12.3])
    #axes[2].set_ylim([-11.2, -11.8])

    # Set labels
    x, y = -0.15, 0.5
    for ax in axes:
        ax.set_ylabel("Instrumental magnitude")
        ax.yaxis.set_label_coords(x, y)
    axes[-1].set_xlabel(f"Julian Day - {JD0} [day]")
    for ax in axes[:-1]:
        ax.axes.xaxis.set_visible(False)

    out = f"maginstobj.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)


    fig = plt.figure(figsize=(14, 16))
    ax1 = fig.add_axes([0.1, 0.66, 0.85, 0.28])
    ax2 = fig.add_axes([0.1, 0.38, 0.85, 0.28])
    ax3 = fig.add_axes([0.1, 0.10, 0.85, 0.28])
    axes = [ax1, ax2, ax3]

    for idx_b, b in enumerate(bands):
        # Ref
        df_ref_b = df_ref[df_ref["band"]==b]
        ax = axes[idx_b]
        for idx_ref,obj in enumerate(objid_all):
            df_temp = df_ref_b[df_ref_b["objID"]==obj]
            
            # For test 2023-04-10
            #if obj==109321497452335551 and b=="r":
            #    assert False, df_temp

            # Catalog manitude for PS
            try:
                catmag = df_temp[f"{b}MeanPSFMag"].values.tolist()[0]
                catmag = np.round(catmag, 1)
            except:
                catmag = None

            # Detected location in 1st frame x0, y0 and last frame x1, y1
            try:
                x0 = df_temp["x"].values.tolist()[0]
                x0 = np.round(x0, 1)
                y0 = df_temp["y"].values.tolist()[0]
                y0 = np.round(y0, 1)
                x1 = df_temp["x"].values.tolist()[-1]
                x1 = np.round(x1, 1)
                y1 = df_temp["y"].values.tolist()[-1]
                y1 = np.round(y1, 1)
                fits0 = df_temp["fits"].values.tolist()[0]
            except:
                x0, y0, x1, y1, fits0 = None, None, None, None, None

            col, mark = colmark(idx_ref)
            ax.errorbar(
                df_temp[key_t]-JD0, df_temp["maginst"], 
                df_temp["maginsterr"], marker=None, color=col, ls="None")
            ax.scatter(
                df_temp[key_t]-JD0, df_temp["maginst"], ec=col, 
                facecolor="None", s=50, marker=mark,
                label=f"{obj} {b}-band ({catmag} mag, x0,y0 ={x0},{y0}, x1,y1={x1},{y1}, fits0={fits0})")

        ax.legend(fontsize=5, ncol=2)

    w_list = []
    for ax in axes:
        ymin, ymax = ax.get_ylim()
        w = ymax - ymin
        w_list.append(w)
    w_max = np.max(w_list)
    for ax in axes:
        ymin, ymax = ax.get_ylim()
        c = np.mean([ymin, ymax])
        ax.set_ylim([c+w_max*0.5, c-w_max*0.5])
    for ax in axes[:-1]:
        ax.axes.xaxis.set_visible(False)

    # Set labels
    x, y = -0.15, 0.5
    for ax in axes:
        ax.set_ylabel("Instrumental magnitude")
        ax.yaxis.set_label_coords(x, y)
    axes[-1].set_xlabel(f"Julian Day - {JD0} [day]")

    out = f"maginstref.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)

    

    if args.rotP:
        print("\n============================================================")
        print("p3 Plot folded instrumental lightcurve (optional)")
        print("==============================================================")
        # rotP in hour
        df_obj["phase"] = ((df_obj[key_t]-JD0)*24.)/rotP_h%1
        df_ref["phase"] = ((df_ref[key_t]-JD0)*24.)/rotP_h%1
        # Number of phase
        df_obj["n_phase"] = 0
        df_ref["n_phase"] = 0
        N_phase = int(np.ceil((np.max(df_obj[key_t]) - np.min(df_obj[key_t]))*24./rotP_h))
        for n in range(N_phase):
            df_obj.loc[((((df_obj[key_t]-JD0)*24.)/rotP_h <= n+1) & (((df_obj[key_t]-JD0)*24.)/rotP_h >= n)), "n_phase"] = n+1

        fig, axes_l, axes_r = myfigure_ncol(3, 2)
        for idx_b, b in enumerate(bands):
            ax_l = axes_l[idx_b]
            ax_r = axes_r[idx_b]
            
            # Obj
            color = color_from_band(b)
            df_obj_b = df_obj[df_obj["band"]==b]

            ax_l.errorbar(
                df_obj_b["phase"], df_obj_b["maginst"], df_obj_b["maginsterr"],
                marker=None, color=color, ls="None")
            ax_l.scatter(
                df_obj_b["phase"], df_obj_b["maginst"], label=f"Obj {b}-band",
                ec=color, facecolor="None")

            # Ref
            df_ref_b = df_ref[df_ref["band"]==b]
            for idx_ref,objid in enumerate(objid_all):
                color = mycolor[idx_ref]
                df_ref_b_1 = df_ref_b[df_ref_b["objID"]==objid]
                ax_r.errorbar(
                    df_ref_b_1["phase"], df_ref_b_1["maginst"], 
                    df_ref_b_1["maginsterr"], marker=None, color=color, ls="None")
                ax_r.scatter(
                    df_ref_b_1["phase"], df_ref_b_1["maginst"], ec=color, 
                    facecolor="None", label=f"Ref {idx_ref+1} {b}-band")


            ax_l.legend()
            ax_r.legend()
            
        
        # Set scales in left/right ================================================
        for axes in [axes_l, axes_r]:
            w_list = []
            for ax in axes:
                ymin, ymax = ax.get_ylim()
                w = ymax - ymin
                w_list.append(w)
            w_max = np.max(w_list)
            for ax in axes:
                ymin, ymax = ax.get_ylim()
                c = np.mean([ymin, ymax])
                ax.set_ylim([c+w_max*0.5, c-w_max*0.5])
        # Set scales in left/right ================================================

        # Set labels
        x, y = -0.15, 0.5
        for ax in axes_l:
            ax.set_ylabel("Instrumental magnitude")
            ax.yaxis.set_label_coords(x, y)
        for ax in [axes_l[-1], axes_r[-1]]:
            if rotP_s:
                label = f"Phase (P={rotP_s} s, JD0={JD0})"
            else:
                label = f"Phase (P={rotP_h} h, JD0={JD0})"
            ax.set_xlabel(label)
            ax.yaxis.set_label_coords(x, y)

        out = f"foldmaginst.png"
        out = os.path.join(outdir, out)
        plt.savefig(out, dpi=200)


    print("\n==================================================================")
    print("p4 Extract trend by summing up flux of stars for correction")
    print("==================================================================")
    fig, axes_l, axes_r = myfigure_ncol(3, 2)
    trend_df_list = []
    for idx_b, b in enumerate(bands):
        df_ref_b = df_ref[df_ref["band"]==b]

        df_trend_b = refladder3(df_ref_b, objid_all, args.verbose)

        # Temporally ================
        #df_trend_b = refladder2(df_ref_b, objid_all, args.verbose)
        # Temporally ================

        df_trend_b["band"] = b
        trend_df_list.append(df_trend_b)
    df_trend = pd.concat(trend_df_list)

    # Corrected flux of the target: F_cor = F_target_0/F_refall_0
    
    # TODO: Update. Need corfac?

    print("\n================================================================")
    print("p5 Plot trend")
    print("==================================================================")
    fig, axes_l, axes_r = myfigure_ncol(3, 2)
    for idx_b, b in enumerate(bands):

        df_obj_b = df_obj[df_obj["band"]==b]
        df_trend_b = df_trend[df_trend["band"]==b]

        nframe_obj = set(df_obj_b["nframe"].values.tolist())
        n_obj = len(nframe_obj)
        nframe_ref = set(df_trend_b["nframe"].values.tolist())
        n_ref = len(nframe_ref)
        if n_obj != n_ref:
            nframe_common = list(nframe_obj & nframe_ref)
            n_com = len(nframe_common)
            n_diff = np.max([n_obj, n_ref]) - n_com
            print(f"  Use common obj ({n_diff} removed )")
            df_obj_b = df_obj_b[df_obj_b["nframe"].isin(nframe_common)]
            df_trend_b = df_trend_b[df_trend_b["nframe"].isin(nframe_common)]
        else:
            print(f"  Use all obj for refladder")
        df_obj_b = df_obj_b.reset_index(drop=True)
        df_trend_b = df_trend_b.reset_index(drop=True)

        # Plot
        ax_l = axes_l[idx_b]
        ax_r = axes_r[idx_b]

        color = color_from_band(b)

        # Raw sum flux
        #ax_l.errorbar(
        #    df_trend_b[key_t]-JD0, df_trend_b["f_total_raw"], df_trend_b["ferr_total_raw"],
        #    marker=None, color=color, ls="None")
        #ax_l.scatter(
        #    df_trend_b[key_t]-JD0, df_trend_b["f_total_raw"], label=f"Raw total flux {b}-band",
        #    ec=color, facecolor="None")

        # Trend of the lightcurve
        ax_r.errorbar(
            df_trend_b[key_t]-JD0, df_trend_b["corfac"], 
            df_trend_b["corfacerr"], marker=None, color=color, ls="None")
        ax_r.scatter(
            df_trend_b[key_t]-JD0, df_trend_b["corfac"], 
            label=f"Corrected total flux {b}-band", ec=color, facecolor="None")
        ax_l.legend()
        ax_r.legend()

    # Set labels
    x, y = -0.15, 0.5
    ymin_list, ymax_list = [], []
    for ax in axes_l:
        ax.set_ylabel("Count")
        ax.yaxis.set_label_coords(x, y)
        ymin, ymax = ax.get_ylim()
    for ax in [axes_l[-1], axes_r[-1]]:
        ax.set_xlabel(f"Julian Day - {JD0} [day]")
        ax.yaxis.set_label_coords(x, y)
    out = f"magtrend.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)


    print("\n================================================================")
    print("p6 Plot trend corrected reference magnitude")
    print("==================================================================")
    fig, axes = myfigure_long1col(N_band)
    for idx_b, b in enumerate(bands):
        df_ref_b = df_ref[df_ref["band"]==b]
        df_trend_b = df_trend[df_trend["band"]==b]
        df_ref_b = df_ref_b.reset_index(drop=True)
        df_trend_b = df_trend_b.reset_index(drop=True)
        
        # Frame numbers with trend
        nframe_t = set(df_trend_b["nframe"].values.tolist())

        #n_ref = len(nframe_ref)
        n_t = len(nframe_t)
        #if n_ref != n_t:
        #    nframe_common = list(nframe_ref & nframe_t)
        #    n_com = len(nframe_common)
        #    n_diff = np.max([n_ref, n_t]) - n_com
        #    print(f"  Use common obj ({n_diff} removed )")
        #    df_ref_b = df_ref_b[df_ref_b["nframe"].isin(nframe_common)]
        #    df_trend_b = df_trend_b[df_trend_b["nframe"].isin(nframe_common)]
        #else:
        #    print(f"  Use all obj for refladder")
        #df_ref_b = df_ref_b.reset_index(drop=True)
        #df_trend_b = df_trend_b.reset_index(drop=True)
        
        for idx_obj,obj in enumerate(objid_all):
            df_temp = df_ref_b[df_ref_b["objID"]==obj]
            nframe_temp = set(df_temp["nframe"].values.tolist())
            nframe_common = list(nframe_temp & nframe_t)

            # Catalog manitude for PS
            try:
                catmag = df_temp[f"{b}MeanPSFMag"].values.tolist()[0]
                catmag = np.round(catmag, 1)
            except:
                catmag = None

            # Detected location in 1st frame x0, y0 and last frame x1, y1
            try:
                x0 = df_temp["x"].values.tolist()[0]
                x0 = np.round(x0, 1)
                y0 = df_temp["y"].values.tolist()[0]
                y0 = np.round(y0, 1)
                x1 = df_temp["x"].values.tolist()[-1]
                x1 = np.round(x1, 1)
                y1 = df_temp["y"].values.tolist()[-1]
                y1 = np.round(y1, 1)
            except:
                x0, y0, x1, y1 = None, None, None, None

            # Use common frames
            df_temp = df_temp[df_temp["nframe"].isin(nframe_common)]
            df_trend_temp = df_trend_b[df_trend_b["nframe"].isin(nframe_common)]
            #print(f"{idx_obj+1} {obj} N_frame={len(df_temp)}")

            df_temp = df_temp.reset_index(drop=True)
            df_trend_temp = df_trend_temp.reset_index(drop=True)

            # Do Correction
            #ratio = df_temp["flux"]/df_trend_temp["f_total_cor"]
            #ratioerr = diverr(
            #    df_temp["flux"], df_temp["fluxerr"], 
            #    df_trend_temp["f_total_cor"], df_trend_temp["ferr_total_cor"]) 
            ratio = df_temp["flux"]/df_trend_temp["corfac"]
            ratioerr = diverr(
                df_temp["flux"], df_temp["fluxerr"], 
                df_trend_temp["corfac"], df_trend_temp["corfacerr"]) 

            # Remove 0 or negative
            idx_use = ratio > 0
            bad = [x for x in ratio if x < 0]
            assert len(bad)==0, "Check the code."

            df_temp["magcor"]    = -2.5*np.log10(ratio)
            df_temp["magcorerr"] = 2.5*log10err(ratio, ratioerr)

            key_mag = "magcor"
            key_magerr = "magcorerr"

            # Plot
            ax = axes[idx_b]

            color = mycolor[idx_obj]
            # Mean is set to 0.2*idx_obj
            offset = np.mean(df_temp[key_mag]) - 0.3*idx_obj
            ax.errorbar(
                df_temp[key_t]-JD0, df_temp[key_mag]-offset, df_temp[key_magerr],
                marker=None, color=color, ls="None")
            ax.scatter(
                df_temp[key_t]-JD0, df_temp[key_mag]-offset, 
                label=f"{obj} {b}-band ({catmag} mag, x0,y0 ={x0},{y0}, x1,y1={x1},{y1})",
                ec=color, facecolor="None")

        ax.legend(ncol=2)

    # Set labels
    x, y = -0.05, 0.5
    ymin_list, ymax_list = [], []
    for ax in axes:
        ax.set_ylabel("Relative magnitude + offset")
        ax.yaxis.set_label_coords(x, y)
        ax.invert_yaxis()
    #for ax in axes:
    #    ymin, ymax = ax.set_ylim([0.1, ymin])
    #   ax.set_ylim([0.1, ymin])
    for ax in [axes[-1]]:
        ax.set_xlabel(f"Julian Day - {JD0} [day]")
        ax.yaxis.set_label_coords(x, y)
    out = f"magcorref.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    
    out = "trend.txt"
    out = os.path.join(outdir, out)
    df_trend.to_csv(out, sep=" ")

    print("\n================================================================")
    print("p5 Plot trend corrected target magnitude")
    print("==================================================================")

    fig, axes_l, axes_r = myfigure_ncol(3, 2)
    for idx_b, b in enumerate(bands):
        df_obj_b = df_obj[df_obj["band"]==b]
        df_trend_b = df_trend[df_trend["band"]==b]

        nframe_obj = set(df_obj_b["nframe"].values.tolist())
        n_obj = len(nframe_obj)
        nframe_ref = set(df_trend_b["nframe"].values.tolist())
        n_ref = len(nframe_ref)
        if n_obj != n_ref:
            nframe_common = list(nframe_obj & nframe_ref)
            n_com = len(nframe_common)
            n_diff = np.max([n_obj, n_ref]) - n_com
            print(f"  Use common obj ({n_diff} removed )")
            df_obj_b = df_obj_b[df_obj_b["nframe"].isin(nframe_common)]
            df_trend_b = df_trend_b[df_trend_b["nframe"].isin(nframe_common)]
        else:
            print(f"  Use all obj for refladder")
        df_obj_b = df_obj_b.reset_index(drop=True)
        df_trend_b = df_trend_b.reset_index(drop=True)

        # Do Correction
        #ratio = df_obj_b["flux"]/df_trend_b["f_total_cor"]
        #ratioerr = diverr(
        #    df_obj_b["flux"], df_obj_b["fluxerr"], 
        #    df_trend_b["f_total_cor"], df_trend_b["ferr_total_cor"]) 
        ratio = df_obj_b["flux"]/df_trend_b["corfac"]
        ratioerr = diverr(
            df_obj_b["flux"], df_obj_b["fluxerr"], 
            df_trend_b["corfac"], df_trend_b["corfacerr"]) 

        # Remove 0 or negative
        idx_use = ratio > 0
        bad = [x for x in ratio if x < 0]
        assert len(bad)==0, "Check the code."
        #ratio = ratio[idx_use]
        #ratioerr = ratioerr[idx_use]

        key_mag = "mag"
        key_magerr = "magerr"
        key_magerr0 = "magerr0"
        key_magerr1 = "magerr1"
       
        df_obj_b["rawmag"]    = -2.5*np.log10(ratio)
        df_obj_b[key_magerr] = 2.5*log10err(ratio, ratioerr)
        # Ratio is "some sort of flux"
        ## Magnitude error of faint side
        df_obj_b[key_magerr0] = 2.5*np.log10(ratio/(ratio-ratioerr))
        ## Magnitude error of bright side
        df_obj_b[key_magerr1] = -2.5*np.log10(ratio/(ratio+ratioerr))


        # Do brightness correction (delta, r) =================================
        # if with args.target
        if args.target:
            if not args.G:
                print("    Note: Set G value directly for Famous asteroids.")
            # Obtain mag_red (reduced magnitude) by distance correction
            # rawmag -> mag_red
            df_obj_b = mag_correction(df_obj_b, key_mag="rawmag")
            # Obtain mag_red_phase by solar phase correction
            # stype is given to estimate slope parameter G !
            # If G is given, not stype but G is used.
            # mag_red -> mag_red_phase
            df_obj_b = phase_correction(df_obj_b, args.stype, args.G)
            #col_use = [
            #    "t_sec_obs", "t_sec_ltcor", "t_jd", "t_jd_ltcor",
            #    "flux", "fluxerr", "magcor", "mag_red", "mag_red_phase", 
            #    f"magcorerr", f"eflag"]
        else:
            print("Do not do magnitude corrections. (distance and solar phase)")
       
        # Rename mag_red_phase -> mag
        df_obj_b = df_obj_b.rename(columns={"mag_red_phase":key_mag})
        # Save slope parameter
        df_obj_b["G"] = args.G
        # Rotational period in hour
        df_obj_b["rotP"] = rotP_h
        # Select usefull columns
        col_use = [
            "flux", "fluxerr", "mag", "magerr", "magerr0", "magerr1", "t_jd_ltcor", 
            "G", "phase", "n_phase"]
        df_obj_b = df_obj_b[col_use]
        out = os.path.join(outdir, f"{target}_refladder_mag_{b}.txt")
        df_obj_b.to_csv(out, sep=" ", index=False)
        # Do brightness correction (delta, r) =================================

        # Plot
        ax_l = axes_l[idx_b]
        ax_r = axes_r[idx_b]

        # Obj corercted
        color = color_from_band(b)
        offset = np.mean(df_obj_b[key_mag])
        ax_l.errorbar(
            df_obj_b[key_t_obj]-JD0, df_obj_b[key_mag]-offset, df_obj_b[key_magerr],
            marker=None, color=color, ls="None")
        ax_l.scatter(
            df_obj_b[key_t_obj]-JD0, df_obj_b[key_mag]-offset, label=f"Obj {b}-band",
            ec=color, facecolor="None")

        if args.rotP:
            # Obj folded
            for idx_p, n in enumerate(range(N_phase)):
                df_temp = df_obj_b[df_obj_b["n_phase"] == n+1]
                ax_r.errorbar(
                    df_temp["phase"], df_temp[key_mag]-offset, df_temp[key_magerr],
                    marker=None, color=mycolor[idx_p], ls="None")
                ax_r.scatter(
                    df_temp["phase"], df_temp[key_mag]-offset, label=f"Obj {b}-band",
                    ec=mycolor[idx_p], facecolor="None")

        ax_l.legend()
        ax_r.legend()

    # Set labels
    x, y = -0.15, 0.5
    ymin_list, ymax_list = [], []
    for ax in axes_l:
        ax.set_ylabel("Relative magnitude")
        ax.yaxis.set_label_coords(x, y)
        ymin, ymax = ax.get_ylim()
        ymin_list.append(ymin)
        ymax_list.append(ymax)
    ymin = np.min(ymin_list)
    ymax = np.max(ymax_list)
    for ax in axes_l + axes_r:
        ax.set_ylim([ymax, ymin])

    for ax in [axes_l[-1]]:
        ax.set_xlabel(f"Julian Day - {JD0} [day]")
        ax.yaxis.set_label_coords(x, y)
    for ax in [axes_r[-1]]:
        if rotP_s:
            label = f"Phase (P={rotP_s} s, JD0={JD0})"
        else:
            label = f"Phase (P={rotP_h} h, JD0={JD0})"
        ax.set_xlabel(label)
        ax.yaxis.set_label_coords(x, y)
    out = f"magcorobj.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
