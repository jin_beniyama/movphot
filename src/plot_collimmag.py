#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot limitting magnitude.
"""
import numpy as np
import pandas as pd
import os 
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from argparse import ArgumentParser as ap

from .prepro import calc_d_from_edge, clean_photres
from movphot.psdb import check_objinfo, PS_checked


plt.rcParams["font.family"] = "DejaVu Sans"
plt.rcParams["font.size"] = 10
plt.rcParams["axes.labelsize"] = 10
plt.rcParams["legend.fontsize"] = 8
plt.rcParams["axes.labelsize"] = 8
plt.rcParams["axes.titlesize"] = 15

mycolor = ["#AD002D", "#1e50a2", "#006e54", "#ffd900", "#EFAEA1", 
           "#69821b", "#ec6800", "#afafb0", "#0095b9", "#89c3eb"]*100

myls = ["solid", "dashed", "dashdot", "dotted", 
        (0, (5, 3, 1, 3, 1, 3)), (0, (4,2,1,2,1,2,1,2))]*100

mymark = ["o", "^", "x", "D", "+", "v", "<", ">", "h", "H"]*100


def plot_limmag(
        df, objID, magsystem, bands, key_x, key_y, cntmfrac_max, 
        dedge_min, eflag_max, nx, ny, title=None):
    """
    Plot cat_mag vs. SNR
    edge removeal, cntm removal, PS flag removal are done in this function.

    Note
    ----
    bad objects (large eflag etc.) are removed in this function,
    not clean_photres.


    Parameters
    ----------
    df : pandas.DataFrame
        cleaned photometry result
    df : array-like
        object ID
    magsystem : str
        magnitude system (PS, SDSS, gaia etc.)
    bands : array-like
        ["g", "r", "i"] etc.
    key_x, key_y : str
        keywords for x and y
    cntmfrac_max : float
        contamination ratio (0.05 accepts up to 5 percent contamination)
    dedge_min : float
        minimum distance from any edge
    eflag_max : float
        eflag threshold
    nx, ny : float
        numbers of pixels along each axis
    title : str
        title of the figure
    """

    N_band = len(bands)
    b1 = bands[0]

    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_axes([0.1, 0.15, 0.85, 0.75])
    ax.set_xlabel(f"{magsystem} magnitude [mag]")
    ax.set_ylabel("S/N")
    ax.set_yscale("log")
  
    for idx_band,band in enumerate(bands):
        label = f"{band}"
        band_l, band_r = band4cterm(band, bands)
        if args.verbose:
            print(f"  Use (band,band_l,band_r) = ({band},{band_l},{band_r})")

        # Band identical color, mark, and column
        color = mycolor[idx_band]
        mark = mymark[idx_band]
        column = df.columns.tolist()

        # Use flux, mag_cat, mag_cat_l, mag_cat_r (+ errors), and eflag
        for obj in objID:
            col_obj = [col for col in column if (len(col))>18 and obj in col]
            df_obj = df[col_obj]  
 
            # outlier removal (not common in frames)
            df_obj = df_obj[df_obj[f"flux_{obj}_{band}"] > 0]
            df_obj = df_obj[df_obj[f"{band}MeanPSFMag_{obj}_{band}"] > 0]
            df_obj = df_obj[df_obj[f"eflag_{obj}_{band}"] < eflag_max]

            # cntmfrac removal
            df_obj = df_obj[df_obj[f"cntmfrac_{obj}_{band}"] < cntmfrac_max]
                    
            # Edge removal
            # Calculate distance from the closest edge
            df_obj[f"dedge_{obj}_{band}"] = calc_d_from_edge(
                df_obj[f"{key_x}_{obj}_{band}"], 
                df_obj[f"{key_y}_{obj}_{band}"], nx, ny)
            df_obj = df_obj[df_obj[f"dedge_{obj}_{band}"] > dedge_min]
            


            # Extract info. of an object (Continued)
            catmag = df_obj[f"{band}MeanPSFMag_{obj}_{band}"]
            catmagerr = df_obj[f"{band}MeanPSFMagErr_{obj}_{band}"]
            flux = df_obj[f"flux_{obj}_{band}"]
            fluxerr = df_obj[f"fluxerr_{obj}_{band}"]
            snr = flux/fluxerr
            
            ax.errorbar(catmag, snr, xerr=catmagerr, color=color, marker=mark, label=label, lw=0)
            label = None

    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    # S/N = 10, 50, 100
    ax.hlines(10, xmin, xmax, ls="dashed")
    ax.hlines(50, xmin, xmax, ls="dotted")
    ax.hlines(100, xmin, xmax, ls="dashdot")
    # ax.vlines(17.5, ymin, ymax, color="gray", ls="dashed")
    ax.set_xlim([xmin, xmax])
    ax.set_ylim([ymin, ymax])
    ax.legend()

    ax.set_title(title)

    band_part = "".join(bands)
    out =  f"Limmagplot_N{len(df)}_{band_part}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)


if __name__ == "__main__":
    parser = ap(description="Plot catmag vs. SNR.")
    parser.add_argument(
        "csv", type=str, help="photometry_result.csv")
    parser.add_argument(
        "obj", type=str, help="object name")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--inst", type=str, default=None,
        help="instrumental name")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--catmag_max", type=float, default=19, 
        help="maximum eflag value")
    parser.add_argument(
        "--catmagerr_max", type=float, default=0.1, 
        help="maximum catalog magnitude error")
    parser.add_argument(
        "--eflag_max", type=float, default=1, 
        help="maximum eflag value")
    parser.add_argument(
        "--gr_min", type=float, default=-1, 
        help="minimum g-r")
    parser.add_argument(
        "--gr_max", type=float, default=1, 
        help="maximum g-r")
    parser.add_argument(
        "--ri_min", type=float, default=-1, 
        help="minimum r-i")
    parser.add_argument(
        "--ri_max", type=float, default=1, 
        help="maximum r-i")
    parser.add_argument(
        "--cntmfrac_max", type=float, default=0.05, 
        help="maximum contamination fraction")
    parser.add_argument(
        "--dedge_min", type=float, default=30, 
        help="maximum distance from the edge")
    parser.add_argument(
        "--nx", type=int, default=2160,
        help="number of pixels along x axis")
    parser.add_argument(
        "--ny", type=int, default=1280,
        help="number of pixels along y axis")
    parser.add_argument(
        "--key_x", type=str, default="x",
        help="keyword for x")
    parser.add_argument(
        "--key_y", type=str, default="y",
        help="keyword for y")
    parser.add_argument(
        "--title", type=str, default=None, 
        help="title of the figure")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, 
        help="output messages")
    args = parser.parse_args()

   
    # Set output directory
    outdir = "plot"
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    df = pd.read_csv(args.csv, sep=" ")

    if args.inst == "TriCCS":
        nx, ny = 2160, 1280
    else:
        nx, ny = args.nx, args.ny

    bands = args.bands
    gr_min, gr_max = args.gr_min, args.gr_max
    ri_min, ri_max = args.ri_min, args.ri_max
    catmag_max, catmagerr_max = args.catmag_max, args.catmagerr_max
    eflag_max = args.eflag_max
    cntmfrac_max = args.cntmfrac_max
    dedge_min = args.dedge_min
    key_x, key_y = args.key_x, args.key_y
    verbose = args.verbose

    df, objID = clean_photres(
        df,  bands, key_x, key_y, gr_min, gr_max, ri_min, ri_max, 
        catmag_max, catmagerr_max, verbose)
    print(f"  DataFrame Dimention (original) {len(df)}")


    N_nan = np.sum(df.isnull().sum())
    assert N_nan == 0, "Nan detected."
    
    # Plot
    plot_limmag(
        df, objID, args.magtype, bands, key_x, key_y, cntmfrac_max, dedge_min, 
        eflag_max, nx, ny, 
        args.title)


