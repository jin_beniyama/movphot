#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Extract arbitary stars from result of photometry.
"""
import numpy as np
import pandas as pd
import os 
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from argparse import ArgumentParser as ap


if __name__ == "__main__":
    parser = ap(description="Result of photometry.")
    parser.add_argument(
        "res", type=str, 
        help="photres_ref_mag14f0to17f0magerr0f05dedge50eflag1.txt")
    parser.add_argument(
        "--gr_min", type=float, default=0, 
        help="minimum g-r value for comparison stars")
    parser.add_argument(
        "--gr_max", type=float, default=1.1, 
        help="maximum g-r value for comparison stars")
    parser.add_argument(
        "--ri_min", type=float, default=0, 
        help="minimum r-i value for comparison stars")
    parser.add_argument(
        "--ri_max", type=float, default=0.8, 
        help="maximum r-i value for comparison stars")
    parser.add_argument(
        "--riinst_min", type=float, default=0.0, 
        help="maximum instrumental r-i")
    parser.add_argument(
        "--riinst_max", type=float, default=0.0, 
        help="maximum instrumental r-i")
    args = parser.parse_args()

    # Read result
    df_all = pd.read_csv(args.res, sep=" ")

    gr_min, gr_max = args.gr_min, args.gr_max
    ri_min, ri_max = args.ri_min, args.ri_max

    g_mag, g_magerr = f"gMeanPSFMag", f"gMeanPSFMagErr"
    r_mag, r_magerr = f"rMeanPSFMag", f"rMeanPSFMagErr"
    i_mag, i_magerr = f"iMeanPSFMag", f"iMeanPSFMagErr"
    
    # Calculate catalog color
    df_all["g_r"] = df_all[g_mag]-df_all[r_mag]
    df_all["r_i"] = df_all[r_mag]-df_all[i_mag]


    
    # Extract unique frames
    df = df_all.drop_duplicates(subset=["objID"])
    obj = set(df.objID)
    print(f"N_obj = {len(df)}")
    print(df)
    print(f"Select g-r")
    # Extract g-r
    df = df[((df["g_r"]) < gr_max) & ((df["g_r"]) > gr_min)]
    # Extract r-i
    df = df[((df["r_i"]) < ri_max) & ((df["r_i"]) > ri_min)]

    print(f"N_obj = {len(df)}")
    print(df)
    #    & ((df[r_mag]-df[i_mag]) < ri_max)
    #    & ((df[r_mag]-df[i_mag]) > ri_min)]

    objID_all = set(df["objID"])
    print(objID_all)

    # Calculate instrumental magnitude of objects
    band_l, band_r = "r", "i"
    if args.riinst_min!=0 or args.riinst_max!=0:
        for obj in objID_all:
            df_obj = df_all[df_all["objID"]==obj]
            df_l = df_obj[df_obj["band"] == band_l]
            df_r = df_obj[df_obj["band"] == band_r]
            nframe_l = list(set(df_l["nframe"].values.tolist()))
            nframe_r = list(set(df_r["nframe"].values.tolist()))
            nframe_common = set(nframe_l) & set(nframe_r)

            df_l_com = df_l[df_l["nframe"].isin(nframe_common)]
            df_r_com = df_r[df_r["nframe"].isin(nframe_common)]
            flux_l = df_l_com[f"flux"].values.tolist()
            flux_r = df_r_com[f"flux"].values.tolist()
            x_l = df_l_com[f"xwin"].values.tolist()
            y_l = df_l_com[f"ywin"].values.tolist()
            x_r = df_r_com[f"xwin"].values.tolist()
            y_r = df_r_com[f"ywin"].values.tolist()
            col_inst = [np.round(-2.5*np.log10(x/y), 2) for x, y in zip(flux_l, flux_r)]
            fits = sorted(df_r_com[f"fits"].values.tolist())
            # Search by inst color
            for x in col_inst:
                if (x < args.riinst_max) and (x > args.riinst_min):
                    print(f"Bad object : {obj}")
                    print(f"N_det = {len(fits)}")
                    print(col_inst)
                    print(flux_l)
                    print(flux_r)
                    print(x_l)
                    print(y_l)
                    print(x_r)
                    print(y_r)
                    print(fits)
