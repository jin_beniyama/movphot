#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Concatenate results of color mesurements.
The purpose is to plot reflectance specra using 
gri and grz results at once.
"""
import os
from argparse import ArgumentParser as ap
import pandas as pd
from movphot.common import checknan

# To add empty columns since gri and grz dataframe have different columns =====
addcol4gri = [
    "g_z", "g_zerr", "z_g", "z_gerr", 
    "r_z", "r_zerr", "z_r", "z_rerr", 
    "r_z_inst", "r_zerr_inst", 
    "z_g_inst", "z_gerr_inst", 
    "t_utc_z", "t_jd_z"]
addcol4grz = [
    "g_i", "g_ierr", "i_g", "i_gerr", 
    "r_i", "r_ierr", "i_r", "i_rerr", 
    "r_i_inst", "r_ierr_inst", 
    "i_g_inst", "i_gerr_inst", 
    "t_utc_i", "t_jd_i"]


if __name__ == "__main__":
    parser = ap(
        description="Handle Seimei obs. data of 2015 RN35 for the paper.")
    parser.add_argument(
        "res", type=str, nargs="*",
        help="Photometry results")
    parser.add_argument(
        "--band", type=str, nargs="*",
        help="3rd bands of photometrc results")
    parser.add_argument(
        "--out", type=str, default=None,
        help="Output file name")
    parser.add_argument(
        "--outdir", type=str, default=".",
        help="Directory for output")
    args = parser.parse_args()

    assert len(args.res) == len(args.band), "Check the arguments"

    if not os.path.isdir(args.outdir):
        os.makedirs(args.outdir)
   

    df_obs_list = []
    for idx_res, x in enumerate(args.res):
        df_obs = pd.read_csv(x, sep=" ", index_col=0)
        
        # Add columns for consistency
        b3 = args.band[idx_res]
        if b3 == "i":
            acol = addcol4gri
        elif b3 == "z":
            acol = addcol4grz

        for x in acol:
            df_obs[x] = 0
        
        # Save obs identification number
        df_obs["n_obs"] = idx_res+1
        df_obs = df_obs.reset_index(drop=True)
       
        if idx_res == 0:
            col_stan = df_obs.columns.tolist()
        else:
            # Sort columns 
            df_obs = df_obs.reindex(columns=col_stan)
        checknan(df_obs)
        print(f"    df_obj N={len(df_obs)} added")
        df_obs_list.append(df_obs)
    df_all = pd.concat(df_obs_list)
    checknan(df_all)
    
    if args.out:
        out = args.out
    else:
        out = f"concat_col.txt"
    out = os.path.join(args.outdir, out)
    df_all.to_csv(out, sep=" ", index=False)
