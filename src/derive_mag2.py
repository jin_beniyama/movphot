#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Determine Color Term (CT) using reference stars in all frames
by setting an offset to 0 in each axis.
Input file has to contain at least 2-bands photometric results.
Also, this script needs color file created with derive_color.py like
colall_N100_X.csv.

Large color (col_inst or col_cat) points are removed in output figures.
(a value err_th is used to remove objects by both catalog and inst. magnitudes)


Definitions of CT and Z
-----------------------
CT (color term):
    slope of color_cat (m_cat,1-m_cat,2) vs m_inst,1 - m_cat,1
Z (magnitude zero point):
    intercept of color_cat (m_cat,1-m_cat,2) vs m_inst,1 - m_cat,1

CTs are common values for an instrument.
Zs could differ even in during the night.


Output Examples
---------------
XXX


Keys
----
magnitude: g_inst, gerr_inst, g_ccor, gerr_ccor, g_wmean, g_wstd
           (g_mean, g_std, g_std_phot, g_SD, g_SE)
color    : g_r, g_rerr, g_r_wmean, g_r_wstd
           (instrumental colors are not used in this script)
CTG/CTI:
CT/Z:



Summary of Procedures
---------------------
(p1, p2, ... are usefull search words.)
  p1  Read photometric results
  p2  Obtain common objID from input DataFrame
  p3  Remove red comparison stars from df 
  p4  Remove blue comparison stars from df 
  p5  Remove large magnitude error comparison stars from df
  p6  Remove comparison stars from df by magnitude
  p7  Calculate CTs (and Zs temporally, not final values)
  p8  Save CTs
"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from movphot.common import log10err, checknan, add_aspect_data, time_correction, mag_correction
from movphot.visualization import myfigure_ncol
from movphot.prepro import plot_res
from movphot.prepro_mag import plot_CTfit2, plot_Zfit2, calc_objmag2, calc_refmag



if __name__ == "__main__":
    parser = ap(description="Derive object magnitude with Color Term")
    parser.add_argument(
        "res_tar", type=str, help="photometry result of target")
    parser.add_argument(
        "res_ref", type=str, help="photometry result of reference")
    parser.add_argument(
        "res_col", type=str, help="text contains object colors")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--target", type=str, default=None,
        help="target name (for periodic analysis)")
    parser.add_argument(
        "--inst", type=str, default=None, help="instrumental name")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--yr_res", nargs=2, default=None, 
        help="y ranges of residual plots")
    parser.add_argument(
        "--key_x", type=str, default="x", help="keyword for x")
    parser.add_argument(
        "--key_y", type=str, default="y", help="keyword for y")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, 
        help="output messages")
    parser.add_argument(
        "--Nobj_min", type=int, default=10, 
        help="minimum object numbers in a frame to be desired")
    parser.add_argument(
        "--Nframe_min", type=int, default=50, 
        help="minimum frames detected in a whole data to be desired")
    parser.add_argument(
        "--Ncut", type=int, default=1, 
        help="Cut dataframe to check CT status.")
    parser.add_argument(
        "--targetstar", nargs=5, default=[0, 0, 0, 0, 0], 
        help="Search object using x0, x1, y0, y1 in CTplot")
    parser.add_argument(
        "--ycolor", type=int, default=1, 
        help="Number of colors to plot CT residual histograms depends on y")
    parser.add_argument(
        "--colormap", action="store_true", default=False,
        help="Plot CT fitting with color map like Fig. 7 in Jackson+2021")
    parser.add_argument(
        "--cbar", action="store_true", default=False,
        help="Use cbar to show time-series info.")
    parser.add_argument(
        "--same", action="store_true", default=False,
        help="Whether use the same color for all ref stars in res plot")
    parser.add_argument(
        "--objID_ref", type=str,  default=None,
        help="Object ID to be analysed")
    parser.add_argument(
        "--N_mc", type=int, default=100, 
        help="Number of trials")
    parser.add_argument(
        "--ny", type=int, default=1280, 
        help="pixel number along y axis for residual plot")
    parser.add_argument(
        "--fr", nargs=2, default=None, type=int,
        help="frame range")
    parser.add_argument(
        "--SNRmin", type=int, default=10, 
        help="demanded SNR for referece stars")
    parser.add_argument(
        "--magmin", nargs="*", type=float, default=[0, 0, 0],
        help="minimum magnitude")
    parser.add_argument(
        "--magmax", nargs="*", type=float, default=[20, 20, 20],
        help="maximum magnitude")
    parser.add_argument(
        "--outdir", type=str, default=None,
        help="output directory")
    parser.add_argument(
        "--loc", type=str, default="371",
        help="location of the observatory")
    parser.add_argument(
        "--jd_band", type=str, default="g", help="filter of time standard")
    parser.add_argument(
        "--overwrite", action="store_true", default=False,
        help="Overwrite the output directory")
    parser.add_argument(
        "--badstar", nargs="*", default=[],
        help="Stars not used in the analysis")
    args = parser.parse_args()

    print("Versions:")
    print(f"  np {np.__version__}")
    print(f"  pd {pd.__version__}")

    # Target name
    if args.target is None:
        target        = "Target"
    else:
        target        = args.target

    if args.fr:
        fmin, fmax = args.fr
        fr_str = f"_f{fmin}to{fmax}"
    else:
        fr_str = ""

    magmin, magmax = args.magmin, args.magmax
    magmin_str = ["f".join(str(x).split(".")) for x in magmin]
    magmin_str = "_".join(magmin_str)
    magmax_str = ["f".join(str(x).split(".")) for x in magmax]
    magmax_str = "_".join(magmax_str)
    mag_str = f"_mag{magmin_str}to{magmax_str}"

    snr_str = f"_snrmin{args.SNRmin}"

    # Do not remove objects with extreme colors
    key_x, key_y   = args.key_x, args.key_y
    # Selection criteria of reference stars
    Nobj_min       = args.Nobj_min
    Nframe_min     = args.Nframe_min
    # Selection criteria of the target
    verbose        = args.verbose
    ycolor         = args.ycolor
    colormap       = args.colormap
    cbar           = args.cbar
    objID_ref      = args.objID_ref
    # Whether use the same color in residual plot for all field stars
    same           = args.same
    N_mc           = args.N_mc

    if colormap:
        cmstr = "cm"
    elif cbar:
        cmstr = "cbar"
    else:
        cmstr = ""
    

    magtype        = args.magtype
    # yrange of residual plot
    yr_res         = args.yr_res

    if args.badstar:
        bad_str = "_".join(args.badstar)
        bad_str = f"_rm{bad_str}"
    else:
        bad_str = ""

    bands = args.bands
    # Number of bands
    N_band = len(bands)
    assert N_band == len(args.magmin)
    assert N_band == len(args.magmax)

    if N_band==2:
        n_band_plot = N_band - 1  
    else:
        n_band_plot = N_band 
    Ncut = args.Ncut
    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]
    
    ny = args.ny

    # FILE NAME OF OBJECT
    filename_tar = args.res_tar.split("/")[-1].split(".")[0]
    filename_ref = args.res_ref.split("/")[-1].split(".")[0]

    # outdir
    outdir_parent = "plotmag"
    os.makedirs(outdir_parent, exist_ok=True)

    
    if args.outdir:
        outdir_child = args.outdir
    else:
        outdir_child = (
            f"{filename_tar}_{filename_ref}_"
            f"Nobjmin{Nobj_min}Nframemin{Nframe_min}{fr_str}{cmstr}{mag_str}{snr_str}{bad_str}"
            )
    outdir = os.path.join(outdir_parent, outdir_child)
    if args.overwrite:
        os.makedirs(outdir, exist_ok=True)
    else:
        os.makedirs(outdir, exist_ok=False)


    print("\nArguments")
    print(f"              key_x, key_y      = {key_x}, {key_y}")
    print(f"              Nobj_min          = {Nobj_min}")


    print("\n================================================================")
    print("p1 Read photometric results")
    print("==================================================================")
    df_tar = pd.read_csv(args.res_tar, sep=" ")
    df_ref = pd.read_csv(args.res_ref, sep=" ")
    print("Number of data is for all bands (not single band)")
    print("  Original")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")

    # Select with frame range
    if args.fr:
        df_tar = df_tar[(df_tar["nframe"]> fmin) & (df_tar["nframe"] < fmax)]
        df_ref = df_ref[(df_ref["nframe"]> fmin) & (df_ref["nframe"] < fmax)]

    # Added 2023-01-09
    # Remove flux < 0 data
    df_tar = df_tar[(df_tar["flux"]> 0)]
    # Added 2023-05-02
    df_ref = df_ref[(df_ref["flux"]> 0)]
    print("  After removal of negative fluxes")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")

    # Added 2023-05-05
    # Remove bright/faint objects
    df_ref = df_ref[(df_ref["flux"]/df_ref["fluxerr"]> args.SNRmin)]
    for idx_b, b in enumerate(bands):
        magmin, magmax = args.magmin[idx_b], args.magmax[idx_b]
        df_ref = df_ref[
            (df_ref[f"{b}MeanPSFMag"] < magmax) 
            & (df_ref[f"{b}MeanPSFMag"] > magmin)]
    print(f"  After removal of faint objects w/SNR < {args.SNRmin}")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")

    # Remove bad stars
    for x in args.badstar:
        df_ref = df_ref[df_ref["objID"]!=int(x)]
        print(f"Do not use {x}")

    # Added 2023-05-11
    objlist = list(set(df_ref.objID.tolist()))
    rm_list = []
    for x in objlist:
        df_temp = df_ref[df_ref.objID==x]
        for b in bands:
            df_temp_b = df_temp[df_temp["band"] == b]
            if len(df_temp_b) < args.Nframe_min:
                rm_list.append([x, b])
    for rm in rm_list:
        x, b = rm
        df_ref = df_ref[~((df_ref.objID==x) & (df_ref.band==b))]
    print(f"  Final")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")


    print("\n================================================================")
    print("p2 Calculate time in second and save for periodic analysis")
    print("==================================================================")
    # For periodic analysis 
    df_tar.loc[:, f"t_sec"] = df_tar[f"t_jd"]*24*3600.
    df_ref.loc[:, f"t_sec"] = df_ref[f"t_jd"]*24*3600.

    T0 = np.min(df_tar["t_sec"])
    # Set the first time to zero
    df_tar[f"t_sec"] -= T0
    df_ref[f"t_sec"] -= T0

    # Effective frames
    nframe_list = list(set(df_tar["nframe"].values.tolist()))
    Nframe = len(nframe_list)
    # Match the frames
    df_ref = df_ref[df_ref["nframe"].isin(nframe_list)]
    df_ref = df_ref.reset_index(drop=True)

    checknan(df_tar)
    checknan(df_ref)


    print("\n================================================================")
    print("p3 Calculate and plot CTs")
    print("==================================================================")
    # Cut and replot CT for the sake of the clarity when Ncut != 1
    df_cut_list = []
    for n in range(Ncut):
        # Extract df
        N0 = int(len(df_ref)/Ncut*n)
        N1 = int(len(df_ref)/Ncut*(n+1))
        # if Ncut !=0, Create separate plot
        df_temp = df_ref[(df_ref["nframe"] > N0) & (df_ref["nframe"] <= N1)]
        # length ~ N_obj * (N1-N0) ~ N_obj * nframe
    
        # Add ax for cbar by hand (color map doesn't need...!!)
        if cbar:
            fig, axes_raw, axes_shift, axes_cbar, axes_CT, axes_res = myfigure4CT(
                n_band=n_band_plot, n_col=3)
        else:
            fig, axes_raw, axes_shift, axes_CT, axes_res = myfigure_ncol(
                n_band=n_band_plot, n_col=3, res=True)
            # Dummy
            axes_cbar = axes_res

        axes_raw[0].set_title("Without shift")
        axes_shift[0].set_title("Shifted to a mean of zero")

        df_CT_list = []
        for idx, band in enumerate(bands):
            # Search target only in selected band
            if band == args.targetstar[4]:
                # target has [x0, x1, y0, y1, bands]
                targetstar_info = [float(x) for x in args.targetstar[0:4]]
            else:
                targetstar_info = None
            
            ax_raw   = axes_raw[idx]
            ax_shift = axes_shift[idx]
            ax_cbar  = axes_cbar[idx]
            ax_CT    = axes_CT[idx]
            ax_res   = axes_res[idx]

            checknan(df_ref)

            # Plot without shift
            # not used if #objects < Nobj_min
            _ = plot_CTfit2(
                df_ref, band, bands, Nobj_min, fig, ax_raw, shift=False, ycolor=ycolor,
                ny=ny, colormap=colormap, cbar=cbar, ax_cbar=ax_cbar, N_mc=N_mc,
                target=targetstar_info, verbose=verbose)
            
            # Plot with shift
            df_CT = plot_CTfit2(
                df_ref, band, bands, Nobj_min, fig, ax_shift, ax_res=ax_res, yr_res=yr_res,
                shift=True, ycolor=ycolor, ny=ny, colormap=colormap, cbar=cbar, ax_cbar=ax_cbar, 
                ax_CT=ax_CT, N_mc=N_mc, verbose=verbose)

            df_CT_list.append(df_CT)

        if Ncut==1:
            out = f"CTstatus_N{Nframe}.png"
        else:
            out = f"CTstatus_{n+1}_N{N1-N0+1}.png"
        out = os.path.join(outdir, out)
        plt.savefig(out, dpi=200)
        plt.close()

        df_CT = pd.concat(df_CT_list, axis=1)
        # Add cut df
        df_cut_list.append(df_CT)
    df_CT = pd.concat(df_cut_list, axis=0)

    # Only plot and finish analyses when Ncut > 1
    if len(df_CT) > 1:
        print("Only plot and finish analyses when Ncut > 1")
        sys.exit()

    checknan(df_ref)

    # For reference 
    print("\n================================================================")
    print("p4 Save CTs")
    print("==================================================================")
    out =  f"CT_N{Nframe}.txt"
    out = os.path.join(outdir, out)
    df_CT.to_csv(out, sep=" ", index=False)



    print("\n================================================================")
    print("p5 Calculate and plot Z of each frame")
    print("==================================================================")
    # axes_Z : 
    #     CT corrected Z light curves
    fig, axes_Z = myfigure_ncol(n_band=len(bands), n_col=1)
    dy_list = []
    df_Z_list = []

    # Initial values
    df_ref["CT"], df_ref["CTerr"] = 0, 0
    df_ref["Z"], df_ref["Zerr"]   = 0, 0

    df_tar["CT"], df_tar["CTerr"] = 0, 0
    df_tar["Z"], df_tar["Zerr"]   = 0, 0

    for idx, band in enumerate(bands):
        ax_Z = axes_Z[idx]

        # Extract CT to determine Z
        CT = df_CT.at[0, f"{band}_CT_MC"]
        CTerr = df_CT.at[0, f"{band}_CTerr_MC"]
        print(f"    CT = {CT:.4f}+-{CTerr:.4f}")

        ax_Z.set_ylabel("Z")
        if idx==(len(bands)-1):
            ax_Z.set_xlabel("Seconds")

        # Calculate and plot Zs (0 inserted if #objects < Nobj_min)
        df_Z = plot_Zfit2(
            df_ref, band, bands, CT, CTerr, Nobj_min, ax_Z, N_mc)

        # Delta y to scale ylim
        ymin, ymax = ax_Z.get_ylim()
        dy_list.append(abs(ymax-ymin))
        df_Z_list.append(df_Z)

        # Add CT
        df_ref.loc[df_ref["band"]==band, "CT"] = CT
        df_ref.loc[df_ref["band"]==band, "CTerr"] = CTerr
        df_tar.loc[df_tar["band"]==band, "CT"] = CT
        df_tar.loc[df_tar["band"]==band, "CTerr"] = CTerr
        # Add Z
        for nframe in df_Z["nframe"]:
            df_Z_n = df_Z[df_Z["nframe"]==nframe]
            Z_n = df_Z_n[f"{band}_Z_MC"].values.tolist()[0]
            Zerr_n = df_Z_n[f"{band}_Zerr_MC"].values.tolist()[0]
            df_ref.at[(df_ref["band"]==band) & (df_ref["nframe"]==nframe), "Z"] = Z_n
            df_ref.at[(df_ref["band"]==band) & (df_ref["nframe"]==nframe), "Zerr"] = Zerr_n
            df_tar.at[(df_tar["band"]==band) & (df_tar["nframe"]==nframe), "Z"] = Z_n
            df_tar.at[(df_tar["band"]==band) & (df_tar["nframe"]==nframe), "Zerr"] = Zerr_n

            if N_band==2:
                # Add the same Z to aboid anappropreate removal by Z
                df_ref.at[(df_ref["band"]==band_r) & (df_ref["nframe"]==nframe), "Z"] = Z_n
                df_ref.at[(df_ref["band"]==band_r) & (df_ref["nframe"]==nframe), "Zerr"] = Zerr_n
                df_tar.at[(df_tar["band"]==band_r) & (df_tar["nframe"]==nframe), "Z"] = Z_n
                df_tar.at[(df_tar["band"]==band_r) & (df_tar["nframe"]==nframe), "Zerr"] = Zerr_n

        # Break the process when N_band==2
        if N_band==2:
            break

    # Set y scale
    dy = np.max(dy_list)
    for idx, band in enumerate(bands):
        ax_Z = axes_Z[idx]
        ymin, ymax = ax_Z.get_ylim()
        ymean = np.mean([ymin, ymax])
        ax_Z.set_ylim([ymean+0.5*abs(dy), ymean-0.5*abs(dy)])
        # Break the process when N_band==2
        if N_band==2:
            break

    band_str = "".join(bands)
    out =  f"Z_N{Nframe}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    plt.close()


    print("\n================================================================")
    print("p6 Remove Z=0 data ")
    print("==================================================================")
    nframe_Z0 = set(df_tar[df_tar["Z"]==0]["nframe"].values.tolist())
    if len(nframe_Z0) > 0:
        print(f"  Remove N={len(nframe_Z0)} (Z = 0)")
        df_tar = df_tar[~df_tar["nframe"].isin(nframe_Z0)]
        df_ref = df_ref[~df_ref["nframe"].isin(nframe_Z0)]
    checknan(df_tar)
    checknan(df_ref)

    print(f"  Reference N={len(df_ref)}")
    print(f"  Target    N={len(df_tar)}")


    # Sometimes "RuntimeError("Optimal parameters not found: " + errmsg)"
    # happens
    print("\n================================================================")
    print("p7 Mag residuals of field stars")
    print("==================================================================")
    # Create 
    # axes_res : 
    #     unnormalized residual (mag_cat - mag_est)
    # axes_res_norm : 
    #     normalized residual (mag_cat - mag_est)/sigma_est
    # fig, axes_res, axes_res_norm = myfigure_ncol(n_band=len(bands), n_col=2)
    # xmin_list, xmax_list, ymax_list = [], [], []
    # for idx, band in enumerate(bands):
    #     ax_res = axes_res[idx]
    #     ax_res_norm = axes_res_norm[idx]
    #     ax_res.set_ylabel("N")

    #     # ToDo CTGerr useless?
    #     CT = df_CT.at[0, f"{band}_CT_MC"]
    #     CTerr = df_CT.at[0, f"{band}_CTerr_MC"]
    #     #print(f"    CT = {CT:.4f}+-{CTerr:.4f}")


    #     if idx==(len(bands)-1):
    #         ax_res.set_xlabel("Residuals")
    #         ax_res_norm.set_xlabel("Normalized residuals")

    #     # Calculate mag residuals of field stars
    #     # i.e., g_res, g_reserr etc.
    #     objID = df_ref["objID"].values.tolist()
    #     df_res = calc_resmag(
    #         df_ref, objID, band, bands, Nobj_min, verbose=False)
    #     # Plot unnormalized residuals
    #     plot_res(
    #         df_res, band, bands, ax_res, mag=True)
    #     # Plot normalized residuals
    #     plot_res(
    #         df_res, band, bands, ax_res_norm, norm=True, mag=True)
    #     # Break the process when N_band==2
    #     if N_band==2:
    #         sys.exit()

    #     # To scale xlim
    #     xmin, xmax = ax_res.get_xlim()
    #     xmin_list.append(xmin)
    #     xmax_list.append(xmax)
    #     # To scale ylim
    #     ymin, ymax = ax_res.get_ylim()
    #     ymax_list.append(ymax)

    # # Set x scale
    # xmin_common = np.min(xmin_list)
    # xmax_common = np.max(xmax_list)
    # # Set y scale
    # ymax_common = np.max(ymax_list)
    # for idx, band in enumerate(bands):
    #     ax_res = axes_res[idx]
    #     ax_res.set_xlim([xmin_common, xmax_common])
    #     ax_res.set_ylim([0, ymax_common])

    # band_str = "".join(bands)
    # out =  f"{args.obj}_resmag_N{len(df)}_{filename_tar}.png"
    # out = os.path.join(outdir, out)
    # plt.savefig(out, dpi=200)
    # plt.close()
    #checknan(df)


    print("\n================================================================")
    print("p8 Calculate instrumental magnitude of the target")
    print("==================================================================")
    # Calculate instrumental magnitude
    df_tar["maginst"]    = -2.5*np.log10(df_tar["flux"])
    df_tar["maginsterr"] = 2.5*log10err(df_tar["flux"], df_tar["fluxerr"])
    df_ref["maginst"]    = -2.5*np.log10(df_ref["flux"])
    df_ref["maginsterr"] = 2.5*log10err(df_ref["flux"], df_ref["fluxerr"])


    low_snr = df_tar[df_tar["flux"]/df_tar["fluxerr"] < 1]
    if len(low_snr) > 0:
        print(f"Low SNR N={len(low_snr)}")
        print("Remove by hand")
        df_tar = df_tar[df_tar["flux"]/df_tar["fluxerr"] > 1]
        df_tar = df_tar.reset_index(drop=True)

    
    print("\n================================================================")
    print("p9 Calculate object magnitude of each frame")
    print("==================================================================")
    df_col = pd.read_csv(args.res_col, sep=" ")
    df = calc_objmag2(df_tar, df_col, band, bands)
    checknan(df)



    print("\n================================================================")
    print("p10 Save photometric result csv")
    print("==================================================================")
    print(f"Target name: {target}")
    df["obj"] = target
    print(f"DataFrame: {df}")
    df = df.reset_index(drop=True)

    # Use g-band time by default
    # The --jd_band option is useful when only g-band data are broken 
    # (e.g., time info. loss).
    print(f"Use as t_jd_{args.jd_band} as time standard")
    # Calculate lt_sec_*, lt_day_t with t_utc
    df = add_aspect_data(df, args.loc, suffix=args.jd_band)
    # Rename
    df = df.rename(columns={
        f"lt_sec_{args.jd_band}":"lt_sec", 
        f"lt_day_{args.jd_band}":"lt_day", 
        f"t_jd_{args.jd_band}":"t_jd"
        })
    # Do time correction 
    #    t_jd      -> t_jd_ltcor
    # since df does not have t_sec_obs
    df = time_correction(df)

    # Do distance correction
    ## Obtain mag_red 
    for b in bands:
        df = mag_correction(df, key_mag=f"{b}mag")
        df = df.rename(
            columns={"mag_red":f"{b}_red"}) 

    out =  f"magall_N{Nframe}_{filename_tar}.txt"
    out = os.path.join(outdir, out)
    df.to_csv(out, sep=" ", index=False)


    print("\n================================================================")
    print("p9 Calculate and plot object magnitude of each frame")
    print("==================================================================")
    df_ref = calc_refmag(df_ref, band, bands)
    checknan(df_ref)

    out =  f"refmagall_N{Nframe}_{filename_tar}.txt"
    out = os.path.join(outdir, out)
    df_ref.to_csv(out, sep=" ", index=False)
