#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot derived mag using derive_mag.py
Input files are only *magall*.txt

Note: keywords are not g_ccor, r_ccor, and i_ccor, but gmag, rmag, and imag.

"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from myplot import mycolor, myls, mymark
from calcerror import round_error
from movphot.common import log10err, adderr, band4CTG
from movphot.visualization import myfigure_ncol, myfigure_colcol


def plot_foldlc(df, ax, band, bands, magtype, JD0, rotP, sec=False):
    """
    Plot folded lightcurve.
    """
    key_t = "t_jd_ltcor"
    key_mag = f"{band}mag"
    key_magerr = f"{band}magerr"

    if JD0 is None:
        JD0 = np.min(df[key_t])

    # Sec to Day to fold
    if sec:
        xlabel = f"Rotational phase (P={rotP} s, JD0={JD0})"
        rotP = rotP/24./3600.
    # Hour to Day to fold
    else:
        xlabel = f"Rotational phase (P={rotP} h, JD0={JD0})"
        rotP = rotP/24.

    ax.set_xlabel(xlabel)
    ax.set_ylabel(f"{magtype} magnitude [mag]")
    band_l, band_r = band4CTG(band, bands)
    
    # For color selectino 
    for idx,x in enumerate(bands):
        if x == band:
            break
    
    # Rotational phase
    df["phase"] = (df[key_t]-JD0)/rotP

    Nphase = int(np.ceil(np.max(df["phase"])))
    if Nphase > 100:
        print("  Nphase > 100. Check the input period.")

    # # Weighted mean
    # c_w = 1/df[key_magerr]**2
    # c_wmean = np.average(df[key_mag], weights=c_w)
    # # SD of weighted mean
    # c_wstd = np.sqrt(1/np.sum(c_w))
    # ## Round error
    # c_mean_str, c_std_str = round_error(c_mean, c_std)
    # c_wmean_str, c_wstd_str = round_error(c_wmean, c_wstd)
    # label_c = (
    #     f"N={len(df)}\n{band} "
    #     f"${c_wmean_str}" + r"\pm" + f"{c_wstd_str}$"
    #     )
    # Mean and std
    #x0, x1 = ax.get_xlim()
    #ax.hlines(c_wmean, x0, x1, color=mycolor[1])
    #ax.hlines(c_wmean+c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    #ax.hlines(c_wmean-c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    #ax.set_xlim([x0, x1])
    
    idx_phase = 0
    for nphase in range(Nphase):
        df_temp = df[(df["phase"] > nphase) & (df["phase"] <= nphase+1)]
        if len(df_temp)==0:
            continue
        df_temp["phase"] = df_temp["phase"]%1
   
        # For open circle
        if Nphase < 10:
            label = f"Phase {idx_phase+1}"
        else:
            label = None
        ax.scatter(
            df_temp["phase"], df_temp[key_mag], color=mycolor[idx_phase], s=70, lw=1, marker=mymark[idx_phase], 
            facecolor="None", edgecolor=mycolor[idx_phase], zorder=-1, label=label)

        ax.errorbar(
            df_temp["phase"], df_temp[key_mag], df_temp[key_magerr], 
            color=mycolor[idx_phase], lw=0.5, ls="", ms=3, marker=" ", capsize=0)
        idx_phase += 1




def plot_lc(df, ax, band, bands, magtype, JD0):
    """
    Plot lightcurve.
    """
    band_l, band_r = band4CTG(band, bands)
    
    # For color selectino 
    for idx,x in enumerate(bands):
        if x == band:
            break
    col = mycolor[idx]

    key_t = "t_jd_ltcor"
    key_mag = f"{band}mag"
    key_magerr = f"{band}magerr"

    if JD0 is None:
        JD0 = np.min(df[key_t])


    ax.set_xlabel(f"JD-{JD0} [day]")
    ax.set_ylabel(f"{magtype} magnitude [mag]")

    # Remove mag == 0
    #df_temp = df[df[key_mag]!=0]
    df_temp = df


    # Time window
    deltaT = np.max(df_temp[key_t]) - np.min(df_temp[key_t])
    #deltaT = np.max(df[f"t_jd_{band}"]) - np.min(df[f"t_jd_{band}"])
    #print(f"  {idx} : Delta T = {deltaT:.2f}")

    # Mean and std
    column = df_temp.columns.tolist()
    c_mean = np.mean(df_temp[key_mag])
    # Photometric uncertainty
    c_std_phot = adderr(df_temp[key_magerr])/len(df_temp)
    # Standard Deviation
    c_SD = np.std(df_temp[key_mag])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df_temp))
    # Total error 
    c_std = np.sqrt(c_std_phot**2 + c_SE**2)

    # Weighted mean
    c_w = 1/df_temp[key_magerr]**2
    c_wmean = np.average(df_temp[key_mag], weights=c_w)
    # Check !!!!!!
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(c_w))

    ## Round error
    c_mean_str, c_std_str = round_error(c_mean, c_std)
    c_wmean_str, c_wstd_str = round_error(c_wmean, c_wstd)
    label_c = (
        f"N={len(df_temp)}\n{band} "
        f"${c_wmean_str}" + r"\pm" + f"{c_wstd_str}$"
        )
   
    # For open circle
    ax.scatter(
      df_temp[key_t]-JD0, df_temp[key_mag], color=col, s=70, lw=1, 
      marker="o", facecolor="None", edgecolor=col, zorder=-1, label=label_c)

    ax.errorbar(
      df_temp[key_t]-JD0, df_temp[key_mag], df_temp[key_magerr], 
      color=col, lw=0.5, ls="", ms=3, marker=" ", capsize=0)

    # Mean and std
    x0, x1 = ax.get_xlim()
    ax.hlines(c_wmean, x0, x1, color=mycolor[1])
    ax.hlines(c_wmean+c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    ax.hlines(c_wmean-c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    ax.set_xlim([x0, x1])

if __name__ == "__main__":
    parser = ap(description="Plot derived magnitudes.")
    parser.add_argument(
        "res", type=str, help="*magall*.txt")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input result")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--JD0", type=float, default=None,
        help="time zero point in Juliand day")
    parser.add_argument(
        "--outdir", type=str, default="finalplot",
        help="Output directory")
    parser.add_argument(
        "--rotP", type=float, default=3.603957,
        help="Rotational period in hour")
    parser.add_argument(
        "--sec", action="store_true", default=False,
        help="Whether input period in sec")
    parser.add_argument(
        "--key_time", type=str, default="t_jd_ltcor",
        help="Time keyword to color plot")
    args = parser.parse_args()

    # Set output directory
    outdir = args.outdir
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    # Rotational period in hour
    if args.sec:
        rotP_s         = args.rotP
        rotP_h         = rotP_s/3600.
    else:
        rotP_s         = None
        rotP_h         = args.rotP

    if args.rotP:
        if args.sec:
            rotP_list = str(rotP_s).split(".")
            str_P = "f".join(rotP_list)
            rotP_str = f"_rotP{str_P}s"
        else:
            rotP_list = str(rotP_h).split(".")
            str_P = "f".join(rotP_list)
            rotP_str = f"_rotP{str_P}h"
            assert args.rotP < 100, "Rotational period in hour is needed."
    else:
        rotP_str = ""
    

    res_str = args.res.split("/")[-1].split(".")[0]

    # Do not remove objects with extreme colors
    bands         = args.bands
    magtype        = args.magtype
    #yr_res         = args.yr_res

    # Number of bands
    N_band = len(bands)
    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]
    JD0 = args.JD0
    


    print("\n================================================================")
    print("p1 Read magall result")
    print("==================================================================")
    df = pd.read_csv(args.res, sep=" ")
    print(f"  N_df = {len(df)} (original)")


    print("\n================================================================")
    print("p2 Plot lightcurve")
    print("==================================================================")
    # axes_time : JD             vs. mag
    # axes_fold : Rotation Phase vs. mag
    fig, axes_time, axes_fold = myfigure_ncol(n_band=len(bands), n_col=2)

    for idx, band in enumerate(bands):
        ax_time = axes_time[idx]
        ax_fold = axes_fold[idx]

        plot_lc(df, ax_time, band, bands, magtype, JD0)
        plot_foldlc(df, ax_fold, band, bands, magtype, JD0, args.rotP, args.sec)
        
        # Binning
        ax_time.grid(which="major", axis="both")
        ax_fold.grid(which="major", axis="both")
        ax_time.legend(loc="upper center")
        ax_fold.legend(loc="upper center")

        ax_time.invert_yaxis()
        ax_fold.invert_yaxis()


    out =  f"maglc_N{len(df)}_{res_str}{rotP_str}.png"
    out = os.path.join(args.outdir, out)
    fig.savefig(out)
    plt.close()



