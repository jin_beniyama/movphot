#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot derived color using derive_color.py
Input files are only *colall*.csv
"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from myplot import mycolor, myls
from calcerror import round_error
from movphot.common import log10err, adderr, checknan
from movphot.visualization import myfigure_ncol, myfigure_colcol_lc, plot_colcol
from movphot.prepro_color import plot_colorlc, plot_foldcolorlc


if __name__ == "__main__":
    parser = ap(description="Plot derived colors.")
    parser.add_argument(
        "res", type=str, help="*colall*.csv")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--ref", action="store_true", 
        help="Show catalog manitude of reference star")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--JD0", default=2459515.5, type=float,
        help="time zero point in Juliand day")
    parser.add_argument(
        "--outdir", type=str, default="finalplot",
        help="Output directory")
    parser.add_argument(
        "--rotP", type=float,  default=None,
        help="Rotational period in hour")
    parser.add_argument(
        "--sec", action="store_true", default=False,
        help="Whether input period in sec")
    parser.add_argument(
        "--large", action="store_true", default=False,
        help="Use large figure")
    parser.add_argument(
        "--tr", type=float, nargs="*",
        help="time range in sec for different color plot")
    parser.add_argument(
        "--key_time", type=str, default="t_jd_ltcor",
        help="Time keyword to color plot")
    args = parser.parse_args()


    # Set output directory
    outdir = args.outdir
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    res_str = args.res.split("/")[-1].split(".")[0]

    # Do not remove objects with extreme colors
    bands         = args.bands
    magtype        = args.magtype
    #yr_res         = args.yr_res

    # Color by time
    key_time = args.key_time

    # Number of bands
    N_band = len(bands)
    if N_band == 2:
        n_band_plot = N_band -1
    else:
        n_band_plot = N_band

    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]
    JD0 = args.JD0

    # Rotational period in hour
    if args.sec:
        rotP_s         = args.rotP
        rotP_h         = rotP_s/3600.
    else:
        rotP_s         = None
        rotP_h         = args.rotP

    if args.rotP:
        if args.sec:
            rotP_list = str(rotP_s).split(".")
            str_P = "f".join(rotP_list)
            rotP_str = f"_rotP{str_P}s"
        else:
            rotP_list = str(rotP_h).split(".")
            str_P = "f".join(rotP_list)
            rotP_str = f"_rotP{str_P}h"
    else:
        rotP_str = ""
    
    # Check time range (tr)
    if args.tr is not None:
        N_tr = int(len(args.tr)/2)
        assert len(args.tr)%2==0, "Invalid tr!"
        # Color for points in tr 
        coltr = ["black", "gray", "brown"]
        # Marker for points in tr 
        marktr = ["^", "D", "v"]
        assert len(args.tr)/2 <= len(coltr), "More coltr are needed!"




    print("\n================================================================")
    print("p1 Read colall csv")
    print("==================================================================")
    df = pd.read_csv(args.res, sep=" ")
    print(f"  N_df = {len(df)} (original)")
    print("Columns")
    print(df.columns.tolist())
    checknan(df)

    # Create title like "1111111111 g=10.0+-0.1, r=10.0+-0.1, i=10.1+-0.1"
    if args.ref:
        objID = df["obj"].values.tolist()[0]
        # For PS g,r,i
        g    = df["gMeanPSFMag"].values.tolist()[0]
        gerr = df["gMeanPSFMagErr"].values.tolist()[0]
        r    = df["rMeanPSFMag"].values.tolist()[0]
        rerr = df["rMeanPSFMagErr"].values.tolist()[0]
        i    = df["iMeanPSFMag"].values.tolist()[0]
        ierr = df["iMeanPSFMagErr"].values.tolist()[0]
        
        g_r    = g - r
        g_rerr = adderr(gerr, rerr)
        r_i    = r - i
        r_ierr = adderr(rerr, ierr)

        # Round values
        g_r, g_rerr = round_error(g_r, g_rerr)
        r_i, r_ierr = round_error(r_i, r_ierr)

        # Round values
        g, gerr = round_error(g, gerr)
        r, rerr = round_error(r, rerr)
        i, ierr = round_error(i, ierr)

        title = (
            f"{objID}\ng-r={g_r}$\pm${g_rerr}, r-i={r_i}$\pm${r_ierr}\n"
            f"(g,r,i = {g}$\pm${gerr},{r}$\pm${rerr},{i}$\pm${ierr})")
    else:
        title = None
        


    # Raw (Not fold) ==========================================================
    # axes_time : JD            vs. Color
    # axes_cc : [ax_cc, ax_r, ax_u, ax_cbar]
    fig, axes_time, axes_cc = myfigure_colcol_lc(n_band=n_band_plot, cbar=True)
    
    # Color lc (left)
    for idx, band in enumerate(bands):
        ax_time = axes_time[idx]
        
        plot_colorlc(df, ax_time, key_time, band, bands, magtype, JD0)
        ax_time.set_xlabel(f"JD-{JD0} [day]")
        ax_time.set_ylabel(f"{magtype} color [mag]")

        ax_time.grid(which="major", axis="both")
        ax_time.legend(loc="upper center")


    if N_band == 3:
        # Color-color diagram (right)
        ax, ax_r, ax_u, ax_cbar = axes_cc

        plot_colcol(
            df, fig, ax, ax_r, ax_u, bands[0], bands[1], bands[2], 
            magtype, JD0, rotP_h, col=mycolor[0], marker="o", 
            key_time=key_time, ax_cbar=ax_cbar)

        if args.tr is not None:
            # Plot points in tr
            for n in range(N_tr):
                tmin, tmax = args.tr[2*n], args.tr[2*n+1]
                df_temp = df[(df[f"t_sec_obs_{band}"] < tmax) & (df[f"t_sec_obs_{band}"] > tmin)]
                print(f"Use {tmin}--{tmax} N={len(df_temp)}")
                # Overplot
                plot_colcol(
                    df_temp, ax, ax_r, ax_u, bands[0], bands[1], bands[2], 
                    magtype, JD0, rotP_h, col=coltr[n], marker=marktr[n], mean=False)

        ax.grid(which="major", axis="both")
        ax.legend(loc="upper center")
        ax_u.set_title(title)

    out =  f"colorplot_N{len(df)}_{res_str}.png"
    out = os.path.join(args.outdir, out)
    fig.savefig(out)
    plt.close()
    # Raw (Not fold) ==========================================================

    
    if args.rotP:
        # Fold ====================================================================
        # axes_fold : rotation phase vs. Color
        # axes_cc : [ax_cc, ax_r, ax_u, ax_cbar]
        fig, axes_fold, axes_cc = myfigure_colcol_lc(n_band=n_band_plot, cbar=True)

        # Folded color lc (left)
        for idx, band in enumerate(bands):
            ax_fold = axes_fold[idx]
            
            plot_foldcolorlc(df, ax_fold, key_time, band, bands, magtype, JD0, rotP_h)
            ax_fold.set_xlabel(f"Rotational phase (P={rotP_h:.4f} h, JD0={JD0})")
            ax_fold.set_ylabel(f"{magtype} color [mag]")
            
            ax_fold.grid(which="major", axis="both")
            ax_fold.legend(loc="upper center")

        if N_band == 3:
            # Color-color diagram (right)
            ax, ax_r, ax_u, ax_cbar = axes_cc

            plot_colcol(
                df, fig, ax, ax_r, ax_u, bands[0], bands[1], bands[2], 
                magtype, JD0, rotP_h, col=mycolor[0], marker="o", 
                key_time=key_time, ax_cbar=ax_cbar)

            if args.tr is not None:
                # Plot points in tr
                for n in range(N_tr):
                    tmin, tmax = args.tr[2*n], args.tr[2*n+1]
                    df_temp = df[(df[f"t_sec_obs_{band}"] < tmax) & (df[f"t_sec_obs_{band}"] > tmin)]
                    print(f"Use {tmin}--{tmax} N={len(df_temp)}")
                    # Overplot
                    plot_colcol(
                        df_temp, ax, ax_r, ax_u, bands[0], bands[1], bands[2], 
                        magtype, JD0, rotP_h, col=coltr[n], marker=marktr[n], mean=False)

            ax.grid(which="major", axis="both")
            ax.legend(loc="upper center")
        ax_u.set_title(title)
        out =  f"colorplot_fold_N{len(df)}_{res_str}.png"
        out = os.path.join(args.outdir, out)
        fig.savefig(out)
        plt.close()
