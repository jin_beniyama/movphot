#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot mag of reference stars derived using derive_mag.py
Input files are only refmagall*.csv

Note: keywords are not g_ccor, r_ccor, and i_ccor, but gmag, rmag, and imag.

"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from myplot import mycolor, myls
from calcerror import round_error, adderr_series
from movphot.common import log10err, adderr, band4CTG
from movphot.visualization import myfigure_ncol, myfigure_colcol, myfigure4ref, colmark




if __name__ == "__main__":
    parser = ap(description="Plot derived magnitudes.")
    parser.add_argument(
        "csv", type=str, help="*magall*.csv")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--JD0", default=2459515.5, type=float,
        help="time zero point in Juliand day")
    parser.add_argument(
        "--outdir", type=str, default="finalplot",
        help="Output directory")
    parser.add_argument(
        "--rotP", type=float, default=3.603957,
        help="Rotational period in hour")
    parser.add_argument(
        "--sec", action="store_true", default=False,
        help="Whether input period in sec")
    parser.add_argument(
        "--key_time", type=str, default="t_jd_ltcor",
        help="Time keyword to color plot")
    args = parser.parse_args()

    # Set output directory
    outdir = args.outdir
    if not os.path.isdir(outdir):
        os.makedirs(outdir)


    csv_str = args.csv.split("/")[-1].split(".")[0]

    # Do not remove objects with extreme colors
    bands         = args.bands
    magtype        = args.magtype
    #yr_res         = args.yr_res

    # Number of bands
    N_band = len(bands)
    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]
    JD0 = args.JD0
    


    print("\n================================================================")
    print("p1 Read refmagall csv")
    print("==================================================================")
    df = pd.read_csv(args.csv, sep=" ")
    objID = list(set(df["objID"].values.tolist()))
    print(f"  N_df = {len(df)} (original)")


    print("\n================================================================")
    print("p2 Plot lightcurve of reference stars")
    print("==================================================================")
    fig, axes_lc, axes_comp, axes_res, axes_resnorm = myfigure4ref(n_band=len(bands))
   
    for idx, band in enumerate(bands):
        ax_lc = axes_lc[idx]
        
        key_t = f"t_jd_{band}"
        key_mag = f"{band}mag"
        key_magerr = f"{band}magerr"

        # Plot lc
        ax_lc.set_xlabel(f"JD-{JD0} [day]", fontsize=15)
        ax_lc.set_ylabel(f"{magtype} magnitude [mag]", fontsize=15)

        for idx_obj, obj in enumerate(objID):
            col, mark = colmark(idx_obj)
            df_temp = df[df["objID"] == obj]
            # For open circle
            ax_lc.scatter(
                df_temp[key_t]-JD0, df_temp[key_mag], color=col, s=70, lw=1, 
                marker=mark, facecolor="None", edgecolor=col, zorder=-1)

            ax_lc.errorbar(
                df_temp[key_t]-JD0, df_temp[key_mag], df_temp[key_magerr], 
                color=col, lw=0.5, ls="", ms=3, marker=" ", capsize=0)

        ax_lc.grid(which="major", axis="both")
        ax_lc.legend(fontsize=10)
        ax_lc.invert_yaxis()


        # Plot Cat mag vs. Inst mag
        ax_comp = axes_comp[idx]
        for idx_obj, obj in enumerate(objID):
            col, mark = colmark(idx_obj)
            df_temp = df[df["objID"] == obj]
            # For open circle
            ax_comp.scatter(
                df_temp[f"{band}MeanPSFMag"], df_temp[f"{band}mag"], 
                color=col, marker=mark, s=70, lw=1, facecolor="None")
            ax_comp.errorbar(
                df_temp[f"{band}MeanPSFMag"], df_temp[f"{band}mag"], 
                xerr=df_temp[f"{band}MeanPSFMagErr"], yerr=df_temp[f"{band}magerr"],
                color=col, lw=0.5, ls="", ms=3, marker=" ", capsize=0)
        xmin, xmax = ax_comp.get_xlim()
        ymin, ymax = ax_comp.get_ylim()
        minval = np.min([xmin, ymin])
        maxval = np.max([xmax, ymax])
        x = np.arange(minval, maxval, 0.01)
        y = x
        ax_comp.plot(
            x, y, label="y=x", color="black", ls="dashed", zorder=-1)
        ax_comp.set_xlim([maxval, minval])
        ax_comp.set_ylim([maxval, minval])
        ax_comp.set_xlabel("Catalog magnitude [mag]", fontsize=15)
        ax_comp.set_ylabel("Observed magnitude [mag]", fontsize=15)
        ax_comp.grid(which="major", axis="both")
        ax_comp.legend(fontsize=10)
 
        # Calculate residual
        res = df[f"{band}MeanPSFMag"] - df[f"{band}mag"]
        reserr = adderr_series(df[f"{band}MeanPSFMagErr"], df[f"{band}magerr"])
        resnorm = res/reserr
        # In absolute
        res_mean = abs(np.mean(res))
        res_median = abs(np.median(res))
        res_min  = abs(np.min(res))
        res_max  = abs(np.max(res))
        res_rms  = np.sqrt(np.mean(res**2))
        print(f"Residual |mean|   = {res_mean}")
        print(f"         |median| = {res_median}")
        print(f"         |min|    = {res_min}")
        print(f"         |max|    = {res_max}")
        print(f"         rms      = {res_rms}")
        label_res = (
            f"(|mean|, |median|, |min|, |max|, rms)\n= ("
            f"({res_mean:.3f}, {res_median:.3f}, {res_min:.3f}, "
            f"{res_max:.3f}, {res_rms:.3f})"
            )

        # Plot residual
        ax_res = axes_res[idx]
        ax_res.hist(
            res, orientation="vertical", histtype="step", label=label_res)
        ax_res.grid(which="major", axis="both")
        ax_res.legend(fontsize=10)
        ax_res.set_xlabel("Residual [mag]", fontsize=15)
        ax_res.set_ylabel(f"N ({len(res)} in total)", fontsize=15)
        xmin, xmax = ax_res.get_xlim()
        valmax = np.max([xmin, xmax])
        ax_res.set_xlim([-valmax, valmax])

        # In absolute
        resnorm_mean = abs(np.mean(resnorm))
        resnorm_median = abs(np.median(resnorm))
        resnorm_min  = abs(np.min(resnorm))
        resnorm_max  = abs(np.max(resnorm))
        resnorm_rms  = np.sqrt(np.mean(resnorm**2))
        print(f"Normalized Residual |mean|   = {resnorm_mean}")
        print(f"                    |median| = {resnorm_median}")
        print(f"                    |min|    = {resnorm_min}")
        print(f"                    |max|    = {resnorm_max}")
        print(f"                    rms      = {resnorm_rms}")
        label_resnorm = (
            f"(|mean|, |median|, |min|, |max|, rms)\n= ("
            f"({resnorm_mean:.3f}, {resnorm_median:.3f}, {resnorm_min:.3f}, "
            f"{resnorm_max:.3f}, {resnorm_rms:.3f})"
            )

        # Plot normalized residual
        ax_resnorm = axes_resnorm[idx]
        ax_resnorm.hist(
            res/reserr, orientation="vertical", histtype="step", label=label_resnorm)
        ax_resnorm.grid(which="major", axis="both")
        ax_resnorm.legend(fontsize=10)
        ax_resnorm.set_xlabel("Normalized residual [mag]", fontsize=15)
        ax_resnorm.set_ylabel(f"N ({len(res)} in total)", fontsize=15)
        xmin, xmax = ax_resnorm.get_xlim()
        valmax = np.max([xmin, xmax])
        ax_resnorm.set_xlim([-valmax, valmax])

    out =  f"refmaglc_N{len(df)}_{csv_str}.png"
    out = os.path.join(args.outdir, out)
    fig.savefig(out)
    plt.close()

