#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Obtain a snapshot of a selected field with stars in Pan-STARRS catalog.
"""
from argparse import ArgumentParser as ap
import os
import time
from astroquery.mast import Catalogs
from astropy.coordinates import SkyCoord
from astropy import units as u
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from movphot.visualization import (mycolor, mymark)

# Avoid SSL error
os.environ['CURL_CA_BUNDLE'] = ''
verify = True
if verify is True or verify is None:
  verify = (os.environ.get('REQUESTS_CA_BUNDLE') or
    os.environ.get('CURL_CA_BUNDLE'))


def hmsdms2deg(ra, dec):
    """Convert hmsdms to degree

    Parameters
    ----------
    ra : str
      right acsension in hms
    dec : str
      declination in dms
    
    Returns
    --------
    ra : str
      right acsension in degree
    dec : str
      declination in degree
    """
    radec = f"{ra} {dec}"
    c = SkyCoord(radec, unit=(u.hourangle, u.degree))
    radec = c.to_string("decimal")
    ra = radec.split(" ")[0]
    dec = radec.split(" ")[1]
    return ra, dec


def query_ps(ra, dec, radius, magmin, magmax):
  """
  Query Pan-STARRS catalog and output as pandas.DataFrame.
  Extract bright and high quality objects.

  Parameters
  ----------
  ra, dec : float
    right ascension, declination of field in degree
  radius : int
    radius of filed of view in degree
  magmin, magmax : float
    the brightest/faintest magnitude to be considered (g-band)

  Return
  ------
  df : pandas.DataFrame
    result DataFrame
  """

  radec = SkyCoord(ra=ra, dec=dec, unit=(u.degree, u.degree))
  columns= ["objID", "objinfoFlag", "qualityFlag", 
           "raMean", "decMean", "raMeanErr", "decMeanErr",
           "nStackDetections", "nDetections",
           "gQfPerfect", "gMeanPSFMag", "gMeanPSFMagErr", "gFlags",
           "rQfPerfect", "rMeanPSFMag", "rMeanPSFMagErr", "rFlags",
           "iQfPerfect", "iMeanPSFMag", "iMeanPSFMagErr", "iFlags",
           "zQfPerfect", "zMeanPSFMag", "zMeanPSFMagErr", "zFlags",
           "yQfPerfect", "yMeanPSFMag", "yMeanPSFMagErr", "yFlags"]

  # Criteria for error and quality
  magerr_min = 1e-10
  magerr_min = 0
  magerr_max = 0.1
  magerr_max = 0.3
  magQ_min = 0.85
  magQ_min = 0.1

  dr = "dr2"
  tabletype = "mean"
  t0 = time.time()
  res = Catalogs.query_criteria(
    coordinates=radec, radius=radius, catalog="PANSTARRS", 
    gMeanPSFMag=[("lte", magmax),("gte", magmin)], 
    rMeanPSFMag=[("lte", magmax),("gte", magmin)],
    iMeanPSFMag=[("lte", magmax),("gte", magmin)],
    gMeanPSFMagErr=[("lte", magerr_max),("gte", magerr_min)],
    rMeanPSFMagErr=[("lte", magerr_max),("gte", magerr_min)],
    iMeanPSFMagErr=[("lte", magerr_max),("gte", magerr_min)],
    gQfPerfect=[("gte", magQ_min)],
    grfPerfect=[("gte", magQ_min)],
    gifPerfect=[("gte", magQ_min)],
    table=tabletype, data_release=dr, columns=columns,
    timeout=6000) 
  t1 = time.time()
  print(f"query time : {t1-t0}s")

  # Avoid int64 error for objID
  ID_str = [str(res[i]["objID"]) for i in range(len(res))]
  res["objID"] = ID_str

  df = res.to_pandas()
  return df


def scalebar(ax, arcmin):
  """
  Plot a scale bar.
  """
  ramin, ramax = ax.get_xlim()
  decmin, decmax = ax.get_ylim()
  rawidth = ramax - ramin 
  decwidth = decmax - decmin

  dec0 = decmin + decwidth*0.05
  dec1 = dec0 + (1/60.)*arcmin
  dec = (dec0+dec1)/2.0

  ra0 = ramin + rawidth*0.05
  ra1 = ra0 + 1/60.*arcmin/np.cos(np.radians(dec))
  ra = (ra0+ra1)/2.0
  ax.hlines(dec, ra0, ra1, color="white")
  ax.vlines(ra, dec0, dec1, color="white")
  ax.text(ra, dec, f"{arcmin} arcmin", color="white", fontsize=10)


def gr_to_V(df):
  """
  Convert gr-band (not reduced) magnitude 
  to V-band magnitude based on Tonry+2012.

  Parameter
  ---------
  df : pandas.DataFrame
    including g,r-band magnitude ("g", "r")

  Return 
  ------
  df : pandas.DataFrame
    including V-band magnitude
  """
  
  df["V"] = df["rMeanPSFMag"] - 0.006 + 0.474*(df["gMeanPSFMag"]-df["rMeanPSFMag"])

  return df


if __name__=="__main__":
    parser = ap(
        description="Obtain a snaphot of stars in the Pan-STARRS catalog",
        prefix_chars="@")
    parser.add_argument(
        "ra", default=120, 
        help="center of right ascention in degree")
    parser.add_argument(
        "dec", default=0, 
        help="center of declination in degree")
    parser.add_argument(
        "radius", default=0.05, type=float, 
        help="object search radius in degree")
    parser.add_argument(
        "magmin", default=14., type=float, 
        help="minimum magnitude")
    parser.add_argument(
        "magmax", default=19., type=float, 
        help="maximum magnitude")
    parser.add_argument(
        "obj", type=str, nargs="*", default=[], 
        help="objects")
    parser.add_argument(
        "ra_obj", nargs="*", default=[], 
        help="ra of objects")
    parser.add_argument(
        "--dec_obj", nargs="*", default=[], 
        help="dec of objects")
    args = parser.parse_args()


    ra, dec = args.ra, args.dec
    if len(ra.split(" ")) > 1:
      ra, dec = hmsdms2deg(ra, dec)
    ra, dec = float(ra), float(dec)
    radius = args.radius
    ramin, ramax = ra - radius, ra + radius
    decmin, decmax = dec - radius, dec + radius
    rawidth = ramax - ramin
    decwidth = decmax - dec


    # Obtain stars in PS catalog
    df = query_ps(
      ra, dec, radius, args.magmin, args.magmax)
    df = gr_to_V(df)

    df_12_13 = df[(df["gMeanPSFMag"] < 13) & (df["gMeanPSFMag"] > 12)]
    df_13_14 = df[(df["gMeanPSFMag"] < 14) & (df["gMeanPSFMag"] > 13)]
    df_14_15 = df[(df["gMeanPSFMag"] < 15) & (df["gMeanPSFMag"] > 14)]
    df_15_16 = df[(df["gMeanPSFMag"] < 16) & (df["gMeanPSFMag"] > 15)]
    df_16_17 = df[(df["gMeanPSFMag"] < 17) & (df["gMeanPSFMag"] > 16)]
    df_17 = df[df["gMeanPSFMag"] > 17]
    

    # Plot stars in box (radius x radius)
    fig = plt.figure(figsize=(10,8))
    ax = fig.add_axes([0.15, 0.15, 0.6, 0.8])
    ax.set_facecolor("gray")
    ax.set_xlabel("Right Ascention [deg]")
    ax.set_ylabel("Declination [deg]")
    #ax.get_xaxis().get_major_formatter().set_useOffset(False)
    #ax.get_yaxis().get_major_formatter().set_useOffset(False)
    ax.scatter(
      df_12_13["raMean"], df_12_13["decMean"], marker=mymark[0], 
      label="PS 12 < g < 13")
    ax.scatter(
      df_13_14["raMean"], df_13_14["decMean"], marker=mymark[1],
      label="PS 13 < g < 14")
    ax.scatter(
      df_14_15["raMean"], df_14_15["decMean"], marker=mymark[2],
      label="PS 14 < g < 15")
    ax.scatter(
      df_15_16["raMean"], df_15_16["decMean"], marker=mymark[3],
      label="PS 15 < g < 16")
    ax.scatter(
      df_16_17["raMean"], df_16_17["decMean"], marker=mymark[4],
      label="PS 16 < g < 17")
    print(df)
    # ax.scatter(
    #   df_17["raMean"], df_17["decMean"], marker=mymark[5],
    #   label="PS g > 17")

    # Add mag and magerr
    for idx,row in df.iterrows():
      ra_temp = row["raMean"]
      dec_temp = row["decMean"]
      g = row["gMeanPSFMag"]
      gerr = row["gMeanPSFMagErr"]
      V = row["V"]
      ax.text(
        ra_temp, dec_temp, f"PS_g {g:.3f}+-{gerr:.3f}\nV {V:.3f}", 
        color="black", fontsize=5)
      
     
    ra_str = str(ra).replace(".", "f")
    dec_str = str(dec).replace(".", "f")
    radius_str = str(radius).replace(".", "f")
    obj_str = ""

    # for idx,obj in enumerate(args.obj):
    #    # hms to deg
    #    ra = args.ra_obj[idx]
    #    dec = args.dec_obj[idx]
    #    if len(ra.split(" ")) > 1:
    #       ra, dec = hmsdms2deg(ra, dec)
    #    ra, dec = float(ra), float(dec)
    #    ax.scatter(
    #       ra, dec,
    #       marker="o", facecolor="", edgecolor=mycolor[idx], s=100,
    #       label=f"{obj}")

    print(f"ra : {ramin}--{ramax}")
    print(f"dec : {decmin}--{decmax}")
    ax.set_ylim([decmin, decmax])
    #ax.set_xlim([ramax, ramin])
    ax.set_xlim([ramin, ramax])
    #ax.invert_xaxis()
    
    # Plot 1 arcmin, 10 arcmin
    scalebar(ax, 1)
    scalebar(ax, 10)

    ax.set_xlim([ramax, ramin])
    obj_str = "_".join(args.obj)
    ax.legend(bbox_to_anchor=(1.01, 0.6), borderaxespad=0)
    out = f"PSmap_ra{ra_str}dec{dec_str}rad{radius_str}{obj_str}.png"
    plt.savefig(out, dpi=200)
