#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Check photometry region in the same condition with phot_color.py
"""
import time
import os
import logging
import pandas as pd
import numpy as np
import sep
# Change the limit (unnecessary?)
sep.set_extract_pixstack(1000000)
from argparse import ArgumentParser as ap
from argparse import ArgumentDefaultsHelpFormatter
from astropy.wcs import FITSFixedWarning
from astropy.wcs import WCS as wcs
import astropy.io.fits as fits
import warnings
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use("Agg")
# to ignore 
## WARNING: FITSFixedWarning: MJD-END = '59281.674688' / MJD at exposure end
## a floating-point value was expected. [astropy.wcs.wcs]
warnings.simplefilter('ignore', category=FITSFixedWarning)
from astroscrappy import detect_cosmics


import movphot as mp
from movphot.common import (
  get_filename, obtain_objorb, modify_ref, concat_allband, extract_obj, nowstr,
  remove_xy, check_cntm, self_cntm, calc_FWHM, extract_identical, dir_from_arg)
from movphot.photfunc import obtain_winpos, remove_background2d
from movphot.visualization import plot_photregion_ref , plot_fwhm
from handle_arr import moving_average


def main(args=None):
    """This is the main function called by the `phot_color` script.

    Parameters
    ----------
    args : argparse.Namespace
        Arguments passed from the command-line as defined below.
    """

    # Start time
    t0 = time.time()
    
    # Do not confuse
    rad_ref = args.rad_ref

    # Parse arguments ===========================================================
    # Since first target is ref stars, not object.
    param_app = dict(
        radius=rad_ref)

    dbdir = args.dbdir
    fitsdir = args.fitsdir
    band = args.band

    # Movphot Class
    rad_mask = rad_ref
    ph = mp.Movphot(
        args.inst, args.obj, args.refmagmin, args.refmagmax,  
        rad_mask, param_app)

    # initial positions `x` and `y` are used for photometry
    if args.nowinpos:
        winpos = False
        key_x, key_y = "x", "y"
    # winpowed positions `xwin` and `ywin` are used for photometry
    else:
        winpos = True
        key_x, key_y = "xwin", "ywin"
   
        
    ph.set_catalog(args.catalog, dbdir)

    # Obtain FWHM list of the 1st band ====================================                    # Note: 
    #   rad_ref (default=20, typical value for TriCCS) is used as
    #   typical FWHM to calculate FWHM of the day
    df_fwhm = calc_FWHM([args.fits], fitsdir, rad_ref)
     # Plot
    out = "temp_fwhm.png"
    # Plot with smoothing
    df_fwhm = plot_fwhm(
      df_fwhm, "median", n_smooth=5, p_scale=ph.p_scale, out=out)
    # output fwhm
    print(df_fwhm)
    # Obtain FWHM list of the 1st band ====================================


    # Set photometry radius for ref and sep
    if args.autorad:
        # Photometry radius is set to autoradfac * fwhm
        ph.radius =  args.autoradfac*df_fwhm.loc[0, "fwhm"]
    else:
        ph.radius =  rad_ref
     
    infits = os.path.join(fitsdir, args.fits)
    # fits source  (HDU0, header + image data)
    src = fits.open(infits)[0]
    # fits header
    hdr = src.header
    # Obtain time and coordinates
    ph.obtain_time_from_hdr(hdr)
    # wcs information
    w = wcs(header=hdr)
    # fits image data
    image = src.data.byteswap().newbyteorder()

    # Detect and remove cosmicrays with L.A.Cosmic
    # ToDo: gain
    cleantype = "maskmed"
    verbose = False
    rays_map, image = detect_cosmics(
        image, satlevel=ph.satu_count, verbose=verbose)
    N_cr = np.count_nonzero(rays_map)
    ny, nx = image.shape
    N_total = nx*ny
    src.data = image
    cr_percent = N_cr/N_total*100
    print(f"  Cosmic ray detection")
    print(f"  N_cr/N_total = {N_cr}/{N_total} <-> {cr_percent:.3f}%")


    # Remove background using the mask (without mask is also ok) ==
    print("  Based on args.refphot (not args.objphot)")
    print(f"    Median before bg sub : {np.median(src.data)}")
    if args.refphot=="app":
        src.data = src.data.byteswap().newbyteorder()
        ph.remove_background(src, None)
        print(f"    Remove background")
        print(f"    Median after bg sub : {np.median(src.data)}")

    if args.refphot=="ann":
        ph.calc_background(src, None)
        print(f"    Do not Remove background")

    src.data = src.data.byteswap().newbyteorder()


    print(f"    ph.bg_rms: {ph.bg_rms}")
    # Remove background using the mask (without mask is also ok) ==

    
    # All reference stars =========================================
    df_ref = ph.phot_catalog(
        src, args.refphot, band, winpos)
    # Remove stars out of field of view
    df_ref = remove_xy(df_ref, nx, ny)
    df_refall =df_ref.copy()

    if len(df_ref)==0:
      print(f"  No reference stars were detected. Skip {idx_fits}.")
      assert False, 1
    # All reference stars =========================================

    # Extract bright stars ========================================
    # Create steller masks with Sep
    # Ignore faint objects
    sigma_bright = 10
    minarea_bright = 10
    print(f"    median  {np.median(src.data)}")
    src.data = src.data.byteswap().newbyteorder()
    objects = sep.extract(
        src.data, sigma_bright, 
        #image, sigma_bright, 
        err=ph.bg_rms, minarea=minarea_bright)
    
    # Aperture photmetry 
    # Note: keywords are x, y, xwin, and ywin
    df_sep = ph.phot_loc(
        src, args.refphot, objects["x"], objects["y"], winpos)
    # Extract bright stars ========================================

    # Note
    # len(df_ref) is always > 0, but len(df_sep) sometimes == 0



    # Extract identical objects not in df_base
    #   1. not in catalog and 2. not target
    ## Distance threshold
    r_th = 3
    # empty DataFrame returned if there is no identical objects.
    df_sep_identical = extract_identical(
      df_ref, df_sep, r_th, key_x, key_y)

    # Avoid ZeroDivisionError.
    if len(df_sep) > 0:
      print(
        f"    Identical bright objects "
        f"{len(df_sep_identical)}/{len(df_sep)}")
    else:
      print(
        f"    No sep detections are detected.")

    # ToDo:
    # Consider photometry area of each object.
    # elongated obj and circle ref stars. etc...

    # Calculate contamination fraction `frac_cntm`
    # Objects in aperture radius (app) or aperture radius + 4 pix
    # are judged as contaminations
    if args.refphot=="app":
        # [Ciecle Photometry]
        #  By default < 2 times photometry radius
        cntmrad = args.cntmradfac*ph.radius
        # Use twice radius for sep detection not in catalog 
        # bright star 
        cntmrad_sep = 1.5 * cntmrad
    if args.refphot=="ann":
        # [Annulus Photometry]
        # ToDo:
        # Change constant for various instruments
        #  < 2 times photometry radius + 2 (gap) + 2 (width of annulus)
        cntmrad = args.cntmradfac*ph.radius + 4
        cntmrad_sep = 1.5 * cntmrad
     
    # Check outer contaminations
    # empty dataframe in cntm is skipped
    df_sep1 = check_cntm(
        df_sep_identical, cntm=[df_ref], 
        cntmrad=[cntmrad_sep], key_x=key_x, key_y=key_y)
    df_ref1 = check_cntm(
        df_ref, cntm=[df_sep_identical], 
        cntmrad=[cntmrad_sep], key_x=key_x, key_y=key_y)

    # Check inner contaminations
    df_sep1 = self_cntm(
        df_sep1, cntmrad=cntmrad_sep, key_x=key_x, key_y=key_y)
    df_ref1 = self_cntm(
        df_ref1, cntmrad=cntmrad, key_x=key_x, key_y=key_y)

    
    # Avoid ZeroDivisionError
    if len(df_sep1) > 0:
        print(f"    Contamination number fraction on obj in df_sep")
        N_cntm_sep = np.count_nonzero(df_sep1["cntmfrac"])
        print(f"                  {N_cntm_sep}/{len(df_sep1)}")
    
    # len(df_ref1) should be lager than 0 
    print(f"    Contamination number fraction on obj in df_ref")
    N_cntm_ref = np.count_nonzero(df_ref1["cntmfrac"])
    print(f"                  {N_cntm_ref}/{len(df_ref1)}")

    
    # Plot photometry regions with 
    #   df_ref (stars in catalog, not 1 linear),
    #   df_sep (detections by sep.extract),
    #   df_obj (target)a
    out = "check_photmap.png"

    # Plot 
    #   1. ref all
    #   2. ref merged
    #   3. sep
    plot_photregion_ref(
        src.data, ph.bg_rms, df_refall, df_ref1, df_sep1,
        ph.radius, key_x=key_x, key_y=key_y, out=out)



if __name__ == "__main__":
  parser = ap(
    description="Do multi-bands data relative photometry.",
    formatter_class=ArgumentDefaultsHelpFormatter)
  parser.add_argument(
    "obj", type=str,
    help="object name")
  parser.add_argument(
    "inst", type=str, choices=["TriCCS", "murikabushi"],
    help="used instrument")
  parser.add_argument(
    "fits", type=str, 
    help="fits file")
  parser.add_argument(
    "fitsdir", type=str, 
    help="fits directory")
  parser.add_argument(
    "band", type=str,
    help="used band")
  parser.add_argument(
    "--catalog", default="ps",
    help="catalog for each bands (ps, gaia, sdss, usnob)")
  parser.add_argument(
    "--rad_ref", type=int, default=15, 
    help="reference aperture radius for loc photal photometry in pixel")
  parser.add_argument(
    "--refphot",  type=str, default="app",
    help='choose loc to use app (faint object) or iso (bright)')
  parser.add_argument(
    "--refmagmin", type=str, default="12.0", 
    help="minimum magnitude to be searched")
  parser.add_argument(
    "--refmagmax", type=str, default="18.0", 
    help="maximum magnitude to be searched")
  parser.add_argument(
    "--dbdir",  type=str, default="~/db4movphot",
    help="database directory name")
  parser.add_argument(
    "--nowinpos", action="store_true", default=False,
    help="Do not use a winpos as an aperture location (for faint objects)")
  parser.add_argument(
    "--outdir", type=str, default=None,
    help="Output directory")
  parser.add_argument(
    "--autorad", action="store_true", default=False,
    help="Use twice the calculated seeing FWHM as photometery radius")
  parser.add_argument(
    "--cntmradfac", type=float, default=2,
    help="Contamination radius is cntmrad*radius")
  parser.add_argument(
    "--autoradfac", type=float, default=2,
    help="Phototmetry radius is cntmrad*radius")
  args = parser.parse_args()
 
  main(args)
