#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Check photometory of reference stars. 
Estimate systematic errors. (i.e., magnitude difference from the catalog)

Example
-------
> phot_status_syserror.py photres_ref_magerr0f05dedge100eflag1.txt --bands g r z
"""
from argparse import ArgumentParser as ap
import os 
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  
from movphot.common import log10err, checknan, band4CTG, adderr, mulerr, adderr_series, mymark
from movphot.visualization import color_from_band


def plot_syserror(tar, ref, CTG, CTI, Nreq, bands, magmaxs, ms=100, target="Target", out=False):
    """
    Plot photometric status of stacked data.

    Parameters
    ----------
    tar : str
        photometry result of target
    ref : str
        photometry result of reference stars
    CTG : str
        CTG file
    CTI : str
        CTI file
    Nreq : int
        Required observations
    bands : array-like
        Bands
    magmaxs : array-like
        Maximum magnitude to be considered
    ms : int
        size of dots
    target : str
        name of the target
    out : str, optional
        output file name
    """
    mycolor = ["#69821b", "#AD002D", "#9400d3", "magenta", "orange"]
    
    df = pd.read_csv(ref, sep=" ")
    df_tar = pd.read_csv(tar, sep=" ")

    # Flux > 0
    df = df[df["flux"] > 0].copy()

    df_tar = df_tar[df_tar["flux"] > 0].copy()

    # Objects detected in all three bands
    b1, b2, b3 = bands
    df_1 = df_tar[df_tar["band"]==b1].copy()
    df_2 = df_tar[df_tar["band"]==b2].copy()
    df_3 = df_tar[df_tar["band"]==b3].copy()

    nf_1 = set(df_1["nframe"].values.tolist())
    nf_2 = set(df_2["nframe"].values.tolist())
    nf_3 = set(df_3["nframe"].values.tolist())
    # Common frames
    nf_common = list(nf_1 & nf_2 & nf_3)
    df_tar = df_tar[df_tar["nframe"].isin(nf_common)].copy()

    # Exclude faint obj
    #print(f"N = {len(df)}")
    for (magmax, b) in zip(magmaxs, bands):

        df = df[df[f"{b}MeanPSFMag"] < magmax].copy()
        #print(f"magmax={magmax}, {b}-band -> N = {len(df)}")

    df_CTG = pd.read_csv(CTG, sep=" ")
    df_CTI = pd.read_csv(CTI, sep=" ")

    # Remove ineffective data
    df_CTI = df_CTI[df_CTI[f"{b1}_{b2}_CTI_MC"]!=0].copy()
    df_CTI = df_CTI[df_CTI[f"{b2}_{b3}_CTI_MC"]!=0].copy()
    checknan(df)

    # Width of each star
    offset_const = -0.3

    fig = plt.figure(figsize=(20, 12))
    # Histograms (color difference)
    ax_u1 = fig.add_axes([0.10, 0.70, 0.35, 0.25])
    ax_u2 = fig.add_axes([0.55, 0.70, 0.35, 0.25])
    # catalog mag. vs. color difference
    ax_l1 = fig.add_axes([0.10, 0.10, 0.25, 0.50])
    ax_l2 = fig.add_axes([0.40, 0.10, 0.25, 0.50])
    ax_l3 = fig.add_axes([0.70, 0.10, 0.25, 0.50])
    
    # Histograms (color difference) ===========================================
    ax_u1.set_xlabel(f"{b1}-{b2} (Catalog - derived)")
    ax_u2.set_xlabel(f"{b2}-{b3} (Catalog - derived)")
    ax_u1.set_ylabel("N")
    axes_u = [ax_u1, ax_u2]

    for idx_b,b in enumerate(bands):
        if idx_b==2:
            continue
        ax_u = axes_u[idx_b]
        color = "black"
        if b=="g":
            key_CTG = "g_r_CTG_MC"
            key_CTGerr = "g_r_CTGerr_MC"
            key_CTI = "g_r_CTI_MC"
            key_CTIerr = "g_r_CTIerr_MC"
            color_name = "g-r"
            color = mycolor[0]
        elif (b =="r") and (bands[-1]=="i"):
            key_CTG = "r_i_CTG_MC"
            key_CTGerr = "r_i_CTGerr_MC"
            key_CTI = "r_i_CTI_MC"
            key_CTIerr = "r_i_CTIerr_MC"
            color_name = "r-i"
            color = mycolor[1]
        elif (b =="r") and (bands[-1]=="z"):
            key_CTG = "r_z_CTG_MC"
            key_CTGerr = "r_z_CTGerr_MC"
            key_CTG = "r_z_CTG_MC"
            key_CTGerr = "r_z_CTGerr_MC"
            color_name = "r-z"
            key_CTI = "r_z_CTI_MC"
            key_CTIerr = "r_z_CTIerr_MC"
            color = mycolor[1]

        CTG = df_CTG[key_CTG].values.tolist()[0]
        CTGerr = df_CTG[key_CTGerr].values.tolist()[0]
        CTI = df_CTI[["nframe", key_CTI]]
        CTIerr = df_CTI[["nframe", key_CTIerr]]
        nf_CTI = set(CTI["nframe"].tolist())

        band_l, band_r = band4CTG(b, bands)

        # Extract 2 bands with effective CTI
        df_l = df[(df["band"] == band_l) & (df["nframe"].isin(nf_CTI))]
        df_r = df[(df["band"] == band_r) & (df["nframe"].isin(nf_CTI))]


        # Extract common objects
        objID_l = set(df_l["objID"].values.tolist())
        objID_r = set(df_r["objID"].values.tolist())
        obj = list(objID_l & objID_r)

        # Sort by first frame
        n0_list = []
        for idx_x,x in enumerate(obj):
            #print(f"{idx_x} = {x}")
            df_temp_l = df_l[df_l["objID"] == x]
            df_temp_r = df_r[df_r["objID"] == x]
            nf_l = set(df_temp_l["nframe"].values.tolist())
            nf_r = set(df_temp_r["nframe"].values.tolist())
            nf_com = list(set(nf_l) & set(nf_r))
            #print(f"nf_com = {nf_com}")
            if len(nf_com)==0:
                n0_list.append(0)
                continue
            n0 = np.min(nf_com)
            n0_list.append(n0)

        zip_lists = zip(n0_list, obj)
        zip_sort = sorted(zip_lists)
        _, obj = zip(*zip_sort)
        obj = list(obj)
        #print(f"obj1 {obj}")

        # Calculate inst mag.
        df_l["maginst"]    = -2.5*np.log10(df_l["flux"])
        df_l["maginsterr"] = 2.5*log10err(df_l["flux"], df_l["fluxerr"])
        df_r["maginst"]    = -2.5*np.log10(df_r["flux"])
        df_r["maginsterr"] = 2.5*log10err(df_r["flux"], df_r["fluxerr"])

        N_plot = 0
        diff_list = []
        for idx_obj,x in enumerate(obj):
            marker = mymark[idx_obj+1]
            # Frame numbers in which obj are detected
            df_obj_l = df_l[df_l["objID"]==x]
            df_obj_r = df_r[df_r["objID"]==x]
            nf_l = set(df_obj_l["nframe"].values.tolist())
            nf_r = set(df_obj_r["nframe"].values.tolist())
            nf_CTI = set(CTI["nframe"].tolist())
            nf_common = list(nf_l & nf_r & nf_CTI)
            N_l = len(nf_l)
            N_r = len(nf_r)
            N_CTI = len(nf_CTI)
            N_com = len(nf_common)
           # print(f"N_l, N_r, Nf_CTI, Nf_common = {N_l}, {N_r}, {N_CTI}, {N_com}")

            df_obj_l = df_obj_l[df_obj_l["nframe"].isin(nf_common)]
            df_obj_r = df_obj_r[df_obj_r["nframe"].isin(nf_common)]
            CTI_com = CTI[CTI["nframe"].isin(nf_common)]
            CTIerr_com = CTIerr[CTIerr["nframe"].isin(nf_common)]
            CTIerr_com = CTIerr_com[key_CTIerr]

            if len(nf_common) < Nreq:
                continue

            checknan(df_obj_l)
            checknan(df_obj_r)

            # Inst. color
            flux_l = df_obj_l[f"flux"]
            flux_r = df_obj_r[f"flux"]
            fluxerr_l = df_obj_l[f"fluxerr"]
            fluxerr_r = df_obj_r[f"fluxerr"]
            instmagerr_l = [2.5*log10err(x, y) for x,y in zip(flux_l, fluxerr_l)]
            instmagerr_r = [2.5*log10err(x, y) for x,y in zip(flux_r, fluxerr_r)]
            col_inst = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
            col_insterr = [adderr(x, y) for x, y in zip(instmagerr_l, instmagerr_r)]

            # Calculate residuals of an object
            catmag_l = df_obj_l[f"{band_l}MeanPSFMag"].values.tolist()[0]
            catmag_r = df_obj_l[f"{band_r}MeanPSFMag"].values.tolist()[0]
            catmagerr_l = df_obj_l[f"{band_l}MeanPSFMagErr"].values.tolist()[0]
            catmagerr_r = df_obj_l[f"{band_r}MeanPSFMagErr"].values.tolist()[0]
            col_cat = catmag_l - catmag_r
            col_caterr = adderr(catmagerr_l, catmagerr_r)

            offset = (N_plot+1)*offset_const - col_cat

            # Derive colors
            # col_cat = col_inst * CTG + CTI
            y = [(a*CTG + b) for (a, b) in zip(col_inst, CTI_com[key_CTI])]

            err1 = mulerr(col_inst, col_insterr, np.full(N_com, CTG), np.full(N_com, CTGerr))
            yerr = adderr_series(pd.Series(err1), CTIerr_com.reset_index(drop=True))

            # Weighted average and error
            c_w = 1/yerr**2
            c_wmean = np.average(y, weights=c_w)
            c_wstd = np.sqrt(1/np.sum(c_w))
            label_w = f"Weighted average {c_wmean:.2f}={c_wstd:.2f}"

            t = df_obj_l["t_jd"]
            label = f"{x} N={N_com}"
            y = [x+offset for x in y]

            # Save diff
            diff = col_cat - c_wmean
            diff_list.append(diff)
            N_plot += 1

        # Plot
        ax_u.hist(diff_list, color=color, histtype="step")
        # Mean and std
        mean, median, stddev = np.mean(diff_list), np.median(diff_list), np.std(diff_list),
        ymin, ymax = ax_u.get_ylim()
        ax_u.vlines(mean, ymin, ymax, ls="dashed", label="Mean", color="red")
        ax_u.vlines(median, ymin, ymax, ls="dotted", label="Median", color="blue")
        ax_u.fill_between([median-stddev, median+stddev], ymin, ymax, color="gray", alpha=0.2, label=f"median+-stddev")

        diff_list_abs = [abs(x) for x in diff_list]
        ax_u.legend()
        title = f" (mean abs. diff., abs. median diff., std dev) = ({np.mean(diff_list_abs):.3f}, {np.median(diff_list_abs):.3f}, {stddev:.3f})"
        ax_u.set_title(title, fontsize=16)

    # Align x range
    xmin1, xmax1 = ax_u1.get_xlim()
    xmin2, xmax2 = ax_u2.get_xlim()
    xabs_max = np.max([abs(xmin1), abs(xmax1), abs(xmin2), abs(xmax2)])
    ax_u1.set_xlim([-xabs_max, xabs_max])
    ax_u2.set_xlim([-xabs_max, xabs_max])

    # Align y range (useless?)
    _, ymax1 = ax_u1.get_ylim()
    _, ymax2 = ax_u2.get_ylim()
    ymax_max = np.max([ymax1, ymax2])
    ax_u1.set_ylim([0, ymax_max])
    ax_u2.set_ylim([0, ymax_max])
    # Histograms (color difference) ===========================================


    # Mag. vs. color difference ===============================================
    axes_l = [ax_l1, ax_l2, ax_l3]
    ax_l1.set_xlabel(f"{b1}-band magnitude [mag]")
    ax_l2.set_xlabel(f"{b2}-band magnitude [mag]")
    ax_l3.set_xlabel(f"{b3}-band magnitude [mag]")
    ax_l1.set_ylabel(f"Catalog color - derived color [mag]")
    ax_l1.grid()
    ax_l2.grid()
    ax_l3.grid()

    for idx_b,b in enumerate(bands):
        if idx_b==2:
            continue
        if b=="g":
            key_CTG = "g_r_CTG_MC"
            key_CTGerr = "g_r_CTGerr_MC"
            key_CTI = "g_r_CTI_MC"
            key_CTIerr = "g_r_CTIerr_MC"
            color_name = "g-r"
            color = mycolor[0]
        elif (b =="r") and (bands[-1]=="i"):
            key_CTG = "r_i_CTG_MC"
            key_CTGerr = "r_i_CTGerr_MC"
            key_CTI = "r_i_CTI_MC"
            key_CTIerr = "r_i_CTIerr_MC"
            color_name = "r-i"
            color = mycolor[1]
        elif (b =="r") and (bands[-1]=="z"):
            key_CTG = "r_z_CTG_MC"
            key_CTGerr = "r_z_CTGerr_MC"
            key_CTG = "r_z_CTG_MC"
            key_CTGerr = "r_z_CTGerr_MC"
            color_name = "r-z"
            key_CTI = "r_z_CTI_MC"
            key_CTIerr = "r_z_CTIerr_MC"
            color = mycolor[1]

        CTG = df_CTG[key_CTG].values.tolist()[0]
        CTGerr = df_CTG[key_CTGerr].values.tolist()[0]
        CTI = df_CTI[["nframe", key_CTI]]
        CTIerr = df_CTI[["nframe", key_CTIerr]]
        nf_CTI = set(CTI["nframe"].tolist())

        band_l, band_r = band4CTG(b, bands)

        # Extract 2 bands with effective CTI
        df_l = df[(df["band"] == band_l) & (df["nframe"].isin(nf_CTI))]
        df_r = df[(df["band"] == band_r) & (df["nframe"].isin(nf_CTI))]

        # Extract common objects
        objID_l = set(df_l["objID"].values.tolist())
        objID_r = set(df_r["objID"].values.tolist())
        obj = list(objID_l & objID_r)

        # Sort by first frame
        n0_list = []
        for idx_x,x in enumerate(obj):
            #print(f"{idx_x} = {x}")
            df_temp_l = df_l[df_l["objID"] == x]
            df_temp_r = df_r[df_r["objID"] == x]
            nf_l = set(df_temp_l["nframe"].values.tolist())
            nf_r = set(df_temp_r["nframe"].values.tolist())
            nf_com = list(set(nf_l) & set(nf_r))
            #print(f"nf_com = {nf_com}")
            if len(nf_com)==0:
                n0_list.append(0)
                continue
            n0 = np.min(nf_com)
            n0_list.append(n0)

        zip_lists = zip(n0_list, obj)
        zip_sort = sorted(zip_lists)
        _, obj = zip(*zip_sort)
        obj = list(obj)
        #print(f"obj1 {obj}")

        # Calculate inst mag.
        df_l["maginst"]    = -2.5*np.log10(df_l["flux"])
        df_l["maginsterr"] = 2.5*log10err(df_l["flux"], df_l["fluxerr"])
        df_r["maginst"]    = -2.5*np.log10(df_r["flux"])
        df_r["maginsterr"] = 2.5*log10err(df_r["flux"], df_r["fluxerr"])

        N_plot = 0
        diff_list = []
        catmag1_list, catmag2_list, catmag3_list = [], [], []
        for idx_obj,x in enumerate(obj):
            marker = mymark[idx_obj+1]
            # Frame numbers in which obj are detected
            df_obj_l = df_l[df_l["objID"]==x]
            df_obj_r = df_r[df_r["objID"]==x]
            nf_l = set(df_obj_l["nframe"].values.tolist())
            nf_r = set(df_obj_r["nframe"].values.tolist())
            nf_CTI = set(CTI["nframe"].tolist())
            nf_common = list(nf_l & nf_r & nf_CTI)
            N_l = len(nf_l)
            N_r = len(nf_r)
            N_CTI = len(nf_CTI)
            N_com = len(nf_common)
           # print(f"N_l, N_r, Nf_CTI, Nf_common = {N_l}, {N_r}, {N_CTI}, {N_com}")

            df_obj_l = df_obj_l[df_obj_l["nframe"].isin(nf_common)]
            df_obj_r = df_obj_r[df_obj_r["nframe"].isin(nf_common)]
            CTI_com = CTI[CTI["nframe"].isin(nf_common)]
            CTIerr_com = CTIerr[CTIerr["nframe"].isin(nf_common)]
            CTIerr_com = CTIerr_com[key_CTIerr]

            if len(nf_common) < Nreq:
                continue

            checknan(df_obj_l)
            checknan(df_obj_r)

            # Inst. color
            flux_l = df_obj_l[f"flux"]
            flux_r = df_obj_r[f"flux"]
            fluxerr_l = df_obj_l[f"fluxerr"]
            fluxerr_r = df_obj_r[f"fluxerr"]
            instmagerr_l = [2.5*log10err(x, y) for x,y in zip(flux_l, fluxerr_l)]
            instmagerr_r = [2.5*log10err(x, y) for x,y in zip(flux_r, fluxerr_r)]
            col_inst = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
            col_insterr = [adderr(x, y) for x, y in zip(instmagerr_l, instmagerr_r)]

            # Calculate residuals of an object
            catmag_l = df_obj_l[f"{band_l}MeanPSFMag"].values.tolist()[0]
            catmag_r = df_obj_l[f"{band_r}MeanPSFMag"].values.tolist()[0]
            catmagerr_l = df_obj_l[f"{band_l}MeanPSFMagErr"].values.tolist()[0]
            catmagerr_r = df_obj_l[f"{band_r}MeanPSFMagErr"].values.tolist()[0]
            col_cat = catmag_l - catmag_r
            col_caterr = adderr(catmagerr_l, catmagerr_r)

            offset = (N_plot+1)*offset_const - col_cat

            # Derive colors
            # col_cat = col_inst * CTG + CTI
            y = [(a*CTG + b) for (a, b) in zip(col_inst, CTI_com[key_CTI])]
            err1 = mulerr(col_inst, col_insterr, np.full(N_com, CTG), np.full(N_com, CTGerr))
            yerr = adderr_series(pd.Series(err1), CTIerr_com.reset_index(drop=True))

            # Weighted average and error
            c_w = 1/yerr**2
            c_wmean = np.average(y, weights=c_w)
            c_wstd = np.sqrt(1/np.sum(c_w))
            label_w = f"Weighted average {c_wmean:.2f}={c_wstd:.2f}"

            t = df_obj_l["t_jd"]
            label = f"{x} N={N_com}"
            y = [x+offset for x in y]

            # Save diff
            diff = col_cat - c_wmean
            diff_list.append(diff)
            N_plot += 1

            # Save catalog mag
            catmag1 = df_obj_l[f"{b1}MeanPSFMag"].values.tolist()[0]
            catmag2 = df_obj_l[f"{b2}MeanPSFMag"].values.tolist()[0]
            catmag3 = df_obj_l[f"{b3}MeanPSFMag"].values.tolist()[0]
            catmag1_list.append(catmag1)
            catmag2_list.append(catmag2)
            catmag3_list.append(catmag3)


        # Plot
        ax_l1.scatter(catmag1_list, diff_list, color=color, label=color_name)
        ax_l2.scatter(catmag2_list, diff_list, color=color, label=color_name)
        ax_l3.scatter(catmag3_list, diff_list, color=color, label=color_name)

        # Asteroid ===========================
        # Calculate inst mag.
        if len(df_tar) > 0:
            df_tar["maginst"]    = -2.5*np.log10(df_tar["flux"])
            df_tar["maginsterr"] = 2.5*log10err(df_tar["flux"], df_tar["fluxerr"])
            df_tar_l = df_tar[df_tar["band"]==band_l]
            df_tar_r = df_tar[df_tar["band"]==band_r]
            # Extract common data with effective CTI
            nf_l = set(df_tar_l["nframe"].values.tolist())
            nf_r = set(df_tar_r["nframe"].values.tolist())
            nf_CTI = set(CTI["nframe"].tolist())
            nf_common = list(nf_l & nf_r & nf_CTI)
            N_l = len(nf_l)
            N_r = len(nf_r)
            N_CTI = len(nf_CTI)
            N_com = len(nf_common)
            #print(f"N_l, N_r, Nf_CTI, Nf_common = {N_l}, {N_r}, {N_CTI}, {N_com}")

            marker = mymark[0]
            color = mycolor[idx_b]
            # Use common
            df_tar_l = df_tar_l[df_tar_l["nframe"].isin(nf_common)]
            df_tar_r = df_tar_r[df_tar_r["nframe"].isin(nf_common)]
            CTI_com = CTI[CTI["nframe"].isin(nf_common)]
            CTIerr_com = CTIerr[CTIerr["nframe"].isin(nf_common)]
            CTIerr_com = CTIerr_com[key_CTIerr]

            checknan(df_tar_l)
            checknan(df_tar_r)

            # Inst. color
            flux_l = df_tar_l[f"flux"]
            flux_r = df_tar_r[f"flux"]
            fluxerr_l = df_tar_l[f"fluxerr"]
            fluxerr_r = df_tar_r[f"fluxerr"]
            instmagerr_l = [2.5*log10err(x, y) for x,y in zip(flux_l, fluxerr_l)]
            instmagerr_r = [2.5*log10err(x, y) for x,y in zip(flux_r, fluxerr_r)]
            col_inst = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
            col_insterr = [adderr(x, y) for x, y in zip(instmagerr_l, instmagerr_r)]

            # Derive colors
            # col_cat = col_inst * CTG + CTI
            y = [(a*CTG + b) for (a, b) in zip(col_inst, CTI_com[key_CTI])]
            err1 = mulerr(col_inst, col_insterr, np.full(N_com, CTG), np.full(N_com, CTGerr))
            yerr = adderr_series(pd.Series(err1), CTIerr_com.reset_index(drop=True))

            # Weighted average of the target
            c_w = 1/yerr**2
            c_wmean = np.average(y, weights=c_w)
            c_wstd = np.sqrt(1/np.sum(c_w))
            label_w = f"  Weighted average of the target {band_l}-{band_r} = {c_wmean:.2f}+-{c_wstd:.2f}"

            # TODO: Plot magnitude of the target on the figure

    # Align x range (useless?)
    xmin1, xmax1 = ax_l1.get_xlim()
    xmin2, xmax2 = ax_l2.get_xlim()
    xmin3, xmax3 = ax_l3.get_xlim()
    xmin_min = np.min([xmin1, xmin2, xmin3])
    xmax_max = np.max([xmax1, xmax2, xmax3])
    ax_l1.set_xlim([xmin_min, xmax_max])
    ax_l2.set_xlim([xmin_min, xmax_max])
    ax_l3.set_xlim([xmin_min, xmax_max])

    # Align y range
    ymin1, ymax1 = ax_l1.get_ylim()
    ymin2, ymax2 = ax_l2.get_ylim()
    ymin3, ymax3 = ax_l3.get_ylim()
    yabs_max = np.max([abs(ymin1), abs(ymax1), abs(ymin2), abs(ymax2), abs(ymin3), abs(ymax3)])
    ax_l1.set_ylim([-yabs_max, yabs_max])
    ax_l2.set_ylim([-yabs_max, yabs_max])
    ax_l3.set_ylim([-yabs_max, yabs_max])

    ax_l1.legend()
    ax_l2.legend()
    ax_l3.legend()
    # Mag. vs. color difference ===============================================

    if out:
        plt.savefig(out)


if __name__ == "__main__":
    parser = ap(description="Point photometry")
    parser.add_argument(
        "tar", type=str,
        help="Photometric result of target")
    parser.add_argument(
        "ref", type=str,
        help="Photometric result of reference stars")
    parser.add_argument(
        "CTG", type=str,
        help="CTG file")
    parser.add_argument(
        "CTI", type=str,
        help="CTI file")
    parser.add_argument(
        "--Nreq", type=int, default=10,
        help="Requierd number of observations (for one object in a single band)")
    parser.add_argument(
        "--bands", type=str, nargs=3, default=["g", "r", "i"],
        help="bands")
    parser.add_argument(
        "--magmaxs", type=float, nargs=3, default=[20, 20, 20],
        help="Maximum magnitude in each band")
    parser.add_argument(
        "--target", type=str, default="Target", 
         help="Target name")
    parser.add_argument(
        "--out", type=str, default="photostatus_syserror.jpg", 
         help="Output file")
    args = parser.parse_args()
    
    plot_syserror(
        args.tar, args.ref, args.CTG, args.CTI, args.Nreq, args.bands, 
        args.magmaxs, ms=100, target=args.target, out=args.out)
