#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Determine Color Transfornation Gradient (CTG) 
and Color Transformation Intercept (CTI) using stars.
Input file has to contain photometric results at least in 2-bands.

This script refers to Jackson+2021a (Asteroid Photometry with PIRATE: 
Optimizations and Techniques for Small Aperture Telescopes, PASP, 133, 075003).
But the definitions of CTG, CTI, and CT are slightly different from the 
original paper since the definition of instrumental magnitudes are different.
(i.e., In Jackson+2021a, 
 instrumental magnitudes are airmass corrected, but here not corrected.)
Time in g-band (t_jd_g) is light-time corrected only for moving object
and saved as t_jd_ltcor.
Normalized residual of color of field stars are also plotted (useless?).


Definitions of CTG and CTI
--------------------------
CTG (color transformation gradient):
    slope of color_cat (m_cat,1-m_cat,2) vs color_inst (m_inst,1-m_inst, 2)
CTI (color transformation intercept):
    intercept of color_cat (m_cat,1-m_cat,2) vs color_inst (m_inst,1-m_inst, 2)

We assume the CTGs are common values for an instrument during the night
and the CTIs could differ even in during the night.

Note
----
Remove data unless all CTIs are estimated.
(ex. Only CTIg_r is successfully estimated and not for CTIr_i,
the frame is not used.)


Output Examples (saved in 'plotcolor' directory)
------------------------------------------------
txt:
CTG_N304.txt
CTGstatus_N304.png
CTI_N304.png
colall_N304_photres_obj_mag13f5to17f0magerr0f05dedge200eflag1.txt
res_N304.png


Summary of Procedures
---------------------
(p1, p2, ... are useful search words.)
  p1  Read photometric results
  p2  Calculate time in second and save for periodic analysis
  p3  Calculate and plot CTGs
  p4  Save CTGs
  p5  Calculate and plot CTI of each frame 
  p6  Remove CTI=0 data
  p7  Save CTIs
  p8  Plot color residuals of field stars")
  p9  Calculate instrumental magnitude of the target
  p10  Calculate object color of each frame
  p11 Save photometric result a moving object
  == Optional ==
  p12 Calculate color of reference star in each frame
  p13 Save photometric result a reference star
"""
import os 
import sys
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from movphot.common import log10err, checknan, add_aspect_data, time_correction
from movphot.visualization import myfigure_ncol, myfigure4CT
from movphot.prepro import band4CTG, plot_objcolor
from movphot.prepro_color import (
    plot_CTGfit2, plot_CTIfit2, calc_res_obj, plot_res_obj, plot_res_obj_y,
    plot_y_res, calc_objcolor2, clean_color2)


if __name__ == "__main__":
    parser = ap(description="Derive object colors.")
    parser.add_argument(
        "res_tar", type=str, 
        help="photometry result of target")
    parser.add_argument(
        "res_ref", type=str, 
        help="photometry result of reference")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input files")
    parser.add_argument(
        "--target", type=str, default=None,
        help="target name (only for periodic analysis)")
    parser.add_argument(
        "--loc", type=str, default="371",
        help="location of the observatory for aspect corrections")
    parser.add_argument(
        "--CTG", type=str, 
        help="CTGs if exists")
    parser.add_argument(
        "--inst", type=str, default=None, 
        help="instrumental name")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--key_x", type=str, default="x", help="keyword for x")
    parser.add_argument(
        "--key_y", type=str, default="y", help="keyword for y")
    parser.add_argument(
        "--yr_res", nargs=2, default=None, 
        help="y ranges of residual plots")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, 
        help="output messages")
    parser.add_argument(
        "--Nobj_min", type=int, default=5, 
        help="minimum object numbers in a frame to be desired")
    parser.add_argument(
        "--Nframe_min", type=int, default=50, 
        help="minimum frames detected in a whole data to be desired")
    parser.add_argument(
        "--N_mc", type=int, default=100, 
        help="Number of trials")
    # Useless from here ??
    parser.add_argument(
        "--Ncut", type=int, default=1, 
        help="Cut dataframe to check CT status. (number of chunks)")
    parser.add_argument(
        "--targetstar", nargs=5, default=[0, 0, 0, 0, 0], 
        help="Info. to search an object using x0, x1, y0, y1, band in CTGplot")
    parser.add_argument(
        "--ycolor", type=int, default=1, 
        help="Number of colors to plot CT residual histograms depends on y")
    parser.add_argument(
        "--colormap", action="store_true", default=False,
        help="Plot CT fitting with color map like Fig. 7 in Jackson+2021")
    parser.add_argument(
        "--cbar", action="store_true", default=False,
        help="Use cbar to show time-series info.")
    parser.add_argument(
        "--same", action="store_true", default=False,
        help="Whether use the same color for all ref stars in res plot")
    parser.add_argument(
        "--ny", type=int, default=1280, 
        help="pixel number along y axis for residual plot")
    parser.add_argument(
        "--objID_ref", type=int,  default=None,
        help="Object ID to be analysed")
    parser.add_argument(
        "--fr", nargs=2, default=None, type=int,
        help="frame range to be analyzed")
    parser.add_argument(
        "--SNRmin", type=int, default=10, 
        help="demanded SNR for referece stars")
    parser.add_argument(
        "--magmin", nargs="*", type=float, default=[0, 0, 0],
        help="minimum magnitude")
    parser.add_argument(
        "--magmax", nargs="*", type=float, default=[20, 20, 20],
        help="maximum magnitude")
    parser.add_argument(
        "--outdir", type=str, default=None,
        help="output directory")
    parser.add_argument(
        "--wocor", action="store_true", default=False,
        help="Without color correction (i.e., CTGs==1)")
    parser.add_argument(
        "--overwrite", action="store_true", default=False,
        help="Overwrite the output directory")
    parser.add_argument(
        "--jd_band", type=str, default="g", help="filter of time standard")
    parser.add_argument(
        "--badstar", nargs="*", default=[],
        help="Stars not used in the analysis")
    args = parser.parse_args()

    print("Versions:")
    print(f"  np {np.__version__}")
    print(f"  pd {pd.__version__}")

    # Parse arguments =========================================================
    # Target name
    if args.target is None:
        target        = "Target"
    else:
        target        = args.target

    if args.fr:
        fmin, fmax = args.fr
        fr_str = f"_f{fmin}to{fmax}"
    else:
        fr_str = ""

    magmin, magmax = args.magmin, args.magmax
    magmin_str = ["f".join(str(x).split(".")) for x in magmin]
    magmin_str = "_".join(magmin_str)
    magmax_str = ["f".join(str(x).split(".")) for x in magmax]
    magmax_str = "_".join(magmax_str)
    mag_str = f"_mag{magmin_str}to{magmax_str}"

    snr_str = f"_snrmin{args.SNRmin}"

    # Do not remove objects with extreme colors
    key_x, key_y   = args.key_x, args.key_y
    # Selection criteria of reference stars
    Nobj_min       = args.Nobj_min
    Nframe_min     = args.Nframe_min
    # Selection criteria of the target
    verbose        = args.verbose
    ycolor         = args.ycolor
    colormap       = args.colormap
    cbar           = args.cbar
    objID_ref      = args.objID_ref
    # Whether use the same color in residual plot for all field stars
    same           = args.same
    N_mc           = args.N_mc

    if colormap:
        cmstr = "cm"
    elif cbar:
        cmstr = "cbar"
    else:
        cmstr = ""
    
    if args.CTG:
        CTGstr = "CTGgiven"
    elif args.wocor:
        CTGstr = "wocor"
    else:
        CTGstr = ""

    # PS or SDSS
    magtype        = args.magtype
    # yrange of residual plot
    yr_res         = args.yr_res

    bands = args.bands
    # Number of bands
    N_band = len(bands)

    assert N_band == len(args.magmin)
    assert N_band == len(args.magmax)

    if N_band==2:
        n_band_plot = N_band - 1  
    else:
        n_band_plot = N_band 
    Ncut = args.Ncut
    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]
    
    ny = args.ny

    # FILE NAME OF OBJECT
    filename_tar = args.res_tar.split("/")[-1].split(".")[0]
    filename_ref = args.res_ref.split("/")[-1].split(".")[0]
    
    if args.badstar:
        bad_str = "_".join(args.badstar)
        bad_str = f"_rm{bad_str}"
    else:
        bad_str = ""


    # outdir
    outdir_parent = "plotcolor"
    os.makedirs(outdir_parent, exist_ok=True)
    
    if args.outdir:
        outdir_child = args.outdir
    else:
        outdir_child = (
            f"{filename_tar}_{filename_ref}_"
            f"Nobjmin{Nobj_min}Nframemin{Nframe_min}{fr_str}{cmstr}{CTGstr}{mag_str}{snr_str}{bad_str}"
            )
    outdir = os.path.join(outdir_parent, outdir_child)
    if args.overwrite:
        os.makedirs(outdir, exist_ok=True)
    else:
        os.makedirs(outdir, exist_ok=False)
    # Parse arguments =========================================================


    print("\nImportant Arguments")
    print(f"              Nobj_min          = {Nobj_min}")
    print(f"              N_mc              = {N_mc}")

    print("\n================================================================")
    print("p1 Read photometric results")
    print("==================================================================")
    df_tar = pd.read_csv(args.res_tar, sep=" ")
    df_ref = pd.read_csv(args.res_ref, sep=" ")
    print("Number of data is for all bands (not single band)")
    print("  Original")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")


    # Added 2023-05-06
    # Filter by frame
    if args.fr:
        df_tar = df_tar[(df_tar["nframe"]> fmin) & (df_tar["nframe"] < fmax)]
        df_ref = df_ref[(df_ref["nframe"]> fmin) & (df_ref["nframe"] < fmax)]
        print(f"  Use only {fmin} < nframe < {fmax}")
   

    # Added 2023-01-09
    # Remove flux < 0 data
    df_tar = df_tar[(df_tar["flux"]> 0)]
    # Added 2023-05-02
    # Remove flux < 0 data
    df_ref = df_ref[(df_ref["flux"]> 0)]
    print("  After removal of negative fluxes")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")

    # Added 2023-05-05
    # Remove bright/faint reference objects
    df_ref = df_ref[(df_ref["flux"]/df_ref["fluxerr"]> args.SNRmin)]
    for idx_b, b in enumerate(bands):
        magmin, magmax = args.magmin[idx_b], args.magmax[idx_b]
        df_ref = df_ref[
            (df_ref[f"{b}MeanPSFMag"] < magmax) 
            & (df_ref[f"{b}MeanPSFMag"] > magmin)]
    print(f"  After removal of faint objects w/SNR < {args.SNRmin}")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")

    # Remove ''bad'' stars
    for x in args.badstar:
        df_ref = df_ref[df_ref["objID"]!=int(x)]
        print(f"Do not use {x}")

    # Added 2023-05-11
    objlist = list(set(df_ref.objID.tolist()))
    rm_list = []
    for x in objlist:
        df_temp = df_ref[df_ref.objID==x]
        for b in bands:
            df_temp_b = df_temp[df_temp["band"] == b]
            if len(df_temp_b) < args.Nframe_min:
                rm_list.append([x, b])
    for rm in rm_list:
        x, b = rm
        df_ref = df_ref[~((df_ref.objID==x) & (df_ref.band==b))]
    print(f"  Final")
    print(f"    N_df_ref = {len(df_ref)}")
    print(f"    N_df_tar = {len(df_tar)}")
    
    
    print("\n================================================================")
    print("p2 Calculate time in second and save for periodic analysis")
    print("==================================================================")
    df_tar.loc[:, f"t_sec"] = df_tar[f"t_jd"]*24*3600.
    df_ref.loc[:, f"t_sec"] = df_ref[f"t_jd"]*24*3600.

    T0 = np.min(df_tar["t_sec"])
    # Set the first time to zero (useless?)
    df_tar[f"t_sec"] -= T0
    df_ref[f"t_sec"] -= T0

    # Effective frames
    nframe_list = list(set(df_tar["nframe"].values.tolist()))
    Nframe = len(nframe_list)
    # Maximum frame
    Nmax = np.max(nframe_list)
    # Match the frames to target
    df_ref = df_ref[df_ref["nframe"].isin(nframe_list)]
    df_ref = df_ref.reset_index(drop=True)

    checknan(df_tar)
    checknan(df_ref)
   

    # Do not calculate CTG when CTG is given or without color correction (wocor)
    if args.CTG or args.wocor:
        print("Do not calculate CTGs.")
        if args.CTG:
            # Read CTG 
            print("  Use given CTGs")
            df_CTG = pd.read_csv(args.CTG, sep=" ")
        else:
            print("  Without color corrections (CTGs==1)")
            d = {
                "g_r_CTG_MC":1, "g_r_CTGerr_MC":0,
                "r_i_CTG_MC":1, "r_i_CTGerr_MC":0,
                "i_g_CTG_MC":1, "i_g_CTGerr_MC":0,
                "r_z_CTG_MC":1, "r_z_CTGerr_MC":0,
                "z_g_CTG_MC":1, "z_g_CTGerr_MC":0}
            df_CTG = pd.DataFrame(d.values(), index=d.keys()).T

    # Calculate CTG 
    else:
        print("\n============================================================")
        print("p3 Calculate and plot CTGs")
        print("==============================================================")
        # Cut and replot CTG for the sake of the clarity when Ncut != 1
        df_cut_list = []
        for n in range(Ncut):
            # Extract df
            N0 = int(Nmax/Ncut*n)
            N1 = int(Nmax/Ncut*(n+1))
            print(f"  {n+1}/{Ncut} Frame range {N0+1}--{N1} (/{Nmax})")
            # if Ncut !=0, Create separate plot
            df_temp = df_ref[(df_ref["nframe"] > N0) & (df_ref["nframe"] <= N1)]
            # length ~ N_obj * (N1-N0) ~ N_obj * nframe
            
            # axes_CTG : 
            #   time series of CTG
            # axes_raw : 
            #   inst. color vs. catalog color with raw data
            # axes_shift : 
            #   inst. color vs. catalog color with shifted fit
            # axes_res : 
            #   residuals (MC model with shift - obs.)

            # Add ax for cbar by hand (color map doesn't need...!!)
            if cbar:
                fig, axes_raw, axes_shift, axes_cbar, axes_CTG, axes_res = myfigure4CT(
                    n_band=n_band_plot, n_col=3)
            else:
                fig, axes_raw, axes_shift, axes_CTG, axes_res = myfigure_ncol(
                    n_band=n_band_plot, n_col=3, res=True)
                # Dummy
                axes_cbar = axes_res
            
            axes_raw[0].set_title("Without shift")
            axes_shift[0].set_title("Shifted to a mean of zero")

            df_CTG_list = []
            for idx, band in enumerate(bands):
                # Search target only in selected band
                if band == args.targetstar[4]:
                    # targetstar_info has [x0, x1, y0, y1, bands]
                    targetstar_info = [float(x) for x in args.targetstar[0:4]]
                else:
                    targetstar_info = None

                ax_raw   = axes_raw[idx]
                ax_shift = axes_shift[idx]
                ax_cbar  = axes_cbar[idx]
                ax_CTG   = axes_CTG[idx]
                ax_res   = axes_res[idx]

                checknan(df_ref)

                # Plot without shift (test useless?)
                # not used if #objects < Nobj_min
                _ = plot_CTGfit2(
                    df_temp, band, bands, Nobj_min, fig, ax_raw, shift=False, ycolor=ycolor,
                    ny=ny, colormap=colormap, cbar=cbar, ax_cbar=ax_cbar, N_mc=N_mc,
                    target=targetstar_info, verbose=verbose)
                # Plot with shift
                df_CTG = plot_CTGfit2(
                    df_temp, band, bands, Nobj_min, fig, ax_shift, ax_res=ax_res, yr_res=yr_res,
                    shift=True, ycolor=ycolor, ny=ny, colormap=colormap, cbar=cbar, ax_cbar=ax_cbar, ax_CTG=ax_CTG, N_mc=N_mc, verbose=verbose)

                df_CTG_list.append(df_CTG)

                if N_band==2:
                    break
            if Ncut == 1:
                out = f"CTGstatus_N{Nframe}.png"
            else:
                out = f"CTGstatus_{n+1}_N{N1-N0}.png"
            out = os.path.join(outdir, out)
            plt.savefig(out, dpi=200)
            plt.close()

            df_CTG = pd.concat(df_CTG_list, axis=1)
            # Add cut df
            df_cut_list.append(df_CTG)
        df_CTG = pd.concat(df_cut_list, axis=0)
        
        # Only plot and finish analyses when Ncut > 1
        if len(df_CTG) > 1:
            print("Only plot CTGs and finish analyses when Ncut > 1")
            sys.exit()

        checknan(df_ref)

        print("\n============================================================")
        print("p4 Save CTGs")
        print("==============================================================")
        out =  f"CTG_N{Nframe}.txt"
        out = os.path.join(outdir, out)
        df_CTG.to_csv(out, sep=" ", index=False)


    print("\n================================================================")
    print("p5 Calculate and plot CTI of each frame")
    print("==================================================================")
    fig, axes_CTI = myfigure_ncol(n_band=n_band_plot, n_col=1)
    dy_list, df_CTI_list = [], []

    # Initial values
    df_ref["CTG"], df_ref["CTGerr"] = 0., 0.
    df_ref["CTI"], df_ref["CTIerr"] = 0., 0.
    df_tar["CTG"], df_tar["CTGerr"] = 0., 0.
    df_tar["CTI"], df_tar["CTIerr"] = 0., 0.
    
    for idx, band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)
        ax_CTI = axes_CTI[idx]

        # Extract CTG to determine CTI
        CTG    = df_CTG.at[0, f"{band_l}_{band_r}_CTG_MC"]
        CTGerr = df_CTG.at[0, f"{band_l}_{band_r}_CTGerr_MC"]
        print(f"    {band_l}-{band_r} CTG = {CTG:.4f}+-{CTGerr:.4f}")

        ax_CTI.set_ylabel("CTI")
        if idx==(len(bands)-1):
            ax_CTI.set_xlabel("Seconds")

        # Calculate and plot CTIs (0 inserted if #objects < Nobj_min)
        df_CTI = plot_CTIfit2(
            df_ref, band, bands, CTG, CTGerr, Nobj_min, ax_CTI, N_mc)

        # Delta y to scale ylim
        ymin, ymax = ax_CTI.get_ylim()
        dy_list.append(ymax-ymin)
        # Add CTI in a band pair (ex. g-r)
        df_CTI_list.append(df_CTI)

        # Add CTG
        df_ref.loc[df_ref["band"]==band, "CTG"]    = CTG
        df_ref.loc[df_ref["band"]==band, "CTGerr"] = CTGerr
        df_tar.loc[df_tar["band"]==band, "CTG"]    = CTG
        df_tar.loc[df_tar["band"]==band, "CTGerr"] = CTGerr
        # Add CTI
        for nframe in df_CTI["nframe"]:
            df_CTI_n = df_CTI[df_CTI["nframe"]==nframe]
            CTI_n = df_CTI_n[f"{band_l}_{band_r}_CTI_MC"].values.tolist()[0]
            CTIerr_n = df_CTI_n[f"{band_l}_{band_r}_CTIerr_MC"].values.tolist()[0]
            df_ref.at[(df_ref["band"]==band) & (df_ref["nframe"]==nframe), "CTI"] = CTI_n
            df_ref.at[(df_ref["band"]==band) & (df_ref["nframe"]==nframe), "CTIerr"] = CTIerr_n
            df_tar.at[(df_tar["band"]==band) & (df_tar["nframe"]==nframe), "CTI"] = CTI_n
            df_tar.at[(df_tar["band"]==band) & (df_tar["nframe"]==nframe), "CTIerr"] = CTIerr_n

            if N_band==2:
                # Add the same CTI to aboid anappropreate removal by CTI
                df_ref.at[(df_ref["band"]==band_r) & (df_ref["nframe"]==nframe), "CTI"] = CTI_n
                df_ref.at[(df_ref["band"]==band_r) & (df_ref["nframe"]==nframe), "CTIerr"] = CTIerr_n
                df_tar.at[(df_tar["band"]==band_r) & (df_tar["nframe"]==nframe), "CTI"] = CTI_n
                df_tar.at[(df_tar["band"]==band_r) & (df_tar["nframe"]==nframe), "CTIerr"] = CTIerr_n
        # Break the process when N_band==2
        if N_band==2:
            break
    
    # Set y scale
    dy = np.max(dy_list)
    for idx, band in enumerate(bands):
        ax_CTI = axes_CTI[idx]
        ymin, ymax = ax_CTI.get_ylim()
        ymean = np.mean([ymin, ymax])
        ax_CTI.set_ylim([ymean-0.5*dy, ymean+0.5*dy])
        # Break the process when N_band==2
        if N_band==2:
            break

    band_str = "".join(bands)
    out =  f"CTI_N{Nframe}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    plt.close()


    print("\n================================================================")
    print("p6 Remove CTI = 0 data ")
    print("==================================================================")
    # Remove frames at least one CTI is zero
    # Target is often not detected on some frames.
    # Thus nframe_CTI0_tar != nframe_CTI_ref happens.
    nframe_CTI0_tar = set(df_tar[df_tar["CTI"]==0]["nframe"].values.tolist())
    if len(nframe_CTI0_tar) > 0:
        print(f"  Remove N={len(nframe_CTI0_tar)} for target (CTI = 0)")
        df_tar = df_tar[~df_tar["nframe"].isin(nframe_CTI0_tar)]

    nframe_CTI0_ref = set(df_ref[df_ref["CTI"]==0]["nframe"].values.tolist())
    if len(nframe_CTI0_ref) > 0:
        print(f"  Remove N={len(nframe_CTI0_ref)} for reference (CTI = 0)")
        df_ref = df_ref[~df_ref["nframe"].isin(nframe_CTI0_ref)]
    checknan(df_tar)
    checknan(df_ref)
    # Data with all CTIs are extracted
    nframe_eff = set(df_tar["nframe"].values.tolist())
    N_eff = len(nframe_eff)
    print(f"  Target with effective object CTIs N={N_eff}")

    print("\n============================================================")
    print("p7 Save CTIs")
    print("==============================================================")
    # len(CTI) == n_frame thanks to 0 padding 
    # But no. of effective frames (non-zero CTIs) are the same with N_eff.
    df_CTI = pd.merge(df_CTI_list[0], df_CTI_list[1], on="nframe")
    out =  f"CTI_N{len(df_CTI)}_eff{N_eff}.txt"
    out = os.path.join(outdir, out)
    df_CTI.to_csv(out, sep=" ", index=False)
     

    print("\n================================================================")
    print("p8 Plot color residuals of field stars")
    print("==================================================================")
    # Create 
    # axes_res: 
    #     unnormalized residual (col_cat - col_est)
    # axes_res_lin: 
    #     y vs. mean res linear plot
    # axes_res_y: 
    #     residuals dependence on y 
    fig, axes_res, axes_lin, axes_res_y, axes_cbar, = myfigure_ncol(
        n_band=n_band_plot, n_col=3, cbar=True)
    xmin_list, xmax_list, ymax_list = [], [], []
    for idx, band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)

        ax_res = axes_res[idx]
        ax_lin = axes_lin[idx]
        ax_res_y = axes_res_y[idx]
        ax_cbar = axes_cbar[idx]

        ax_res.set_ylabel("N")
        ax_lin.set_ylabel("Mean residual [mag]")
        ax_res_y.set_ylabel("y")

        if idx==(len(bands)-1):
            ax_res.set_xlabel("Residuals (cat - obs)")
            ax_lin.set_xlabel("Mean y")
            ax_res_y.set_xlabel(f"g-r")

        # Calculate residuals of each object
        res_dict = calc_res_obj(df_ref, bands, band)

        # Plot residuals of each object
        plot_res_obj(res_dict, ax_res)

        # Plot y vs. residual relation
        plot_y_res(res_dict, ax_lin, band)

        # Plot object color (gr) vs. y with colorbar of residuals 
        plot_res_obj_y(res_dict, fig, ax_res_y, ax_cbar)

        # To scale xlim
        xmin, xmax = ax_res.get_xlim()
        xmin_list.append(xmin)
        xmax_list.append(xmax)
        # To scale ylim
        ymin, ymax = ax_res.get_ylim()
        ymax_list.append(ymax)
        # Break the process when N_band==2
        if N_band==2:
            break

    # Set x scale
    xmin_common, xmax_common = np.min(xmin_list), np.max(xmax_list)
    # Set y scale
    ymax_common = np.max(ymax_list)
    for idx, band in enumerate(bands):
        ax_res = axes_res[idx]
        ax_res.set_xlim([xmin_common, xmax_common])
        ax_res.set_ylim([0, ymax_common])
        ax_res_y.set_ylim([0, ny])
        # Break the process when N_band==2
        if N_band==2:
            break

    band_str = "".join(bands)
    out =  f"res_N{Nframe}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    plt.close()


    print("\n================================================================")
    print("p9 Calculate instrumental magnitude of the target")
    print("==================================================================")
    # Calculate instrumental magnitude
    df_tar["maginst"]    = -2.5*np.log10(df_tar["flux"])
    df_tar["maginsterr"] = 2.5*log10err(df_tar["flux"], df_tar["fluxerr"])
    df_ref["maginst"]    = -2.5*np.log10(df_ref["flux"])
    df_ref["maginsterr"] = 2.5*log10err(df_ref["flux"], df_ref["fluxerr"])
    # Sometimes NaN detected when there is any negative flux
    checknan(df_tar)

    print("\n================================================================")
    print("p10 Calculate object color of each frame")
    print("==================================================================")
    df = calc_objcolor2(df_tar, band, bands)
    print(f"  N_target = {len(df)}")
    checknan(df)
    # Obtain nframe 
    nframes = df["nframe"].tolist()
    print(f"  Frames: {nframes}")

    print("\n================================================================")
    print("p11 Save photometric result a moving object")
    print("==================================================================")
    # Extract color related columns
    df = clean_color2(df, magtype, bands)
    df["obj"] = target
    df = df.reset_index(drop=True)
    
    # Use g-band time by default
    # The --jd_band option is useful when only g-band data are broken 
    # (e.g., time info. loss).
    print(f"Use as t_jd_{args.jd_band} as time standard")
    # Calculate lt_sec_*, lt_day_t with t_utc
    if len(df)==0:
        print(f"No data is effective for {target}")
    else:
        df = add_aspect_data(df, args.loc, suffix=args.jd_band)
        # Do only time correction
        #    t_jd_*      -> t_jd_ltcor_*
        # since df does not have t_sec_obs
        df = time_correction(df, suffix=args.jd_band)

        # Rename
        df = df.rename(columns={
            f"lt_sec_{args.jd_band}":"lt_sec", f"lt_day_{args.jd_band}":"lt_day", 
            f"t_jd_{args.jd_band}":"t_jd",
            f"t_sec_ltcor_{args.jd_band}":"t_sec_ltcor", 
            f"t_jd_ltcor_{args.jd_band}":"t_jd_ltcor"
            })

        out =  f"colall_N{Nframe}_eff{N_eff}_{filename_tar}.txt"
        out = os.path.join(outdir, out)
        df.to_csv(out, sep=" ", index=True)


    if objID_ref:
        print("\n============================================================")
        print("p12 Calculate color of reference star in each frame")
        print("==============================================================")
        df_ref0 = df_ref[df_ref["objID"] == objID_ref]
        df_ref0 = calc_objcolor2(df_ref0, band, bands)
        checknan(df_ref0)


        print("\n============================================================")
        print("p13 Save photometric result a reference star")
        print("==============================================================")
        df_ref0 = clean_color2(df_ref0, magtype, bands)
        df_ref0["obj"] = objID_ref
        df_ref0 = df_ref0.reset_index(drop=True)
        # Use g-band time by default
        # The --jd_band option is useful when only g-band data are broken 
        # (e.g., time info. loss).
        print(f"    Use as t_jd_{args.jd_band} as time standard")
        df_ref0 = df_ref0.rename(columns={f"t_jd_{args.jd_band}":"t_jd"})
        nframe_eff = set(df_ref0["nframe"].values.tolist())
        N_eff = len(nframe_eff)
        out =  f"colall_N{Nframe}_Neff{N_eff}_{objID_ref}.txt"
        out = os.path.join(outdir, out)
        df_ref0.to_csv(out, sep=" ", index=True)

