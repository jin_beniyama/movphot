#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Check background information (complicated).
"""
import os
import numpy as np
from argparse import ArgumentParser as ap
import datetime
import sep
import astropy.io.fits as fits
from remove_background import remove_background2d
from movphot.photfunc import calc_FWHM_fits, create_saturation_mask
import matplotlib.pyplot as plt
import mpl_toolkits.axes_grid1


def out_bgmedian(image):
  """Output median values.
  """
  n_grid = 5
  # 5 x 5 grids
  ny, nx = image.shape
  # Width
  w_x, w_y = nx/n_grid, ny/n_grid
  for idx_x in range(n_grid):
    for idx_y in range(n_grid):
        y0 = idx_y*w_y
        y1 = y0 + w_y
        x0 = idx_x*w_x
        x1 = x0 + w_x
        data_temp = data[int(y0):int(y1), int(x0):int(x1)]
        print(f"  x,y = {idx_x},{idx_y}  {np.median(data_temp):.1f}")


def plot_bgstatus(data, axes, bw=64, bh=64, fw=3, fh=3, mask=None, out="out.png"):
  bkg = sep.Background(data, mask=mask, bw=bw, bh=bh, fw=fw, fh=fh)
  level, err = bkg.globalback, bkg.globalrms
  print(f"  level, err = {level}, {err}")
  bg = bkg.back()
  data_sub= data - bg



  divider = mpl_toolkits.axes_grid1.make_axes_locatable(axes[0])
  cax = divider.append_axes('right', '5%', pad='3%')
  im = axes[0].imshow(bg)
  fig.colorbar(im, cax=cax)
  divider = mpl_toolkits.axes_grid1.make_axes_locatable(axes[1])
  cax = divider.append_axes('right', '5%', pad='3%')
  im = axes[1].imshow(data_sub)
  fig.colorbar(im, cax=cax)

  ## Binning
  data_bin = np.zeros((n_y, n_x))
  for idx_y in range(n_y):
    for idx_x in range(n_x):
      y0 = idx_y*w_y
      y1 = y0 + w_y
      x0 = idx_x*w_x
      x1 = x0 + w_x
      data_temp = data_sub[y0:y1, x0:x1]
      data_bin[idx_y, idx_x] = np.median(data_temp)
  m, s = np.mean(data_bin), np.std(data_bin)
  divider = mpl_toolkits.axes_grid1.make_axes_locatable(axes[2])
  cax = divider.append_axes('right', '5%', pad='3%')
  im = axes[2].imshow(data_bin, vmin=m-s, vmax=m+s, origin="lower")
  fig.colorbar(im, cax=cax)

  # Along x axis
  data_x = np.median(data_sub, axis=0)
  # Along y axis
  data_y = np.median(data_sub, axis=1)

  axes[3].plot(data_x, ms=3)

  axes[4].plot(data_y, ms=3)


if __name__ == "__main__":
  parser = ap(description="Plot sky background.")
  parser.add_argument("fits", help="2-d fits")
  parser.add_argument(
    "--xr", type=int, nargs=2, default=None, 
    help="x range")
  parser.add_argument(
    "--yr", type=int, nargs=2, default=None, 
    help="y range")
  parser.add_argument(
    "--ref", type=int, nargs="*", help="x,y in pixel coordinate")
  parser.add_argument(
    "--radius", type=float, default=5, help="aperture radius in unit of pixel")
  parser.add_argument(
    "--bgsub", action="store_true", default=False,
    help='use header BGCOUNT, BGSTDDEV for background estimation if False')
  parser.add_argument(
    "--wogain", action="store_true", help="No gain aperture photometry")
  parser.add_argument(
    "--ann", action="store_true", default=False,
    help="Do local background subtraction using annulus region")
  parser.add_argument(
    "--ellipse", action="store_true", default=False,
    help="extract and ellipse photometry")
  parser.add_argument(
    "--width", type=int, default=30, help="photometry width near x, y")
  parser.add_argument(
    "--all", action="store_true", default=False, 
    help="do photometry for entire fits")
  parser.add_argument(
    "--sigma", type=int, default=5, 
    help="photometry threshold is bkgerr times sigma")
  parser.add_argument(
    "--fwhmout", type=str, default=None, 
    help="output png file")
  parser.add_argument(
    "--center", nargs=2, type=int, default=[50, 50], 
    help="Center coordinate of the 100x100 plot")
  parser.add_argument(
    "--out", type=str, default=None, 
    help="output csv file")
  parser.add_argument(
    "--outdir", type=str, default="bgplot", 
    help="Output directory")
  args = parser.parse_args()
  
  radius = args.radius
  sigma = args.sigma
 
  # For TriCCS
  hdr_date = "DATE-OBS"
  # Starting time
  hdr_time = "GEXP-STR"
  hdr_gain = "GAIN"
  hdr_exp  = "EXPTIME"

  if args.wogain:
    gain = None

  outdir = args.outdir
  if not os.path.isdir(outdir):
    os.makedirs(outdir)
  
  filename = args.fits.split(".")[0]

  src = fits.open(args.fits)[0]
  obstime = src.header[hdr_time]
  gain = src.header[hdr_gain]
  obs_start = datetime.datetime.strptime(obstime, "%Y-%m-%dT%H:%M:%S.%f")
  data = src.data.byteswap().newbyteorder()

  if len(data.shape) == 3:
    print("Use 1st frame of the input fits")
    data = data[0]

  # Cut image
  if args.xr:
      xmin, xmax = args.xr
      ymin, ymax = args.yr
      data = data[int(ymin):int(ymax), int(xmin):int(xmax)]

  ny, nx = data.shape[0], data.shape[1]
  
  print("")
  print("  Header Information")
  print(f"    Gain :{gain}")
  print(f"    Exposure Starting Time :{obstime}")
  print(f"    Obsrvation Starting Time :{obs_start}")
  print("")



  # 1. Use 100x100 only
  cx, cy = args.center
  #data_100x100 = data[cy-50:cy+50, cx-50:cx+50]
  #data = data[cy-50:cy+50, cx-50:cx+50]
  data  = data.copy(order='C')

  print(f"  Original Background Mean   100x100 :{np.mean(data)}")
  print(f"  Original Background Median 100x100 :{np.median(data)}")

  # Plot images
  # ax1_0 : raw  ax1_1 : bg (no mask)  ax1_2 : bg (with satu mask)  ax1_3: bg (nomask)
  #              ax2_1 : bgsubtracted  ax2_2 : bgsubtracted         ax2_3: bgsubtracted
  #              ax3_1 : median        ax3_2 : median               ax3_3: median
  #              ax4_1 : median        ax4_2 : median               ax4_3: median
  #              ax5_1 : median        ax5_2 : median               ax5_3: median

  fig = plt.figure(figsize=(20, 20))
  x0, y0 = 0.05, 0.05
  x_width, y_width = 0.15, 0.10
  x_gap, y_gap = 0.05, 0.05

  ax1_0 = fig.add_axes([x0, 1-(y0+1*(y_width+y_gap)), x_width, y_width])
  ax1_1 = fig.add_axes([x0+1*(x_width+x_gap), 1-(y0+1*(y_width+y_gap)), x_width, y_width])
  ax1_2 = fig.add_axes([x0+2*(x_width+x_gap), 1-(y0+1*(y_width+y_gap)), x_width, y_width])
  ax1_3 = fig.add_axes([x0+3*(x_width+x_gap), 1-(y0+1*(y_width+y_gap)), x_width, y_width])

  ax2_1 = fig.add_axes([x0+1*(x_width+x_gap), 1-(y0+2*(y_width+y_gap)), x_width, y_width])
  ax2_2 = fig.add_axes([x0+2*(x_width+x_gap), 1-(y0+2*(y_width+y_gap)), x_width, y_width])
  ax2_3 = fig.add_axes([x0+3*(x_width+x_gap), 1-(y0+2*(y_width+y_gap)), x_width, y_width])

  ax3_0 = fig.add_axes([x0+0*(x_width+x_gap), 1-(y0+3*(y_width+y_gap)), x_width, y_width])
  ax3_1 = fig.add_axes([x0+1*(x_width+x_gap), 1-(y0+3*(y_width+y_gap)), x_width, y_width])
  ax3_2 = fig.add_axes([x0+2*(x_width+x_gap), 1-(y0+3*(y_width+y_gap)), x_width, y_width])
  ax3_3 = fig.add_axes([x0+3*(x_width+x_gap), 1-(y0+3*(y_width+y_gap)), x_width, y_width])

  ax4_1 = fig.add_axes([x0+1*(x_width+x_gap), 1-(y0+4*(y_width+y_gap)), x_width, y_width])
  ax4_2 = fig.add_axes([x0+2*(x_width+x_gap), 1-(y0+4*(y_width+y_gap)), x_width, y_width])
  ax4_3 = fig.add_axes([x0+3*(x_width+x_gap), 1-(y0+4*(y_width+y_gap)), x_width, y_width])

  ax5_1 = fig.add_axes([x0+1*(x_width+x_gap), 1-(y0+5*(y_width+y_gap)), x_width, y_width])
  ax5_2 = fig.add_axes([x0+2*(x_width+x_gap), 1-(y0+5*(y_width+y_gap)), x_width, y_width])
  ax5_3 = fig.add_axes([x0+3*(x_width+x_gap), 1-(y0+5*(y_width+y_gap)), x_width, y_width])


  ax1_0.set_title("raw", fontsize=15)
  ax1_1.set_title("bg (no mask)", fontsize=15)
  ax1_2.set_title("bg (mask)", fontsize=15)
  ax1_3.set_title("bg (no mask, HOGE)", fontsize=15)

  ax2_1.set_title("bgsub", fontsize=15)
  ax2_2.set_title("bgsub", fontsize=15)
  ax2_3.set_title("bgsub", fontsize=15)

  ax3_0.set_title("raw bin", fontsize=15)
  ax3_1.set_title("bin", fontsize=15)
  ax3_2.set_title("bin", fontsize=15)
  ax3_3.set_title("bin", fontsize=15)

  ax4_1.set_title("median", fontsize=15)
  ax4_2.set_title("median", fontsize=15)
  ax4_3.set_title("median", fontsize=15)

  ax5_1.set_title("median", fontsize=15)
  ax5_2.set_title("median", fontsize=15)
  ax5_3.set_title("median", fontsize=15)

  
  
  # raw      
  m, s = np.mean(data), np.std(data)
  divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax1_0)
  cax = divider.append_axes('right', '5%', pad='3%')
  im = ax1_0.imshow(data, vmin=m-s, vmax=m+s, origin="lower")
  fig.colorbar(im, cax=cax)
  
  ## Binning
  n_x, n_y = 60, 30
  w_x, w_y = int(np.ceil(nx/n_x)), int(np.ceil(ny/n_y))
  data_bin = np.zeros((n_y, n_x))
  try:
      for idx_y in range(n_y):
          for idx_x in range(n_x):
              y0 = idx_y*w_y
              y1 = y0 + w_y
              x0 = idx_x*w_x
              x1 = x0 + w_x
              #print(f"  x: {x0}--{x1}")
              #print(f"  y: {y0}--{y1}")
              data_temp = data[y0:y1, x0:x1]
              data_bin[idx_y, idx_x] = np.median(data_temp)
          m, s = np.mean(data_bin), np.std(data_bin)
          divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax3_0)
          cax = divider.append_axes('right', '5%', pad='3%')
          im = ax3_0.imshow(data_bin, vmin=m-s, vmax=m+s, origin="lower")
          fig.colorbar(im, cax=cax)
      # Without mask
      plot_bgstatus(data, [ax1_1, ax2_1, ax3_1, ax4_1, ax5_1])
      # bgsub with saturation mask
      # For TriCCS
      satumask = create_saturation_mask(data, 10000)
      plot_bgstatus(data, [ax1_2, ax2_2, ax3_2, ax4_2, ax5_2], mask=satumask)
      # without mask, fw and fh=6
      plot_bgstatus(data, [ax1_3, ax2_3, ax3_3, ax4_3, ax5_3], mask=mask, fh=6, fw=6)

  except:
      pass
  out = filename + "bgstatus.png"
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=200)

  assert False, 1

  # without mask, fw and fh=6
  bkg = sep.Background(data, mask=None, bw=64, bh=64, fw=6, fh=6)
  level, err = bkg.globalback, bkg.globalrms
  bg_nomask_6 = bkg.back()
  data_bgsub_nomask_6 = data - bg_nomask_6
  out_bgmedian(data_bgsub_nomask_6)

  divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax1_3)
  cax = divider.append_axes('right', '5%', pad='3%')
  im = ax1_3.imshow(bg_nomask_6)
  fig.colorbar(im, cax=cax)
  divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax2_3)
  cax = divider.append_axes('right', '5%', pad='3%')
  im = ax2_3.imshow(data_bgsub_nomask_6)
  fig.colorbar(im, cax=cax)

  ## Binning
  data_bin = np.zeros((n_y, n_x))
  for idx_y in range(n_y):
    for idx_x in range(n_x):
      y0 = idx_y*w_y
      y1 = y0 + w_y
      x0 = idx_x*w_x
      x1 = x0 + w_x
      data_temp = data_bgsub_nomask_6[y0:y1, x0:x1]
      data_bin[idx_y, idx_x] = np.median(data_temp)
  m, s = np.mean(data_bin), np.std(data_bin)
  divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax3_3)
  cax = divider.append_axes('right', '5%', pad='3%')
  im = ax3_3.imshow(data_bin, vmin=m-s, vmax=m+s, origin="lower")
  fig.colorbar(im, cax=cax)

