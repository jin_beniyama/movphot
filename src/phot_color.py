#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Do photometory for 3-band data taken with the Seimei/TriCCS.
(In prep. for murikabushi/MITSuME)
By default, 2nd band (R for murikabushi, r for TriCCS)
is used to predict orbit of the target.

Output file is only 'photometry_result.txt'.
See README.md to plot photometric results.

For moving object (called 'target' in this code) photometry
(i.e. non-sidereal tracked image),
you can use different photometry radius by setting radius_target and radius_ref.

Note:
  xwin, ywin   : windowed position
      position after windowed algolithm
  x, y : initial position
      position in catalog for reference stars
      position after expolaration for moving object (target)
  The 'detect_cosmics' does not work well for image with bad pixel(s),
  even with 'inmask'.
  We use bad pixel mask to remove objects close to bad pixel(s).

ToDo
----
- Calculate limiting magnitude to decide refmagmax automatically.
  Empirically limiting magnitdes (S/N=10) of TriCCS in 5 s exposure are
      g ~ 19 mag, r ~ 18.5 mag, i ~ 18.0 mag, z ~ 18.0 mag.
- Output background map
- Orbit prediction using ephemris of JPL HORIZONS

Example
-------
1. circle aperture photometry
>>> phot_color.py --target 3200 --inst TriCCS --flist glist_w_408.txt 
rlist_w_408.txt ilist_w_408.txt --fitsdir /data/asteroid/TriCCS_2021/
Phaethon2021/20211126/3200/wcs --standard standard_g_408.txt --bands g r i
--catalog ps --autorad  --refphot app --tarphot app --dbdir 
/home/neo/Script/catalog/ --idx_standard 0  --phot --refmagmax 21

2. isophotal photometry
(in prep.)
"""
import time
import sys
import os
import logging
import pandas as pd
import numpy as np
from scipy.spatial import KDTree
from skimage.morphology import binary_dilation, disk
from argparse import ArgumentParser as ap
from argparse import ArgumentDefaultsHelpFormatter
from astropy.wcs import FITSFixedWarning
from astropy.wcs import WCS as wcs
import astropy.io.fits as fits
import sep
from astroscrappy import detect_cosmics

# movphot
import movphot as mp
from movphot.common import (
    dir_from_arg, obtain_objorb, remove_xy, checknan)
from movphot.movphot import DBPATH
from movphot.photfunc import (
    calc_FWHM_fits, create_saturation_mask, create_badpixel_mask, triccsgain)
from movphot.visualization import plot_photregion_wbg, plot_fwhm

# TODO: fix ===================================================================
import warnings
# to ignore 
## WARNING: FITSFixedWarning: MJD-END = '59281.674688' / MJD at exposure end
## a floating-point value was expected. [astropy.wcs.wcs]
warnings.simplefilter('ignore', category=FITSFixedWarning)
# TODO: fix ===================================================================


def main(args=None):
    """This is the main function called by the `phot_color` script.

    Parameters
    ----------
    args : argparse.Namespace
        Arguments passed from the command-line as defined below.
    """
    
    # Start time
    t0 = time.time()

    # Parse arguments =========================================================
    # Do not confuse
    # Photometry radius of reference stars
    rad_ref  = args.rad_ref
    # Photometry radius of reference target (i.e., normally moving object)
    rad_tar  = args.rad_tar
    # Radius of mask for photometry of sources (catalog stars and sep detections)
    rad_mask = rad_ref

    # Common gap between aperture and annulus and width of annulus
    # rad_ref is set since reference stars are firstly measured
    param_app = dict(radius=rad_ref, ann_gap=args.ann_gap, ann_width=args.ann_width)
    # Dummy (for a long time... in prep.)
    param_iso = dict(
        r_disk=args.r_disk, epsilon=args.epsilon,
       sigma=args.sigma, minarea=args.minarea)
    # Number of filters
    N_band  = len(args.flist)
    # Fits directory
    fitsdir = args.fitsdir
    # Path to database of reference stars
    if args.dbdir is None:
        dbdir = DBPATH
    else:
        dbdir   = args.dbdir

    # Use some catalogs for different system  (e.g. MITSuME g and RI)
    if len(args.catalogs)==1:
        catalogs = [args.catalogs[0]]*N_band
    else:
        catalogs = args.catalogs
    # Maximum mangiutdes of reference stars to be considered
    if len(args.refmagmax)==1:
        refmagmax = [args.refmagmax[0]]*N_band
    else:
        refmagmax = args.refmagmax
    # Parse arguments =========================================================

    # Check length of fitslist and band
    assert len(args.flist)==len(args.bands), f"Check fitslist and bands!"


    # Check fitslist dimensions
    for idx,f in enumerate(args.flist):
        with open(f, "r") as f:
            lines = f.readlines()
            n_fits = len(lines)
        if idx==0:
            n_fits_stan = n_fits
        else:
            assert n_fits == n_fits_stan, "Check length of fitslists."
        pass

    # Fits to be analyzed (i.e., partial analysis for test)
    # N0 and N1 or Nana are needed.
    # If Nana==None -> N0 to N1-1 (idx)
    # If N1==None   -> N0 to N0+Nana-1 (idx)
    # If N0,N1,Nana -> N0 to N1-1 (idx)
    if args.N1:
        N0 = args.N0
        N1 = args.N1
        idx_ana = [x for x in range(N0-1, N1-1)]
    elif args.Nana:
        N0 = args.N0
        N1 = args.N0 + args.Nana
        idx_ana = [x for x in range(N0-1, N1-1)]
    else:
        idx_ana = [x for x in range(0, n_fits)]


    # Create output directories ===============================================
    ## Set --outdir test etc. for test
    if args.outdir:
        outdir = args.outdir
    else:
        outdir = dir_from_arg(args)
        os.makedirs(outdir, exist_ok=True)

    ## For photometry region png in output directory
    if args.photmap:
        photmapdir = os.path.join(outdir, "photregion")
        os.makedirs(photmapdir, exist_ok=True)

    ## For fwhm
    fwhmdir = os.path.join(outdir, "fwhm")
    os.makedirs(fwhmdir, exist_ok=True)
    # Create output directories ===============================================
 

    # Save arguments in log file ==============================================
    logging.basicConfig(
        filename=os.path.join(outdir, f"{args.target}phot.log"), 
        level=logging.DEBUG)
    logging.info("Parameters")
    for arg, value in sorted(vars(args).items()):
        logging.info(f"Argument {arg}: {value}")
    # Save arguments in log file ==============================================


    # Movphot Class
    ph = mp.Movphot(
        args.inst, args.target, 0, refmagmax[0],  
        rad_mask, param_app, param_iso, args.bw, args.fw)

    # Do orbit prediction using (idx_standard)-th band data.
    # The 2nd band is used by default.
    if args.standard:
        flist_standard = args.flist[args.idx_standard]
        orb = obtain_objorb(ph.hdr_kwd, flist_standard, fitsdir, args.standard)
  

    # Do photometry of reference stars and target simultaneously ==============
    # for loop (band)
    #     for loop (fits)
    #         1. reference star photometry
    #         2. target object  photemetry

    # List to save photometric results in all bands as df_list [df, df,...].
    # The structure of df:
    #   flux, fluxerr, x, y, eflag, ra, dec, band, frame ...
    # Results of target and ref. stars are distinguished by objtype keyword.
    df_list = []

    # Hyper parameters
    sigma_bright           = 3.
    minarea_bright         = 5.
    mask_radius            = 7.
    mask_dilation_radius   = 5.

    # Initial positions `x` and `y` are used for photometry
    if args.nowinpos:
        winpos = False
        key_x, key_y = "x", "y"
    # Windowed positions `xwin` and `ywin` are used for photometry
    else:
        winpos = True
        key_x, key_y = "xwin", "ywin"
   

    # Calculate FWHM ==========================================================
    if args.autorad:
        df_fwhm_list = []
        for idx_band, (band, flist) in enumerate(zip(args.bands, args.flist)):
            print(f"\n# Estimate {band}-band FWHM")
            # Open fitslist of the band
            with open(flist, "r") as f:
                f = f.readlines()
                # Number of frames
                N_frame = len(f)

                fwhm_mean_list, fwhm_std_list = [], []
                frame_list, jd_list = [], []
                for idx_fits, line in enumerate(f):
                    # Skip the fits not to be analyzed
                    if idx_fits not in idx_ana:
                        continue

                    # Rad_ref (default=15, typical value for TriCCS) is used as
                    # typical radius to calculate FWHM of the frame
                    infits = line.split("\n")[0]
                    infits = os.path.join(fitsdir, infits)

                    # Initial radius of the band
                    fwhm_mean, fwhm_std = calc_FWHM_fits(infits, rad_ref)
                    print(f"  {idx_fits} FWHM={fwhm_mean:.2f}+-{fwhm_std:.2f}")

                    src = fits.open(infits)[0]
                    # fits header
                    hdr = src.header
                    # Obtain time (self.t_utc, self.t_mjd, and self.t_jd) in hdr
                    ph.obtain_time_from_hdr(hdr)

                    fwhm_mean_list.append(fwhm_mean)
                    fwhm_std_list.append(fwhm_std)
                    frame_list.append(idx_fits+1)
                    jd_list.append(ph.t_jd)
              
            if idx_band==0:
                df_fwhm_band = pd.DataFrame({
                    f"nframe":frame_list, 
                    f"jd":jd_list, 
                    f"fwhm_{band}":fwhm_mean_list, 
                    f"fwhmerr_{band}":fwhm_std_list, 
                    })
            else:
                df_fwhm_band = pd.DataFrame({
                    f"fwhm_{band}":fwhm_mean_list, 
                    f"fwhmerr_{band}":fwhm_std_list, 
                    })
            df_fwhm_list.append(df_fwhm_band)
        df_fwhm = pd.concat(df_fwhm_list, axis=1)
        # Plot
        rad_ref_str = "f".join(str(rad_ref).split("."))
        out = os.path.join(
            fwhmdir, f"fwhm_rad{rad_ref_str}.png")
        df_fwhm = plot_fwhm(
            df_fwhm, args.bands, "median", n_smooth=5, 
            p_scale=ph.p_scale, out=out)
        # Save data
        out = os.path.join(
           fwhmdir, f"fwhm_rad{rad_ref_str}.txt")
        df_fwhm.to_csv(out, sep=" ", index=False)
    # Calculate FWHM ==========================================================


    # BAND loop
    print("===== Start the photometry =====")
    for idx_band, (band, flist) in enumerate(zip(args.bands, args.flist)):

        print(f"\n# Start {band}-band photometry")
        # Set catalog of the band
        ph.set_catalog(catalogs[idx_band], dbdir)
        # Set refmagmin and refmagmax of the band
        ph.refmagmax = refmagmax[idx_band]
        # Open fitslist of the band
        with open(flist, "r") as f:
            # List to save a single band photometric results
            f = f.readlines()
            # Number of frames
            N_frame = len(f)

            # FITS loop
            for idx_fits, line in enumerate(f):
                # Skip the fits not to be analyzed
                if idx_fits not in idx_ana:
                    continue

                # Set photometry radius for ref and sep
                if args.autorad:
                    # nframe - 1 = idx_fits !!
                    fwhm = (
                        df_fwhm[df_fwhm["nframe"]==idx_fits+1]
                        [f"fwhm_{band}"].values[0])
                    # Photometry radius is set to autoradfac * fwhm
                    ph.radius =  args.autoradfac*fwhm
                else:
                    ph.radius =  args.rad_ref
                 
                print(f"  {band}-band Frame {idx_fits+1}/{N_frame} start")
                infits = line.split("\n")[0]
                fitsID = infits.split("/")[-1].split(".")[0]
                infits = os.path.join(fitsdir, infits)
                # fits source  (HDU0, header + image data)
                src = fits.open(infits)[0]
                # fits header
                hdr = src.header
                # Obtain time (self.t_utc, self.t_mjd, and self.t_jd) in hdr
                ph.obtain_time_from_hdr(hdr)
                # wcs information
                w = wcs(header=hdr)
                # fits image data
                image = src.data.byteswap(inplace=True).newbyteorder()
                ny, nx = image.shape
                # Total number of pixels
                N_total = nx * ny

                # ra, dec of (0, 0)
                ra0, dec0  = w.all_pix2world(0, 0, 0)
                # ra, dec of (nx, ny)
                ra1, dec1  = w.all_pix2world(nx, ny, 0)
                print(f"    Coordinates of {infits}")
                print(f"      (ra0, dec0) = ({ra0:.2f}, {dec0:.2f})")
                print(f"      (ra1, dec1) = ({ra1:.2f}, {dec1:.2f})")
                # The coor. of figure created with matplotlib are as follows.
                # (ny,0)   .... (nx, ny)
                # ...          .......
                # (0, 0) .... (nx, 0)
                if dec0 < dec1:
                    northistop = True
                else:
                    northistop = False
                if ra0 > ra1:
                    eastisleft = True
                else:
                    eastisleft = False

                # Detect and remove cosmicrays with L.A.Cosmic ================
                cleantype = "medmask"
                verbose   = False
                # For test
                #verbose   = True

                # image_cr : bool map
                # gain in electrons / ADU (i.e., inverse gain)
                invgain = hdr[ph.hdr_kwd["gain"]]
                invgain = triccsgain(invgain)
                print(f"    Input of detect_cosmic invgain, satu_c = {invgain}, {ph.satu_count}")
                print(f"    (median, std) before detect_cosmic : ({np.median(image):.2f}, {np.std(image):.2f})")
                # Note:
                #    We do not give gain since background subtraction is done with gain.
                image_cr, image = detect_cosmics(
                    #image, satlevel=ph.satu_count, verbose=verbose, gain=invgain,
                    image, satlevel=ph.satu_count, verbose=verbose,
                    cleantype=cleantype)
                N_cr = np.count_nonzero(image_cr)
                cr_percent = N_cr / N_total * 100
                print(
                    f"    Cosmic ray detection\n"
                    f"      N_cr/N_total = {N_cr}/{N_total} "
                    f"<-> {cr_percent:.3f}%")
                # Detect and remove cosmicrays with L.A.Cosmic ================
                print(f"    (median, std) after detect_cosmic : ({np.median(image):.2f}, {np.std(image):.2f})")


                # Extract target location in the frame by interpolation =======
                ra_target, dec_target = orb.calcloc(obstime=ph.t_mjd)
                x_target, y_target    = w.all_world2pix(ra_target, dec_target, 0)
                # Round values (simply to reduce the data size)
                x_target, y_target = np.round(x_target, 2), np.round(y_target, 2)
                print(f"    Extract the target at x, y = {x_target}, {y_target}")
                # Input should be array-like
                x_target, y_target = np.array([x_target]), np.array([y_target])
                # Extract target location in the frame by interpolation =======


                # Create saturation mask ======================================
                # Create 'fat' mask 
                # to suppress contamination from saturated sources
                r_dilation        = 10.0
                satumask = create_saturation_mask(
                    image, ph.satu_count, r_dilation)
                # Create a bit 'fat' mask for background subtraction
                # Do not use only bright part
                r_dilation_narrow = 3.0
                satumask_narrow = create_saturation_mask(
                    image, ph.satu_count, r_dilation_narrow)
                # Create saturation mask ======================================


                # Create bad pixel mask =======================================
                badpixelmask = create_badpixel_mask(args.inst, band, nx, ny)
                # Create bad pixel mask =======================================


                # Create target mask ==========================================
                # This mask is used to distinguish overwrapping target and and
                # ref. stars.
                targetmask = np.zeros(image.shape, dtype=bool)
                ## Assume the target is not elongated
                sep.mask_ellipse(
                    targetmask, x_target, y_target, a=[1], b=[1], 
                    theta=[0], r=mask_radius)
                ## Mask size is 7 pix x 5 pix = 35 pix 
                targetmask = binary_dilation(targetmask, disk(mask_dilation_radius))
                # Create bad pixel mask =======================================

                # Mask for catalog stars
                mask4catalog = satumask | badpixelmask | targetmask


                # Remove background using the mask (without mask is also ok) ==
                print("    Remove background (with args.refphot)")
                if args.refphot=="app":
                    image_bgsub = ph.remove_background(image, satumask_narrow)
                    # For  photregion plot
                    image_bg = ph.return_background(image, satumask_narrow)
                    print(f"    image_bgsub (median, std) after bgsub : ({np.median(image_bgsub):.2f}, {np.std(image_bgsub):.2f})")
                    image = image_bgsub

                if args.refphot=="ann":
                    # For sep.extract, we need to remove background.
                    image_bgsub = ph.remove_background(image, satumask_narrow)
                    print(f"    (median, std) after bgsub : ({np.median(image_bgsub):.2f}, {np.std(image_bgsub):.2f})")
                    # For  photregion plot
                    image_bg = ph.return_background(image, satumask_narrow)
                    print(f"      Do not Remove background for final photometry")
                # Remove background using the mask (without mask is also ok) ==


                if not args.standard:
                    # Only do photometry of reference stars
                    # TODO: Update for not moving objects (SSSBs) observer
                    print("      Current version needs moving object! Finish.")
                    df_obj = pd.DataFrame()
                    sys.exit()
                

                # Do 1st photometry ===========================================
                # Do 1st photometry of the reference stars
                # Note
                # len(df_ref) is always > 0, but len(df_sep) sometimes == 0
                # Objects in saturation mask or bad pixel mask have non-0 eflag 
                df_ref = ph.phot_catalog(
                    image, hdr, args.refphot, band, winpos, mask4catalog)
                print(
                    f"    Extract ref stars N={len(df_ref)}\n"
                    "     (extracted in with broad FoV)")
                # Remove stars out of field of view using image size
                if len(df_ref)!=0:
                    df_ref = remove_xy(df_ref, nx, ny)
                print(
                    f"    Extract ref stars N={len(df_ref)}\n"
                    "     (extracted in the FoV)")
                if len(df_ref)==0:
                    # Print caution
                    print(
                        f"      No reference stars were detected.")

                # Do 1st photometry of the target
                # Set photometry radius for object
                if args.autorad:
                    # Use automatically calculated radius (i.e., the same 
                    # radius with ref and sep) for target as well
                    pass
                else:
                    # Use constant radius for object
                    ph.radius =  args.rad_tar


                # The window positions are saved as `xwin` and `ywin`
                # satumask is arbitratry since target is masked below with
                # mask4target
                df_target = ph.phot_loc(
                    image, hdr, args.tarphot, x_target, y_target, winpos)
                # Do 1st photometry ===========================================
 

                # Create masks ================================================
                # 1. mask4target (all sep detections except for the target)
                # 2. mask4ref (all sep detections)
                # Note: 
                #   If the data array is not background-subtracted or the 
                #   threshold is too low, you will tend to get a single large
                #   object when you run the source detection with sep.extract. 
                #   Or, more likely, an exception will be raised due to 
                #   exceeding the internal memory constraints of the 
                #   sep.extract function. (sep Documentation, Release 1.0.3)
                
                # Extract objects except for target
                ## Create a mask for target (mask_tar) to remove the target 
                ## from sep detections
                mask_tar = np.zeros(image.shape, dtype=bool)
                ## Assume the object is not elongated
                sep.mask_ellipse(
                    mask_tar, df_target[key_x], df_target[key_y], a=[1], b=[1], 
                    theta=[0], r=mask_radius)
                ## Mask size is 7 pix x 5 pix = 35 pix 
                mask_tar = binary_dilation(mask_tar, disk(mask_dilation_radius))
                ## Extract "bright" sep detections except for target
                objects = sep.extract(
                    image_bgsub, sigma_bright, mask=mask_tar,
                    err=ph.bg_rms, minarea=minarea_bright)
                
                ## Create two masks for target photometry
                ##   The second mask, mask_ref, is necesary to remove
                ##   contaminations by overwrapped stars!
                ### Create mask_sep (sep detections except for the target)
                if args.wosepmask:
                    mask_sep = np.zeros(image.shape, dtype=bool)
                else:
                    mask_sep = np.zeros(image.shape, dtype=bool)
                    sep.mask_ellipse(mask_sep, objects['x'], objects['y'],
                        a=objects["a"], b=objects["b"], theta=objects["theta"], 
                        r=mask_radius)
                    mask_sep = binary_dilation(mask_sep, disk(mask_dilation_radius))
                ### Create reference star mask (catalog stars)
                mask_ref = np.zeros(image.shape, dtype=bool)
                if len(df_ref)!=0:
                    sep.mask_ellipse(
                        mask_ref, df_ref[key_x], df_ref[key_y], a=[1], b=[1], 
                        theta=[0], r=mask_radius)
                    mask_ref = binary_dilation(mask_ref, disk(mask_dilation_radius))


                ## Combine 
                ##   mask_sep 
                ##   mask_ref (inluding overwrapped star) 
                ##   bad pixel mask
                mask4target = mask_sep | mask_ref | badpixelmask

                # Extract all objects
                objects_all = sep.extract(
                    image_bgsub, sigma_bright, mask=None,
                    err=ph.bg_rms, minarea=minarea_bright)
                N_obj = len(objects_all)
                print(f"    All sep detections N_obj = {N_obj}")
                # Create masks ================================================
 

                # Do 2nd photometry ===========================================
                # To check whether target/ref stars are in mask4target/mask4ref 
                # Check photometry results of the reference stars

                # Set photometry radius
                if args.autorad:
                    pass
                else:
                    ph.radius =  rad_ref
                
                # Bright contaminations leed to the merged object.
                # Add eflag to objects with possible contaminations.
                # Set eflag=256 for faint contamination and
                #     eflag=512 for bright contamination.
                # Create dilation mask region by myself 
                # without sep.mask_ellipse to speed up
                # df_ref contains saturated objects with non-zero eflag
                if len(df_ref)!=0:
                  for idx_ref, (x_ref, y_ref) in enumerate(
                      zip(df_ref[key_x], df_ref[key_y])):

                      # Criterion 1. To remove faint contamination
                      x_all, y_all = objects_all["x"], objects_all["y"]
                      tree_all = KDTree(list(zip(x_all, y_all)), leafsize=10)
                      # TODO: Optimize threshold.
                      # This threshold is very diffucult to decide.
                      # In fact, the threshold should be depend on the 
                      # brightness ratio. 
                      # 4 x FWHM
                      th_faint = 2
                      match_rad = th_faint*ph.radius
                      res = tree_all.query_ball_point((x_ref, y_ref), match_rad)

                      if len(res) != 1:
                          # With any objects other than itself or None
                          df_ref.loc[idx_ref, "eflag"] = 256
                          continue

                      # Criterion 2. To remove bright contamination
                      # Extract itself in objects_all
                      idx_res = res[0]
                      # eflag != 0 when objects are merged
                      eflag = objects_all["flag"][idx_res]
                      #print(f"  {idx_ref} eflag={eflag}")
                      if eflag != 0:
                          # Merged objects
                          df_ref.loc[idx_ref, "eflag"] = 512
                    
                # Do photometry of the target
                if args.autorad:
                    pass
                else:
                    ph.radius =  rad_tar
                # Use the x_target and y_target above, which are predicted loc.
                df_target = ph.phot_loc(
                    image, hdr, args.tarphot, x_target, y_target, 
                    winpos, mask4target)
                # Do 2nd photometry ===========================================

                
                col_target = df_target.columns.tolist()
                col_ref = df_ref.columns.tolist()

                # The precedure is not used if len(df_ref) ==0
                if len(df_ref)!=0:
                    assert set(col_target).issubset(col_ref), "Identical column exist"
                    col_refonly = [x for x in col_ref if x not in col_target]

                    # 0 padding to match the dimensions
                    for c in col_refonly:
                        df_target[c] = "0"
                else:
                    pass
                 
                # Add object type 
                df_ref["objtype"]    = "ref"
                df_target["objtype"] = "target"

                # Concatnate
                df_frame = pd.concat([df_ref, df_target], axis=0)
                df_frame = df_frame.reset_index(drop=True)


                idx = df_frame.index.tolist()

                # Add ra and dec with xwin and ywin
                df_frame["ra"], df_frame["dec"] = (
                    w.all_pix2world(df_frame["x"], df_frame["y"], 0))
                if not args.nowinpos:
                    # Add also rawin and decwin with x and y
                    df_frame["rawin"], df_frame["decwin"] = (
                        w.all_pix2world(df_frame[key_x], df_frame[key_y], 0))

                # Add band, nframe, and fits
                df_frame["band"]   = band
                df_frame["nframe"] = idx_fits + 1
                df_frame["fits"]   = fitsID
               
                # May not the NaN be with you.
                checknan(df_frame)

                # Add info. of fits
                # (e.g., background, time)
                ph.add_photometry_info(df_frame)

                # Save all objects in n-th frame in m-band
                df_list.append(df_frame)
                
                # Plot photometry regions with df_ref and df_target
                if args.photmap:
                    out = os.path.join(photmapdir, f"{fitsID}_photmap.png")
                    if args.autorad:
                        rad_ref_p = ph.radius
                        rad_target_p = ph.radius
                    else:
                        rad_ref_p = rad_ref
                        rad_target_p = rad_tar
                    # Plot df_obj, df_ref
                    label_tar = f"target r={rad_target_p:.1f} pix"
                    label_ref = f"target r={rad_ref_p:.1f} pix"

                    plot_photregion_wbg(
                        image_bgsub, image_bg, image_cr, ph.bg_rms, northistop, 
                        eastisleft, args.bw, args.fw, 
                        df_list=[df_target, df_ref], 
                        rad_list=[rad_target_p, rad_ref_p], 
                        key_x_list=[key_x, key_x], key_y_list=[key_y, key_y], 
                        label_list=[label_tar, label_ref], mask=mask4target, 
                        out=out)
                print("")
    # Do photometry of reference stars (and target object) simultaneously =====

 
    # Concatenate all results
    df_all = pd.concat(df_list, axis=0)
    
    # Add common info for photometry
    # e.g. pixel scale, instrument name
    ph.add_instrumental_info(df_all)

    # Save all photometric result into a single file
    out = "photometry_result.txt"
    out = os.path.join(outdir, out)
    df_all.to_csv(out, sep=" ", index=False)

    # End time
    t1 = time.time()

    # Ouptut elapsed time
    t_total = t1 - t0
    t_frame = t_total/N_frame
    print(
      "\n\nt_elapse\n"
      f"  {t_total:.1f}s\n"
      f"  {t_frame:.1f}s (N={N_frame})")
    print("===== Succesfully finish the photometry =====")


if __name__ == "__main__":
    parser = ap(
        description="Do multi-bands data relative photometry.",
        formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--target", type=str, required=True,
        help="target name")
    parser.add_argument(
        "--inst", type=str, required=True, choices=["TriCCS", "murikabushi"],
        help="used instrument")
    parser.add_argument(
        "--flist", required=True, nargs="*", 
        help="fitslist of each bands")
    parser.add_argument(
        "--fitsdir", type=str, required=True,
        help='all fits directory')
    parser.add_argument(
        "--standard", type=str,
        help="standard fits list text")
    parser.add_argument(
        "--idx_standard", type=int, default=1,
        help="index of standard fits")
    parser.add_argument(
        "--bands", nargs="*", required=True,
        help="used bands")
    parser.add_argument(
        "--catalogs", nargs="*", required=True,
        help="catalog for each bands (ps, gaia, sdss, usnob)")
    parser.add_argument(
        "--rad_ref", type=float, default=15,
        help="reference aperture radius in each band in pixel")
    parser.add_argument(
        "--rad_tar", type=float, default=15,
        help="reference aperture radius in each band in pixel")
    parser.add_argument(
        "--ann_gap", type=int, default=3, 
        help="gap between aperture and annulus in pixel")
    parser.add_argument(
        "--ann_width", type=int, default=3, 
        help="width of annulus in pixel")
    parser.add_argument(
        "--r_disk", type=int, default=15, 
        help="aperture disk radius for isophotal photometry in pixel")
    parser.add_argument(
        "--sigma", dest='sigma', type=int, default=3,
        help="extraction threshold in  object-merging process")
    parser.add_argument(
        "--epsilon", dest='epsilon', type=int, default=20,
        help='range parameter in object-merging process in pixel')
    parser.add_argument(
        "--minarea", dest='minarea', type=int, default=3,
        help='minimum area to be extracted in pixel')
    parser.add_argument(
        "--refphot",  type=str, default="app",
        help='app, ann (faint object), or iso (bright)')
    parser.add_argument(
        "--tarphot",  type=str, default="app",
        help='app, ann (faint object), or iso (bright)')
    parser.add_argument(
        "--refmagmax", nargs="*", required=True, 
        help="maximum magnitude to be searched") 
    parser.add_argument(
        "-p", "--photmap", action='store_true',
        help='create photometry region map (a bit slow)')
    parser.add_argument(
        "--dbdir",  type=str, default=None,
        help="database directory name")
    parser.add_argument(
        "--nowinpos", action="store_true", default=False,
        help="Do not use a winpos as an aperture location (for faint objects)")
    parser.add_argument(
        "--outdir", type=str, default=None,
        help="Output directory")
    parser.add_argument(
        "--N0", type=int, default=0,
        help="Initial frame to be analyzed")
    parser.add_argument(
        "--N1", type=int, default=None,
        help="Final frame to be analyzed (higher priority than Nana)")
    parser.add_argument(
        "--Nana", type=int, default=None,
        help="Number of frames to be analyzed (N0 to N0+Nana-1)")
    parser.add_argument(
        "--autorad", action="store_true", default=False,
        help="Use twice the calculated seeing FWHM as photometery radius")
    parser.add_argument(
        "--autoradfac", type=float, default=2,
        help="Phototmetry radius is cntmrad*radius")
    parser.add_argument(
        "--bw", type=int, default=128,
        help="Box width of background")
    parser.add_argument(
        "--fw", type=int, default=3,
        help="Median filter width of background")
    parser.add_argument(
        "--wosepmask", action="store_true", default=False,
        help="Do not use mask made with sep bright detections.")
    args = parser.parse_args()
 
    main(args)
