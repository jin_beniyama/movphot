#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Determine Color Transfornation Gradient (CTG) and Color Transformation 
IOntercept (CTI) using stars in all frames.
Input csv has to contain at least 2-bands photometric results.

Definitions of CTG and CTI
------------------------------
  CTG (color transformation gradient):
    slope of color_cat (m_cat,1-m_cat,2) vs color_inst (m_inst,1-m_inst, 2)
  CTI (color transformation intercept):
    intercept of color_cat (m_cat,1-m_cat,2) vs color_inst (m_inst,1-m_inst, 2)

CTGs are common values for an instrument. (outputs of this script)
CTIs could differ even in during the same night.

Normalized residual of color of field stars are also plotted.


Output Examples
(saved in plotcolor)
---------------
csv:
3200_CTG_N10_gri_magmin13f5magmax17f0Nobjmin5eflagth1.csv
3200_colall_N10_gri_magmin13f5magmax17f0Nobjmin5eflagth1.csv
3200_CTIstatus_N10_gri_magmin13f5magmax17f0Nobjmin5eflagth1.png
3200_rescolor_N10_gri_magmin13f5magmax17f0Nobjmin5eflagth1.png
3200_CTGstatus_N10_gri_magmin13f5magmax17f0Nobjmin5eflagth1.png
3200_corcolorlc_N10_gri_magmin13f5magmax17f0Nobjmin5eflagth1.png


Summary of Procedures
---------------------
(p1, p2, ... are usefull search words.)
  p1  Read photometric result
  p2  Clean the reference stars
  p3  Calculate distance from the edge in each frame
  p4  Calculate time in second and save for periodic analysis
  p5  Calculate CTGs
  p6  Save CTGs
  p7  Calculate instrumental magnitude of the target
  p8  Calculate and plot CTI of each frame 
  p9  Calculate object color of each frame
  p10 Color estimation status of field stars
  p11 Clean the target
  p12 Calculate mean colors (after cleaning)
  p13 Plot object color of each frame
  p14 Save photometric result of a moving object
  p15 (Save photometric result of a target)
"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from movphot.common import log10err, checknan
from movphot.visualization import myfigure_ncol, myfigure4CT
from movphot.prepro import (
    clean_photres, calc_d_from_edge, calc_CTG, plot_CTG, plot_CTGfit, 
    calc_CTI, plot_CTI, band4CTG, calc_objcolor, calc_meancolor, plot_objcolor, 
    clean_color, calc_rescolor, plot_res)
from movphot.prepro_color import calc_refcolor


if __name__ == "__main__":
    parser = ap(description="Get Color Transformation Gradient")
    parser.add_argument(
        "csv", type=str, help="photometry_result.csv")
    parser.add_argument(
        "obj", type=str, help="object name")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--inst", type=str, default=None, help="instrumental name")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--catmag_min", type=float, default=13.5,
        help="minimum magnitude")
    parser.add_argument(
        "--catmag_max", type=float, default=20,
        help="maximum magnitude")
    parser.add_argument(
        "--catmagerr_max", type=float, default=0.1, 
        help="maximum catalog magnitude error")
    parser.add_argument(
        "--eflag_max", type=float, default=1, 
        help="maximum eflag value for both object and comparison stars")
    parser.add_argument(
        "--gr_min", type=float, default=0, 
        help="minimum g_r value for comparison stars")
    parser.add_argument(
        "--gr_max", type=float, default=1.1, 
        help="maximum g_r value for comparison stars")
    parser.add_argument(
        "--ri_min", type=float, default=0, 
        help="minimum g_r value for comparison stars")
    parser.add_argument(
        "--ri_max", type=float, default=0.8, 
        help="maximum g_r value for comparison stars")
    parser.add_argument(
        "--cntmfrac_max", type=float, default=0.05, 
        help="maximum contamination fraction")
    parser.add_argument(
        "--dedge_min", type=int, default=30, 
        help="maximum distance from the edge")
    parser.add_argument(
        "--nx", type=int, default=2160, help="number of pixels along x axis")
    parser.add_argument(
        "--ny", type=int, default=1280, help="number of pixels along y axis")
    parser.add_argument(
        "--yr_res", nargs=2, default=None, 
        help="y ranges of residual plots")
    parser.add_argument(
        "--key_x", type=str, default="x", help="keyword for x")
    parser.add_argument(
        "--key_y", type=str, default="y", help="keyword for y")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, 
        help="output messages")
    parser.add_argument(
        "--Nobj_min", type=int, default=10, 
        help="minimum object numbers in a frame to be desired")
    parser.add_argument(
        "--Ncut", type=int, default=1, 
        help="Cut dataframe to check CT status.")
    parser.add_argument(
        "--target", nargs=5, default=[0, 0, 0, 0, 0], 
        help="Search object using x0, x1, y0, y1, band in CTplot")
    parser.add_argument(
        "--ycolor", type=int, default=1, 
        help="Number of colors to plot CT residual histograms depends on y")
    parser.add_argument(
        "--colormap", action="store_true", default=False,
        help="Plot CT fitting with color map like Fig. 7 in Jackson+2021")
    parser.add_argument(
        "--cbar", action="store_true", default=False,
        help="Use cbar to show time-series info.")
    parser.add_argument(
        "--same", action="store_true", default=False,
        help="Whether use the same color for all ref stars in res plot")
    parser.add_argument(
        "--objID_ref", type=str,  default=None,
        help="Object ID to be analysed")
    args = parser.parse_args()


    # Set output directory
    outdir = "plotcolor"
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    # Parse arguments
    # nx and ny are used to remove objects close to edge region
    if args.inst == "TriCCS":
        nx, ny = 2160, 1280
    else:
        nx, ny = args.nx, args.ny
    bands = args.bands

    # Do not remove objects with extreme colors
    target         = args.obj
    gr_min, gr_max = args.gr_min, args.gr_max
    ri_min, ri_max = args.ri_min, args.ri_max
    catmag_min     = args.catmag_min
    catmag_max     = args.catmag_max
    catmagerr_max  = args.catmagerr_max
    cntmfrac_max   = args.cntmfrac_max
    dedge_min      = args.dedge_min
    key_x, key_y   = args.key_x, args.key_y
    # Selection criteria of reference stars
    Nobj_min    = args.Nobj_min
    # Selection criteria of the target
    eflag_max      = args.eflag_max
    verbose        = args.verbose
    ycolor         = args.ycolor
    colormap       = args.colormap
    cbar           = args.cbar
    objID_ref      = args.objID_ref
    # Whether use the same color in residual plot for all field stars
    same           = args.same


    if colormap:
        cmstr = "cm"
    elif cbar:
        cmstr = "cbar"
    else:
        cmstr = ""
    magtype        = args.magtype
    # yrange of residual plot
    yr_res         = args.yr_res

    # Number of bands
    N_band = len(bands)
    Ncut = args.Ncut
    # For filename
    band_part = "".join(bands)
    # First band
    b1 = bands[0]
    # For output files
    catmag_max_str = "f".join(str(catmag_max).split("."))
    catmag_min_str = "f".join(str(catmag_min).split("."))
    eflag_max_str = "f".join(str(eflag_max).split("."))

    filename_tail = (
        f"{band_part}_magmin{catmag_min_str}magmax{catmag_max_str}Nobjmin"
        f"{Nobj_min}eflagth{eflag_max_str}dedge{dedge_min}")

    print("\nArguments")
    print(f"Select colors gr color          = {gr_min}--{gr_max}")
    print(f"              ri color          = {ri_min}--{ri_max}")
    print(f"              catalog mag       > {catmag_min}")
    print(f"              catalog mag       < {catmag_max}")
    print(f"              catalog mag error < {catmagerr_max}")
    print(f"              distance to edge  > {dedge_min}")
    print(f"              key_x, key_y      = {key_x}, {key_y}")
    print(f"              Nobj_min          = {Nobj_min}")
    print(f"              eflag             < {eflag_max}")


    print("\n================================================================")
    print("p1 Read photometric csv")
    print("==================================================================")
    df = pd.read_csv(args.csv, sep=" ")
    print(f"  N_df = {len(df)} (original)")


    print("\n================================================================")
    print("p2 Clean the reference stars")
    print("==================================================================")
    # only gr_min, gr_max
    df, objID = clean_photres(
        df, bands, key_x, key_y, gr_min, gr_max, ri_min, ri_max, 
        catmag_min, catmag_max, catmagerr_max, verbose)

    
    print("\n================================================================")
    print("p3 Calculate distance from the edge in each frame")
    print("==================================================================")
    # Suppress PerformanceWarning
    df = df.copy()
    for b in bands:
        for obj in objID:
            df.loc[:,f"dedge_{obj}_{b}"] = calc_d_from_edge(
                df[f"{key_x}_{obj}_{b}"], df[f"{key_y}_{obj}_{b}"], nx, ny)


    print("\n================================================================")
    print("p4 Calculate time in second and save for periodic analysis")
    print("==================================================================")
    for b in bands:
        # For periodic analysis 
        df.loc[:, f"t_sec_obs_{b}"] = df[f"t_mjd_{b}"]*24*3600.
        # Set the first time to zero
        df[f"t_sec_obs_{b}"] -= df.at[0, f"t_sec_obs_{b}"]


    print("\n================================================================")
    print("p5 Calculate and plot CTGs")
    print("==================================================================")
    # Cut and replot CTG for the sake of the clarity
    df_cut_list = []
    for n in range(Ncut):
        # Extract df
        N0 = int(len(df)/Ncut*n)
        N1 = int(len(df)/Ncut*(n+1))
        # if Ncut !=0, Create separate plot
        df_temp = df[N0:N1]

        # axes_CTG : 
        #   time series of CTG
        # axes_raw : 
        #   inst. color vs. catalog color with raw data
        # axes_shift : 
        #   inst. color vs. catalog color with shifted fit
        # axes_res : 
        #   residuals (MC model with shift - obs.)

        # Need ax for cbar by hand (color map doesn~t need...!!)
        if cbar:
            fig, axes_raw, axes_shift, axes_cbar, axes_CTG, axes_res = myfigure4CT(
                n_band=len(bands), n_col=3)
        else:
            fig, axes_raw, axes_shift, axes_CTG, axes_res = myfigure_ncol(
                n_band=len(bands), n_col=3, res=True)
            # Dummy
            axes_cbar = axes_res
        
        axes_raw[0].set_title("Without shift")
        axes_shift[0].set_title("Shifted to a mean of zero")

        df_CTG_list = []
        for idx,band in enumerate(bands):
            # Search target only in selected band
            if band == args.target[4]:
                # target_info has [x0, x1, y0, y1, bands]
                target_info = [float(x) for x in args.target[0:4]]
            else:
                target_info = None

            ax_raw = axes_raw[idx]
            ax_shift = axes_shift[idx]
            ax_cbar = axes_cbar[idx]
            ax_CTG = axes_CTG[idx]
            ax_res = axes_res[idx]
            # Calculate CTGs 
            CTG_df_list = calc_CTG(
                df_temp, objID, band, bands, Nobj_min, eflag_max, dedge_min, 
                cntmfrac_max, target_info)
            # Plot without shift (test)
            _ = plot_CTGfit(
                CTG_df_list, band, bands, fig, ax_raw, shift=False, ycolor=ycolor,
                ny=ny, colormap=colormap, cbar=cbar, ax_cbar=ax_cbar)
            # Plot with shift
            df_CTG = plot_CTGfit(
                CTG_df_list, band, bands, fig, ax_shift, ax_res=ax_res, yr_res=yr_res,
                shift=True, ycolor=ycolor, ny=ny, colormap=colormap, cbar=cbar, ax_cbar=ax_cbar)
            # Time-series CTG
            plot_CTG(
                CTG_df_list, band, bands, ax_CTG)

            df_CTG_list.append(df_CTG)
        out =  (
            f"{args.obj}_CTGstatus{n+1}_N{len(df_temp)}"
            f"_{filename_tail}{cmstr}.png")
        out = os.path.join(outdir, out)
        plt.savefig(out, dpi=200)
        plt.close()
        df_CTG = pd.concat(df_CTG_list, axis=1)
        # Add cut df
        df_cut_list.append(df_CTG)
    df_CTG = pd.concat(df_cut_list, axis=0)
    # Quit if N_band == 2
    if N_band==2:
        sys.exit()

    # Only plot and finish analyses when Ncut > 1
    if len(df_CTG) > 1:
        print("Only plot and finish analyses when Ncut > 1")
        sys.exit()

    checknan(df)

    print("\n================================================================")
    print("p6 Save CTGs")
    print("==================================================================")
    out =  f"{args.obj}_CTG_N{len(df)}_{filename_tail}.csv"
    df_CTG.to_csv(out, sep=" ", index=False)


    print("\n================================================================")
    print("p7 Calculate instrumental magnitude of the target")
    print("==================================================================")
    for band in bands:
        # To avoid RuntimeWarning: divide by zero encountered in log10 when f=0
        s_maginst = [
            -2.5*np.log10(f) if f > 0 else 0 for f in df[f"flux_{band}"]]
        s_maginsterr = 2.5*log10err(
            df[f"flux_{band}"], df[f"fluxerr_{band}"])
        df.insert(0, f"mag_{band}_inst", s_maginst)
        df.insert(0, f"magerr_{band}_inst", s_maginsterr) 


    print("\n================================================================")
    print("p8 Calculate and plot CTI of each frame")
    print("==================================================================")
    # axes_fit_CTI : 
    #     CTG corrected CTI light curves
    fig, axes_CTI = myfigure_ncol(n_band=len(bands), n_col=1)
    dy_list = []
    df_CTI_list = []
    for idx, band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)

        ax_CTI = axes_CTI[idx]
        # ToDo CTGerr useless?
        CTG = df_CTG.at[0, f"{band_l}_{band_r}_CTG_MC"]
        CTGerr = df_CTG.at[0, f"{band_l}_{band_r}_CTGerr_MC"]
        print(f"CTG, CTGerr = {CTG:.4f}, {CTGerr:.4f}")

        ax_CTI.set_ylabel("CTI")
        if idx==(len(bands)-1):
            ax_CTI.set_xlabel("Seconds")

        # Calculate CTIs  (0 if #objects is few)
        df_CTI = calc_CTI(
            df, objID, band, bands, CTG, CTGerr, 
            Nobj_min, eflag_max, dedge_min, cntmfrac_max)
        # Plot CTIs
        plot_CTI(
            df_CTI, band, bands, ax_CTI)

        # Break the process when N_band==2
        if N_band==2:
            sys.exit()

        # Delta y to scale ylim
        ymin, ymax = ax_CTI.get_ylim()
        dy_list.append(ymax-ymin)
        df_CTI_list.append(df_CTI)

    df_CTI = pd.concat(df_CTI_list, axis=1)

    # Set y scale
    dy = np.max(dy_list)
    for idx, band in enumerate(bands):
        ax_CTI = axes_CTI[idx]
        ymin, ymax = ax_CTI.get_ylim()
        ymean = np.mean([ymin, ymax])
        ax_CTI.set_ylim([ymean-0.5*dy, ymean+0.5*dy])

    band_str = "".join(bands)
    out =  f"{args.obj}_CTIstatus_N{len(df)}_{filename_tail}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    plt.close()




    print("\n================================================================")
    print("p9 Remove CTI=0 data ")
    print("==================================================================")
    for b in bands:
        band_l, band_r = band4CTG(b, bands)
        df_CTI0 = df_CTI[df_CTI[f"{band_l}_{band_r}_CTI_MC"] == 0]
        if len(df_CTI0) > 0:
            print(f"  Remove N={len(df_CTI0)} ({b}-band CTI = 0)")
            idx_rm = df_CTI0.index.tolist()
            df     = df[~df.index.isin(idx_rm)]
            df_CTI = df_CTI[~df_CTI.index.isin(idx_rm)]
    checknan(df)
    

    print("\n================================================================")
    print("p10 Calculate object color and save them in a single txt")
    print("==================================================================")
    # Create 
    for idx, band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)

        # ToDo CTGerr useless?
        CTG = df_CTG.at[0, f"{band_l}_{band_r}_CTG_MC"]
        CTGerr = df_CTG.at[0, f"{band_l}_{band_r}_CTGerr_MC"]
        print(f"CTG, CTGerr = {CTG:.4f}, {CTGerr:.4f}")

        # Calculate corrected color of field stars with CTI and CTG
        # i.e., g_r_res, g_r_reserr etc.
        # Also includes idx and objID to identify 
        df.to_csv("tempref.csv", sep=" ", index=False)
        df_res = calc_refcolor(
            df, objID, band, bands, CTG, CTGerr, df_CTI,
            Nobj_min, eflag_max, dedge_min, cntmfrac_max, verbose=False)
        assert False, 1


    print("\n================================================================")
    print("p10 Color residuals of field stars")
    print("==================================================================")
    # Create 
    # axes_res : 
    #     unnormalized residual (col_cat - col_est)
    # axes_res_norm : 
    #     normalized residual (col_cat - col_est)/sigma_est
    fig, axes_res, axes_res_norm = myfigure_ncol(n_band=len(bands), n_col=2)
    xmin_list, xmax_list, ymax_list = [], [], []
    for idx, band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)

        ax_res = axes_res[idx]
        ax_res_norm = axes_res_norm[idx]
        ax_res.set_ylabel("N")

        # ToDo CTGerr useless?
        CTG = df_CTG.at[0, f"{band_l}_{band_r}_CTG_MC"]
        CTGerr = df_CTG.at[0, f"{band_l}_{band_r}_CTGerr_MC"]
        print(f"CTG, CTGerr = {CTG:.4f}, {CTGerr:.4f}")


        if idx==(len(bands)-1):
            ax_res.set_xlabel("Residuals")
            ax_res_norm.set_xlabel("Normalized residuals")

        # Calculate color residuals of field stars
        # i.e., g_r_res, g_r_reserr etc.
        # Also includes idx and objID to identify 
        df_res = calc_rescolor(
            df, objID, band, bands, CTG, CTGerr, df_CTI,
            Nobj_min, eflag_max, dedge_min, cntmfrac_max, verbose=False)
        # Plot unnormalized residuals
        plot_res(
            df_res, band, bands, ax_res, same=same)
        # Plot normalized residuals
        plot_res(
            df_res, band, bands, ax_res_norm, norm=True, same=same)
        # Break the process when N_band==2
        if N_band==2:
            sys.exit()

        # To scale xlim
        xmin, xmax = ax_res.get_xlim()
        xmin_list.append(xmin)
        xmax_list.append(xmax)
        # To scale ylim
        ymin, ymax = ax_res.get_ylim()
        ymax_list.append(ymax)

    # Set x scale
    xmin_common = np.min(xmin_list)
    xmax_common = np.max(xmax_list)
    # Set y scale
    ymax_common = np.max(ymax_list)
    for idx, band in enumerate(bands):
        ax_res = axes_res[idx]
        ax_res.set_xlim([xmin_common, xmax_common])
        ax_res.set_ylim([0, ymax_common])

    band_str = "".join(bands)
    out =  f"{args.obj}_rescolor_N{len(df)}_{filename_tail}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    plt.close()


    print("\n================================================================")
    print("p10 Calculate object color of each frame")
    print("==================================================================")
    for idx, band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)
        assert len(df)==len(df_CTI), "Check the dimentions of df and df_CTI"
        #df = calc_objcolor(df, df_CTG, df_CTI, band, bands)
        df = calc_objcolor(df, df_CTI, band, bands)
    checknan(df)


    print("\n================================================================")
    print("p11 Clean the target")
    print("==================================================================")

    # Select object by eflag
    N_bef = len(df)
    for b in bands:
        df = df[df[f"eflag_{b}"] < eflag_max]
    #df = df.reset_index(drop=True)
    print(f"  N_df = {len(df)} (after eflag cleaning)")
    
    # Select object by winposflag in derive_mag.py as well!
    #assert False, 1

    # Select object by cntm
    for j,band in enumerate(bands):
        df = df[df[f"cntmfrac_{band}"] < cntmfrac_max]
    #df = df.reset_index(drop=True)
    print(f"  N_df = {len(df)} (after cntm cleaning)")
    checknan(df)

    df_all = df.copy()

    # Useless? only for plot_objcolor()
    print("\n================================================================")
    print("p12 Calculate mean colors (after cleaning)")
    print("==================================================================")
    for idx, band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)
        # Note:
        # CTI == 0 frames (due to the lack of field stars)
        # are removed in the function !
        df = calc_meancolor(df, band, bands)
    checknan(df)


    print("\n================================================================")
    print("p13 Plot object color of each frame")
    print("==================================================================")
    # 1. instrumental color 
    #    ex) g_r_inst, g_rerr_inst
    # 2. CTG, CTI corrected color 
    #    from color_inst = color_cat*CTG + CTI
    #    color_cat = (color_inst - CTI)/CTG
    #    ex) g_r_ccor, g_rerr_ccor
    fig, axes_inst, axes_cor = myfigure_ncol(n_band=len(bands), n_col=2)
    dy_list = []
    for idx, band in enumerate(bands):


        band_l, band_r = band4CTG(band, bands)

        ax_inst = axes_inst[idx]
        ax_cor = axes_cor[idx]

        if idx==0:
            ax_inst.set_title("Instrumental color")
            ax_cor.set_title("CTG/CTI corrected color")
        if idx==(len(bands)-1):
            ax_inst.set_xlabel("Time [sec]")
            ax_cor.set_xlabel("Time [sec]")

        plot_objcolor(df, band, bands, ax_inst, ax_cor)
        ax_inst.set_ylabel("Color [mag]")
        ax_inst.legend()
        ax_cor.legend()

        # Delta y to scale ylim
        ymin_inst, ymax_inst = ax_inst.get_ylim()
        ymin_cor, ymax_cor = ax_cor.get_ylim()
        dy_inst = ymax_inst - ymin_inst
        dy_cor = ymax_cor - ymin_cor
        dy_list.append(np.max([dy_inst, dy_cor]))

        # Break this process when CTGs==1 and N_band==2
        if N_band==2:
            break

    # Set y scale
    dy = np.max(dy_list)

    for idx, band in enumerate(bands):
        ax_inst = axes_inst[idx]
        ymin_inst, ymax_inst = ax_inst.get_ylim()
        ymean_inst = np.mean([ymin_inst, ymax_inst])
        ax_inst.set_ylim([ymean_inst+0.5*dy, ymean_inst-0.5*dy])

        ax_cor = axes_cor[idx]
        ymin_cor, ymax_cor = ax_cor.get_ylim()
        ymean_cor = np.mean([ymin_cor, ymax_cor])
        ax_cor.set_ylim([ymean_cor+0.5*dy, ymean_cor-0.5*dy])

    out =  f"{args.obj}_corcolorlc_N{len(df)}_{filename_tail}.png"
    out = os.path.join(outdir, out)
    fig.savefig(out, dpi=200)


    print("\n================================================================")
    print("p14 Save photometric result a moving object")
    print("==================================================================")
    df = clean_color(df, magtype, band, bands)
    df["obj"] = target
    out =  f"{args.obj}_colall_N{len(df)}_{filename_tail}.csv"
    df.to_csv(out, sep=" ", index=True)


    print("\n================================================================")
    print("p14 Save photometric result a reference star")
    print("==================================================================") 
    if objID_ref in objID:
        print(f"    Calculate and save colors of {objID_ref}")
        
        # calc_objcolor needs instrumental magnitudes
        #     mag_g_inst, mag_r_inst, 
        #     magerr_g_inst, magerr_r_inst etc.

        # Create emply dataframe
        df_ref = pd.DataFrame()

        # Inherit time info of the moving object
        for band in bands:
            df_ref[f"t_mjd_{band}"] = df[f"t_mjd_{band}"]
            df_ref[f"t_jd_{band}"] = df[f"t_jd_{band}"]
            df_ref[f"t_sec_obs_{band}"] = df[f"t_sec_obs_{band}"]

        # Calculate instrumental magnitude
        # The reference star is often not detected all frames since 
        # not tracked unlike a moving object.
        # In such case, flux is set 0 in "movphot".
        # If we remove such kind of detections below,
        # the data dimention of a final output csv is different from that of 
        # a moving object (not so a big problem, just a caution).
 

        # Remove by flux
        N_all = len(df_all)
        rm_idx = []
        for band in bands:
            rm_idx_list = [
                idx for idx,x in zip(df_all.index.values.tolist(), 
                df_all[f"flux_{objID_ref}_{band}"]) if x <= 0]
            #rm_idx_list = df_all[~df_all[f"flux_{objID_ref}_{band}"] > 0].index.values.tolist()
            df_all = df_all[df_all[f"flux_{objID_ref}_{band}"] > 0]
            rm_idx += rm_idx_list
        N_nondet = len(set(rm_idx))
        if N_nondet > 0:
            df_ref = df_ref[~df_ref.index.isin(rm_idx)]
            print(f"  Some undet data of the ref star are removed. N={N_nondet}.")
      
        # Remove by dedge (useless!! calc_objcolor can remove such outliers)
        assert False, 1

        rm_idx = []
        for band in bands:
            rm_idx_list = [
                idx for idx,x in zip(df_all.index.values.tolist(), df_all[f"dedge_{objID_ref}_{band}"]) if x <= dedge_min]
            #rm_idx_list = df_all[~df_all[f"flux_{objID_ref}_{band}"] > 0].index.values.tolist()
            df_all = df_all[df_all[f"dedge_{objID_ref}_{band}"] > dedge_min]
            rm_idx += rm_idx_list
        N_dedge = len(set(rm_idx))
        if N_dedge > 0:
            df_ref = df_ref[~df_ref.index.isin(rm_idx)]
            print(f"  Some close edge data of the ref star are removed. N={N_dedge}.")


        for band in bands:
            # To avoid RuntimeWarning: divide by zero encountered in log10 when f=0
            s_maginst = [
            -2.5*np.log10(f) if f > 0 else 0 for f in df_all[f"flux_{objID_ref}_{band}"]]
            s_maginsterr = 2.5*log10err(
                df_all[f"flux_{objID_ref}_{band}"], df_all[f"fluxerr_{objID_ref}_{band}"])

            # When happends ? Negative fluxerr ? 
            df_ref_nega_ferr = df_all[df_all[f"fluxerr_{objID_ref}_{band}"] < 0]
            N_nega_ferr_ref = len(df_ref_nega_ferr)
            if N_nega_ferr_ref > 0:
                print(N_nega_ferr_ref)
                print(df_ref_nega_ferr)
                assert False, "Negative flux error detected."

            df_ref.insert(0, f"mag_{band}_inst", s_maginst)
            df_ref.insert(0, f"magerr_{band}_inst", s_maginsterr) 
        df_ref.to_csv("hoge.csv", sep=" ", index=False)
        checknan(df_ref)
        
        # Calculate corrected magnitude
        for idx, band in enumerate(bands):
            band_l, band_r = band4CTG(band, bands)
            # ToDo: df_CTI contains CTGs...
            df_ref = calc_objcolor(df_ref, df_CTI, band, bands)

        checknan(df_ref)
        df_ref = clean_color(df_ref, magtype, band, bands)
        df_ref["obj"] = objID_ref
        out =  f"{objID_ref}_colall_N{len(df_ref)}_{filename_tail}.csv"
        df_ref.to_csv(out, sep=" ", index=True)

