#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot folded relative light curve(s) without color correction.
3 bands lc and color are plotted at once. 
Input csv should have 't_second', 'mag', and 'magerr'.

Period should be sec or hour

Time (key_x) should be sec or day (--unit_x)


Example
-------
plot_relativelc.py --obj "2021TY14" --csv 2021TY14_refladder_mag_g.csv 2021TY14_refladder_mag_r.csv 2021TY14_refladder_mag_i.csv --period 15.281 --bands g r i --magoffset 0 0.8 1.6 --coloffset 3.5 4.5 5.5 --yr 6.5 -1 --n_epoch 3 --nbin_phase 10
"""
import os 
import sys
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from binning import phasebin, moving_average

mycolor = ["#AD002D", "#1e50a2", "#006e54", "#ffd900", "#EFAEA1", 
           "#69821b", "#ec6800", "#afafb0", "#0095b9", "#89c3eb"]*100

myls = ["solid", "dashed", "dashdot", "dotted", 
        (0, (5, 3, 1, 3, 1, 3)), (0, (4,2,1,2,1,2,1,2))]*100

mymark = ["o", "^", "x", "D", "+", "v", "<", ">", "h", "H"]*100

color_bin = ["#AD002D", "#00ced1", "#a020f0", "#ff00ff", "#add8e6"]
color_move = "#0095b9"
# Functions ==================================================================



def adderr(*args):
  """Calculate additional error.

  Parameters
  ----------
  args : array-like
    list of values

  Return
  ------
  err : float
    calculated error
  """
  err = np.sqrt(np.sum(np.square(args)))
  return err


def adderr_series(*args):
  """Add error of multiple pandas.Series.

  Parameters
  ----------
  args : array-like
    list of pandas.Series 

  Return
  ------
  err_s : pandas.Series
    single pandas.Series of calculated error
  """ 
  for i,x in enumerate(args):
    assert type(x) is pd.core.frame.Series, "Input should be Series."
    #assert type(x)==type(pd.Series()), "Sould be Series"
    if i==0:
      temp = x.map(np.square)
    else:
      temp += x.map(np.square)
  err_s = temp.map(np.sqrt)
  return err_s


def band4cterm(band, bands):
  """Return 2 bands for color term determination.
  
  Parameters
  ----------
  band : str
    band of fits
  bands : str
    3 bands

  Returns
  -------
  mag_l, mag_r : float
    used magnitude
  """

  if bands == ["g", "r", "i"]:

    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "i"
    elif band=="i":
      mag_l, mag_r = "g", "i"

  elif bands == ["g", "r", "z"]:
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "z"
    elif band=="z":
      mag_l, mag_r = "g", "z"

  elif bands == ["r", "z"]:
    mag_l, mag_r = "r", "z"

  return mag_l, mag_r



# Functions finish ============================================================


if __name__ == "__main__":
  parser = ap(description="Plot folded relative light curves.")
  parser.add_argument(
    "--csv", type=str, required=True, nargs="*", 
    help="photometry results")
  parser.add_argument(
    "--obj", type=str, required=True, 
    help="object name")
  parser.add_argument(
    "--period", type=float, required=True, 
    help="rotation period used to fold light curve(s)")
  parser.add_argument(
    "--hour", action="store_true", default=False,
    help="rotation period in hour")
  parser.add_argument(
    "--bands", nargs="*", default=["g", "r", "i"],
    help="bands")
  parser.add_argument(
    "--magoffset", nargs="*", type=float, required=True,
    help="offset for magnitude")
  parser.add_argument(
    "--coloffset", nargs="*", type=float, required=True,
    help="offset for color")
  parser.add_argument(
    "--yr", default=None, type=float, nargs=2,
    help="Y range")
  parser.add_argument(
    "--nbin_phase", default=30, type=int,
    help="number for binning in phase space")
  parser.add_argument(
    "--nbin_move", default=0, type=int,
    help="number for binning for moving average")
  parser.add_argument(
    "--n_epoch", default=0, type=int,
    help="number epochs of binned light curves")
  parser.add_argument(
    "--key_x", type=str, default="t_sec_ltcor", 
    help="keyword for x")
  parser.add_argument(
    "--unit_x", type=str, default="sec", 
    help="unit of x")
  parser.add_argument(
    "--key_y", type=str, default="mag_red_phase", 
    help="keyword for y (default is reduced and phase corrected)")
  parser.add_argument(
    "--key_yerr", type=str, default="magerr", 
    help="keyword for y error")
  parser.add_argument(
    "--magonly", action="store_true", default=False,
    help="magnitude only")
  args = parser.parse_args()

 
  assert len(args.csv)==len(args.bands), "N_csv and N_band are not consistent."

  # Set output directory
  outdir = "plot"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  # Determine color depending on bands
  bands = args.bands
  # For filename
  band_part = "".join(bands)
  # Number of bands
  N_band = len(bands)
  # First band
  b1 = bands[0]

  # Rotational period
  rotP = args.period

  # Key
  key_x, key_y, key_yerr = args.key_x, args.key_y, args.key_yerr
  

  if N_band==3:
    if (bands[2]=="i") or (bands[2]=="I"):
      colors = ["#1e50a2", "darkgreen", "darkorange", "gray", "black", "darkgray"]
    if (bands[2]=="z"):
      colors = ["#1e50a2", "darkgreen", "#8b008b", "gray", "black", "darkgray"]
   
  if N_band==2:
     colors = ["#1e50a2", "darkgreen", "gray", "black"]

  if N_band==1:
     colors = ["#1e50a2", "black"]

  # Create a figure
  fig = plt.figure(figsize=(12, 12))
  ax = fig.add_axes([0.10, 0.10, 0.7, 0.85])
  if args.hour:
    ax.set_xlabel(f"Rotational phase (P = {rotP} hour)")
    # hour to sec
    rotP = args.period*3600.
  else:
    ax.set_xlabel(f"Rotational phase (P = {rotP} s)")
  ax.set_ylabel("Relative magnitude [mag]")

  # # For histogram
  # ax_hist = fig.add_axes([0.90, 0.13, 0.08, 0.25])
  # ax_hist.set_xlabel("N")
  # #ax_hist.set_ylim([-1,1])
  # ax_hist.axes.yaxis.set_visible(False)

  dfs = dict()
  # Offsets
  magoff = args.magoffset
  coloff = args.coloffset

  # Width of binning
  width_phasebin = 1.0/args.nbin_phase
  nbin_move = args.nbin_move
  # Number of points per epoch

  # Plot mag lc
  for idx, (csv, b) in enumerate(zip(args.csv, args.bands)):
    offset = magoff[idx]
    df = pd.read_csv(csv, sep=" ")
    # unit of x is sec  (rotP is in sec here)
    if (args.unit_x=="sec"):
      x = (df[key_x]/rotP)%1
    elif (args.unit_x=="day"):
      sec_per_day = 3600*24
      x = (df[key_x]*sec_per_day/rotP)%1

    print(f"  Mag mean ({b}): {np.mean(df[key_y])}")

    y = df[key_y] + offset
    yerr = df[key_yerr]
    ax.errorbar(
      x, y, yerr, color=colors[idx], fmt="o", ms=4, lw=0.5, marker=mymark[idx],
      capsize=0, label=f"{b} N={len(x)}")
    
    if args.n_epoch > 0:
      N_per_epoch = int(len(df)/args.n_epoch)
      for n in range(args.n_epoch):
        x_n = x[n*N_per_epoch:(n+1)*N_per_epoch]
        y_n = y[n*N_per_epoch:(n+1)*N_per_epoch]
        yerr_n = yerr[n*N_per_epoch:(n+1)*N_per_epoch]
        # Binning
        x_bin, y_bin, yerr_bin = phasebin(
            x_n, y_n, width_phasebin, yerr_n)
        label_bin = f"{b} Epoch folding {n+1}"
        ax.errorbar(
          x_bin, y_bin, yerr_bin, color=color_bin[n], ms=5, marker=mymark[idx],
          label = label_bin)



    # Moving average before folding
    if nbin_move:
      label_move = (
        f"{b} Moving average\n" + "$N_{bin}$" + f"={nbin_move}")
      y_move, yerr_move = moving_average(
          df[key_x], y, nbin_move, yerr)
      ax.errorbar(
        x, y_move, yerr=None, color=color_move, ms=3, lw=0.5, fmt="o", marker=mymark[idx],
        label = label_move)
      #ax.errorbar(
      #  x, y_move, yerr_move, color=color_move, ms=8, marker=mymark[idx+1],
      #  label = label_move)

    dfs[b] = df
  
  # N_band==1 or Mag only
  if (N_band==1) or (args.magonly):
    ax.invert_yaxis()
    ax.legend(bbox_to_anchor=(1.0, 0.95), borderaxespad=0)
    if args.yr:
      ymin, ymax = args.yr
      ax.set_ylim([ymin, ymax])
      ymin_str = str(ymin).replace(".", "f")
      ymax_str = str(ymax).replace(".", "f")
      yr_str = f"yr{ymin_str}to{ymax_str}"
    else:
      yr_str = ""

    bands_str = "".join(bands)
    magoff = [str(x) for x in magoff]
    magoff_str = "_".join(magoff)
    magoff_str = magoff_str.replace(".", "f")
    p_str = str(args.period).replace(".", "f")

    out = (
      f"{args.obj}_folded_relativelc_{bands_str}_P{p_str}_magoff{magoff_str}"
      f"nbinp{args.nbin_phase}nbinm{args.nbin_move}"
      f"nepoch{args.n_epoch}{yr_str}.png"
      )
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=150)
    sys.exit()


  # Plot color lc
  for idx, (csv, b) in enumerate(zip(args.csv, args.bands)):
    offset = coloff[idx]
    b1, b2 = band4cterm(b, bands)
    col = dfs[b1][key_y] - dfs[b2][key_y]
    colerr = adderr_series(dfs[b1][key_yerr], dfs[b2][key_yerr])
    col_cor = [c+offset for c in col]
    
    # unit of x is sec  (rotP is in sec here)
    if (args.unit_x=="sec"):
      x = (df[key_x]/rotP)%1
    elif (args.unit_x=="day"):
      sec_per_day = 3600*24
      x = (df[key_x]*sec_per_day/rotP)%1

    y = col_cor
    yerr = colerr
    label_bin = f"binned {b1}-{b2}"

    ax.errorbar(
      x, y, yerr, color=colors[idx+N_band], fmt="o", ms=4, lw=0.5, marker=mymark[idx],
      capsize=0,
      label = f"{b1}-{b2}")

    for n in range(args.n_epoch):
      x_n = x[n*N_per_epoch:(n+1)*N_per_epoch]
      y_n = y[n*N_per_epoch:(n+1)*N_per_epoch]
      yerr_n = yerr[n*N_per_epoch:(n+1)*N_per_epoch]

      # Binning
      label_bin = f"{b1}-{b2} Epoch folding {n+1}"
      x_bin, y_bin, yerr_bin = phasebin(
          x_n, y_n, width_phasebin, yerr_n)
      ax.errorbar(
        x_bin, y_bin, yerr_bin, color=color_bin[n], ms=5, marker=mymark[idx],
        label=label_bin)


    # Moving average before folding
    if nbin_move:
      label_move = (
        f"{b1}-{b2} Moving average\n" + "$N_{bin}$" + f"={nbin_move}")
      y_move, yerr_move = moving_average(
          df[key_x], y, nbin_move, yerr)
      # Calculate yerr !!
      ax.errorbar(
        x, y_move, yerr=None, color=color_move, fmt="o", ms=3, lw=0.5, 
        marker=mymark[idx], label=label_move)
      #ax.errorbar(
      #  x, y_move, yerr_move, color=color_move, ms=8, 
      #  marker=mymark[idx+1], label=label_move)
      
    ## Add histograms
    #col_res = col - np.mean(col)
    #hist_max, hist_min = np.max(col_res), np.min(col_res)
    #N_bin = np.int((hist_max - hist_min)*30)
    #ax_hist.hist(
    #  col_res, bins=N_bin, range=(hist_min, hist_max), 
    #  orientation="horizontal", label=f"N={len(col_res)}")


  ax.invert_yaxis()
  ax.legend(bbox_to_anchor=(1.0, 0.95), borderaxespad=0)
  if args.yr:
    ymin, ymax = args.yr
    ax.set_ylim([ymin, ymax])
    ymin_str = str(ymin).replace(".", "f")
    ymax_str = str(ymax).replace(".", "f")
    yr_str = f"yr{ymin_str}to{ymax_str}"
  else:
    yr_str = ""

  bands_str = "".join(bands)
  magoff = [str(x) for x in magoff]
  magoff_str = "_".join(magoff)
  magoff_str = magoff_str.replace(".", "f")
  coloff = [str(x) for x in coloff]
  coloff_str = "".join(coloff)
  coloff_str = coloff_str.replace(".", "f")
  p_str = str(args.period).replace(".", "f")

  out = (
    f"{args.obj}_folded_relativelc_{bands_str}_P{p_str}_magoff{magoff_str}"
    f"coloff{coloff_str}nbinp{args.nbin_phase}nbinm{args.nbin_move}"
    f"nepoch{args.n_epoch}{yr_str}.png" 
    )
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=150)


