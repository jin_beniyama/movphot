#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Replot magnitude light curves of an object using 
  the ColorTern (CT) estimated in `get_CT.py` 
and 
  the object color estimated in `plot_objcolor.py`
in *magall*.csv created by `plot_objmag.py`

Photmetric status is also created.

Output Examples
---------------
2021TY14_CTcormaglc_N4_gri_CT*.png
2021TY14_refstatus_N4_gri.png
2021TY14_refstatus_N4_gri_yrange.png
2021TY14_refstatus_N4_gri_grcolor.png


Summary of Procedures
---------------------
  p1 Replot object light curve
  p2 Plot photometric status
"""


import os 
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  
import matplotlib
matplotlib.use('Agg')

plt.rcParams["font.family"] = "DejaVu Sans"
plt.rcParams["font.size"] = 10
plt.rcParams["axes.labelsize"] = 10
plt.rcParams["legend.fontsize"] = 8
plt.rcParams["xtick.labelsize"] = 8 
plt.rcParams["ytick.labelsize"] = 8 

mycolor = ["#AD002D", "#1e50a2", "#006e54", "#ffd900", "#EFAEA1", 
           "#69821b", "#ec6800", "#afafb0", "#0095b9", "#89c3eb"]*100

myls = ["solid", "dashed", "dashdot", "dotted", 
        (0, (5, 3, 1, 3, 1, 3)), (0, (4,2,1,2,1,2,1,2))]*100

mymark = ["o", "^", "x", "D", "+", "v", "<", ">", "h", "H"]*100

# Functions ==================================================================

def myfigure(n):
  if n == 2:
    fig = plt.figure(figsize=(16, 8))
    ax1 = fig.add_axes([0.10, 0.15, 0.35, 0.8])
    ax2 = fig.add_axes([0.6, 0.15, 0.35, 0.8])
    return fig, ax1, ax2




def adderr(*args):
  """Calculate additional error.

  Parameters
  ----------
  args : array-like
    list of values

  Return
  ------
  err : float
    calculated error
  """
  err = np.sqrt(np.sum(np.square(args)))
  return err


def mulerr(val1, err1, val2, err2):
  """
  Serieses can be parameters?.
  """
  return np.sqrt((val2*err1)**2 + (val1*err2)**2)

def adderr_series(*args):
  """Add error of multiple pandas.Series.

  Parameters
  ----------
  args : array-like
    list of pandas.Series 

  Return
  ------
  err_s : pandas.Series
    single pandas.Series of calculated error
  """ 
  for i,x in enumerate(args):
    assert type(x) is pd.core.frame.Series, "Input should be Series."
    #assert type(x)==type(pd.Series()), "Sould be Series"
    if i==0:
      temp = x.map(np.square)
    else:
      temp += x.map(np.square)
  err_s = temp.map(np.sqrt)
  return err_s


def log10err(val, err):
  """Calculate log10 error.
  """
  return err/val/np.log(10)


def band4cterm(band, bands):
  """Return 2 bands for color term determination.
  
  Parameters
  ----------
  band : str
    band of fits
  bands : str
    3 bands

  Returns
  -------
  mag_l, mag_r : float
    used magnitude
  """

  if bands == ["g", "r", "i"]:

    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "i"
    elif band=="i":
      mag_l, mag_r = "g", "i"

  elif bands == ["g", "r", "z"]:
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "z"
    elif band=="z":
      mag_l, mag_r = "g", "z"

  elif bands == ["r", "z"]:
    mag_l, mag_r = "r", "z"

  return mag_l, mag_r


def calc_inst_color(df, bands, magkwd):
  """Calculate instrumental color.

  Parameters
  ----------
  df : pandas.DataFrame
    input DataFrame
  bands : list
    bands in DataFrame
  magkwd : dict
    magnitude keyword

  Return
  ------
  df : pandas.DataFrame
    instrumental magnitude calculated DataFrame
  """
  if bands == ["g", "r", "i"]:
    df["g_r"] = df[magkwd["g"]] - df[magkwd["r"]]
    df["r_i"] = df[magkwd["r"]] - df[magkwd["i"]]

  if bands == ["g", "r", "z"]:
    df["g_r"] = df[magkwd["g"]] - df[magkwd["r"]]
    df["r_z"] = df[magkwd["r"]] - df[magkwd["z"]]

  return df


def obtain_magkwd(bands):
  """Obtain magnitude keyword to calculate instrumental color.
  for only this script.
  
  Parameters
  ----------
  bands : list
    bands in DataFrame

  Return
  ------
  magkwd : dict
    magnitude keyword dictionary
  """
    
  if bands == ["g", "r", "i"]:
    magkwd = dict(g="mag_g", r="mag_r", i="mag_i")
  elif bands == ["g", "r", "z"]:
    magkwd = dict(g="mag_g", r="mag_r", z="mag_z")
  return magkwd


def diverr(val1, err1, val2, err2):
  """Calculate error for division.
  
  Parameters
  ----------
  val1 : float or pandas.Series 
    value 1
  err1 : float or pandas.Series 
    error 1
  val2 : float or pandas.Series 
    value 2
  err2 : float or pandas.Series 
    error 2
  """
  return np.sqrt((err1/val2)**2 + (err2*val1/val2**2)**2)

# Functions finish ============================================================


if __name__ == "__main__":
  parser = ap(description="Color determination only ploy")
  parser.add_argument(
    "magcsv", type=str, help="*magall*")
  parser.add_argument(
    "obj", type=str, help="object name")
  parser.add_argument(
    "magtype", type=str, choices=["SDSS", "PS"],
    help="griz magnitude type of input csv")
  parser.add_argument(
    "--bands", nargs="*", default=["g", "r", "i"],
    help="3 bands")
  parser.add_argument(
    "--magmax", type=float, default=20,
    help="maximum magnitude")
  parser.add_argument(
    "--magmin", type=float, default=12,
    help="minimum magnitude")
  parser.add_argument(
    "--err_th", type=float, default=1, 
    help="maximum magnitude error value for both object and comparison stars")
  parser.add_argument(
    "--eflag_th", type=float, default=1, 
    help="maximum eflag value for both object and comparison stars")
  parser.add_argument(
    "--CT", type=float, nargs="*", required=True,
    help="CT")
  parser.add_argument(
    "--CTerr", type=float, nargs="*", required=True,
    help="CT error")
  parser.add_argument(
    "--gr_max", type=float, default=1.5, 
    help="maximum g_r value for comparison stars")
  parser.add_argument(
    "--gr_min", type=float, default=0, 
    help="minimum g_r value for comparison stars")
  parser.add_argument(
    "--yr", default=None, nargs=2,
    help="Y range of magnitude of CTG/CTI corrected colors.")
  args = parser.parse_args()


  key_x, key_y = "x", "y"
  # Set output directory
  outdir = "plot"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  # Selection criteria
  err_th = args.err_th
  eflag_th = args.eflag_th
  gr_min = args.gr_min
  gr_max = args.gr_max
  magmin, magmax = args.magmin, args.magmax

  # Determine color depending on bands
  bands = args.bands
  # For filename
  band_part = "".join(bands)
  # Number of bands
  N_band = len(bands)
  # First band
  b1 = bands[0]

  if N_band==2:
    colors = ["green", "red"]
  elif N_band==3:
    if (bands[2]=="i") or (bands[2]=="I"):
      colors = ["green", "red", "magenta"]
    elif bands[2]=="z":
      colors = ["green", "red", "blueviolet"]
  elif N_band==4:
    # Assume g, r, i, z
    colors = ["green", "red", "magenta", "blueviolet"]


  CTs = args.CT
  CTerrs = args.CTerr

  # Add CTGs and CTGerrs in file name
  CT_part = "CT"
  for x, y in zip(CTs, CTerrs):
     CT_part = (
       CT_part + "f".join(str(x).split(".")) 
       + "_" + "f".join(str(y).split(".")) + "_"
      )
  # Remove a last "_"
  CT_part = CT_part.rstrip("_")

  assert len(CTs)==len(CTerrs), "Invalid length of arguments."


  df = pd.read_csv(args.magcsv, sep=" ")


  # p1 Replot object light curve ==============================================
  print("\n==================================================================")
  print("p1 Replot object light curve")
  print("==================================================================")
  # 1. instrumental magnitude
  #    ex) t_sec_g, mag_g_inst, magerr_g_inst
  # 2. CT ignored magnitude
  #    ex) t_sec_g, mag_g_simple, magerr_g_simple
  # 3. CT corrected magnitude
  #    ex) t_sec_g, g, gerr

  fig = plt.figure(figsize=(16, 20))
  ax1 = fig.add_axes([0.15, 0.70, 0.5, 0.20])
  ax2 = fig.add_axes([0.15, 0.50, 0.5, 0.20])
  ax3 = fig.add_axes([0.15, 0.30, 0.5, 0.20])
  ax4 = fig.add_axes([0.15, 0.10, 0.5, 0.20])
  ax1_hist = fig.add_axes([0.75, 0.70, 0.20, 0.20])
  ax2_hist = fig.add_axes([0.75, 0.50, 0.20, 0.20])
  ax3_hist = fig.add_axes([0.75, 0.30, 0.20, 0.20])
  ax4_hist = fig.add_axes([0.75, 0.10, 0.20, 0.20])

  for ax in [ax1, ax2, ax3]:
    ax.axes.xaxis.set_visible(False)

  ax1.set_ylabel("Relative [mag]")
  ax2.set_ylabel("Pan-STARRS [mag]")
  ax3.set_ylabel("Pan-STARRS [mag]")
  ax4.set_ylabel("Mag. Diff. [mag]")
  ax4.set_xlabel("Time [sec]")
  for ax in [ax1_hist, ax2_hist, ax3_hist]:
    ax.set_ylabel("Mag. Error [mag]")
  
  # For plot range
  sigma = 5
  nbin = 20
  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)
    mark = mymark[idx]
    #  c_mean = np.mean(df[f"{band_l}_{band_r}_inst"])
    #  c_std = np.std(df[f"{band_l}_{band_r}_inst"])

    # 1. instrumental magnitude
    ax1.errorbar(
      df[f"t_sec_{band}"], df[f"mag_{band}_inst"], 
      df[f"magerr_{band}_inst"], label=f"{band} inst.",
      ms=1, lw=1, fmt="o", marker=mark)
    # Histogram of error
    # Limit to 5 sigma
    med, std = np.median(df[f"magerr_{band}_inst"]), np.std(df[f"magerr_{band}_inst"])
    ymin, ymax = 0, med+sigma*std
    prange = (ymin, ymax)
    ax1_hist.hist(
      df[f"magerr_{band}_inst"],
      orientation="horizontal", histtype="step", bins=nbin, range=prange,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    ax1_hist.set_ylim([ymin, ymax])

    # 2. CT ignored magnitude (consider only magzpt)
    ax2.errorbar(
      df[f"t_sec_{band}"], df[f"{band}_simple"], 
      df[f"{band}err_simple"], label=f"{band} CT ignored",
      ms=1, lw=1, fmt="o", marker=mark)
    # Limit to 5 sigma
    med, std = np.median(df[f"{band}err_simple"]), np.std(df[f"{band}err_simple"])
    ymin, ymax = 0, med+sigma*std
    # Histogram of error
    ax2_hist.hist(
      df[f"{band}err_simple"],
      orientation="horizontal", histtype="step", bins=nbin, range=prange,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    ax2_hist.set_ylim([ymin, ymax])


    # 3. CT corrected magnitude
    ax3.errorbar(
      df[f"t_sec_{band}"], df[f"{band}"], 
      df[f"{band}err"], label=f"{band} CT corrected.",
      ms=1, lw=1, fmt="o", marker=mark)
    # Limit to 5 sigma
    med, std = np.median(df[f"{band}err"]), np.std(df[f"{band}err"])
    ymin, ymax = 0, med+sigma*std
    # Histogram of error
    ax3_hist.hist(
      df[f"{band}err"],
      orientation="horizontal", histtype="step", bins=nbin, range=prange,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    ax3_hist.set_ylim([ymin, ymax])
    


    # 4. Magnitude difference (CT corrected - CT ignored)
    diff = df[f"{band}"] - df[f"{band}_simple"]
    differr = adderr_series(
      df[f"{band}err"], df[f"{band}err_simple"])
    ax4.errorbar(
      df[f"t_sec_{band}"], diff, differr, 
      label=f"{band} CT corrected - CT ignored",
      ms=1, lw=0.5, fmt="o", marker=mark)

    # Plot mean and std lines in sub plot
    diff_median, diff_std = np.median(diff), np.std(diff)
    diff_min, diff_max = diff_median - sigma*diff_std, diff_median + sigma*diff_std
    # Add a histogram of residuals
    ax4_hist.hist(
      diff, 
      orientation="horizontal", histtype="step",
      label=f"{band} (median,std)=({diff_median:.4f},{std:.4f})")
    ax4_hist_xmax = ax4_hist.get_xlim()[1]
    # mean
    #ax4_hist.hlines(diff_mean, 0, ax4_hist_xmax, ls=myls[0], color=mycolor[idx])
    # std
    #ax4_hist.hlines(diff_mean-diff_std, 0, ax4_hist_xmax, ls=myls[1], color=mycolor[idx])
    #ax4_hist.hlines(diff_mean+diff_std, 0, ax4_hist_xmax, ls=myls[2], color=mycolor[idx])
    ax4_hist.set_ylim([diff_min, diff_max])
    ax4_hist.set_xlim([0, ax4_hist_xmax])

    # Break this process when CTs==1 and N_band==2
    if len(CTs)==1:
      break
  

  # Select yrange
  if args.yr:
    ymin, ymax = args.yr
    ax2.set_ylim([ymin, ymax])
    ax3.set_ylim([ymin, ymax])

  for ax in [ax1, ax2, ax3, ax4]:
    ax.invert_yaxis()
  for ax in fig.axes:
    ax.legend(fontsize=10)

  # Align axis
  x, y = -0.10, 0.5
  ax1.yaxis.set_label_coords(x, y)
  ax2.yaxis.set_label_coords(x, y)
  ax3.yaxis.set_label_coords(x, y)
  ax4.yaxis.set_label_coords(x, y)

  out = f"{args.obj}_CTcormaglc_N{len(df)}_{band_part}_{CT_part}.png"
  out = os.path.join(outdir, out)
  fig.savefig(out, dpi=200)
  # Plot object color light curve finish ======================================


  # p3 Obtain common objID from input DataFrame ===============================
  print("\n==================================================================")
  print("p3 Obtain common objID from input DataFrame start")
  print("==================================================================")
  # 18 is length of objID in Pan-STARRS catalog
  # objID is extracted from XX__(objid)__YY
  column = df.columns.tolist()
  print(f"  Column number(~30 / object): {len(column)}")
  # list of objID for all bands
  objID_all = []
  for band in bands:
    col_band = [col for col in column if (len(col))>18 and f"_{band}" in col]
    objID_band = [col.split("_")[1] for col in col_band]
    objID_band = set(objID_band)
    objID_all.append(objID_band)
  # Extract common objID 
  objID = objID_all[0]
  for n in range(N_band-1):
    objID = objID & objID_all[n+1]
  print(f"  Common objects N_obj={len(objID)}")
  objID = list(objID)
  objID = sorted(objID)
  # Obtain common objID from input DataFrame finish ===========================


  # p4 Remove red comparison stars from df ====================================
  print("\n==================================================================")
  print("p4 Remove red comparison stars from df")
  print("==================================================================")
  rm_list = []
  column = df.columns.tolist()
  # Use g-r color 
  for obj in objID:
    df_red = df[
      (df[f"gMeanPSFMag_{obj}_{b1}"]-df[f"rMeanPSFMag_{obj}_{b1}"]) > gr_max]
    if len(df_red)!=0:
      # Extract info. from first detection
      idx_1st = df_red.index[0]
      x = df_red.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
      y = df_red.at[idx_1st, f"{key_y}_{obj}_{b1}"]
      print(
        f"  Remove red (g-r > {gr_max}) object : "
        f"{obj} (x,y) = ({x:.1f},{y:.1f})"
      )
      # Remove red object in df
      col_red = [col for col in column if obj in col]
      df = df.drop(col_red, axis=1)
      rm_list.append(obj)
  # Remove red object in objID
  for x in rm_list:
    objID.remove(x)

  if len(rm_list)==0:
    print(f"  No red (g-r > {gr_max}) object is detected.")
  # Remove red comparison stars from df finish ================================

  # p5 Remove blue comparison stars from df ====================================
  print("\n==================================================================")
  print("p5 Remove blue comparison stars from df")
  print("==================================================================")
  rm_list = []
  column = df.columns.tolist()
  for obj in objID:
    df_blue = df[
      (df[f"gMeanPSFMag_{obj}_{b1}"]-df[f"rMeanPSFMag_{obj}_{b1}"]) < gr_min]
    if len(df_blue)!=0:
      # Extract info. from first detection
      idx_1st = df_blue.index[0]
      x = df_blue.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
      y = df_blue.at[idx_1st, f"{key_y}_{obj}_{b1}"]
      print(
        f"  Remove blue (g-r < {gr_min}) object : "
        f"{obj} (x,y) = ({x:.1f},{y:.1f})"
      )
      # Remove blue object in df
      col_blue = [col for col in column if obj in col]
      df = df.drop(col_blue, axis=1)
      rm_list.append(obj)
  # Remove red object in objID
  for x in rm_list:
    objID.remove(x)

  if len(rm_list)==0:
    print(f"  No blue (g-r < {gr_min}) object is detected.")
  # Remove red comparison stars from df finish ================================


  # p6 Remove large magnitude error comparison stars from df ==================
  print("\n==================================================================")
  print("p6 Remove large magnitude error comparison stars from df")
  print("==================================================================")
  rm_list = []
  column = df.columns.tolist()
  for b in bands:
    for obj in objID:
      df_rm = df[df[f"{b}MeanPSFMagErr_{obj}_{b1}"] > err_th]
      if len(df_rm)!=0:
        # Extract info. from first detection
        idx_1st = df_rm.index[0]
        x = df_rm.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
        y = df_rm.at[idx_1st, f"{key_y}_{obj}_{b1}"]
        # Remove large error objects
        print(
          f"  Remove large magnitude error (> {err_th} > mag)object : "
          f"{obj} (x,y) = ({x:.1f},{y:.1f})")
        col_err = [col for col in column if obj in col]
        df = df.drop(col_err, axis=1)
        rm_list.append(obj)


  for x in rm_list:
    objID.remove(x)

  if len(rm_list)==0:
    print(
      f"  No large magnitude error (> {err_th} mag) "
      f"object is detected.")
  # Remove large magnitude error comparison stars from df finish ==============


  # p7 Remove comparison stars from df by magnitude ===========================
  print("\n==================================================================")
  print("p7 Remove comparison stars from df by magnitude")
  print("==================================================================")
  rm_list = []
  column = df.columns.tolist()
  # Note :
  # magmin is not used here (used in p7) since some detections may have
  # mag == 0 when objects are outside the fov.
  for b in bands:
    for obj in objID:
      
      # Use only catalog mag 
      df_rm = df[(df[f"{b}MeanPSFMag_{obj}_{b1}"] > magmax) ]
      #print(f"N_rm (catalog mag ) : {len(df_rm)}")
      if len(df_rm)!=0:
        # Extract info. from first detection
        idx_1st = df_rm.index[0]
        x = df_rm.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
        y = df_rm.at[idx_1st, f"{key_y}_{obj}_{b1}"]
        # Remove large error objects
        print(f"  Remove an object by mag: {obj} (x,y) = ({x:.1f},{y:.1f})")
        #df = df.drop(col_err, axis=1)
        rm_list.append(obj)

  # Convert a list to a set to avoid removal of duplicated objects
  rm_set = set(rm_list)
  for x in rm_set:
    objID.remove(x)

  if len(rm_list)==0:
    print(
      f"  No large magnitude (> {magmax} mag) "
      f"object is detected.")
  # Remove comparison stars from df by magnitude finish =======================

  # p2 Plot photometric status ================================================
  print("\n==================================================================")
  print("p1 Replot object light curve")
  print("==================================================================")
  # Create 
  #   Cat mag vs. CT ignored mag 
  #               (calculated with magzpt of each band (g_Z_simple etc.))
  # Use only 
  #   gMeanPSFMag_146990215466415692_g 
  #   gMeanPSFMagErr_146990215466415692_g 
  #   flux_146990215466415692_g
  #   fluxerr_146990215466415692_g

  # Useless ? 
  fig = plt.figure(figsize=(14, 12 ))
  ax1 = fig.add_axes([0.12, 0.70, 0.4, 0.23])
  ax2 = fig.add_axes([0.12, 0.40, 0.4, 0.23])
  ax3 = fig.add_axes([0.12, 0.10, 0.4, 0.23])
  ax1_res = fig.add_axes([0.6, 0.70, 0.15, 0.23])
  ax2_res = fig.add_axes([0.6, 0.40, 0.15, 0.23])
  ax3_res = fig.add_axes([0.6, 0.10, 0.15, 0.23])
  # Normalized residuals 
  ax1_res_norm = fig.add_axes([0.8, 0.70, 0.15, 0.23])
  ax2_res_norm = fig.add_axes([0.8, 0.40, 0.15, 0.23])
  ax3_res_norm = fig.add_axes([0.8, 0.10, 0.15, 0.23])

  ax1.set_ylabel("Observed magnitude [mag]")
  ax2.set_ylabel("Observed magnitude [mag]")
  ax3.set_ylabel("Observed magnitude [mag]")
  ax3.set_xlabel("Catalog magnitude [mag]")
  ax1_res.set_xlabel("Catalog-Observed [mag]")
  ax2_res.set_xlabel("Catalog-Observed [mag]")
  ax3_res.set_xlabel("Catalog-Observed [mag]")
  ax1_res_norm.set_xlabel("Norm C-O [mag]")
  ax2_res_norm.set_xlabel("Norm C-O [mag]")
  ax3_res_norm.set_xlabel("Norm C-O [mag]")

  axes = [ax1, ax2, ax3]
  axes_res = [ax1_res, ax2_res, ax3_res]
  axes_res_norm = [ax1_res_norm, ax2_res_norm, ax3_res_norm]
  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)
    mag_list, magerr_list, magzpt_list = [], [], []
    magzpt_list, magzpterr_list, flux_list, fluxerr_list = [], [], [], []
    nframe_list = []
    # For loop of frames
    for n in range(len(df)):
      # Use n-th frame dataframe
      df_n = df.loc[n:n]
      col_n = df_n.columns.tolist()
      for obj in objID:
        #magzpt = df_n.filter(like=f"{band}_Z_simple", axis=1)
        #magzpterr = df_n.filter(like=f"{band}_Zerr_simple", axis=1)
        magzpt = df_n.at[n, f"{band}_Z_simple"]
        magzpterr = df_n.at[n, f"{band}_Zerr_simple"]
        col_obj = [col for col in column if (len(col))>18 and obj in col]
        df_obj = df_n[col_obj]
        # Extract info. of an object
        catmag = df_obj.at[n, f"{band}MeanPSFMag_{obj}_{band}"]
        catmagerr = df_obj.at[n, f"{band}MeanPSFMagErr_{obj}_{band}"]
        flux = df_obj.at[n, f"flux_{obj}_{band}"]
        fluxerr = df_obj.at[n, f"fluxerr_{obj}_{band}"]
        #mag = df_obj.filter(like=f"{band}MeanPSFMag_{obj}_{band}", axis=1)
        #magerr = df_obj.filter(like=f"{band}MeanPSFMagErr_{obj}_{band}", axis=1)
        #flux = df_obj.filter(like=f"flux_{obj}_{band}", axis=1)
        #fluxerr = df_obj.filter(like=f"fluxerr_{obj}_{band}", axis=1)

        #mag = mag[mag.columns[0]].values[0]
        #magerr = magerr[magerr.columns[0]].values[0]
        #magzpt = magzpt[magzpt.columns[0]].values[0]
        #magzpterr = magzpterr[magzpterr.columns[0]].values[0]
        #flux = flux[flux.columns[0]].values[0]
        #fluxerr = fluxerr[fluxerr.columns[0]].values[0]
        if flux > 0:
          print(f"mag,magerr,magzpt,flux,fluxerr={catmag},{catmagerr},{magzpt},{flux},{fluxerr}")
          mag_list.append(catmag)
          magerr_list.append(catmagerr)
          magzpt_list.append(magzpt)
          magzpterr_list.append(magzpterr)
          flux_list.append(flux)
          fluxerr_list.append(fluxerr)
          nframe_list.append(n+1)
    
    # cat mag. vs. CT ignored mag.
    magcor_list = [Z-2.5*np.log10(F) for Z,F in zip(magzpt_list, flux_list)]
    # Error of CT ignored mag
    magcorerr_list = [
      adderr(err1, log10err(val2, err2)) for err1,err2,val2 in 
      zip(magzpterr_list, fluxerr_list, flux_list)]
    # Difference (cat mag. - CT ignored mag.)
    diff_list = [x-y for x,y in zip(magcor_list, mag_list)]
    # Error of difference
    differr_list = [adderr(err1,err2) for err1,err2 in zip(magcorerr_list, magerr_list)]

    axes[idx].scatter(
      mag_list, magcor_list, s=5, label=f"{band} Nframe={len(df)}")
    # Histogram of difference
    # Limit to 5 sigma
    med, std = np.median(diff_list), np.std(diff_list)
    #ymin, ymax = 0, med+sigma*std
    #prange = (ymin, ymax)
    nbin = 20
    axes_res[idx].hist(
      diff_list, histtype="step", bins=nbin,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    
    # Normalized histograms
    diff_norm_list = [x/y for x,y in zip(diff_list, differr_list)]
    med, std = np.median(diff_norm_list), np.std(diff_norm_list)
    axes_res_norm[idx].hist(
      diff_norm_list,
      histtype="step", bins=nbin, density=True,
      #range=prange,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    # Normal distribution
    mu, sigma = 0, 1
    label_norm = f"Normal (mu,std)=({mu}, {sigma})"
    xmin, xmax = axes_res_norm[idx].get_xlim()
    xr = np.arange(xmin, xmax, 0.1) 
    axes_res_norm[idx].plot(
      xr, 1/(sigma * np.sqrt(2 * np.pi)) * np.exp( - (xr - mu)**2 / 
      (2 * sigma**2) ), lw=2, color="green", label=label_norm, marker="")

    # Plot y=x
    xmin, xmax = axes[idx].get_xlim()
    x = np.arange(xmin, xmax, 0.1)
    y = x
    axes[idx].plot(x, y, label="y=x", ls=myls[1], color="black", marker="", ms=3)
    axes[idx].legend()
    axes_res[idx].legend()
    axes_res_norm[idx].legend()
  out = f"{args.obj}_refstatus_N{len(df)}_{band_part}.png"
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=200)
  # Plot photometric status finish ============================================


  # p3 Plot photometric status by y coordinates ===============================
  print("\n==================================================================")
  print("p1 Replot object light curve by y coordinates")
  print("==================================================================")
  # Create 
  #   Cat mag vs. CT ignored mag 
  #               (calculated with magzpt of each band (g_Z_simple etc.))
  # Use only 
  #   gMeanPSFMag_146990215466415692_g 
  #   gMeanPSFMagErr_146990215466415692_g 
  #   flux_146990215466415692_g
  #   fluxerr_146990215466415692_g

  # Useless ? 
  fig = plt.figure(figsize=(12, 12 ))
  ax1 = fig.add_axes([0.12, 0.70, 0.6, 0.23])
  ax2 = fig.add_axes([0.12, 0.40, 0.6, 0.23])
  ax3 = fig.add_axes([0.12, 0.10, 0.6, 0.23])
  ax1_res = fig.add_axes([0.75, 0.70, 0.2, 0.23])
  ax2_res = fig.add_axes([0.75, 0.40, 0.2, 0.23])
  ax3_res = fig.add_axes([0.75, 0.10, 0.2, 0.23])


  ax1.set_ylabel("Observed magnitude [mag]")
  ax2.set_ylabel("Observed magnitude [mag]")
  ax3.set_ylabel("Observed magnitude [mag]")
  ax3.set_xlabel("Catalog magnitude [mag]")

  # Residuals
  ax1_res.set_xlabel("Catalog-Observed [mag]")
  ax2_res.set_xlabel("Catalog-Observed [mag]")
  ax3_res.set_xlabel("Catalog-Observed [mag]")

  axes = [ax1, ax2, ax3]
  axes_res = [ax1_res, ax2_res, ax3_res]
  # Create yranges
  nx, ny = 2160, 1280
  n_y = 3
  yranges = []
  yunit = ny/n_y
  for i in range(n_y):
    ymin, ymax = i*yunit, (i+1)*yunit
    yranges.append((ymin, ymax))

  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)
    mag_list, magerr_list, magzpt_list = [], [], []
    magzpt_list, flux_list, fluxerr_list = [], [], []
    nframe_list, y_list = [], []
    # For loop of frames
    for n in range(len(df)):
      # Use n-th frame dataframe
      df_n = df.loc[n:n]
      col_n = df_n.columns.tolist()
      for obj in objID:
        magzpt = df_n.at[n, f"{band}_Z_simple"]
        magzpterr = df_n.at[n, f"{band}_Zerr_simple"]
        col_obj = [col for col in column if (len(col))>18 and obj in col]
        df_obj = df_n[col_obj]
        # Extract info. of an object
        catmag = df_obj.at[n, f"{band}MeanPSFMag_{obj}_{band}"]
        catmagerr = df_obj.at[n, f"{band}MeanPSFMagErr_{obj}_{band}"]
        flux = df_obj.at[n, f"flux_{obj}_{band}"]
        fluxerr = df_obj.at[n, f"fluxerr_{obj}_{band}"]
        y = df_obj.at[n, f"y_{obj}_{band}"]

        if flux > 0:
          print(f"mag,magerr,magzpt,flux,fluxerr={catmag},{catmagerr},{magzpt},{flux},{fluxerr}")
          mag_list.append(catmag)
          magerr_list.append(catmagerr)
          magzpt_list.append(magzpt)
          flux_list.append(flux)
          fluxerr_list.append(fluxerr)
          nframe_list.append(n+1)
          y_list.append(y)
    
    # cat mag. vs. CT ignored mag.
    magcor_list = [Z-2.5*np.log10(F) for Z,F in zip(magzpt_list, flux_list)]
    axes[idx].scatter(
      mag_list, magcor_list, s=5, label=f"{band} Nframe={len(df)}")
    # cat mag. - CT ignored mag.
    diff_list = [mc-mo for mc,mo in zip(magcor_list, mag_list)]
    # Histogram of difference
    med, std = np.median(diff_list), np.std(diff_list)
    #ymin, ymax = 0, med+sigma*std
    #prange = (ymin, ymax)
    nbin = 20

    # All
    axes_res[idx].hist(
      diff_list,
      orientation="horizontal", histtype="step", bins=nbin,
      color="black",
      #range=prange,
      label=f"{band} (mu,std)=({med:.4f},{std:.4f})")
    # Color by y range
    hist_label, hist_diff, hist_c = [], [], []
    for j,yr in enumerate(yranges):
      ymin, ymax = np.int(np.ceil(yr[0])), np.int(np.ceil(yr[1]))
      mag_list_yr = [mo for mo,y in zip(mag_list, y_list) if (ymin < y < ymax)]
      magcor_list_yr = [mc for mc,y in zip(magcor_list, y_list) if (ymin < y < ymax)]
      diff_list_yr = [mc-mo for mc,mo in zip(magcor_list_yr, mag_list_yr)]
      med, std = np.median(diff_list_yr), np.std(diff_list_yr)
      axes[idx].scatter(
        mag_list_yr, magcor_list_yr, s=5, color=mycolor[j], 
        label=f"{band} y ({ymin}--{ymax})")

      label=f"y ({ymin}--{ymax}) (mu,std)=({med:.4f},{std:.4f})"
      hist_label.append(label)
      hist_diff.append(diff_list_yr)
      hist_c.append(mycolor[j])
    # Bar stacked 
    axes_res[idx].hist(
      hist_diff,
      orientation="horizontal", histtype="barstacked", bins=nbin, color=hist_c,
      ls=myls[idx+1],label=hist_label)

    # Plot y=x
    xmin, xmax = axes[idx].get_xlim()
    x = np.arange(xmin, xmax, 0.1)
    y = x
    axes[idx].plot(x, y, label="y=x", ls=myls[1], color="black", marker="", ms=3)
    axes[idx].legend()
    axes_res[idx].legend()
  out = f"{args.obj}_refstatus_N{len(df)}_{band_part}_yrange.png"
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=200)
  # Plot photometric status by y coordinates finish ===========================


  # p4 Plot photometric status by color =======================================
  print("\n==================================================================")
  print("p4 Plot object light curve by color")
  print("==================================================================")
  # Create 
  #   Cat mag vs. CT ignored mag 
  #               (calculated with magzpt of each band (g_Z_simple etc.))
  # Use only 
  #   gMeanPSFMag_146990215466415692_g 
  #   gMeanPSFMagErr_146990215466415692_g 
  #   flux_146990215466415692_g
  #   fluxerr_146990215466415692_g

  fig = plt.figure(figsize=(12, 12 ))
  ax1 = fig.add_axes([0.12, 0.70, 0.6, 0.23])
  ax2 = fig.add_axes([0.12, 0.40, 0.6, 0.23])
  ax3 = fig.add_axes([0.12, 0.10, 0.6, 0.23])
  ax1_res = fig.add_axes([0.75, 0.70, 0.2, 0.23])
  ax2_res = fig.add_axes([0.75, 0.40, 0.2, 0.23])
  ax3_res = fig.add_axes([0.75, 0.10, 0.2, 0.23])
  ax1.set_ylabel("Observed magnitude [mag]")
  ax2.set_ylabel("Observed magnitude [mag]")
  ax3.set_ylabel("Observed magnitude [mag]")
  ax3.set_xlabel("Catalog magnitude [mag]")
  ax1_res.set_xlabel("Catalog-Observed [mag]")
  ax2_res.set_xlabel("Catalog-Observed [mag]")
  ax3_res.set_xlabel("Catalog-Observed [mag]")
  axes = [ax1, ax2, ax3]
  axes_res = [ax1_res, ax2_res, ax3_res]

  # Define color
  mag1_band, mag2_band = "g", "r"

  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)
    mag_list, magerr_list, magzpt_list = [], [], []
    magzpt_list, flux_list, fluxerr_list = [], [], []
    nframe_list, col_list = [], []
    # For loop of frames
    for n in range(len(df)):
      # Use n-th frame dataframe
      df_n = df.loc[n:n]
      col_n = df_n.columns.tolist()
      for obj in objID:
        magzpt = df_n.at[n, f"{band}_Z_simple"]
        magzpterr = df_n.at[n, f"{band}_Zerr_simple"]
        col_obj = [col for col in column if (len(col))>18 and obj in col]
        df_obj = df_n[col_obj]
        # Extract info. of an object
        catmag = df_obj.at[n, f"{band}MeanPSFMag_{obj}_{band}"]
        catmagerr = df_obj.at[n, f"{band}MeanPSFMagErr_{obj}_{band}"]
        flux = df_obj.at[n, f"flux_{obj}_{band}"]
        fluxerr = df_obj.at[n, f"fluxerr_{obj}_{band}"]
        mag1 = df_obj.at[n, f"{mag1_band}MeanPSFMag_{obj}_{mag1_band}"]
        mag2 = df_obj.at[n, f"{mag2_band}MeanPSFMag_{obj}_{mag2_band}"]
        print(f"mag1, mag2 = {mag1}, {mag2}")
        col = mag1 - mag2
        if (flux > 0) and (mag1 > 0) and (mag2 > 0):
          print(f"mag,magerr,magzpt,flux,fluxerr={catmag},{catmagerr},{magzpt},{flux},{fluxerr}")
          mag_list.append(catmag)
          magerr_list.append(catmagerr)
          magzpt_list.append(magzpt)
          flux_list.append(flux)
          fluxerr_list.append(fluxerr)
          nframe_list.append(n+1)
          col_list.append(col)
    
    # Search color range !
    colmin, colmax = 0, 1.5
    colmin, colmax = np.min(col_list), np.max(col_list)
    print(f"Col range: {colmin:.2f}--{colmax:.2f}")
    n_c = 3
    cranges = []
    cunit = (colmax-colmin)/n_c
    # Create color ranges (cranges)
    for i in range(n_c):
      cmin, cmax = colmin + i*cunit, colmin + (i+1)*cunit
      cranges.append((cmin, cmax))
    

    # cat mag. vs. CT ignored mag.
    magcor_list = [Z-2.5*np.log10(F) for Z,F in zip(magzpt_list, flux_list)]
    axes[idx].scatter(
      mag_list, magcor_list, s=5, label=f"{band} Nframe={len(df)}")
    # cat mag. - CT ignored mag.
    diff_list = [mc-mo for mc,mo in zip(magcor_list, mag_list)]
    # Histogram of difference
    med, std = np.median(diff_list), np.std(diff_list)
    #ymin, ymax = 0, med+sigma*std
    #prange = (ymin, ymax)
    nbin = 20

    # All
    axes_res[idx].hist(
      diff_list,
      orientation="horizontal", histtype="step", bins=nbin,
      color="black",
      #range=prange,
      label=f"{band} (mu,std)=({med:.4f},{std:.4f})")
    # Color by color (mag1-mag2)
    hist_label, hist_diff, hist_c = [], [], []
    for j,cr in enumerate(cranges):
      cmin, cmax = cr[0], cr[1]
      mag_list_cr = [mo for mo,c in zip(mag_list, col_list) if (cmin <= c <= cmax)]
      magcor_list_cr = [mc for mc,c in zip(magcor_list, col_list) if (cmin <= c <= cmax)]
      diff_list_cr = [mc-mo for mc,mo in zip(magcor_list_cr, mag_list_cr)]
      med, std = np.median(diff_list_cr), np.std(diff_list_cr)
      axes[idx].scatter(
        mag_list_cr, magcor_list_cr, s=5, color=mycolor[j], 
        label=f"{mag1_band}-{mag2_band} ({cmin:.2f}--{cmax:.2f})")

      label=f"{mag1_band}-{mag2_band} ({cmin:.2f}--{cmax:.2f}) (mu,std)=({med:.4f},{std:.4f})"
      hist_label.append(label)
      hist_diff.append(diff_list_cr)
      hist_c.append(mycolor[j])
    # Bar stacked 
    axes_res[idx].hist(
      hist_diff,
      orientation="horizontal", histtype="barstacked", bins=nbin, color=hist_c,
      ls=myls[idx+1],label=hist_label)

    # Plot y=x
    xmin, xmax = axes[idx].get_xlim()
    x = np.arange(xmin, xmax, 0.1)
    y = x
    axes[idx].plot(x, y, label="y=x", ls=myls[1], color="black", marker="", ms=3)
    axes[idx].legend()
    axes_res[idx].legend()
  out = f"{args.obj}_refstatus_N{len(df)}_{band_part}_{mag1_band}{mag2_band}color.png"
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=200)
  # Plot photometric status by color ==========================================
