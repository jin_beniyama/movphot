#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Output CTG.
"""
import os 
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  


if __name__ == "__main__":
  parser = ap(description="Output CTG")
  parser.add_argument(
    "csv", type=str, help="CTG....csv")
  parser.add_argument(
    "type", type=str, choices=["r", "w", "MC"],
    help="raw or weighted, or Monte Carlo")
  args = parser.parse_args()


  df = pd.read_csv(args.csv, sep=" ")
  
  # Assume g, r, and i
  if args.type=="r":
    CTG = f"{df.at[0, 'g_r_CTG_r']} {df.at[0, 'r_i_CTG_r']} {df.at[0, 'g_i_CTG_r']}"
    CTGerr = f"{df.at[0, 'g_r_CTGerr_r']} {df.at[0, 'r_i_CTGerr_r']} {df.at[0, 'g_i_CTGerr_r']}"
  if args.type=="w":
    CTG = f"{df.at[0, 'g_r_CTG_w']} {df.at[0, 'r_i_CTG_w']} {df.at[0, 'g_i_CTG_w']}"
    CTGerr = f"{df.at[0, 'g_r_CTGerr_w']} {df.at[0, 'r_i_CTGerr_w']} {df.at[0, 'g_i_CTGerr_w']}"
  if args.type=="MC":
    CTG = f"{df.at[0, 'g_r_CTG_MC']} {df.at[0, 'r_i_CTG_MC']} {df.at[0, 'g_i_CTG_MC']}"
    CTGerr = f"{df.at[0, 'g_r_CTGerr_MC']} {df.at[0, 'r_i_CTGerr_MC']} {df.at[0, 'g_i_CTGerr_MC']}"

  print(
    f"--CTG {CTG} --CTGerr {CTGerr}")
