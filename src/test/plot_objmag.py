#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot magnitude light curves of an object using 
  the ColorTern (CT) estimated in `get_CT.py` 
and 
  the object color estimated in plot_objcolor.py.


Output Examples
---------------
2021TY14_CTcormaglc_N4_gri_CT*.png
2021TY14_Zstatus_N4_gri_CT*.png


Summary of Procedures
---------------------
(p1, p2, ... are usefull search words.)
  p1  Read photometric csv 
      (photometry_result.csv, 
       and 
       objcolors.csv (CTG and CTI corrected object color).
  p2  Add CT to the DataFrame
  p3  Obtain common objID from input DataFrame
  p4  Remove red comparison stars from df 
  p5  Remove blue comparison stars from df 
  p6  Remove large magnitude error comparison stars from df
  p7  Remove comparison stars from df by magnitude
  p8  Calculate instrumental magnitude of the target 
  p9  Calculate Zs of each frame 
  p10 Remove Z=0 data
  p11 Calculate object magnitude using CTs and Zs
  p12 Remove outliers (nan,inf,larg error/eflag)
  p13 Calculate time in second
  p14 Calculate mean and weighted mean mag
  p15 Plot object light curve 
  p16 Save photometric result csv
"""

import os 
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  
import matplotlib
matplotlib.use('Agg')


from movphot.movphot.common import (
    get_filename, adderr, adderr_series, diverr, mulerr, log10err, linear_fit, 
    linear_fit_Z, linear_fit_Z_fixslope)
from movphot.movphot.visualization import myfigure_objmag as myfigure
from movphot.movphot.scripts.prepro import (
    clean_photres, calc_d_from_edge, calc_CTG, plot_CTG)


if __name__ == "__main__":
  parser = ap(description="Color determination")
  parser.add_argument(
    "photres", type=str, help="photometry_result.csv")
  parser.add_argument(
    "colres", type=str, help="objcolor.csv")
  parser.add_argument(
    "obj", type=str, help="object name")
  parser.add_argument(
    "magtype", type=str, choices=["SDSS", "PS"],
    help="griz magnitude type of input csv")
  parser.add_argument(
    "--bands", nargs="*", default=["g", "r", "i"],
    help="3 bands")
  parser.add_argument(
    "--magmax", type=float, default=20,
    help="maximum magnitude")
  parser.add_argument(
    "--magmin", type=float, default=12,
    help="minimum magnitude")
  parser.add_argument(
    "--err_th", type=float, default=1, 
    help="maximum magnitude error value for both object and comparison stars")
  parser.add_argument(
    "--eflag_th", type=float, default=1, 
    help="maximum eflag value for both object and comparison stars")
  parser.add_argument(
    "--CT", type=float, nargs="*", required=True,
    help="CT")
  parser.add_argument(
    "--CTerr", type=float, nargs="*", required=True,
    help="CT error")
  parser.add_argument(
    "--gr_max", type=float, default=1.5, 
    help="maximum g_r value for comparison stars")
  parser.add_argument(
    "--gr_min", type=float, default=0, 
    help="minimum g_r value for comparison stars")
  parser.add_argument(
    "--yr", default=None, nargs=2,
    help="Y range of magnitude of CTG/CTI corrected colors.")
  args = parser.parse_args()


  # For old version
  key_x, key_y = "x", "y"
  key_x, key_y = "x1", "y1"
  key_x, key_y = "x", "y"


  # Set output directory
  outdir = "plot"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  # Selection criteria
  err_th = args.err_th
  eflag_th = args.eflag_th
  gr_min = args.gr_min
  gr_max = args.gr_max
  magmin, magmax = args.magmin, args.magmax

  # Determine color depending on bands
  bands = args.bands
  # For filename
  band_part = "".join(bands)
  # Number of bands
  N_band = len(bands)
  # First band
  b1 = bands[0]

  if N_band==2:
    colors = ["green", "red"]
  elif N_band==3:
    if (bands[2]=="i") or (bands[2]=="I"):
      colors = ["green", "red", "magenta"]
    elif bands[2]=="z":
      colors = ["green", "red", "blueviolet"]
  elif N_band==4:
    # Assume g, r, i, z
    colors = ["green", "red", "magenta", "blueviolet"]


  CTs = args.CT
  CTerrs = args.CTerr

  # Add CTGs and CTGerrs in file name
  CT_part = "CT"
  for x, y in zip(CTs, CTerrs):
     CT_part = (
       CT_part + "f".join(str(x).split(".")) 
       + "_" + "f".join(str(y).split(".")) + "_"
      )
  # Remove a last "_"
  CT_part = CT_part.rstrip("_")

  assert len(CTs)==len(CTerrs), "Invalid length of arguments."

  # p1 Read photometric csv ==================================================
  print("\n==================================================================")
  print("p1 Read photometric csv start")
  print("==================================================================")
  filename = get_filename(args.photres)
  df = pd.read_csv(args.photres, sep=" ")
  print(f"DataFrame Dimention (original) {len(df)}")
  N_nan = np.sum(df.isnull().sum())
  # Do not remove 'nan' here.
  # Some 'nan' arise from ... where? (see movphot.movphot etc.)
  if N_nan !=0:
    print(f"not a number is found and removed. N_nan={N_nan}")
    df = df.fillna(0)
    print(f"DataFrame Dimention (after nan removal) {len(df)}")

  # Read objcolor estimated in `plot_objcolor.py`.
  df_col = pd.read_csv(args.colres, sep=" ")
  print(f"DataFrame Dimention (color) {len(df_col)}")

  assert len(df) == len(df_col), "Check the input two csvs."

  # Concatenate df and df_col
  df = pd.concat([df,df_col],axis=1)
  # Drop duplicated columns (t_mjd_g etc.)
  df =df.loc[:,~df.columns.duplicated()]
  
  
  # Match dimensions
  df = df[df_col["eflag_color"]==0]
  print(f"DataFrame Dimension (final) {len(df)}")
  df = df.reset_index(drop=True)

  # Read photometric csv ======================================================

  # p2 Add CT to the DataFrame ===============================================
  print("\n==================================================================")
  print("p2 Add CT to the DataFrame")
  print("==================================================================")
  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)
    CT = CTs[idx]
    CTerr = CTerrs[idx]
    CT_list = [CT]*len(df)
    CTerr_list = [CTerr]*len(df)

    s_CT = pd.Series(CT_list, name="CT")
    s_CTerr = pd.Series(CTerr_list, name="CTerr")
    # Do not insert when already exists
    try:
      df.insert(0, f"{band_l}_{band_r}_CT", s_CT)
      df.insert(0, f"{band_l}_{band_r}_CTerr", s_CTerr)
    except:
      pass
    
    # Break this process when CTs==1 and N_band==2
    if len(CTs)==1:
      break
  # Add CT to the DataFrame ==================================================


  # p3 Obtain common objID from input DataFrame ===============================
  print("\n==================================================================")
  print("p3 Obtain common objID from input DataFrame start")
  print("==================================================================")
  # 18 is length of objID in Pan-STARRS catalog
  # objID is extracted from XX__(objid)__YY
  column = df.columns.tolist()
  print(f"  Column number(~30 / object): {len(column)}")
  # list of objID for all bands
  objID_all = []
  for band in bands:
    col_band = [col for col in column if (len(col))>18 and f"_{band}" in col]
    objID_band = [col.split("_")[1] for col in col_band]
    objID_band = set(objID_band)
    objID_all.append(objID_band)
  # Extract common objID 
  objID = objID_all[0]
  for n in range(N_band-1):
    objID = objID & objID_all[n+1]
  print(f"  Common objects N_obj={len(objID)}")
  objID = list(objID)
  objID = sorted(objID)
  # Obtain common objID from input DataFrame finish ===========================


  # p4 Remove red comparison stars from df ====================================
  print("\n==================================================================")
  print("p4 Remove red comparison stars from df")
  print("==================================================================")
  rm_list = []
  column = df.columns.tolist()
  # Use g-r color 
  for obj in objID:
    df_red = df[
      (df[f"gMeanPSFMag_{obj}_{b1}"]-df[f"rMeanPSFMag_{obj}_{b1}"]) > gr_max]
    if len(df_red)!=0:
      # Extract info. from first detection
      idx_1st = df_red.index[0]
      x = df_red.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
      y = df_red.at[idx_1st, f"{key_y}_{obj}_{b1}"]
      print(
        f"  Remove red (g-r > {gr_max}) object : "
        f"{obj} (x,y) = ({x:.1f},{y:.1f})"
      )
      # Remove red object in df
      col_red = [col for col in column if obj in col]
      df = df.drop(col_red, axis=1)
      rm_list.append(obj)
  # Remove red object in objID
  for x in rm_list:
    objID.remove(x)

  if len(rm_list)==0:
    print(f"  No red (g-r > {gr_max}) object is detected.")
  # Remove red comparison stars from df finish ================================


  # p5 Remove blue comparison stars from df ====================================
  print("\n==================================================================")
  print("p5 Remove blue comparison stars from df")
  print("==================================================================")
  rm_list = []
  column = df.columns.tolist()
  for obj in objID:
    df_blue = df[
      (df[f"gMeanPSFMag_{obj}_{b1}"]-df[f"rMeanPSFMag_{obj}_{b1}"]) < gr_min]
    if len(df_blue)!=0:
      # Extract info. from first detection
      idx_1st = df_blue.index[0]
      x = df_blue.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
      y = df_blue.at[idx_1st, f"{key_y}_{obj}_{b1}"]
      print(
        f"  Remove blue (g-r < {gr_min}) object : "
        f"{obj} (x,y) = ({x:.1f},{y:.1f})"
      )
      # Remove blue object in df
      col_blue = [col for col in column if obj in col]
      df = df.drop(col_blue, axis=1)
      rm_list.append(obj)
  # Remove red object in objID
  for x in rm_list:
    objID.remove(x)

  if len(rm_list)==0:
    print(f"  No blue (g-r < {gr_min}) object is detected.")
  # Remove red comparison stars from df finish ================================


  # p6 Remove large magnitude error comparison stars from df ==================
  print("\n==================================================================")
  print("p6 Remove large magnitude error comparison stars from df")
  print("==================================================================")
  column = df.columns.tolist()
  for b in bands:
    rm_list = []
    for obj in objID:
      df_rm = df[df[f"{b}MeanPSFMagErr_{obj}_{b1}"] > err_th]
      if len(df_rm)!=0:
        # Extract info. from first detection
        idx_1st = df_rm.index[0]
        x = df_rm.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
        y = df_rm.at[idx_1st, f"{key_y}_{obj}_{b1}"]
        # Remove large error objects
        print(
          f"  Remove large magnitude error (> {err_th} > mag)object : "
          f"{obj} (x,y) = ({x:.1f},{y:.1f})")
        col_err = [col for col in column if obj in col]
        df = df.drop(col_err, axis=1)
        rm_list.append(obj)

    for x in rm_list:
      objID.remove(x)
    # Break this process when CTGs==1 and N_band==2
    if len(CTs)==1:
      break


  if len(rm_list)==0:
    print(
      f"  No large magnitude error (> {err_th} mag) "
      f"object is detected.")
  # Remove large magnitude error comparison stars from df finish ==============


  # p7 Remove comparison stars from df by magnitude ===========================
  print("\n==================================================================")
  print("p7 Remove comparison stars from df by magnitude")
  print("==================================================================")
  rm_list = []
  column = df.columns.tolist()
  # Note :
  # magmin is not used here (used in p7) since some detections may have
  # mag == 0 when objects are outside the fov.
  for b in bands:
    for obj in objID:
      
      # Use only catalog mag 
      df_rm = df[(df[f"{b}MeanPSFMag_{obj}_{b1}"] > magmax) ]
      #print(f"N_rm (catalog mag ) : {len(df_rm)}")
      if len(df_rm)!=0:
        # Extract info. from first detection
        idx_1st = df_rm.index[0]
        x = df_rm.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
        y = df_rm.at[idx_1st, f"{key_y}_{obj}_{b1}"]
        # Remove large error objects
        print(f"  Remove an object by mag: {obj} (x,y) = ({x:.1f},{y:.1f})")
        #df = df.drop(col_err, axis=1)
        rm_list.append(obj)

  # Convert a list to a set to avoid removal of duplicated objects
  rm_set = set(rm_list)
  for x in rm_set:
    objID.remove(x)

  if len(rm_list)==0:
    print(
      f"  No large magnitude (> {magmax} mag) "
      f"object is detected.")
  # Remove comparison stars from df by magnitude finish =======================


  # p8 Calculate instrumental magnitude of the target =========================
  print("\n==================================================================")
  print("p8 Calculate instrumental magnitude of the target")
  print("==================================================================")
  for band in bands:
    # 1. instrumental magnitude
    # To avoid RuntimeWarning: divide by zero encountered in log10 when f=0
    s_maginst = [-2.5*np.log10(f) if f > 0 else 0 for f in df[f"flux_{band}"]]
    s_maginsterr = 2.5*log10err(
      df[f"flux_{band}"], df[f"fluxerr_{band}"])
    df.insert(0, f"mag_{band}_inst", s_maginst)
    df.insert(0, f"magerr_{band}_inst", s_maginsterr) 
  # Calculate instrumental magnitude of the target finish =====================


  # p9 Calculate Zs of each frame ===========================================
  # Objects which have NaN or elag in their DataFrame are not removed in p8,
  # but are removed in p11.
  print("\n==================================================================")
  print("p9 Calculate Zs of each frame")
  print("==================================================================")
  
  x0, x1, y0, y1 = 200, 1960, 200, 1080
  x0, x1, y0, y1 = 320, 1840, 320, 960
  # axes_CTfit : 
  #   CT corrected m_cat-m_inst vs. catalog color (mag_l-mag_r) 
  # axes_CTfit_Z : 
  #   CT corrected Z light curves
  # axes_fit : 
  #   CT ignored m_cat-m_inst vs. catalog color (mag_l-mag_r) 
  # axes_fit_Z : 
  #   CT ignored Z light curves (fitting)
  # axes_hist : 
  #   histogram of Z
  # axes_hist_Z : 
  #   CT ignored Z light curves (histogram)

  fig, axes_CTfit, axes_CTfit_Z, axes_fit, axes_fit_Z, axes_hist, axes_hist_Z = myfigure(len(bands))
  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)
    print(f"  {band}band (band_l,band_r) = ({band_l},{band_r})")

    # For figures
    ax_CTfit = axes_CTfit[idx]
    ax_CTfit_Z = axes_CTfit_Z[idx]
    ax_fit = axes_fit[idx]
    ax_fit_Z = axes_fit_Z[idx]
    ax_hist = axes_hist[idx]
    ax_hist_Z = axes_hist_Z[idx]

    color = mycolor[idx]
    mark = mymark[idx]

    # Lists for fitting
    # CT corrected Z
    Z_list, Zerr_list = [],[]
    # CT ignored Z (fitting)
    Z_fit_list, Zerr_fit_list = [],[]
    Z_fit_MC_list, Zerr_fit_MC_list = [],[]
    # CT ignored Z (histogram)
    Z_simple_list, Zerr_simple_list = [],[]
    xmin_CT_list, xmax_CT_list = [], []
    ymin_CT_list, ymax_CT_list = [], []
    xmin_fit_list, xmax_fit_list = [], []
    ymin_fit_list, ymax_fit_list = [], []
    
    # Loop for frames
    for n in range(len(df)):
      cat_sub_inst_list, cat_sub_insterr_list = [], []
      col_cat_list, col_caterr_list = [], []
      mag_cat_list, magerr_cat_list = [], []
      mag_inst_list, magerr_inst_list = [], []
      # Use n-th frame dataframe
      df_n = df.loc[n:n]
      col_n = df_n.columns.tolist()

      # Use flux, mag_cat, mag_cat_l, mag_cat_r (+ errors), and eflag
      for obj in objID:
        col_obj = [col for col in column if (len(col))>18 and obj in col]
        df_obj = df_n[col_obj]

        # Extract info. of an object
        catmag = df_obj.at[n, f"{band}MeanPSFMag_{obj}_{band}"]
        catmag_l = df_obj.at[n, f"{band_l}MeanPSFMag_{obj}_{band}"]
        catmag_r = df_obj.at[n, f"{band_r}MeanPSFMag_{obj}_{band}"]

        # Use Catalog magnitude to judge and skip 0 filled object 
        # [Check!]
        if catmag==0 or catmag_l==0 or catmag_r==0:
          #print(f"Skip {obj} in frame {n}")
          continue

        # Extract info. of an object (Continued)
        catmagerr = df_obj.at[n, f"{band}MeanPSFMagErr_{obj}_{band}"]
        flux = df_obj.at[n, f"flux_{obj}_{band}"]
        fluxerr = df_obj.at[n, f"fluxerr_{obj}_{band}"]

        catmagerr_l = df_obj.at[n, f"{band_l}MeanPSFMagErr_{obj}_{band}"]
        flux_l = df_obj.at[n, f"flux_{obj}_{band_l}"]
        fluxerr_l = df_obj.at[n, f"fluxerr_{obj}_{band_l}"]
        x_l = df_obj.at[n, f"{key_x}_{obj}_{band_l}"]
        y_l = df_obj.at[n, f"{key_y}_{obj}_{band_l}"]
        eflag_l = df_obj.at[n, f"eflag_{obj}_{band_l}"]

        catmagerr_r = df_obj.at[n, f"{band_r}MeanPSFMagErr_{obj}_{band}"]
        flux_r = df_obj.at[n, f"flux_{obj}_{band_r}"]
        fluxerr_r = df_obj.at[n, f"fluxerr_{obj}_{band_r}"]
        x_r = df_obj.at[n, f"{key_x}_{obj}_{band_r}"]
        y_r = df_obj.at[n, f"{key_y}_{obj}_{band_r}"]
        eflag_r = df_obj.at[n, f"eflag_{obj}_{band_r}"]

        # # To judge whether large error
        # maginsterr_l = 2.5*log10err(flux_l, fluxerr_l)
        # maginsterr_r = 2.5*log10err(flux_r, fluxerr_r)

        # Do not use bad data
        if ((flux > 0) and (flux_l > 0) and (flux_r > 0)
           and (magmin < catmag < magmax) 
           and (x0 < x_l < x1) and (y0 < y_l < y1)
           and (x0 < x_r < x1) and (y0 < y_r < y1)
           and (eflag_l < eflag_th) and (eflag_r < eflag_th)):
           #and (maginsterr_l < err_th) and (maginsterr_r < err_th)):


          # cat_sub_inst (m_cat - m_inst)
          cat_sub_inst = catmag_l - (-2.5*np.log10(flux_l))
          instmagerr_l = 2.5*log10err(flux_l, fluxerr_l)
          cat_sub_insterr = adderr(catmagerr_l, instmagerr_l)
          
          mag_inst = -2.5*np.log10(flux)
          cat_sub_inst = catmag - mag_inst
          magerr_inst = 2.5*log10err(flux, fluxerr)
          cat_sub_insterr = adderr(catmagerr, magerr_inst)

          cat_sub_inst_list.append(cat_sub_inst)
          cat_sub_insterr_list.append(cat_sub_insterr)
          
          mag_inst_list.append(mag_inst)
          magerr_inst_list.append(magerr_inst)
          mag_cat_list.append(catmag)
          magerr_cat_list.append(catmagerr)

          # col_cat (mag_l - mag_r)
          col_cat = catmag_l - catmag_r
          col_caterr = adderr(catmagerr_l, catmagerr_r)
          col_cat_list.append(col_cat)
          col_caterr_list.append(col_caterr)
            
      # If Z is not estimated due to the lack of reference stars,
      # add 0 to the dataframe to keep the length of DataFrame
      if len(col_cat_list)==0:
        Z_list.append(0)
        Zerr_list.append(0)
        Z_simple_list.append(0)
        Zerr_simple_list.append(0)
        print(
        f"    {band}-band, {n}-th frame Z is not estimated and "
        f"skip the frame."
        )
        continue

      # Create dataframe for handling
      df_temp = pd.DataFrame(
        {"mag_inst":mag_inst_list,
         "magerr_inst":magerr_inst_list,
         "mag_cat":mag_cat_list,
         "magerr_cat":magerr_cat_list,
         "col_cat":col_cat_list,
         "col_caterr":col_caterr_list,
         "cat_sub_inst":cat_sub_inst_list,
         "cat_sub_insterr":cat_sub_insterr_list})

      # Remove large error
      df_temp = df_temp[(df_temp["col_caterr"] < err_th) 
              & (df_temp["cat_sub_insterr"] < err_th)]

      # If Z is not estimated due to the lack of reference stars,
      # add 0 to the dataframe to keep the length of DataFrame
      if len(df_temp)==0:
        Z_list.append(0)
        Zerr_list.append(0)
        Z_simple_list.append(0)
        Zerr_simple_list.append(0)
        print(
        f"    {band}-band, {n}-th frame Z is not estimated and "
        f"skip the frame."
        )
        continue 
      
      # Z, CT consider ========================================================
      # Estimate Z using ref stars in fov
      # Weighted fit  with cat_sub_insterr (not col_cat, small one)
      _CT = CTs[idx]
      param_w, cov_w = curve_fit(
        linear_fit_Z, df_temp["col_cat"], df_temp["cat_sub_inst"], 
        sigma=df_temp["cat_sub_insterr"], absolute_sigma=True)
      paramerr_w = np.sqrt(np.diag(cov_w))
      Z_w = param_w[0]
      Zerr_w = paramerr_w[0]
      print(
        f"    {band}-band, {n}-th frame Z wei = {Z_w:.2f}+-{Zerr_w:.2f}")
      # Use wide range
      x = np.arange(-2, 5, 0.01)
      ## Fitting curve
      ax_CTfit.plot(
        x, _CT*x+Z_w, marker="", lw=0.3, ls=myls[2], color="black"
        )
      # Z, CT consider finish =================================================


      ## Z, CT ignore (fitting) ===============================================
      # Estimate Z using ref stars in fov fixing the slope=1
      # m_inst = m_cat - Z
      # Weighted fit  with magerr_inst (not mag_cat, small one)
      param_w, cov_w = curve_fit(
        linear_fit_Z_fixslope, df_temp["mag_inst"], df_temp["mag_cat"], 
        sigma=df_temp["magerr_inst"], absolute_sigma=True)
      paramerr_w = np.sqrt(np.diag(cov_w))
      Z_fit = param_w[0]
      Zerr_fit = paramerr_w[0]
      Z_fit_list.append(Z_fit)
      Zerr_fit_list.append(Zerr_fit)
      
      # Use mean and std of Monte Carlo results
      N_mc = 100
      # Set seed 0
      seed = 0
      np.random.seed(seed)
      Z_temp_list = []
      for n in range(N_mc):
        # Initialization
        mag_inst_sample = np.random.normal(
          df_temp["mag_inst"], df_temp["magerr_inst"], len(df_temp))
        mag_cat_sample = np.random.normal(
          df_temp["mag_cat"], df_temp["magerr_cat"], len(df_temp))

        param_sample, cov_sample = curve_fit(
          linear_fit_Z_fixslope, mag_inst_sample, mag_cat_sample, 
          sigma=df_temp["magerr_inst"], absolute_sigma=True)
        Z_sample = param_sample[0]
        Z_temp_list.append(Z_sample)
      
      Z_mean_MC = np.mean(Z_temp_list)
      Z_std_MC = np.std(Z_temp_list)
      print(f"  MC mean, std = {Z_mean_MC}, {Z_std_MC}")

      Z_fit_MC_list.append(Z_mean_MC)
      Zerr_fit_MC_list.append(Z_std_MC)
      # Use wide range
      x = np.arange(10, 25, 0.01)
      
      ## Fitting curve
      ax_fit.plot(
        x, x-Z_fit, marker="", lw=0.3, ls=myls[2], color="black"
        )
      ## Z, CT ignore (fitting) finish ========================================

      ## Z, CT ignore (histogram) =================================================
      # Estimate simple Z from `m_cat = -2.5log10(F_ref) + Z`
      #                        `Z = m_cat - m_inst`
      Z_median = np.median(df_temp["cat_sub_inst"])
      # Use median deviation as uncertainty
      Z_median_dev = np.median(abs(df_temp["cat_sub_inst"]-Z_median))
      Z_simple_list.append(Z_median)
      Zerr_simple_list.append(Z_median_dev)
      label = (
        f"{n:04d} N={len(df_temp)}, (median,dev)=({Z_median},{Z_median_dev})")
      label= None
      ax_hist.hist(
        df_temp["cat_sub_inst"], histtype="step", label=label)
      ## Z, CT ignore (histogram) finish =========================================


      ## col vs. m_cat - m_inst ===============================================
      ax_CTfit.errorbar(
        df_temp["col_cat"], df_temp["cat_sub_inst"], 
        xerr=df_temp["col_caterr"], yerr=df_temp["cat_sub_insterr"],
        fmt="o", linewidth=0.2, ms=1, color=color, marker=mark)
      
      # For xlim and ylim setting
      xmin, xmax = np.min(df_temp["col_cat"]), np.max(df_temp["col_cat"])
      ymin, ymax = np.min(df_temp["cat_sub_inst"]), np.max(df_temp["cat_sub_inst"])
      xmin_CT_list.append(xmin)
      xmax_CT_list.append(xmax)
      ymin_CT_list.append(ymin)
      ymax_CT_list.append(ymax)

      xmin, xmax = np.min(df_temp["mag_cat"]), np.max(df_temp["mag_cat"])
      ymin, ymax = np.min(df_temp["mag_inst"]), np.max(df_temp["mag_inst"])
      xmin_fit_list.append(xmin)
      xmax_fit_list.append(xmax)
      ymin_fit_list.append(ymin)
      ymax_fit_list.append(ymax)


      # Add to the dataframe
      Z_list.append(Z_w)
      Zerr_list.append(Zerr_w)
      ## col vs. m_cat - m_inst finish ========================================

      ## cat_mag vs. m_inst ===================================================
      ax_fit.errorbar(
        df_temp["mag_cat"], df_temp["mag_inst"], 
        xerr=df_temp["magerr_cat"], yerr=df_temp["magerr_inst"],
        fmt="o", linewidth=0.2, ms=1, color=color, marker=mark)
      ## cat_mag vs. m_inst finish ============================================

    s_Z = pd.Series(Z_list, name="Z")
    s_Zerr = pd.Series(Zerr_list, name="Zerr")
    s_Z_fit = pd.Series(Z_fit_list, name="Z_fit")
    s_Zerr_fit = pd.Series(Zerr_fit_list, name="Zerr_fit")
    s_Z_fit_MC = pd.Series(Z_fit_MC_list, name="Z_fit_MC")
    s_Zerr_fit_MC = pd.Series(Zerr_fit_MC_list, name="Zerr_fit_MC")
    s_Z_simple = pd.Series(Z_simple_list, name="Z_simple")
    s_Zerr_simple = pd.Series(Zerr_simple_list, name="Zerr_simple")

    # Do not insert when already exists
    try:
      df.insert(0, f"{band}_Z", s_Z)
      df.insert(0, f"{band}_Zerr", s_Zerr)
      df.insert(0, f"{band}_Z_fit", s_Z_fit)
      df.insert(0, f"{band}_Zerr_fit", s_Zerr_fit)
      df.insert(0, f"{band}_Z_fit_MC", s_Z_fit_MC)
      df.insert(0, f"{band}_Zerr_fit_MC", s_Zerr_fit_MC)
      df.insert(0, f"{band}_Z_simple", s_Z_simple)
      df.insert(0, f"{band}_Zerr_simple", s_Zerr_simple)

    except:
      pass

    col_cat_label = "$m_{cat," + band_l + "} - m_{cat," + band_r + "}$"
    cat_sub_inst_label = "$m_{cat," + band_l + "} - m_{inst," + band_l + "}$"
    ax_CTfit.set_xlabel(col_cat_label)
    ax_CTfit.set_ylabel(cat_sub_inst_label)

    mag_cat_label = "$m_{cat," + band + "}$"
    mag_inst_label = "$m_{inst," + band + "}$"
    ax_fit.set_xlabel(mag_cat_label)
    ax_fit.set_ylabel(mag_inst_label)

    ax_hist.set_xlabel("Magnitude Zero point")
    ax_hist.set_ylabel("N")

    # CT corrected Z
    ax_CTfit_Z.set_xlabel("Frames")
    ax_CTfit_Z.set_ylabel("CT corrected Z")
    ax_CTfit_Z.errorbar(
      df.index.values, df[f"{band}_Z"], 
      yerr=df[f"{band}_Zerr"],
      fmt="o", linewidth=0.2, ms=1, color=color)

    # CT ignored Z (fit)
    ax_fit_Z.set_xlabel("Frames")
    ax_fit_Z.set_ylabel("CT ignored (fitting)")
    # Error1: return of scipy (do not consider xerr (mag_cat error)!)
    ax_fit_Z.errorbar(
      df.index.values, df[f"{band}_Z_fit"], 
      yerr=df[f"{band}_Zerr_fit"],
      fmt="^", linewidth=0.1, ms=0.5, color="gray")
    # Error2: Monte Carlo Method
    ax_fit_Z.errorbar(
      df.index.values, df[f"{band}_Z_fit"], 
      yerr=df[f"{band}_Zerr_fit_MC"],
      fmt="o", linewidth=0.2, ms=1, color=color)
    
    # CT ignored Z (histogram)
    ax_hist_Z.set_ylabel("N")
    ax_hist_Z.set_xlabel("Frames")
    ax_hist_Z.set_ylabel("CT ignored Z (histogram)")
    ax_hist_Z.errorbar(
      df.index.values, df[f"{band}_Z_simple"], 
      yerr=df[f"{band}_Zerr_simple"],
      fmt="o", linewidth=0.2, ms=1, color=color)


    xmin, xmax = np.min(xmin_CT_list), np.max(xmax_CT_list)
    ymin, ymax = np.min(ymin_CT_list), np.max(ymax_CT_list)
    # Margin in percent
    margin = 10
    x_w, y_w = xmax-xmin, ymax-ymin
    xmin -= (margin/100)*x_w
    xmax += (margin/100)*x_w
    ymin -= (margin/100)*y_w
    ymax += (margin/100)*y_w
    ax_CTfit.set_xlim([xmin, xmax])
    ax_CTfit.set_ylim([ymin, ymax])
 
    xmin, xmax = np.min(xmin_fit_list), np.max(xmax_fit_list)
    ymin, ymax = np.min(ymin_fit_list), np.max(ymax_fit_list)
    # Margin in percent
    margin = 10
    x_w, y_w = xmax-xmin, ymax-ymin
    xmin -= (margin/100)*x_w
    xmax += (margin/100)*x_w
    ymin -= (margin/100)*y_w
    ymax += (margin/100)*y_w
    ax_fit.set_xlim([xmin, xmax])
    ax_fit.set_ylim([ymin, ymax])

    ax_CTfit.legend(fontsize=10)
    ax_CTfit_Z.legend(fontsize=10)
    ax_fit.legend(fontsize=10)
    ax_fit_Z.legend(fontsize=10)
    ax_hist.legend(fontsize=10)
    ax_hist_Z.legend(fontsize=10)

    # Add title 
    if idx==0:
      ax_CTfit.set_title("Color Term Corrected")
      ax_fit.set_title("Color Term Ignored (fitting)")
      ax_hist.set_title("Color Term Ignored (histogram)")


  band_str = "".join(bands)
  out =  f"{args.obj}_Zstatus_N{len(df)}_{band_str}_{CT_part}.png"
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=200)
  plt.close()

  # Break this process when CTs==1 and N_band==2
  if len(CTs)==1:
    sys.exit()
  # Calculate Zs of each frame finish ========================================
   

  # p10 Remove Z=0 data =====================================================
  print("\n==================================================================")
  print("p10 Remove Z=0 data")
  print("==================================================================")
  for b in bands:
    band_l, band_r = band4cterm(b, bands)
    N0 = len(df)
    df = df[df[f"{band}_Z"] != 0]
    N1 = len(df)
    print(f"  Remove N={N0-N1} ({b}-band Z = 0)")
    # Break this process when CTs==1 and N_band==2
    if len(CTs)==1:
      break
  # Remove Z=0 data finish ==================================================
  

  # p11 Calculate object color using CTs and Zs ===============================
  print("\n==================================================================")
  print("p11 Calculate object color using CTs and Zs")
  print("==================================================================")
  # 1. CT, Z corrected mag
  #    from m_cat - m_inst = color_cat*CT + Z
  #    m_cat = color_cat*CT + Z + m_inst
  #    ex) g, gerr

  # 2. CT ignored mag
  #    from m_cat - m_inst = Z
  #    m_cat = Z + m_inst
  #    ex) g_simple, gerr_simple

  for band in bands:
    band_l, band_r = band4cterm(band, bands)

    # 1. CT, Z corrected magnitude
    s_mag = (
      df[f"{band_l}_{band_r}"]*df[f"{band_l}_{band_r}_CT"]
      + df[f"{band}_Z"] + df[f"mag_{band_l}_inst"]
      )

    # Calculate error 
    temperr = mulerr(
      df[f"{band_l}_{band_r}"], df[f"{band_l}_{band_r}err"],
    df[f"{band_l}_{band_r}_CT"], df[f"{band_l}_{band_r}_CTerr"]
      )
    s_magerr = adderr_series(
      temperr,
      df[f"{band}_Zerr"],
      df[f"magerr_{band_l}_inst"]
      ) 

    # 2. CT ignored magnidude
    s_mag_simple = (
      df[f"{band}_Z_simple"] + df[f"mag_{band}_inst"])
    s_magerr_simple = adderr_series(
      df[f"{band}_Zerr_simple"], df[f"magerr_{band}_inst"])

    # Do not insert when already exists
    try:
      df.insert(0, f"{band}", s_mag)
      df.insert(0, f"{band}err", s_magerr) 
      df.insert(0, f"{band}_simple", s_mag_simple)
      df.insert(0, f"{band}err_simple", s_magerr_simple) 
    except:
      pass

    # Break this process when CTs==1 and N_band==2
    if len(CTs)==1:
      break
  # Calculate object color using CTs and Zs finish ===========================


  # p12 Remove outliers (nan,inf,larg error/eflag) =============================
  print("\n==================================================================")
  print("p12 Remove outliers (nan,inf,larg error/eflag)")
  print("==================================================================")
  # Convert inf to nan
  df = df.replace([np.inf, -np.inf], np.nan)
  # Drop nan 
  df = df.dropna(how="any", axis=0)
  N_nan = np.sum(df.isnull().sum())
  assert N_nan==0, f"Nan N={N_nan}"
  # Drop by eflag
  N_bef = len(df)
  col_err_th = 1
  for band in bands:
    df = df[df[f"eflag_{band}"] < eflag_th]
    # Break this process when CTGs==1 and N_band==2
    if len(CTs)==1:
      break
  df = df.reset_index(drop=True)
  N_aft = len(df)
  print(f"Final dataframe {N_aft} ({N_bef-N_aft} removed by errflag, eflag)")
  # Check
  N_nan = np.sum(df.isnull().sum())
  assert N_nan==0, f"Nan N={N_nan}"
  # Remove outliers (nan,inf,larg error/eflag) finish =========================


  # p13 Calculate time in second ===============================================
  print("\n==================================================================")
  print("p13 Calculate time in second")
  print("==================================================================")
  for band in bands:
    print(band)
    print(df[f"t_mjd_{band}"]*24*2600)
    df[f"t_sec_{band}"] = df[f"t_mjd_{band}"]*24*3600.
    print(band)
    df[f"t_sec_{band}"] -= df.at[0, f"t_sec_{band}"]
  # Calculate time in second finish ===========================================
  

  # p14 Calculate mean and weighted magnitude =================================
  print("\n==================================================================")
  print("p14 Calculate mean and weighted mean magnitude")
  print("==================================================================")
  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)

    # Average
    c_mean = np.mean(df[f"{band}"])
    # Photometric uncertainty 
    c_std_phot = adderr(df[f"{band}err"])/len(df)
    # Standard Deviation
    c_SD = np.var(df[f"{band}"])
    # Standard error
    c_SE = c_SD/len(df)
    # Total error
    c_std_total = np.sqrt(c_std_phot**2 + c_SE**2)

    # Weighted mean
    w = 1/df[f"{band}err"]**2
    c_wmean = np.average(df[f"{band}"], weights=w)
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(w))

    # Add stacked info 
    df[f"{band}_mean"] = c_mean
    df[f"{band}_std_total"] = c_std_total
    df[f"{band}_std_phot"] = c_std_phot
    df[f"{band}_SD"] = c_SD
    df[f"{band}_SE"] = c_SE

    df[f"{band}_wmean"] = c_wmean
    df[f"{band}_wstd"] = c_wstd

    print(
      f"{band}\n"
      f"  Normal mean : {c_mean:.4f}+-{c_std_total:.4f}\n"
      f"    (std^2 = std_phot^2 + SE^2," 
      f"{c_std_total:.4f}^2={c_std_phot:.4f}^2+{c_SE}^2,\n"
      f"    standard deviation(SD) : {c_SD:.4f})\n"
      f"  Weighted mean : {c_wmean:.4f}+-{c_wstd:.4f}"
      )

  # Calculate mean and weighted mean magnitude finish =========================


  # p15 Replot object light curve ==============================================
  print("\n==================================================================")
  print("p15 Replot object light curve")
  print("==================================================================")
  # 1. instrumental magnitude
  #    ex) t_sec_g, mag_g_inst, magerr_g_inst
  # 2. CT ignored magnitude
  #    ex) t_sec_g, mag_g_simple, magerr_g_simple
  # 3. CT corrected magnitude
  #    ex) t_sec_g, g, gerr

  fig = plt.figure(figsize=(16, 20))
  ax1 = fig.add_axes([0.15, 0.70, 0.5, 0.20])
  ax2 = fig.add_axes([0.15, 0.50, 0.5, 0.20])
  ax3 = fig.add_axes([0.15, 0.30, 0.5, 0.20])
  ax4 = fig.add_axes([0.15, 0.10, 0.5, 0.20])
  ax1_hist = fig.add_axes([0.75, 0.70, 0.20, 0.20])
  ax2_hist = fig.add_axes([0.75, 0.50, 0.20, 0.20])
  ax3_hist = fig.add_axes([0.75, 0.30, 0.20, 0.20])
  ax4_hist = fig.add_axes([0.75, 0.10, 0.20, 0.20])

  for ax in [ax1, ax2, ax3]:
    ax.axes.xaxis.set_visible(False)

  ax1.set_ylabel("Relative [mag]")
  ax2.set_ylabel("Pan-STARRS [mag]")
  ax3.set_ylabel("Pan-STARRS [mag]")
  ax4.set_ylabel("Mag. Diff. [mag]")
  ax4.set_xlabel("Time [sec]")
  for ax in [ax1_hist, ax2_hist, ax3_hist]:
    ax.set_ylabel("Mag. Error [mag]")
  
  # For plot range
  sigma = 5
  nbin = 20
  for idx,band in enumerate(bands):
    band_l, band_r = band4cterm(band, bands)
    mark = mymark[idx]
    #  c_mean = np.mean(df[f"{band_l}_{band_r}_inst"])
    #  c_std = np.std(df[f"{band_l}_{band_r}_inst"])

    # 1. instrumental magnitude
    ax1.errorbar(
      df[f"t_sec_{band}"], df[f"mag_{band}_inst"], 
      df[f"magerr_{band}_inst"], label=f"{band} inst.",
      ms=1, lw=1, fmt="o", marker=mark)
    # Histogram of error
    # Limit to 5 sigma
    med, std = np.median(df[f"magerr_{band}_inst"]), np.std(df[f"magerr_{band}_inst"])
    ymin, ymax = 0, med+sigma*std
    prange = (ymin, ymax)
    ax1_hist.hist(
      df[f"magerr_{band}_inst"],
      orientation="horizontal", histtype="step", bins=nbin, range=prange,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    ax1_hist.set_ylim([ymin, ymax])

    # 2. CT ignored magnitude (consider only magzpt)
    ax2.errorbar(
      df[f"t_sec_{band}"], df[f"{band}_simple"], 
      df[f"{band}err_simple"], label=f"{band} CT ignored",
      ms=1, lw=1, fmt="o", marker=mark)
    # Limit to 5 sigma
    med, std = np.median(df[f"{band}err_simple"]), np.std(df[f"{band}err_simple"])
    ymin, ymax = 0, med+sigma*std
    # Histogram of error
    ax2_hist.hist(
      df[f"{band}err_simple"],
      orientation="horizontal", histtype="step", bins=nbin, range=prange,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    ax2_hist.set_ylim([ymin, ymax])


    # 3. CT corrected magnitude
    ax3.errorbar(
      df[f"t_sec_{band}"], df[f"{band}"], 
      df[f"{band}err"], label=f"{band} CT corrected.",
      ms=1, lw=1, fmt="o", marker=mark)
    # Limit to 5 sigma
    med, std = np.median(df[f"{band}err"]), np.std(df[f"{band}err"])
    ymin, ymax = 0, med+sigma*std
    # Histogram of error
    ax3_hist.hist(
      df[f"{band}err"],
      orientation="horizontal", histtype="step", bins=nbin, range=prange,
      label=f"{band} (median,std)=({med:.4f},{std:.4f})")
    ax3_hist.set_ylim([ymin, ymax])
    


    # 4. Magnitude difference (CT corrected - CT ignored)
    diff = df[f"{band}"] - df[f"{band}_simple"]
    differr = adderr_series(
      df[f"{band}err"], df[f"{band}err_simple"])
    ax4.errorbar(
      df[f"t_sec_{band}"], diff, differr, 
      label=f"{band} CT corrected - CT ignored",
      ms=1, lw=0.5, fmt="o", marker=mark)

    # Plot mean and std lines in sub plot
    diff_median, diff_std = np.median(diff), np.std(diff)
    diff_min, diff_max = diff_median - sigma*diff_std, diff_median + sigma*diff_std
    # Add a histogram of residuals
    ax4_hist.hist(
      diff, 
      orientation="horizontal", histtype="step",
      label=f"{band} (median,std)=({diff_median:.4f},{std:.4f})")
    ax4_hist_xmax = ax4_hist.get_xlim()[1]
    # mean
    #ax4_hist.hlines(diff_mean, 0, ax4_hist_xmax, ls=myls[0], color=mycolor[idx])
    # std
    #ax4_hist.hlines(diff_mean-diff_std, 0, ax4_hist_xmax, ls=myls[1], color=mycolor[idx])
    #ax4_hist.hlines(diff_mean+diff_std, 0, ax4_hist_xmax, ls=myls[2], color=mycolor[idx])
    ax4_hist.set_ylim([diff_min, diff_max])
    ax4_hist.set_xlim([0, ax4_hist_xmax])

    # Break this process when CTs==1 and N_band==2
    if len(CTs)==1:
      break
  

  # Select yrange
  if args.yr:
    ymin, ymax = args.yr
    ax2.set_ylim([ymin, ymax])
    ax3.set_ylim([ymin, ymax])

  for ax in [ax1, ax2, ax3, ax4]:
    ax.invert_yaxis()
  for ax in fig.axes:
    ax.legend(fontsize=10)

  # Align axis
  x, y = -0.10, 0.5
  ax1.yaxis.set_label_coords(x, y)
  ax2.yaxis.set_label_coords(x, y)
  ax3.yaxis.set_label_coords(x, y)
  ax4.yaxis.set_label_coords(x, y)

  out = f"{args.obj}_CTcormaglc_N{len(df)}_{band_part}_{CT_part}.png"
  out = os.path.join(outdir, out)
  fig.savefig(out, dpi=200)
  # Plot object color light curve finish ======================================


  # p16 Save photometric result csv ===========================================
  print("\n==================================================================")
  print("p16 Save photometric result csv")
  print("==================================================================")
  # Save neo magnitude in PS system

  out = f"{args.obj}_magall_{band_part}_{CT_part}.csv"
  df.to_csv(out, sep=" ", index=False)
  # Save photometric result csv finish  ======================================== 
