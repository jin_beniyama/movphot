#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot photometric precision of a star in field of view.
A target star is corrected by the flux of the reference star.

Create normalized/raw flux light curves
x axis : time in sec
y axis : relative flux = flux_star / flux_bright

Objects in edge regions are removed.

Summary of Procedures
---------------------
(p1, p2, ... are usefull search words.)
  p1  Read photometric csv
  p2  Obtain 3-bands common objID (XX__(objid)__XX) from DataFrame 
  p3  Calculate time in second and save for periodic analysis 
  p4  Plot normalized flux
  p5  Plot raw flux
"""

import os 
import sys
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from myplot import mycolor, mymark, myls, myfigure, myfigure_ver

# Functions ==================================================================


def linear_fit(x, a, b):
  """Linear fit function for scipy.optimize.
  """
  return a*x + b


def adderr(*args):
  """Calculate additional error.

  Parameters
  ----------
  args : array-like
    list of values

  Return
  ------
  err : float
    calculated error
  """
  err = np.sqrt(np.sum(np.square(args)))
  return err


def adderr_series(*args):
  """Add error of multiple pandas.Series.

  Parameters
  ----------
  args : array-like
    list of pandas.Series 

  Return
  ------
  err_s : pandas.Series
    single pandas.Series of calculated error
  """ 
  for i,x in enumerate(args):
    assert type(x) is pd.core.frame.Series, "Input should be Series."
    #assert type(x)==type(pd.Series()), "Sould be Series"
    if i==0:
      temp = x.map(np.square)
    else:
      temp += x.map(np.square)
  err_s = temp.map(np.sqrt)
  return err_s


def log10err(val, err):
  """Calculate log10 error.
  """
  return err/val/np.log(10)



def band4cterm(band, bands):
  """Return 2 bands for color term determination.
  
  Parameters
  ----------
  band : str
    band of fits
  bands : str
    3 bands

  Returns
  -------
  mag_l, mag_r : float
    used magnitude
  """

  if bands == ["g", "r", "i"]:

    #if band=="g":
    #  mag_l, mag_r = "g", "r"
    #elif band=="r":
    #  mag_l, mag_r = "g", "r"
    #elif band=="i":
    #  mag_l, mag_r = "g", "i"
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "i"
    elif band=="i":
      mag_l, mag_r = "g", "i"

  elif bands == ["g", "r", "z"]:
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "z"
    elif band=="z":
      mag_l, mag_r = "g", "z"

  return mag_l, mag_r


def calc_inst_color(df, bands, magkwd):
  """Calculate instrumental color.

  Parameters
  ----------
  df : pandas.DataFrame
    input DataFrame
  bands : list
    bands in DataFrame
  magkwd : dict
    magnitude keyword

  Return
  ------
  df : pandas.DataFrame
    instrumental magnitude calculated DataFrame
  """
  if bands == ["g", "r", "i"]:
    df["g_r"] = df[magkwd["g"]] - df[magkwd["r"]]
    df["r_i"] = df[magkwd["r"]] - df[magkwd["i"]]

  if bands == ["g", "r", "z"]:
    df["g_r"] = df[magkwd["g"]] - df[magkwd["r"]]
    df["r_z"] = df[magkwd["r"]] - df[magkwd["z"]]

  return df


def diverr(val1, err1, val2, err2):
  """Calculate error for division.
  
  Parameters
  ----------
  val1 : float or pandas.Series 
    value 1
  err1 : float or pandas.Series 
    error 1
  val2 : float or pandas.Series 
    value 2
  err2 : float or pandas.Series 
    error 2
  """
  return np.sqrt((err1/val2)**2 + (err2*val1/val2**2)**2)


# Functions finish ============================================================


if __name__ == "__main__":
  parser = ap(description="Photometric precision test.")
  parser.add_argument(
    "csv", type=str, help="photres.csv")
  parser.add_argument(
    "--magmin", type=float, default=12,
    help="minimum magnitude")
  parser.add_argument(
    "--magmax", type=float, default=20,
    help="maximum magnitude")
  parser.add_argument(
    "--err_th", type=float, default=1, 
    help="maximum magnitude error value for both object and comparison stars")
  parser.add_argument(
    "--eflag_th", type=float, default=1, 
    help="maximum eflag value for both object and comparison stars")
  parser.add_argument(
    "--bands", nargs=3, default=["g", "r", "i"],
    help="3 bands")
  parser.add_argument(
    "--rawmagrange", nargs=2, type=float, default=None, 
    help="raw mag range")
  parser.add_argument(
    "--rawcolrange", nargs=2, type=float, default=None, 
    help="raw color range")
  parser.add_argument(
    "--target", type=int, default=None, 
    help="target obj ID")
  parser.add_argument(
    "--ref", type=int, default=None, 
    help="reference obj ID")
  args = parser.parse_args()


  # Set output directory
  outdir = "plot"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  # Selection criteria
  err_th = args.err_th
  eflag_th = args.eflag_th
  magmin, magmax = args.magmin, args.magmax

  # Determine color
  bands = args.bands
  if (bands[2]=="i") or (bands[2]=="I"):
    colors = ["green", "red", "magenta"]
  elif bands[2]=="z":
    colors = ["green", "red", "blueviolet"]

 

  # p1 Read photometric csv ==================================================
  print("\n==================================================================")
  print("p1 Read photometric csv start")
  print("==================================================================")
  df = pd.read_csv(args.csv, sep=" ")
  print(f"DataFrame Dimention (original) {len(df)}")
  N_nan = np.sum(df.isnull().sum())
  # Do not remove 'nan' here.
  # Some 'nan' arise from nonsiderial tracking 
  if N_nan !=0:
    print(f"not a number is found and removed. N_nan={N_nan}")
    df = df.fillna(0)
    print(f"DataFrame Dimention (after nan removal) {len(df)}")
  N_nan = np.sum(df.isnull().sum())
  # Read photometric csv ======================================================
  

  # p2 Obtain 3-bands common objID (XX__(objid)__XX) from DataFrame ===========
  print("\n==================================================================")
  print("p2 Obtain 3-bands common objID (XX__(objid)__XX) from DataFrame start")
  print("==================================================================")
  # 18 is length of objID for Pan-STARRS catalog
  column = df.columns.tolist()
  print(f"column number(~30 per object): {len(column)}")
  # list of objID for each bands
  objID_bands = []
  for band in bands:
    col_ref = [col for col in column if (len(col))>18]
    col_band = [col for col in column if (len(col))>18 and f"_{band}" in col]
    objID_band = [col.split("_")[1] for col in col_band]
    objID_band = list(set(objID_band))
    #print(f"objID {band} N={len(objID_band)}")
    objID_bands.append(objID_band)
  # Common objID 
  objID = list(set(objID_bands[0]) & set(objID_bands[1]) & set(objID_bands[2]))
  # Sort for reproducibility 
  objID = sorted(objID)
  # Output
  print(f"3-bands common objects N_obj={len(objID)}")
  # 1st frame list
  frame1st_list = []
  # 1st frame coordinates
  x1st_list = []
  y1st_list = []
  # Count uneffective lines
  N_frame_obj_list = []
  # Magnitude list
  mag_list = []
  for obj in objID:
    # Search 1-band ref star
    col_ref = [col for col in column if obj in col]
    df_temp = df[col_ref]
    df_temp = df_temp[df_temp[f"flux_{obj}_{bands[0]}"]!=0]

    # Add 1st frame
    frame1st_list.append(df_temp.index.tolist()[0])

    df_temp = df_temp.reset_index(drop=True)
    # Add 1st frame coordinates
    x1st_list.append(df_temp.at[0, f"x1_{obj}_{bands[0]}"])
    y1st_list.append(df_temp.at[0, f"y1_{obj}_{bands[0]}"])
    # Add effective frame numbers
    N_frame_obj = len(df_temp)
    N_frame_obj_list.append(N_frame_obj)
    # Add catalog magnitude
    mag_list.append(df_temp.at[0, f"{bands[0]}MeanPSFMag_{obj}_{bands[0]}"])

  N_line = 3
  N_iter = np.int(np.ceil(len(objID)/N_line))
  print(f"objID (N_frame, 1stframe, x, y, {bands[0]}mag)")
  for n in range(N_iter):
    objs = objID[n*N_line:(n+1)*N_line]
    N_frame = N_frame_obj_list[n*N_line:(n+1)*N_line]
    mags = mag_list[n*N_line:(n+1)*N_line]
    frame1st = frame1st_list[n*N_line:(n+1)*N_line]
    x1st = x1st_list[n*N_line:(n+1)*N_line]
    y1st = y1st_list[n*N_line:(n+1)*N_line]
    if len(objs)==1:
        print(
          f"{objs[0]} ({N_frame[0]:4d}, {frame1st[0]:4d}, "
          f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f})")
    elif len(objs)==2:
      print(
        f"{objs[0]} ({N_frame[0]:4d}, {frame1st[0]:4d}, " 
        f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f}) "
        f"{objs[1]} ({N_frame[1]:4d}, {frame1st[1]:4d}, "
        f"{x1st[1]:5.0f}, {y1st[1]:5.0f}, {mags[1]:.1f})")
    else:
      print(
        f"{objs[0]} ({N_frame[0]:4d}, {frame1st[0]:4d}, " 
        f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f}) "
        f"{objs[1]} ({N_frame[1]:4d}, {frame1st[1]:4d}, "
        f"{x1st[1]:5.0f}, {y1st[1]:5.0f}, {mags[1]:.1f}) "
        f"{objs[2]} ({N_frame[2]:4d}, {frame1st[2]:4d}, "
        f"{x1st[2]:5.0f}, {y1st[2]:5.0f}, {mags[2]:.1f})")
  # Obtain reference dataframe and objID finish ================================


  # p3 Calculate time in second and save for periodic analysis ===============
  print("\n==================================================================")
  print("p3 Calculate time in second and save for periodic analysis")
  print("==================================================================")
  for band in bands:
    df[f"t_sec_{band}"] = df[f"t_mjd_{band}"]*24*3600.
    df[f"t_sec_{band}"] -= df.at[0, f"t_sec_{band}"]
  # Calculate time in second and save for periodic analysis finish ============


  # p4. Plot normalized flux ==================================================
  print("\n==================================================================")
  print("p4. Plot normalized flux")
  print("==================================================================")
  if args.target and args.ref:
    fig = plt.figure(figsize=(14, 12))
    ax1 = fig.add_axes([0.15, 0.64, 0.8, 0.27])
    ax2 = fig.add_axes([0.15, 0.37, 0.8, 0.27])
    ax3 = fig.add_axes([0.15, 0.1, 0.8, 0.27])
    axes = [ax1, ax2, ax3]
    target = args.target
    ref = args.ref
    ymin_list, ymax_list = [], []
    print(f"Target ID : {target}")
    print(f"ref    ID : {ref}")
    # Use common frames
    df = df[(df[f"flux_{target}_{bands[0]}"] > 0)
            & (df[f"flux_{target}_{bands[1]}"] > 0)
            & (df[f"flux_{target}_{bands[2]}"] > 0)
            & (df[f"flux_{ref}_{bands[0]}"] > 0)
            & (df[f"flux_{ref}_{bands[1]}"] > 0)
            & (df[f"flux_{ref}_{bands[2]}"] > 0)
            ]

    # Remove edge region 
    x0, x1, y0, y1 = 150, 2010, 150, 1130
    for obj in [target, ref]:
      for b in bands:
        df = df[(df[f"x1_{obj}_{b}"] > x0) 
                & (df[f"x1_{obj}_{b}"] < x1) 
                & (df[f"y1_{obj}_{b}"] > y0) 
                & (df[f"y1_{obj}_{b}"] < y1)] 
        print(len(df))
    df = df.reset_index(drop=True)

    # Target and Reference info
    mag_target, mag_ref = [], []
    b1, b2, b3 = bands
    for b in bands:
      mag_target.append(df.at[0, f"{b}MeanPSFMag_{target}_{b}"])
      mag_ref.append(df.at[0, f"{b}MeanPSFMag_{ref}_{b}"])
    title = (
      f"Target____: ({b1}:{mag_target[0]:.1f}, {b2}:{mag_target[1]:.1f}, "
      f"{b3}:{mag_target[2]:.1f}) {target}\n"
      f"Reference_: ({b1}:{mag_ref[0]:.1f}, {b2}:{mag_ref[1]:.1f}, "
      f"{b3}:{mag_ref[2]:.1f}) {ref}"
      )
    for idx,band in enumerate(bands):
      ax = axes[idx]
      color = mycolor[idx]
      mark = mymark[idx]
      
      flux_target = df[f"flux_{target}_{band}"]
      fluxerr_target = df[f"fluxerr_{target}_{band}"]
      flux_ref = df[f"flux_{ref}_{band}"]
      fluxerr_ref = df[f"fluxerr_{ref}_{band}"]
      
      # Devide by the flux of the reference
      ratio = flux_target/flux_ref
      # Normalize by mean
      ratio_norm = ratio - np.mean(ratio) + 1
      # photometric Error
      ratio_err = diverr(flux_target, fluxerr_target, flux_ref, fluxerr_ref)
      # Total Error (photometric errro + standard deviation)
      err_std = np.std(ratio_norm)
      N_frame = len(ratio_err)
      err_phot = adderr(ratio_err)/N_frame
      print(f"err_std, err_phot = {err_std}, {err_phot}")
      err_total = adderr(
        err_phot, err_std)

      label = f"{band} band (" + "$\sigma=$" + f"{err_total:.4f})"
      label = f"{band}-band N={len(df)}, SD={err_total:.4f}"
      ax.errorbar(
        df[f"t_sec_{band}"], 
        ratio_norm, yerr=ratio_err, label=label,
        fmt="o", linewidth=0.5, color=color, marker=mark)
      ax.set_ylabel("Relative flux")
      ax.legend()
      ymin, ymax = ax.get_ylim()
      ymin_list.append(ymin)
      ymax_list.append(ymax)
    
    ymin = np.min(ymin_list)
    ymax = np.max(ymax_list)
    for ax in axes:
      ax.set_ylim([ymin, ymax])
    ax1.set_title(title)
    ax3.set_xlabel("Time [s]")
    out =  f"phot_precision_target{target}ref{ref}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=300)
    plt.close()
  # Plot normalized flux finish ===============================================


  # p5. Plot raw flux =========================================================
  print("\n==================================================================")
  print("p5. Plot raw flux")
  print("==================================================================")
  if args.target and args.ref:
    fig = plt.figure(figsize=(10, 12))
    ax1 = fig.add_axes([0.15, 0.66, 0.8, 0.28])
    ax2 = fig.add_axes([0.15, 0.38, 0.8, 0.28])
    ax3 = fig.add_axes([0.15, 0.1, 0.8, 0.28])
    axes = [ax1, ax2, ax3]
    target = args.target
    ref = args.ref
    ymin_list, ymax_list = [], []
    title = f"Target: {target} , reference: {ref}"
    # Use common frames
    df = df[(df[f"flux_{target}_{bands[0]}"] > 0)
            & (df[f"flux_{target}_{bands[1]}"] > 0)
            & (df[f"flux_{target}_{bands[2]}"] > 0)
            & (df[f"flux_{ref}_{bands[0]}"] > 0)
            & (df[f"flux_{ref}_{bands[1]}"] > 0)
            & (df[f"flux_{ref}_{bands[2]}"] > 0)
            ]

    for idx,band in enumerate(bands):
      ax = axes[idx]
      
      color = mycolor[idx]
      mark = mymark[idx]
      
      flux_target = df[f"flux_{target}_{band}"]
      fluxerr_target = df[f"fluxerr_{target}_{band}"]
      flux_ref = df[f"flux_{ref}_{band}"]
      fluxerr_ref = df[f"fluxerr_{ref}_{band}"]

      #ratio_norm = ratio - np.mean(ratio) + 1


      label = f"{band} band (" + "$\sigma=$" + f"{err_total:.4f})"
      label = f"{band} band (SD={err_total:.4f})"
      # Target
      ax.errorbar(
        df[f"t_sec_{band}"], flux_target, yerr=fluxerr_target,
        label=f"{band}-band Target",
        fmt="o", linewidth=0.5, color=color, marker=mark)
      # ref
      ax.errorbar(
        df[f"t_sec_{band}"], flux_ref, yerr=fluxerr_ref,
        label=f"{band}-band Reference",
        fmt="o", linewidth=0.5, color="black", marker="D")
      ax.set_ylabel("Raw flux")
      ax.legend()
      ymin, ymax = ax.get_ylim()
      ymin_list.append(ymin)
      ymax_list.append(ymax)
    
    ymin = np.min(ymin_list)
    ymax = np.max(ymax_list)
    #for ax in axes:
    #  ax.set_ylim([ymin, ymax])
    ax1.set_title(title, fontsize=20)
    ax3.set_xlabel("Time [s]")
    out =  f"phot_precision_raw_target{target}ref{ref}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=100)
    plt.close()
  # Plot raw flux finish ======================================================
