#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot light curves of an object at once.
Input files are such as
  `3200_refladder_mag_g_epoch1.csv`, 3200_refladder_mag_g_epoch2.csv.
Use 
  absolute time (t_jd_ltcor) 
  mag (brightness corrected in plot_refladder_total.py).

"""

import os 
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

mycolor = ["#AD002D", "#1e50a2", "#006e54", "#ffd900", "#EFAEA1", 
           "#69821b", "#ec6800", "#afafb0", "#0095b9", "#89c3eb"]*100

mymark = ["o", "^", "x", "D", "+", "v", "<", ">", "h", "H"]*100

# Functions ==================================================================

def adderr(*args):
  """Calculate additional error.

  Parameters
  ----------
  args : array-like
    list of values

  Return
  ------
  err : float
    calculated error
  """
  err = np.sqrt(np.sum(np.square(args)))
  return err


def adderr_series(*args):
  """Add error of multiple pandas.Series.

  Parameters
  ----------
  args : array-like
    list of pandas.Series 

  Return
  ------
  err_s : pandas.Series
    single pandas.Series of calculated error
  """ 
  for i,x in enumerate(args):
    assert type(x) is pd.core.frame.Series, "Input should be Series."
    #assert type(x)==type(pd.Series()), "Sould be Series"
    if i==0:
      temp = x.map(np.square)
    else:
      temp += x.map(np.square)
  err_s = temp.map(np.sqrt)
  return err_s


def log10err(val, err):
  """Calculate log10 error.
  """
  return err/val/np.log(10)


def diverr(val1, err1, val2, err2):
  """Calculate error for division.
  
  Parameters
  ----------
  val1 : float or pandas.Series 
    value 1
  err1 : float or pandas.Series 
    error 1
  val2 : float or pandas.Series 
    value 2
  err2 : float or pandas.Series 
    error 2
  """
  return np.sqrt((err1/val2)**2 + (err2*val1/val2**2)**2)


def mulerr(val1, err1, val2, err2):
   return np.sqrt((val2*err1)**2 + (val1*err2)**2)




if __name__ == "__main__":
  parser = ap(description="Plot light curve at once.")
  parser.add_argument(
    "obj", type=str, help="object name")
  parser.add_argument(
    "csv", type=str, nargs="*", help="photres.csv")
  parser.add_argument(
    "--yr", default=None, type=float, nargs=2,
    help="Y range")
  parser.add_argument(
    "--color_phase", default=None, type=float, nargs=2,
    help="Color arbitrary phase detections")
  parser.add_argument(
    "--JD0", default=2459514.5, type=float,
    help="time zero point in Juliand day (default is 2021-10-27)")
  parser.add_argument(
    "--offset", type=float, nargs="*",
    help="mag offset")
  parser.add_argument(
    "--label", nargs="*",
    help="label in plot")
  parser.add_argument(
    "--period", type=float, required=True, 
    help="rotation period used to fold light curve(s)")
  parser.add_argument(
    "--hour", action="store_true",
    help="rotation period in hour")
  parser.add_argument(
    "--mag", default="", 
    help="magnitude type")
  parser.add_argument(
    "--out", default="out.csv", 
    help="output csv name")
  parser.add_argument(
    "--outpng", default=None, 
    help="output png")
  args = parser.parse_args()
  
  # Set output directory
  outdir = "plot"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  if args.offset:
    assert len(args.csv)==len(args.offset)==len(args.label), "Check the arguments."

  # Rotational period
  rotP = args.period

  # Combined light curve
  fig = plt.figure(figsize=(15,15))
  ax1 = fig.add_axes([0.1, 0.55, 0.85, 0.3])
  ax2 = fig.add_axes([0.1, 0.10, 0.85, 0.3])
  ax1.set_xlabel("Julian day")
  ax1.set_ylabel("Relative magnitude [mag]")
  ax1.invert_yaxis()
  if args.hour:
    ax2.set_xlabel(f"Rotational phase (P = {rotP} hour, JDo(LTC):{args.JD0})")
    # hour to day
    rotP = args.period/24.
  else:
    ax2.set_xlabel(f"Rotational phase (P = {rotP} s, JDo(LTC):{args.JD0})")
    # sec to day
    rotP = args.period/3600./24.

  ax2.set_ylabel(f"{args.mag} magnitude [mag]")
  ax2.invert_yaxis()
  df_all = []
  for idx,csv in enumerate(args.csv):
    df = pd.read_csv(csv, sep=" ")
    
    N_obs = len(df)
    if args.label:
      label = args.label[idx]
    else:
      label = f"obs {idx+1} N={N_obs}"
    if args.offset:
      offset = args.offset[idx]
      if offset == 0:
        label = f"{label} N={N_obs}"
      else:
        label = f"{label} N={N_obs} (offset {offset})"
    else:
      offset = 0


    ax1.errorbar(
      df["t_jd_ltcor"], df["mag"]+offset, df["magerr"], 
      color=mycolor[idx], fmt="o", marker=mymark[idx], ms=5, lw=1, label=label)

    # Set zero point
    JD0 = args.JD0
    df["t_jd_ltcor_cor"] = df["t_jd_ltcor"]-JD0
    x = (df["t_jd_ltcor_cor"]/rotP)%1
    df["mag"] = df["mag"] + offset
    ax2.errorbar(
      x, df["mag"], df["magerr"], color=mycolor[idx], fmt="o", ms=5, lw=1, 
      marker=mymark[idx], capsize=0, label=label)
    df_all.append(df)

    # Color detections in arbitrary phase
    if args.color_phase:
      p0, p1 = args.color_phase
      print(f"Color by phase: ({p0}, {p1})")
      in_phase = (p0 < x) & (x < p1)
      x_color =  x[in_phase]
      y_color=  df["mag"][in_phase]
      yerr_color=  df["magerr"][in_phase]
      
      # x axis: phase
      label = f"Selected N={len(x_color)} ({p0} < phase < {p1})"
      ax2.errorbar(
        x_color, y_color, yerr_color, color="gray", fmt="^", ms=5, lw=1, 
        marker="^", capsize=0, label=label)

      # x axis: JD plot
      idx_phase = y_color.index
      x_phase =  df["t_jd_ltcor"][idx_phase]
      y_phase =  df["mag"][idx_phase]
      yerr_phase =  df["magerr"][idx_phase]

      ax1.errorbar(
        x_phase, y_phase, yerr_phase, 
        color="gray", fmt="^", marker="^", ms=5, lw=1, label=label)


  if args.yr:
    ymin, ymax = args.yr
    ax1.set_ylim([ymin, ymax])
    ax2.set_ylim([ymin, ymax])
  ax1.set_title(args.obj)
  ax1.legend(fontsize=10)
  ax2.legend(fontsize=10)
  if args.outpng:
    out = args.outpng
  else:
    out = f"{args.obj}lcatonce_N{len(args.csv)}.png"
  out = os.path.join(outdir, out)
  plt.savefig(out)


  # Save offset corrected csv
  df = pd.concat(df_all, axis=0)
  df.to_csv(args.out, sep=" ", index=False)

