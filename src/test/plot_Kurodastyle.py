#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot light curves of an object in Kuroda style.
Input files are such as
  `3200_refladder_mag_g_epoch1.csv`, 3200_refladder_mag_g_epoch2.csv.
Use 
  absolute time (t_jd_ltcor) 
  mag (brightness corrected in plot_refladder_total.py).

"""

import os 
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  
from scipy.stats import sigmaclip

mycolor = ["#AD002D", "#1e50a2", "#006e54", "#ffd900", "#EFAEA1", 
           "#69821b", "#ec6800", "#afafb0", "#0095b9", "#89c3eb"]*100

mymark = ["o", "^", "x", "D", "+", "v", "<", ">", "h", "H"]*100

# Functions ==================================================================

def get_filename(file_path):
  return file_path.split("/")[-1].split(".")[0]


def adderr(*args):
  """Calculate additional error.

  Parameters
  ----------
  args : array-like
    list of values

  Return
  ------
  err : float
    calculated error
  """
  err = np.sqrt(np.sum(np.square(args)))
  return err


def adderr_series(*args):
  """Add error of multiple pandas.Series.

  Parameters
  ----------
  args : array-like
    list of pandas.Series 

  Return
  ------
  err_s : pandas.Series
    single pandas.Series of calculated error
  """ 
  for i,x in enumerate(args):
    assert type(x) is pd.core.frame.Series, "Input should be Series."
    #assert type(x)==type(pd.Series()), "Sould be Series"
    if i==0:
      temp = x.map(np.square)
    else:
      temp += x.map(np.square)
  err_s = temp.map(np.sqrt)
  return err_s


def log10err(val, err):
  """Calculate log10 error.
  """
  return err/val/np.log(10)


def diverr(val1, err1, val2, err2):
  """Calculate error for division.
  
  Parameters
  ----------
  val1 : float or pandas.Series 
    value 1
  err1 : float or pandas.Series 
    error 1
  val2 : float or pandas.Series 
    value 2
  err2 : float or pandas.Series 
    error 2
  """
  return np.sqrt((err1/val2)**2 + (err2*val1/val2**2)**2)


def mulerr(val1, err1, val2, err2):
   return np.sqrt((val2*err1)**2 + (val1*err2)**2)


def myfigure(n):
  """
  """
  if n==1:
    fig = plt.figure(figsize=(8.5, 6))
    ax = fig.add_subplot()
    axes = [ax]
  if n==3:
    fig = plt.figure(figsize=(20, 6))
    ax1 = fig.add_axes([0.07, 0.15, 0.25, 0.8])
    ax2 = fig.add_axes([0.4, 0.15, 0.25, 0.8])
    ax3 = fig.add_axes([0.73, 0.15, 0.25, 0.8])
    axes = [ax1, ax2, ax3]
  return fig, axes


if __name__ == "__main__":
  parser = ap(description="Plot light curve at once.")
  parser.add_argument(
    "obj", type=str, help="object name")
  parser.add_argument(
    "csv", type=str, nargs="*",
    help="photres.csv")
  parser.add_argument(
    "--JD0", default=2459514.5, type=float,
    help="time zero point in Juliand day (default is 2021-10-27)")
  parser.add_argument(
    "--period", type=float, required=True, 
    help="rotation period used to fold light curve(s)")
  parser.add_argument(
    "--mag", nargs="*", 
    help="magnitude type")
  parser.add_argument(
    "--label", nargs="*", 
    help="text in label is {obj} + {label}")
  parser.add_argument(
    "--eflagtest", action="store_true", default=False, 
    help="eflag test (0, 256, or others)")
  args = parser.parse_args()
  
  # Set output directory
  outdir = "plot"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  print(len(args.csv))
  print(len(args.label))
  print(len(args.mag))
  assert len(args.csv)==len(args.label)==len(args.mag), "Check input args."
  N_band = len(args.csv)

  filename = get_filename(args.csv[0])

  # Zero point
  JD0 = args.JD0

  # Rotational period in hour
  rotP_hour = args.period
  rotP_sec = args.period*3600.

  # hour to day
  rotP = args.period/24.
  plt.rcParams['font.family'] = 'Times New Roman'


  # Plot combined light curve

  fig, axes = myfigure(N_band)
  

  if not args.eflagtest:

    for idx,ax in enumerate(axes):
      ax.set_xlabel("Rotation Phase", fontsize=20)
      ax.set_ylabel(f"{args.mag[idx]} Magnitude [mag]", fontsize=20)
      ax.invert_yaxis()

      df = pd.read_csv(args.csv[idx], sep=" ")
      # Subtract clipped mean
      sigma = 3
      N_clip = 3
      mag_clip, _, _ = sigmaclip(df["mag"], sigma, sigma)
      df["mag"] -= np.mean(mag_clip)
      
      arc_day = np.max(df["t_jd_ltcor"]) - np.min(df["t_jd_ltcor"])
      n_phase = int(np.ceil(arc_day / rotP)) 
      print(f"  Arc: {arc_day}")
      print(f"  Number of Rotation Phase: {n_phase}")
      print(f"  JD0: {JD0}")

      cl = ["red", "blue", "#00ff00", "orange"]
      ml = ["o", "s", "x", "^"]
      i_phase = 0
      i_phase_plot = 0
      N_use = 0
      N_data = len(df)
      while N_use!=N_data:
        # Obtain i-th phase data
        df_temp = df[(df["t_jd_ltcor"] < JD0+(i_phase+1)*rotP) 
          & (df["t_jd_ltcor"] > JD0+i_phase*rotP)]

        if len(df_temp)==0:
          i_phase += 1
          continue
        else:
          x = ((df_temp["t_jd_ltcor"]-JD0)/rotP)%1
          label = f"p{i_phase_plot+1}"
          color = cl[i_phase_plot]
          marker = ml[i_phase_plot]
          # For bar
          ax.errorbar(
            x, df_temp["mag"], df_temp["magerr"], color=color,fmt="o",  ms=7, lw=1, 
            marker=marker, capsize=2, label=label, mec=None)
          N_use += len(df_temp)
          i_phase += 1
          i_phase_plot += 1
        print(f"Phase: {i_phase}, N_data={N_data}, N_use={N_use}")


      ## Set y lim 
      #ax.set_ylim([0.3, -0.3])
      ## Add text
      #ax.text(0.4, -0.2, f"{args.obj}\n{args.label}", size=20)

      ## Set y lim 
      ax.set_ylim([0.2, -0.2])
      # Add text
      ax.text(0.4, -0.12, f"{args.obj}\n{args.label[idx]}", size=20)
      ax.text(0.05, 0.18, f"JD0={JD0}, N={N_data}", size=15)
      ax.set_xlim([0, 1])


      ax.legend(fontsize=10).get_frame().set_alpha(1)
    
    print(f"N={len(df)}")
    JD0 = "f".join(str(JD0).split("."))
    rotP_hour ="f".join(str(rotP_hour).split("."))
    out = f"{filename}_Kuroda_style_JD0{JD0}_rotP{rotP_hour}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out)

  
  # Plot eflag==0, eflag==256, and others
  if args.eflagtest:
    for idx,ax in enumerate(axes):
      ax.set_xlabel("Rotation Phase", fontsize=20)
      ax.set_ylabel(f"{args.mag[idx]} Magnitude [mag]", fontsize=20)
      ax.invert_yaxis()

      df = pd.read_csv(args.csv[idx], sep=" ")
      # Subtract clipped mean
      sigma = 3
      N_clip = 3
      mag_clip, _, _ = sigmaclip(df["mag"], sigma, sigma)
      df["mag"] -= np.mean(mag_clip)
     
      
      arc_day = np.max(df["t_jd_ltcor"]) - np.min(df["t_jd_ltcor"])
      n_phase = int(np.ceil(arc_day / rotP)) 
      print(f"  Arc: {arc_day}")
      print(f"  Number of Rotation Phase: {n_phase}")
      print(f"  JD0: {JD0}")

      cl = ["red", "blue", "#00ff00", "orange"]
      ml = ["o", "s", "x", "^"]
      

      # eflag==0
      df_eflag0 = df[df["eflag"]==0]
      x = ((df_eflag0["t_jd_ltcor"]-JD0)/rotP)%1
      label = f"eflag==0"
      color = cl[0]
      marker = ml[0]
      ax.errorbar(
        x, df_eflag0["mag"], df_eflag0["magerr"], color=color,
        fmt="o",  ms=7, lw=1, marker=marker, capsize=2, label=label, mec=None)

      # eflag==256
      df_eflag256 = df[df["eflag"]==256]
      x = ((df_eflag256["t_jd_ltcor"]-JD0)/rotP)%1
      label = f"eflag==256"
      color = cl[1]
      marker = ml[1]
      ax.errorbar(
        x, df_eflag256["mag"], df_eflag256["magerr"], color=color,
        fmt="o",  ms=7, lw=1, marker=marker, capsize=2, label=label, mec=None)
   
      # Others
      df_other = df[(df["eflag"]!=0) & (df["eflag"]!=256)]
      x = ((df_other["t_jd_ltcor"]-JD0)/rotP)%1
      label = f"eflag!=0 and eflag!=256"
      color = cl[2]
      marker = ml[2]
      ax.errorbar(
        x, df_other["mag"], df_other["magerr"], color=color,
        fmt="o",  ms=7, lw=1, marker=marker, capsize=2, label=label, mec=None)

      ## Set y lim 
      ax.set_ylim([0.2, -0.2])
      # Add text
      N_data = len(df)
      ax.text(0.4, -0.12, f"{args.obj}\n{args.label[idx]}", size=20)
      ax.text(0.05, 0.18, f"JD0={JD0}, N={N_data}", size=15)
      ax.set_xlim([0, 1])


      ax.legend(fontsize=10).get_frame().set_alpha(1)
    
    print(f"N={len(df)}")
    JD0 = "f".join(str(JD0).split("."))
    rotP_hour ="f".join(str(rotP_hour).split("."))
    out = f"{filename}_Kuroda_style_JD0{JD0}_rotP{rotP_hour}_eflag.png"
    out = os.path.join(outdir, out)
    plt.savefig(out)
