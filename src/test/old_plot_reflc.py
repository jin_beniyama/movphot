#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot light curves of reference stars.
"""

import os 
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from aspect_data import (
  add_aspect_data, time_correction, mag_correction, phase_correction)

mycolor = ["#AD002D", "#1e50a2", "#006e54", "#ffd900", "#EFAEA1", 
           "#69821b", "#ec6800", "#afafb0", "#0095b9", "#89c3eb"]*100

mymark = ["o", "^", "x", "D", "+", "v", "<", ">", "h", "H"]*100

# Functions ==================================================================

def adderr(*args):
  """Calculate additional error.

  Parameters
  ----------
  args : array-like
    list of values

  Return
  ------
  err : float
    calculated error
  """
  err = np.sqrt(np.sum(np.square(args)))
  return err


def adderr_series(*args):
  """Add error of multiple pandas.Series.

  Parameters
  ----------
  args : array-like
    list of pandas.Series 

  Return
  ------
  err_s : pandas.Series
    single pandas.Series of calculated error
  """ 
  for i,x in enumerate(args):
    assert type(x) is pd.core.frame.Series, "Input should be Series."
    #assert type(x)==type(pd.Series()), "Sould be Series"
    if i==0:
      temp = x.map(np.square)
    else:
      temp += x.map(np.square)
  err_s = temp.map(np.sqrt)
  return err_s


def log10err(val, err):
  """Calculate log10 error.
  """
  return err/val/np.log(10)


def diverr(val1, err1, val2, err2):
  """Calculate error for division.
  
  Parameters
  ----------
  val1 : float or pandas.Series 
    value 1
  err1 : float or pandas.Series 
    error 1
  val2 : float or pandas.Series 
    value 2
  err2 : float or pandas.Series 
    error 2
  """
  return np.sqrt((err1/val2)**2 + (err2*val1/val2**2)**2)


def mulerr(val1, err1, val2, err2):
   return np.sqrt((val2*err1)**2 + (val1*err2)**2)


def flux_figure(N_band):
  """Create figures for flux light curves.

  Parameter
  ---------
  N_band : int
    number of observed band

  Returns
  -------
  fig : matplotlib.figure.Figure
    matplotlib.figure.Figure class object
  axes_raw : list of matplotlib.axes._axes.Axes
    Axes for raw light curves
  axes_norm : list of matplotlib.axes._axes.Axes
    Axes for normalized light curves
  """
  if N_band == 2:
    fig = plt.figure(figsize=(20, 8))
    # Raw
    ax1 = fig.add_axes([0.1, 0.55, 0.35, 0.4])
    ax2 = fig.add_axes([0.1, 0.15, 0.35, 0.4])
    # Normalized
    ax3 = fig.add_axes([0.6, 0.55, 0.35, 0.4])
    ax4 = fig.add_axes([0.6, 0.15, 0.35, 0.4])
    axes_raw = [ax1, ax2]
    axes_norm = [ax3, ax4]

  if N_band == 3:
    fig = plt.figure(figsize=(20, 12))
    # Raw
    ax1 = fig.add_axes([0.1, 0.66, 0.35, 0.28])
    ax2 = fig.add_axes([0.1, 0.38, 0.35, 0.28])
    ax3 = fig.add_axes([0.1, 0.1, 0.35, 0.28])
    # Normalized
    ax4 = fig.add_axes([0.6, 0.66, 0.35, 0.28])
    ax5 = fig.add_axes([0.6, 0.38, 0.35, 0.28])
    ax6 = fig.add_axes([0.6, 0.1, 0.35, 0.28])
    axes_raw = [ax1, ax2, ax3]
    axes_norm = [ax4, ax5, ax6]

  return fig, axes_raw, axes_norm


def lc_figure_with_template(N_band):
  """Create figures for object light curves with atmospheric templates.

  Parameter
  ---------
  N_band : int
    number of observed band

  Returns
  -------
  fig : matplotlib.figure.Figure
    matplotlib.figure.Figure class object
  ax_flux : list of matplotlib.axes._axes.Axes
    Axes for raw flux light curves
  ax_templates : list of matplotlib.axes._axes.Axes
    Axes for atmospheric template light curves
  ax_mag : list of matplotlib.axes._axes.Axes
    Axes for corrected magnitude light curves
  """
  if N_band == 2:
    fig = plt.figure(figsize=(20, 8))
    # Raw
    ax1 = fig.add_axes([0.08, 0.6, 0.4, 0.33])
    ax2 = fig.add_axes([0.08, 0.18, 0.4, 0.33])
    # template
    ax3 = fig.add_axes([0.08, 0.55, 0.4, 0.05])
    ax4 = fig.add_axes([0.08, 0.15, 0.4, 0.05])
    # Normalized
    ax5 = fig.add_axes([0.6, 0.55, 0.38, 0.4])
    ax6 = fig.add_axes([0.6, 0.15, 0.38, 0.4])
    axes_flux = [ax1, ax2]
    axes_template = [ax3, ax4]
    axes_mag = [ax5, ax6]

  if N_band == 3:
    fig = plt.figure(figsize=(20, 12))
    # Raw
    ax1 = fig.add_axes([0.08, 0.71, 0.4, 0.23])
    ax2 = fig.add_axes([0.08, 0.43, 0.4, 0.23])
    ax3 = fig.add_axes([0.08, 0.15, 0.4, 0.23])
    # template
    ax4 = fig.add_axes([0.08, 0.66, 0.4, 0.05])
    ax5 = fig.add_axes([0.08, 0.38, 0.4, 0.05])
    ax6 = fig.add_axes([0.08, 0.1, 0.4, 0.05])
    # Normalized
    ax7 = fig.add_axes([0.6, 0.66, 0.38, 0.28])
    ax8 = fig.add_axes([0.6, 0.38, 0.38, 0.28])
    ax9 = fig.add_axes([0.6, 0.1, 0.38, 0.28])
    axes_flux = [ax1, ax2, ax3]
    axes_template = [ax4, ax5, ax6]
    axes_mag = [ax7, ax8, ax9]

  return fig, axes_flux, axes_template, axes_mag


def col_figure(N_band):
  """Create figures for color light curves.

  Parameter
  ---------
  N_band : int
    number of observed band

  Returns
  -------
  fig : matplotlib.figure.Figure
    matplotlib.figure.Figure class object
  ax_col : list of matplotlib.axes._axes.Axes
    Axes for color light curves
  """
  if N_band == 2:
    fig = plt.figure(figsize=(10, 8))
    # Raw
    ax1 = fig.add_axes([0.1, 0.55, 0.85, 0.4])
    ax2 = fig.add_axes([0.1, 0.15, 0.85, 0.4])
    axes_col = [ax1, ax2]

  if N_band == 3:
    fig = plt.figure(figsize=(10, 12))
    # Raw
    ax1 = fig.add_axes([0.1, 0.66, 0.85, 0.28])
    ax2 = fig.add_axes([0.1, 0.38, 0.85, 0.28])
    ax3 = fig.add_axes([0.1, 0.1, 0.85, 0.28])
    axes_col = [ax1, ax2, ax3]

  return fig, axes_col


def band4cterm(band, bands):
  """Return 2 bands for color term determination.
  
  Parameters
  ----------
  band : str
    band of fits
  bands : str
    3 bands

  Returns
  -------
  mag_l, mag_r : float
    used magnitude
  """

  if bands == ["g", "r", "i"]:

    #if band=="g":
    #  mag_l, mag_r = "g", "r"
    #elif band=="r":
    #  mag_l, mag_r = "g", "r"
    #elif band=="i":
    #  mag_l, mag_r = "g", "i"
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "i"
    elif band=="i":
      mag_l, mag_r = "g", "i"

  elif bands == ["g", "r", "z"]:
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "z"
    elif band=="z":
      mag_l, mag_r = "g", "z"

  elif bands == ["r", "z"]:
    mag_l, mag_r = "r", "z"

  return mag_l, mag_r

# Functions finish ============================================================


if __name__ == "__main__":
  parser = ap(description="Plot light curve using the reference ladder.")
  parser.add_argument(
    "csv", type=str, help="photres.csv")
  parser.add_argument(
    "obj", type=str, help="object name")
  parser.add_argument(
    "--bands", nargs="*", default=["g", "r", "i"],
    help="observed bands")
  parser.add_argument(
    "--flux_min", nargs="*", type=float, default=[300, 300, 300],
    help="minimum flux of reference stars to be used")
  parser.add_argument(
    "--flux_max", nargs="*", type=float, default=[1000000, 1000000, 1000000],
    help="maximum flux of reference stars to be used")
  parser.add_argument(
    "--eflag_th", type=float, default=1, 
    help="maximum eflag value for both object and comparison stars")
  parser.add_argument(
    "--N_min_frame", type=int, default=10, 
    help="minimum frame numbers to be desired")
  parser.add_argument(
    "--sigma", type=int, default=3, 
    help="a threshold value to remove outliers in sigma clipping")
  parser.add_argument(
    "--yr", default=None, type=float, nargs=2,
    help="Y range")
  parser.add_argument(
    "--JD0", default=2459514.5, type=float,
    help="time zero point in Juliand day (default is 2021-10-27)")
  parser.add_argument(
    "--loc", type=str, default="371",
    help='location of the observatory')
  parser.add_argument(
    "--stype", default="C", choices=["C", "S", "X", "D", "V"],
    help='spectral type(ex. C/S/X/D/V)')
  args = parser.parse_args()
  
  # Set output directory
  outdir = "plot"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  # Selection criteria
  eflag_th = args.eflag_th
  N_min_frame = args.N_min_frame

  # Determine color
  bands = args.bands
  # Number of bands
  N_band = len(bands)
  # First band
  if N_band==2:
    colors = ["green", "red"]
  elif N_band==3:
    if (bands[2]=="i") or (bands[2]=="I"):
      colors = ["green", "red", "magenta"]
    elif bands[2]=="z":
      colors = ["green", "red", "blueviolet"]
  elif N_band==4:
    # Assume g, r, i, z
    colors = ["green", "red", "magenta", "blueviolet"]

  # Minimum flux of reference stars to be used
  flux_min = args.flux_min
  flux_max = args.flux_max

  assert len(bands)==len(flux_min),  "Inconsistent arguments are set."


  # p1 Read photometric csv ==================================================
  print("\n==================================================================")
  print("p1 Read photometric csv")
  print("==================================================================")
  df = pd.read_csv(args.csv, sep=" ")
  print(f"  DataFrame Dimention (original) {len(df)}")
  N_nan = np.sum(df.isnull().sum())
  # Do not remove 'nan' here.
  # Some 'nan' arise from nonsiderial tracking 
  if N_nan !=0:
    print(f"not a number is found and removed. N_nan={N_nan}")
    df = df.fillna(0)
    print(f"DataFrame Dimention (after nan removal) {len(df)}")
  N_nan = np.sum(df.isnull().sum())
  # Read photometric csv ======================================================
  

  # p2  Obtain common objID from input DataFrame ==============================
  print("\n==================================================================")
  print("p2 Obtain common objID from input DataFrame")
  print("==================================================================")
  # 18 is the length of an object name of the Pan-STARRS catalog
  column = df.columns.tolist()
  print(f"  column number(~30 per object): {len(column)}")
  # List of objID for each bands
  objID_all = []
  for band in bands:
    col_band = [col for col in column if (len(col))>18 and f"_{band}" in col]
    objID_band = [col.split("_")[1] for col in col_band]
    objID_band = set(objID_band)
    objID_all.append(objID_band)
  # Extract common objID 
  objID = objID_all[0]
  for n in range(N_band-1):
    objID = objID & objID_all[n+1]
  print(f"  Common objects N_obj={len(objID)}")
  objID = list(objID)
  objID = sorted(objID)

  # Obtain info.
  # 1st frame list
  frame1st_list = []
  # 1st frame coordinates
  x1st_list = []
  y1st_list = []
  # Count uneffective lines
  N_frame_obj_list = []
  # Magnitude list
  mag_list = []
  # Remove object 
  obj_rm = []

  # Define edge region 
  x0, x1, y0, y1 = 120, 1960, 200, 1080
  for obj in objID:

    # Search 1-band ref star
    col_ref = [col for col in column if obj in col]
    df_temp = df[col_ref]

    # Select objects that have enough flux and are not on edge regions
    for idx,b in enumerate(bands):
      df_temp = df_temp[
        (df_temp[f"flux_{obj}_{b}"] > flux_min[idx])
        & (df_temp[f"flux_{obj}_{b}"] < flux_max[idx])
        & (df_temp[f"x1_{obj}_{b}"] > x0)
        & (df_temp[f"x1_{obj}_{b}"] < x1)
        & (df_temp[f"y1_{obj}_{b}"] > y0)
        & (df_temp[f"y1_{obj}_{b}"] < y1)]
    # Number of detections of the object in a video
    N_frame_obj = len(df_temp)

    if N_frame_obj >= N_min_frame:
      # Use the object which were detected in enough frames
      # Add effective frame numbers
      N_frame_obj_list.append(N_frame_obj)

      # Add 1st frame
      frame1st_list.append(df_temp.index.tolist()[0])

      df_temp = df_temp.reset_index(drop=True)
      # Add 1st frame coordinates
      x1st_list.append(df_temp.at[0, f"x1_{obj}_{bands[0]}"])
      y1st_list.append(df_temp.at[0, f"y1_{obj}_{bands[0]}"])
      # Add catalog magnitude
      mag_list.append(df_temp.at[0, f"{bands[0]}MeanPSFMag_{obj}_{bands[0]}"])
    else:
      # Do not use the object which were not detected in enough frames
      obj_rm.append(obj)

  # Remove N_frame_obj < N_min_frame objects
  for obj in obj_rm:
    objID.remove(obj)

  # Number of output lines
  N_line = 3
  N_iter = np.int(np.ceil(len(objID)/N_line))
  print(f"  objID (N_frame, 1stframe, x, y, {bands[0]}mag)")
  for n in range(N_iter):
    objs = objID[n*N_line:(n+1)*N_line]
    N_frame = N_frame_obj_list[n*N_line:(n+1)*N_line]
    mags = mag_list[n*N_line:(n+1)*N_line]
    frame1st = frame1st_list[n*N_line:(n+1)*N_line]
    x1st = x1st_list[n*N_line:(n+1)*N_line]
    y1st = y1st_list[n*N_line:(n+1)*N_line]
    if len(objs)==1:
        print(
          f"    {objs[0]} ({N_frame[0]:4d}, {frame1st[0]:4d}, "
          f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f})")
    elif len(objs)==2:
      print(
        f"    {objs[0]} ({N_frame[0]:4d}, {frame1st[0]:4d}, " 
        f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f}) "
        f"{objs[1]} ({N_frame[1]:4d}, {frame1st[1]:4d}, "
        f"{x1st[1]:5.0f}, {y1st[1]:5.0f}, {mags[1]:.1f})")
    else:
      print(
        f"    {objs[0]} ({N_frame[0]:4d}, {frame1st[0]:4d}, " 
        f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f}) "
        f"{objs[1]} ({N_frame[1]:4d}, {frame1st[1]:4d}, "
        f"{x1st[1]:5.0f}, {y1st[1]:5.0f}, {mags[1]:.1f}) "
        f"{objs[2]} ({N_frame[2]:4d}, {frame1st[2]:4d}, "
        f"{x1st[2]:5.0f}, {y1st[2]:5.0f}, {mags[2]:.1f})")
  # Obtain common objID from input DataFrame ==================================

  # p3 Remove outlier (nan,inf,large eflag, negative flux) ====================
  print("\n==================================================================")
  print("p3 Remove outlier (nan,inf,large eflag, negative flux)")
  print("==================================================================")
  # Convert inf to nan
  df = df.replace([np.inf, -np.inf], np.nan)

  # Drop nan 
  df = df.dropna(how="any", axis=0)
  N_nan = np.sum(df.isnull().sum())
  assert N_nan==0, f"Nan N={N_nan}"
  # Drop by magnitude error and eflag
  N_bef = len(df)
  for j,band in enumerate(bands):
    if j==0:
      df_e = df[df[f"eflag_{band}"] >= eflag_th]
    else:
      df_e = df_e[df_e[f"eflag_{band}"] >= eflag_th]
    df = df[df[f"eflag_{band}"] < eflag_th]
    df = df[df[f"flux_{band}"] > 0]
  df = df.reset_index(drop=True)
  df_e = df_e.reset_index(drop=True)
  N_aft = len(df)
  print(f"  Final dataframe {N_aft} ({N_bef-N_aft} removed by errflag, eflag)")
  # Check
  N_nan = np.sum(df.isnull().sum())
  assert N_nan==0, f"Nan N={N_nan}"
  # Remove outlier (nan,inf,large eflag, negative flux) finish ================


  # p4 Calculate time in second for periodic analysis =========================
  print("\n==================================================================")
  print("p4 Calculate time in second and save for periodic analysis")
  print("==================================================================")
  for band in bands:
    # For periodic analysis 
    df[f"t_sec_obs_{band}"] = df[f"t_mjd_{band}"]*24*3600.
    df[f"t_sec_obs_{band}"] -= df.at[0, f"t_sec_obs_{band}"]
    df_e[f"t_sec_obs_{band}"] = df_e[f"t_mjd_{band}"]*24*3600.
    df_e[f"t_sec_obs_{band}"] -= df_e.at[0, f"t_sec_obs_{band}"]

    # Use t_jd for analysis with absolute time 

  # Calculate time in second for periodic analysis finish =====================


  # p5 Plot raw and total flux light curves of stars =====================
  print("\n==================================================================")
  print("p5 Plot magcat vs maginst")
  print("==================================================================")
  
  df.to_csv("hoge.csv", sep=" ", index=False)
  fig = plt.figure(figsize=(10, 10))
  # magcat vs maginst
  ax_raw = fig.add_axes([0.1, 0.55, 0.8, 0.3])
  ax_raw.set_xlabel("$mag_{cat}$")
  ax_raw.set_ylabel("$mag_{inst}$")
  # magcat vs maginst (magzpt corrected)
  ax_cor = fig.add_axes([0.1, 0.15, 0.8, 0.3])
  ax_cor.set_xlabel("$mag_{cat}$")
  ax_cor.set_ylabel("$mag_{inst} + zero point$")
  b = "g"
  idx_b = 0
  # Loop for band
  # Loop for frame
  magzpt_list = []
  Neff = len(df)
  for idx_frame, row in df.iterrows():
    # Loop for object
    for idx_obj,obj in enumerate(objID):
      if row[f"flux_{obj}_{b}"] <= flux_min[idx_b]:
        print("Skip low flux object")
        continue

      magcat = row[f"{b}MeanPSFMag_{obj}_{b}"]
      maginst = -2.5*np.log10(row[f"flux_{obj}_{b}"])
      if idx_obj==0:
        label = f"frame {idx_frame}"
      else:
        label = None
      ax_raw.scatter(
        magcat, maginst, marker=mymark[idx_frame], 
        color=mycolor[idx_frame], s=10, label=label)
      magzpt_temp = row[f"{b}MeanPSFMag_{obj}_{b}"] + 2.5*(np.log10(row[f"flux_{obj}_{b}"]))
      magzpt_list.append(magzpt_temp)
    # Calculate the magzpt of the frame
    magzpt = np.median(magzpt_list)
    print(f"magpzt of a {idx_frame}-th/{Neff} frame {magzpt:.3f}")

    # Loop for object again
    for idx_obj,obj in enumerate(objID):
      if row[f"flux_{obj}_{b}"] <= flux_min[idx_b]:
        print("Skip low flux object")
        continue
      magcat = row[f"{b}MeanPSFMag_{obj}_{b}"]
      maginst = -2.5*np.log10(row[f"flux_{obj}_{b}"])
      magcor = maginst + magzpt
      if idx_obj==0:
        label = f"frame {idx_frame}"
      else:
        label = None
      ax_cor.scatter(
        magcat, magcor, marker=mymark[idx_frame], 
        color=mycolor[idx_frame], s=10, label=label)
    if idx_frame > 3:
       break
  # y = x
  step = 0.1
  x = np.arange(14, 20, 0.5)
  ax_cor.plot(
    x, x, ms=0, lw=1, ls="dashed", color="black", label="y=x")

  for ax in fig.axes:
    ax.set_xlim([14, 20])
    ax.legend(fontsize=10)
  fig.savefig(f"hoge{idx_frame}.png")
  assert False, 1



  

  assert False, df
  # Log scale
  for ax in axes_raw:
    ax.set_yscale("log")
  ymin_list, ymax_list = [], []
  
  sigma = args.sigma
  
  df_flux_temp = []
  for idx_b, band in enumerate(bands):
    print(f"  {band}-band")


    
  for b in bands:
    df_flux_total[f"t_sec_obs_{b}"] = df[f"t_sec_obs_{b}"]
  # Plot raw and total flux light curves of stars finish =================
  

  # p6 Remove large photometric error data by SNR (flux/fluxerr) ==============
  print("\n==================================================================")
  print("p6 Remove large photometric error data by SNR (flux/fluxerr)")
  print("==================================================================")
  th_snr = 3
  for b in bands:
    snr_obj = df[f"flux_{band}"]/df[f"fluxerr_{band}"]
    df = df[snr_obj > th_snr]
  # Remove large photometric error data by SNR (flux/fluxerr) finish ==========
    

  # p7 Plot raw and corrected light curves of the target ================
  print("\n==================================================================")
  print("p7 Plot raw and corrected light curves of the target")
  print("==================================================================")

  # Create axes depending on the number of bands
  # i.e. axes_flux      = [ax1, ax2, ax3] of [ax1, ax2] or [ax1]
  #      axes_templates = [ax4, ax5, ax6] of [ax3, ax4] or [ax2]
  #      axes_mag       = [ax7, ax8, ax9] of [ax5, ax6] or [ax3]
  fig, axes_flux, axes_template, axes_mag = lc_figure_with_template(N_band)

  # For templates 
  plt.rcParams["ytick.labelsize"] = 10

  # Log scale
  for ax in axes_flux:
    ax.set_yscale("log")

  # List for y axis scaling
  ymin_list_mag, ymax_list_mag = [], []
  ymin_list_template, ymax_list_template = [], []
  for idx,band in enumerate(bands):
    ax_flux = axes_flux[idx]
    ax_template = axes_template[idx]
    ax_mag = axes_mag[idx]

    color = mycolor[idx]
    mark = mymark[idx]

    # total flux template
    flux_total = df_flux_total[f"flux_total_{band}"]
    fluxerr_total = df_flux_total[f"fluxerr_total_{band}"]
    flux_total_raw = df_flux_total[f"flux_total_raw_{band}"]
    ax_template.errorbar(
      df_flux_total[f"t_sec_obs_{band}"], flux_total, yerr=fluxerr_total,
      fmt="o", linewidth=0.1, ms=1, color="gray", label="Corrected total flux")
    ax_template.errorbar(
      df_flux_total[f"t_sec_obs_{band}"], flux_total_raw,
      fmt="^", linewidth=0.1, ms=1, color="black", label="Raw total flux")

    # Obj 
    flux_obj = df[f"flux_{band}"]
    fluxerr_obj = df[f"fluxerr_{band}"]
    ax_flux.errorbar(
      df[f"t_sec_obs_{band}"], flux_obj, yerr=fluxerr_obj,
      fmt="o", linewidth=0.2, ms=3, color=color, marker=mark, 
      label=f"{args.obj} raw flux")

    # Normalized mangnitude
    ratio = flux_obj/flux_total
    ratioerr = diverr(flux_obj, fluxerr_obj, flux_total, fluxerr_total)
    # Remove 0 or negative
    idx_use = ratio > 0
    ratio = ratio[idx_use]
    ratioerr = ratioerr[idx_use]

    df = df[idx_use]

    mag = -2.5*np.log10(ratio)
    mag = mag - np.mean(mag)
    magerr = 2.5*log10err(ratio, ratioerr)
    df.insert(0, f"mag_{band}", mag)
    df.insert(0, f"magerr_{band}", magerr)

    ax_mag.errorbar(
      df[f"t_sec_obs_{band}"], mag, yerr=magerr,
      fmt="o", linewidth=0.2, ms=3, color=color, marker=mark,
      label=f"{args.obj} {band}-band magnitude")
    ax_mag.invert_yaxis()
    

    # Save template y range
    #ymin_template, ymax_template = ax_template.get_ylim()
    #ymin_list_template.append(ymin_template)
    #ymax_list_template.append(ymax_template)

    # Save magnitude y range
    ymin_mag, ymax_mag = ax_mag.get_ylim()
    ymin_list_mag.append(ymin_mag)
    ymax_list_mag.append(ymax_mag)

    ax_flux.set_ylabel("Flux")
    ax_mag.set_ylabel("Relative magnitude")
    ax_flux.legend()
    ax_mag.legend()
    ax_template.legend()

  # Scaling (template)
  #ymin_template = np.min(ymin_list_template)
  #ymax_template = np.max(ymax_list_template)
  #print(
  #  f"  Scaling y-axes of templates to "
  #  f"{ymin_template:.2f}--{ymax_template:.2f}"
  #)
  #for ax in axes_template:
  #  ax.set_ylim(ymin_template, ymax_template)
  # Scaling (mag)
  ymin_mag = np.min(ymin_list_mag)
  ymax_mag = np.max(ymax_list_mag)
  # Margin for errorbars
  ymin_mag = ymin_mag*1.3
  ymax_mag = ymax_mag*1.3
  print(f"  Scaling y-axes of magnitude to {ymax_mag:.2f}--{ymin_mag:.2f}")
  for ax in axes_mag:
    ax.set_ylim(ymax_mag, ymin_mag)
    ax.invert_yaxis()

  # Remove useless xlables
  for ax in axes_template[:-1]:
    ax.axes.xaxis.set_visible(False)

  axes_template[-1].set_xlabel("Time [s]")
  axes_mag[-1].set_xlabel("Time [s]")
  if args.yr:
    for ax in axes_mag:
      ymin, ymax = args.yr
      ax.set_ylim([ymin, ymax])
  out =  f"{args.obj}_corrected_lc.png"
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=100)
  plt.close()
  # Plot raw and mean normalized light curves of the target finish ============


  # p8 Plot color light curves ================================================
  print("\n==================================================================")
  print("p7 Plot color light curves")
  print("==================================================================")

  # Create axes depending on the number of bands
  # i.e. axes_col      = [ax1, ax2, ax3] of [ax1, ax2] or [ax1]
  fig, axes_col = col_figure(N_band)

  # List for y axis scaling
  ymin_list_col, ymax_list_col = [], []
  for idx,band in enumerate(bands):
    ax_col = axes_col[idx]

    color = mycolor[idx]
    mark = mymark[idx]
    band_l, band_r = band4cterm(band, bands)

    # Obj  color
    col = df[f"mag_{band_l}"] - df[f"mag_{band_r}"]
    colerr = adderr_series(
      df[f"magerr_{band_l}"], df[f"magerr_{band_r}"])

    ax_col.errorbar(
      df[f"t_sec_obs_{band}"], col, yerr=colerr,
      fmt="o", linewidth=0.2, ms=3, color=color, marker=mark,
      label=f"{args.obj} {band_l}-{band_r} color")
    ax_col.invert_yaxis()

    # Save color y range
    ymin_col, ymax_col = ax_col.get_ylim()
    ymin_list_col.append(ymin_col)
    ymax_list_col.append(ymax_col)

    ax_col.set_ylabel("Relative magnitude")
    ax_col.legend()

    # Save for output
    try:
      df.insert(0, f"{band_l}_{band_r}", col)
      df.insert(0, f"{band_l}_{band_r}err", colerr)
    except:
      # Pass if already exists
      pass

  # Scaling (mag)
  ymin_col = np.min(ymin_list_col)
  ymax_col = np.max(ymax_list_col)
  # Margin for errorbars
  ymin_col = ymin_col*1.3
  ymax_col = ymax_col*1.3
  print(f"  Scaling y-axes of color to {ymax_col:.2f}--{ymin_col:.2f}")
  for ax in axes_col:
    ax.set_ylim(ymax_col, ymin_col)
    ax.invert_yaxis()

  axes_col[-1].set_xlabel("Time [s]")

  if args.yr:
    for ax in fig.axes:
      ymin, ymax = args.yr
      ax.set_ylim([ymin, ymax])
  out =  f"{args.obj}_color_lc.png"
  out = os.path.join(outdir, out)
  plt.savefig(out, dpi=100)
  plt.close()


  # Plot color light curves finish ============================================


  # p9 Save data for the periodic analysis ====================================
  print("\n==================================================================")
  print("p8 Save data for the periodic analysis")
  print("==================================================================")
  # Correct time and brightness with JD0 (time zero point)

  # UPDATE! add obj in color_phot.py
  df["obj"] = args.obj


  # Obtain aspect data
  # All columns (mag, t_sec_obs, t_sec_ltcor, red_mag, red_mag_phase) 
  # have suffix 
  
  for b in bands:
    # r (asteroid-Sun), delta (asteroid-observer), alpha, PAB
    # Use t_jd_{band}
    df = add_aspect_data(df, args.loc, args.JD0, b)
    # Do time correction (t_sec_obj to t_sec_ltcor)
    df = time_correction(df, b)
    # Do brightness correction (delta, r)
    # Mag is reduced and magnitude (mag_red)
    df = mag_correction(df, b)
    # Do phase correction (alpha)
    df = phase_correction(df, args.stype, b)

  for band in bands:
    band_l, band_r = band4cterm(band, bands)
    
    # Time, magnitude, and magnitude error
    df_temp = df[
      [f"t_sec_obs_{band}", 
       f"t_sec_ltcor_{band}", 
       f"t_jd_{band}", 
       f"t_jd_ltcor_{band}", 
       f"mag_{band}", 
       f"mag_red_{band}", 
       f"mag_red_phase_{band}", 
       f"magerr_{band}"]]

    # UPDATE ! 
    # Change column names for periodic analysis !

    df_temp = df_temp.rename(
      columns={f"t_sec_obs_{band}":"t_sec_obs", 
               f"t_sec_ltcor_{band}":"t_sec_ltcor", 
               f"t_jd_{band}":"t_jd", 
               f"t_jd_ltcor_{band}":"t_jd_ltcor", 
               f"mag_{band}": "mag_raw", 
               f"mag_red_{band}": "mag_red", 
               f"mag_red_phase_{band}": "mag", 
               f"magerr_{band}": "magerr"})
    df_temp.to_csv(
      f"{args.obj}_refladder_mag_{band}.csv", sep=" ", index=False)

    # Color 
    df_temp = df[
      [f"t_sec_obs_{band}", 
       f"t_sec_ltcor_{band}", 
       f"{band_l}_{band_r}", 
       f"{band_l}_{band_r}err"]]

    # UPDATE ! 
    # Change column names for periodic analysis !
    df_temp = df_temp.rename(
      columns={f't_sec_obs_{band}':'t_sec_obs', 
               f't_sec_ltcor_{band}':'t_sec_ltcor', 
               f"{band_l}_{band_r}": "mag", 
               f"{band_l}_{band_r}err": "magerr"})
    df_temp.to_csv(
      f"{args.obj}_refladder_col_{band_l}_{band_r}.csv", sep=" ", index=False)
    
  # Save data for the periodic analysis finish ================================
