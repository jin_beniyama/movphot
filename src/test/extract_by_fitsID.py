#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Extract photometry results for selected fitsID from photometry_result.txt.
"""
import os 
from argparse import ArgumentParser as ap
import pandas as pd


if __name__ == "__main__":
  parser = ap(description="Get Color Transformation Gradient")
  parser.add_argument(
    "csv", type=str, help="photometry_result.csv")
  parser.add_argument(
    "IDlist", type=str, default=None,
    help="fits ID list")
  args = parser.parse_args()

  
  df = pd.read_csv(args.csv, sep=" ")
  
  # Use a header of first band
  col = df.columns.tolist()
  col_fits = [c for c in col if "fits" in c]
  col_fits = col_fits[0]
  print(col_fits)
  
  # Select fits
  with open(args.IDlist, "r") as f:
    use_fits = f.readlines()
    # Remove \n
    use_fits = [x.split("\n")[0] for x in use_fits]
    # Remove .fits
    use_fits = [x.split(".")[0] for x in use_fits]
      
  df = df[df[col_fits].isin(use_fits)]
  print(df)
