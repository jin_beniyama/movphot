#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Output CT.
"""
import os 
from argparse import ArgumentParser as ap
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  


if __name__ == "__main__":
  parser = ap(description="Output CT")
  parser.add_argument(
    "csv", type=str, help="CT....csv")
  parser.add_argument(
    "type", type=str, choices=["r", "w"],
    help="raw or weighted")
  args = parser.parse_args()


  df = pd.read_csv(args.csv, sep=" ")
  
  # Assume g, r, and i
  if args.type=="r":
    CT = f"{df.at[0, 'g_r_CT_r']} {df.at[0, 'r_i_CT_r']} {df.at[0, 'g_i_CT_r']}"
    CTerr = f"{df.at[0, 'g_r_CTerr_r']} {df.at[0, 'r_i_CTerr_r']} {df.at[0, 'g_i_CTerr_r']}"
  if args.type=="w":
    CT = f"{df.at[0, 'g_r_CT_w']} {df.at[0, 'r_i_CT_w']} {df.at[0, 'g_i_CT_w']}"
    CTerr = f"{df.at[0, 'g_r_CTerr_w']} {df.at[0, 'r_i_CTerr_w']} {df.at[0, 'g_i_CTerr_w']}"

  print(
    f"--CT {CT} --CTerr {CTerr}")
