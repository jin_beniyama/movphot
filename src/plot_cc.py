#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot color color diagrams.
Input files are only *colall*.csv.
Objects in SDSSMOC can be plotted as well.

Note:
    Histograms are not for SDSSMOC, but for photometric results.

"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from myplot import mycolor, myls
from Tonry2012 import SDSS2PS_mag

from calcerror import adderr, round_error
from hoya.core import extract_hoya, DBPATH


def myfigure():
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_axes([0.15, 0.12, 0.75, 0.75])
    ax_u = fig.add_axes([0.15, 0.87, 0.75, 0.08])
    ax_r = fig.add_axes([0.90, 0.12, 0.08, 0.75])
    return fig, ax, ax_r, ax_u


if __name__ == "__main__":
    parser = ap(description="Plot color color diagrams.")
    parser.add_argument(
        "res", type=str, nargs="*", help="*colall*.txt")
    parser.add_argument(
        "--label", type=str, nargs="*", default=None,
        help="Arbitrary labels (optinal).")
    parser.add_argument(
        "--magtype", type=str, choices=["SDSS", "PS"],default="PS",
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="observed bands")
    parser.add_argument(
        "--each", action="store_true", default=False,
        help="Plot each point")
    parser.add_argument(
        "--mean", action="store_true", default=False,
        help="Plot mean value")
    parser.add_argument(
        "--c1r", nargs=2, type=float, default=False,
        help="Color range of c1")
    parser.add_argument(
        "--c2r", nargs=2, type=float, default=False,
        help="Color range of c2")
    parser.add_argument(
        "--SDSSMOC", action="store_true", default=False,
        help="Plot objects in SDSSMOC")
    parser.add_argument(
        "--stype", type=str, nargs="*", default=["S", "C", "X", "B"],
        help="Spectral types of objects in SDSSMOC.")
    parser.add_argument(
        "--out", type=str, default=False,
        help="Output file")
    parser.add_argument(
        "--outdir", type=str, default="result",
        help="Output directory")
    args = parser.parse_args()
  
    # Convert to PS magnitude in case input type is PS. (args.magtype==PS)
    # Photometric results are not changed.
    # Colors of SDSSMOC asteroids are changed.

    # Create Spectral type dataframe 
    col = ["obj", "stype", "H"]
    mymark = ["o", "^", "D", "v", "<", ">", "*", "h", "H"]

    fig, ax, ax_r, ax_u = myfigure()

    # Set filters
    if args.bands == ["g", "r", "i"]:
        if args.c1r:
            c1min, c1max = args.c1r
        else:
            c1min, c1max = 0.3, 0.7
        if args.c2r:
            c2min, c2max = args.c2r
        else:
            c2min, c2max = 0, 0.4
        ax.set_xlabel(f"$g-r$")
        ax.set_ylabel(f"$r-i$")
        b1, b2, b3 = "g", "r", "i"
    elif args.bands == ["g", "r", "z"]:
        if args.c1r:
            c1min, c1max = args.c1r
        else:
            c1min, c1max = 0.3, 0.7
        if args.c2r:
            c2min, c2max = args.c2r
        else:
            c2min, c2max = -0.2, 0.6
        ax.set_xlabel(f"$g-r$")
        ax.set_ylabel(f"$r-z$")
        b1, b2, b3 = "g", "r", "z"

    print(f"Color range")
    print(f"    c1 = {c1min}--{c1max}")
    print(f"    c2 = {c2min}--{c2max}")
    

    # Plot photometric results ================================================
    for idx, x in enumerate(args.res):
        print(f"Read {x}")
        df = pd.read_csv(x, sep=" ")

        color = mycolor[idx]
        marker = mymark[idx]
        ls = myls[idx]
    
        # Calculate weighted mean colors ======================================
        c1_w = 1/df[f"{b1}_{b2}err"]**2
        c1_wmean = np.average(df[f"{b1}_{b2}"], weights=c1_w)
        # Check !!!!!!
        # SD of weighted mean
        c1_wstd = np.sqrt(1/np.sum(c1_w))
        c2_w = 1/df[f"{b2}_{b3}err"]**2
        c2_wmean = np.average(df[f"{b2}_{b3}"], weights=c2_w)
        # SD of weighted mean
        c2_wstd = np.sqrt(1/np.sum(c2_w))
        # Calculate weighted mean colors ======================================
    
        # Standard mean
        N = len(df)
        c1_mean = np.average(df[f"{b1}_{b2}"])
        c2_mean = np.average(df[f"{b2}_{b3}"])
        # Standard deviation
        c1_SD   = np.std(df[f"{b1}_{b2}"])
        c2_SD   = np.std(df[f"{b2}_{b3}"])
        # Standard error
        c1_SE   = c1_SD/np.sqrt(N)
        c2_SE   = c2_SD/np.sqrt(N)
        # Sum of errors
        c1_sumerr   = adderr(df[f"{b1}_{b2}err"])/np.sqrt(N)
        c2_sumerr   = adderr(df[f"{b2}_{b3}err"])/np.sqrt(N)
        
        ## Round error
        c1_wmean_str, c1_wstd_str = round_error(c1_wmean, c1_wstd)
        c2_wmean_str, c2_wstd_str = round_error(c2_wmean, c2_wstd)
        label = (
            f"N={len(df)}\n"
            f"Weighted\n"
            f"{b1}-{b2} ${c1_wmean_str}" + r"\pm" + f"{c1_wstd_str}$\n"
            f"{b2}-{b3} ${c2_wmean_str}" + r"\pm" + f"{c2_wstd_str}$")
        print(label)

        # c1_mean_str1, c1_SD_str = round_error(c1_mean, c1_SD)
        # c2_mean_str1, c2_SD_str = round_error(c2_mean, c2_SD)
        # c1_mean_str2, c1_SE_str = round_error(c1_mean, c1_SE)
        # c2_mean_str2, c2_SE_str = round_error(c2_mean, c2_SE)
        # c1_mean_str3, c1_sumerr_str = round_error(c1_mean, c1_sumerr)
        # c2_mean_str3, c2_sumerr_str = round_error(c2_mean, c2_sumerr)
        # label = (
        #     f"N={len(df)}\n"
        #     f"Weighted\n"
        #     f"{b1}-{b2} ${c1_wmean_str}" + r"\pm" + f"{c1_wstd_str}$\n"
        #     f"{b2}-{b3} ${c2_wmean_str}" + r"\pm" + f"{c2_wstd_str}$\n"
        #     #f"Standard Deviation\n"
        #     #f"{b1}-{b2} ${c1_mean_str1}" + r"\pm" + f"{c1_SD_str}$\n"
        #     #f"{b2}-{b3} ${c2_mean_str1}" + r"\pm" + f"{c2_SD_str}$\n"
        #     f"Standard Error\n"
        #     f"{b1}-{b2} ${c1_mean_str2}" + r"\pm" + f"{c1_SE_str}$\n"
        #     f"{b2}-{b3} ${c2_mean_str2}" + r"\pm" + f"{c2_SE_str}$\n"
        #     #f"Sum of uncertainties\n"
        #     #f"{b1}-{b2} ${c1_mean_str3}" + r"\pm" + f"{c1_sumerr_str}$\n"
        #     #f"{b2}-{b3} ${c2_mean_str3}" + r"\pm" + f"{c2_sumerr_str}$\n"
        #     )
  
        range_c1 = (c1min, c1max)
        range_c2 = (c2min, c2max)
        nbin = 20
        # Use arbitrary labels if specified
        if args.label:
            label = args.label[idx]
        else:
            label = df.at[0, "obj"]
        ax_u.hist(
            df[f"{b1}_{b2}"], color=color, histtype="step", range=range_c1, bins=nbin,
            density=True)
        ax_r.hist(
            df[f"{b2}_{b3}"], orientation="horizontal", color=color, histtype="step", bins=nbin,
            density=True, range=range_c2)

        # For open circle
        if args.each:
            ax.scatter(
              df[f"{b1}_{b2}"], df[f"{b2}_{b3}"], color=color, s=70, lw=1, 
              marker=marker, facecolor="None", edgecolor=color, zorder=10, label=label)
            ax.errorbar(
              df[f"{b1}_{b2}"], df[f"{b2}_{b3}"], xerr=df[f"{b1}_{b2}err"], 
              yerr=df[f"{b2}_{b3}err"], color=color, lw=0.5, ms=3, ls="None",
              marker=None, capsize=0, zorder=0)
        if args.mean:
            marker = "*"
            ax.scatter(
                c1_wmean, c2_wmean,
                s=600, lw=2, 
                marker=marker, facecolor=color, edgecolor="black", zorder=10, label=label)
            ax.errorbar(
                c1_wmean, c2_wmean, c1_wstd, c2_wstd, 
                color=color, lw=2, ms=3, ls="None",
                marker=None, capsize=0, zorder=1)
            ax_r_xmax = ax_r.get_xlim()[1]
            ax_r.hlines(c2_wmean, 0, ax_r_xmax, color=color, ls=myls[1])
    
            ax_u_ymax = ax_u.get_ylim()[1]
            ax_u.vlines(c1_wmean, 0, ax_u_ymax, color=color, ls=myls[1])
        else:
            print("Do not plot!")
    # Plot photometric results ================================================
    

    # Plot objects in SDSSMOC =================================================
    if args.SDSSMOC:
        # Read SDSS MOC data
        df_S21 = extract_hoya(DBPATH, "Sergeyev2021")
        # Select high possibility objects
        p_th = 0.8
        df_S21 = df_S21[df_S21["p_stype"].astype(float) > p_th]
        print(f"N_SDSS = {len(df_S21)} (p > {p_th})")
        if args.magtype=="PS":
            # Change system to PS system
            print("Photometric results are in the Pan-STARRS.")
            print("Convert colors in the SDSS to those in PS.")
            df_S21 = SDSS2PS_mag(df_S21)
            # Rename
            df_S21 = df_S21.rename(
              columns={"g":"g_temp", "gerr":"gerr_temp", 
                       "r":"r_temp", "rerr":"rerr_temp", 
                       "i":"i_temp", "ierr":"ierr_temp", 
                       "z":"z_temp", "zerr":"zerr_temp"})
            df_S21 = df_S21.rename(
              columns={"g_PS":"g", "gerr_PS":"gerr", 
                       "r_PS":"r", "rerr_PS":"rerr", 
                       "i_PS":"i", "ierr_PS":"ierr", 
                       "z_PS":"z", "zerr_PS":"zerr"})

        # Plot 
        for stype in args.stype:
            df_type = df_S21[df_S21["stype"]==stype]
            df_type = df_type.reset_index(drop=True)
            print(f"  {stype}-type N={len(df_type)}")
            if len(df_type) == 0:
                continue
            if stype=="S":
                color = mycolor[0]
                mark = "o"
                cmap = "Reds"
            if stype=="C":
                color = mycolor[1]
                mark = "^"
                cmap = "Blues"
            if stype=="X":
                color = "gray"
                mark = "v"
                cmap = "Greys"
            if stype=="B":
                color = "green"
                mark = "D"
                cmap = "Greens"
            # No Q-type in SDSSMOC
            if stype=="Q":
                color = "orange"
            
            x = df_type[b1]-df_type[b2]
            y = df_type[b2]-df_type[b3]

            # 1. Simple plot
            ax.scatter(
                x, y,
                color=color, s=10, lw=1.0, marker=mark, facecolor="None", 
                edgecolor=color, zorder=-1, label=stype)

            # 2. Calculate and plot the point density
            # xy = np.vstack([x,y])
            # from scipy.stats import gaussian_kde
            # z = gaussian_kde(xy)(xy)
            # # Sort the points by density, so that the densest points are plotted last
            # idx = z.argsort()
            # x, y, z = x[idx], y[idx], z[idx]
            # ax.scatter(x, y, c=z, s=10, cmap=cmap)

            # 3. 2dhist
            ax.hist2d(x, y, bins=[np.linspace(c1min, c1max, 10),np.linspace(c2min, c2max, 10)], cmap=cmap)
    # Plot objects in SDSSMOC =================================================


    ax.grid(which="major", axis="both")
    ax.legend(markerscale=1).get_frame().set_alpha(1.0)
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()

    #ax_u.set_xlim(xmin, xmax)
    #ax_r.set_ylim(ymin, ymax)
    ax.set_xlim([c1min, c1max])
    ax.set_ylim([c2min, c2max])
    ax_u.set_xlim([c1min, c1max])
    ax_r.set_ylim([c2min, c2max])

    ax_u.axes.xaxis.set_visible(False)
    ax_r.axes.yaxis.set_visible(False)
    if args.out:
        out = args.out
    else:
        out =  f"colcol_Nobj{len(args.res)}.png"
    fig.savefig(out)
    plt.close()
    assert False, 1

    
    # kNN =====================================================================
    # Under construction. 2023-03-30
    from sklearn.neighbors import KNeighborsClassifier
    stype_use = args.stype
    kwd_label = "stype"
    c1, c2 = f"{b1}_{b2}", f"{b2}_{b3}"
    param_use = [c1, c2]
    df_S21[c1] = df_S21[b1] - df_S21[b2]
    df_S21[c2] = df_S21[b2] - df_S21[b3]
    # Remove outliers
    df_S21 = df_S21[(df_S21[c1] > c1min) & (df_S21[c1] < c1max)]
    df_S21 = df_S21[(df_S21[c2] > c2min) & (df_S21[c2] < c2max)]
    df_S21 = df_S21[df_S21[kwd_label].isin(stype_use)]

    print(f"Used params: {param_use}")
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot()
    plt.subplots_adjust(wspace=0.2, hspace=0.3)

    from matplotlib.colors import ListedColormap
    cm = plt.cm.RdBu

    i = 0
    X = df_S21[param_use]
    y = df_S21[kwd_label]
    # x = g_r, y = r_i
    x_min, x_max = np.min(X[c1]), np.max(X[c1])
    y_min, y_max = np.min(X[c2]), np.max(X[c2])
    print(f"x range {x_min}--{x_max}")
    print(f"y range {y_min}--{y_max}")

    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, 0.01), np.arange(y_min, y_max, 0.01))

    knn3 = KNeighborsClassifier(n_neighbors=5)
    knn3 = KNeighborsClassifier(n_neighbors=5, weights="distance")
    knn3.fit(df_S21[param_use], df_S21[kwd_label])
    print(df_S21[kwd_label])
    assert False, df_S21[param_use]
    clf = knn3
    clf.fit(X, y)
    Z = clf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:,1]
    Z = Z.reshape(xx.shape)
    # If 4 classes(S, C, X and other), values are 0, 0.33, 0.66 and 1
    N_type = len(set(df_S21["stype"]))
    print(f"N_type = {N_type}")
    #ax.contourf(xx, yy, Z, N_type, cmap=cm)
    ax.contourf(xx, yy, Z, 20, cmap=cm)

    # Plot 
    for stype in args.stype:
        df_type = df_S21[df_S21["stype"]==stype]
        print(f"  {stype}-type N={len(df_type)}")
        if len(df_type) == 0:
            continue
        if stype=="S":
            color = mycolor[0]
            mark = "o"
        if stype=="C":
            color = mycolor[1]
            mark = "^"
        if stype=="X":
            color = "gray"
            mark = "v"
        if stype=="B":
            color = "green"
            mark = "D"
        # No Q-type in SDSSMOC
        if stype=="Q":
            color = "orange"

        ax.scatter(
          df_type[b1]-df_type[b2], df_type[b2]-df_type[b3],
          color=color, s=30, lw=1.0, marker=mark, facecolor="None", 
          edgecolor=color, zorder=1000, label=stype)

    ax.set_title(f"kNN k=3, N-type={N_type}")
    ax.set_xlim([c1min, c1max])
    ax.set_ylim([c2min, c2max])
    plt.savefig("result.png", bbox_inches="tight")

