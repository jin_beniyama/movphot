#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Check PSF of objects.
"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import matplotlib.pyplot as plt  
import sep
import astropy.io.fits as fits
from matplotlib.ticker import AutoMinorLocator

from myplot import mycolor, mymark, myls
from movphot.photfunc import obtain_winpos, calc_FWHM_fits, remove_background2d, triccsgain
from movphot.common import extract1darr, fit_psf, radial_profile


plt.rcParams["grid.color"] = "gray" 
plt.rcParams["grid.linestyle"] = "dashed" 
plt.rcParams["grid.linewidth"] = "0.4" 


if __name__ == "__main__":
    parser = ap(description="Plot color color diagrams.")
    parser.add_argument(
        "fits", type=str, nargs="*", 
        help="input fits file")
    parser.add_argument(
        "--inst", type=str, required=True, choices=["TriCCS", "murikabushi"],
        help="used instrument (for gain)")
    parser.add_argument(
        "--oneside", action="store_true", default=False,
        help="Plot averaged oneside radial profile")
    parser.add_argument(
        "-x", nargs="*", type=float, 
        help="object x in pixel coordinate")
    parser.add_argument(
        "-y", nargs="*", type=float,
        help="object y in pixel coordinate")
    parser.add_argument(
        "--fittype", type=str, default="Moffat",
        help="Fitting type Gauss or Moffat")
    parser.add_argument(
        "-w", "--width", type=int, default=15,
        help="widht of the cutting region")
    parser.add_argument(
        "--width_fit", type=int, default=7,
        help="widht of the cutting region")
    parser.add_argument(
        "-r", "--radius", type=int, default=10,
        help="radius to obtain winpos")
    parser.add_argument(
        "--p_scale", type=float, default=0.35,
        help="pixel scale in arcsec/s")
    parser.add_argument(
        "--norm", action="store_true", default=False,
        help="Normalize brightness by the model peak")
    parser.add_argument(
        "--outtype", type=str, default="png",
        help="Output type")
    parser.add_argument(
        "--outdir", type=str, default="psf",
        help="Output directory")
    args = parser.parse_args()
  

    N_fits  = len(args.fits)
    N_obj   = len(args.x)
    oneside = args.oneside
    
    # Picel scale arcsec/pix
    p_scale = args.p_scale

    if N_fits > 1:
        print("Binning")
        infits = 1
    else:
        infits = args.fits[0]

    # fits source  (HDU0, header + image data)
    src = fits.open(infits)[0]
    # fits header
    hdr = src.header
    # Gain
    if args.inst == "TriCCS":
        # 0.34 -> 0.350 arcsec/pixel (2022-04-28)
        p_scale = 0.350
        gain = hdr["GAINCNFG"]
        # Convert str(ex. x4) to float value(1.5)
        # e/ADU
        gain = triccsgain(gain)

    # 2-d image
    image = src.data.byteswap().newbyteorder()
    # BG subtraction
    image, _ = remove_background2d(image, mask=None)
    width = args.width
    # Widht in fitting
    width_fit = args.width_fit
    radius = args.radius
    ny, nx = image.shape
    print(f"Shape of image nx, ny = {nx}, {ny}")
    
    # Calculate fwhm of the fits with clean stars
    fwhm_mean, fwhm_std = calc_FWHM_fits(infits, radius)
    fwhm_mean_arcsec = fwhm_mean*p_scale
    fwhm_std_arcsec = fwhm_std*p_scale
    print(f"FWHM of image {fwhm_mean:.3f}+-{fwhm_std:.3f} pix")
    print(f"          <-> {fwhm_mean_arcsec:.3f}+-{fwhm_std_arcsec:.3f} arcsec")

    
    # Plot cross section for certain direction ================================
    if not oneside:
        # For filename
        tail = ""
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_axes([0.15, 0.13, 0.84, 0.77])
        ax.set_xlabel("Radius [pix]")
        if args.norm:
            ax.set_ylabel("Normalized Flux")
        else:
            ax.set_ylabel("Count [ADU]")
        for idx_obj, (x, y) in enumerate(zip(args.x, args.y)):
            print(f"Object {idx_obj} at ({x}, {y})")
            
            color = mycolor[idx_obj]

            # Cut arrays
            xmin, xmax = x - width, x + width
            ymin, ymax = y - width, y + width
            image_cut = image[int(ymin):int(ymax), int(xmin):int(xmax)]
            
            # Obtain windowed positions
            xwin, ywin, _ = obtain_winpos(image, [x], [y], [radius], nx, ny)
            xwin, ywin = xwin[0], ywin[0]
            xwin_str = str(int(xwin))
            ywin_str = str(int(ywin))
            print("Search baricenter")
            print(f"{x}, {y} -> {xwin}, {ywin}")
            
            # theta = 0 -> x-direction
            theta = 0
            # For plot
            pix_x, val_x = extract1darr(image, x, y, theta, width)       
            # For fitting
            pix_x_fit, val_x_fit = extract1darr(image, x, y, theta, width_fit)       

            # For plot
            x_model = np.arange(-width, width, 0.1)
          
            # Fitting
            if args.fittype == "Gauss":
                params_G = fit_psf(pix_x_fit, val_x_fit, fittype="Gauss")
                A_G, mu_G, sigma_G = params_G
                sigma_G_arcsec = sigma_G * p_scale
                FWHM_G = 2.35 * sigma_G
                FWHM_G_arcsec = 2.35 * sigma_G * p_scale
                print(f"Params: A_G, mu_G, sigma_G = {A_G:.1f}, {mu_G:.1f}, {sigma_G:.1f}")
                y_model = A_G*np.exp(-0.5*(x_model)**2/sigma_G**2)
                label = (
                    f"Gauss FWHM={FWHM_G:.2f} pix = {FWHM_G_arcsec:.2f} arcsec")

            if args.fittype == "Moffat":
                # Moffat fit
                params_M = fit_psf(pix_x_fit, val_x_fit, fittype="Moffat")
                A_M, mu_M, sigma_M, beta_M = params_M
                # See Tabeshian+2019 3.1
                FWHM_M = 2*sigma_M*np.sqrt(2**(1/beta_M)-1)
                FWHM_M_arcsec = FWHM_M * p_scale
                print(f"Params: A_M, sigma_M, beta_M = {A_M:.1f}, {sigma_M:.1f}, {beta_M:.1f}")
                y_model = A_M*(1+(x_model)**2/sigma_M**2)**(-beta_M)
                label = (
                    f"Moffat FWHM={FWHM_M:.2f} pix = {FWHM_M_arcsec:.2f} arcsec")


            if args.norm:
                norm = np.max(y_model)
            else:
                norm = 1
            ax.plot(x_model, y_model/norm, label=label, color=color, ls=myls[1])

            # Subtract mean of moffat from the data
            pix_x_mean0 = [x-mu_M for x in pix_x]
            # Data
            ax.scatter(
                pix_x_mean0, val_x/norm, label=f"Data ({xwin_str}, {ywin_str})", 
                color=color)
            tail += f"x{xwin_str}y{ywin_str}"

            # Fitting region
            ymin, ymax = ax.get_ylim()
            ax.vlines(-width_fit, ymin, ymax, ls=myls[2], color="black")
            ax.vlines(width_fit, ymin, ymax, ls=myls[2], color="black")
            ax.set_ylim([ymin, ymax])

        # Add arcsec
        ax_twin = ax.twiny()
        ax_twin.set_xlabel("Radius [arcsec]")
        xmin, xmax = ax.get_xlim()
        ax_twin.set_xlim([xmin*p_scale, xmax*p_scale])

        ax.grid(which="major", axis="both")
        ax.legend(fontsize=10)
        out = f"psf1dx_{tail}.{args.outtype}"
        plt.savefig(out)
    # Plot cross section ======================================================


    # Plot averaged radial profile ============================================
    # Plot error but errors are not including in fitting 
    if oneside:

        # For filename
        tail = ""
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_axes([0.15, 0.13, 0.84, 0.77])
        ax.set_xlabel("Radius [arcsec]")
        if args.norm:
            ax.set_ylabel("Normalized Flux")
        else:
            ax.set_ylabel("Count [ADU]")
        for idx_obj, (x, y) in enumerate(zip(args.x, args.y)):
            color = mycolor[idx_obj]
            mark = mymark[idx_obj]
            ls = myls[idx_obj+1]
            print(f"Object {idx_obj} at ({x}, {y})")
            
            color = mycolor[idx_obj]
        
            # !! Order is important !!
            # At first, Cut arrays
            xmin, xmax = x - width, x + width
            ymin, ymax = y - width, y + width
            image_cut = image[int(ymin):int(ymax), int(xmin):int(xmax)]
            # Now, object is close to the center
            x, y = x - xmin, y - ymin
      
            # Second, search barycenter
            # Obtain windowed positions
            xwin, ywin, _ = obtain_winpos(image, [x], [y], [radius], nx, ny)
            xwin, ywin = xwin[0], ywin[0]

            xwin_str = str(int(xwin))
            ywin_str = str(int(ywin))
            print("Search baricenter")
            print(f"{x}, {y} -> {xwin}, {ywin}")
            center = (xwin, ywin)
            # Need gain for poisson error calculation
            val, valerr = radial_profile(image_cut, center, gain)
            x_fit_pix = np.arange(0, len(val), 1)

            print(f"Shape of rardial profile: {val.shape}")

            # For plot
            x_model_pix = np.arange(0, len(val), 0.1)
          
            # Fitting in pixel
            if args.fittype == "Gauss":
                params_G = fit_psf(
                    x_fit_pix, val, yerr=valerr, fittype="Gauss")
                if oneside:
                    A_G, sigma_G = params_G
                    mu_G = 0
                else:
                    A_G, mu_G, sigma_G = params_G
                sigma_G_arcsec = sigma_G * p_scale
                FWHM_G = 2.35 * sigma_G
                FWHM_G_arcsec = 2.35 * sigma_G * p_scale
                print(f"Params: A_G, mu_G, sigma_G = {A_G:.1f}, {mu_G:.1f}, {sigma_G:.1f}")
                y_model = A_G*np.exp(-0.5*(x_model_pix)**2/sigma_G**2)
                label = (
                    f"Gauss FWHM={FWHM_G:.2f} pix = {FWHM_G_arcsec:.2f} arcsec")

            if args.fittype == "Moffat":
                # Moffat fit
                params_M = fit_psf(
                    x_fit_pix, val, yerr=valerr, fittype="Moffat", oneside=True)
                if oneside:
                    A_M, sigma_M, beta_M = params_M
                else:
                    A_M, mu_M, sigma_M, beta_M = params_M
                # See Tabeshian+2019 3.1
                FWHM_M = 2*sigma_M*np.sqrt(2**(1/beta_M)-1)
                FWHM_M_arcsec = FWHM_M * p_scale
                print(f"Params: A_M, sigma_M, beta_M = {A_M:.1f}, {sigma_M:.1f}, {beta_M:.1f}")
                y_model = A_M*(1+(x_model_pix)**2/sigma_M**2)**(-beta_M)
                label = (
                    f"Moffat FWHM={FWHM_M:.2f} pix = {FWHM_M_arcsec:.2f} arcsec")

            if args.norm:
                norm = np.max(y_model)
            else:
                norm = 1

            # Plot in arcsec
            x_fit_arcsec = [x*p_scale for x in x_fit_pix]
            x_model_arcsec = [x*p_scale for x in x_model_pix]

            ax.plot(x_model_arcsec, y_model/norm, label=label, color=color, ls=ls)

            # Plot Data in arcsec
            ax.scatter(
                x_fit_arcsec, val/norm, label=f"Data ({xwin_str}, {ywin_str})", 
                facecolor="None", edgecolor=color, marker=mark, s=100)
            ax.errorbar(
                x_fit_arcsec, val/norm, valerr/norm, color=color, 
                lw=2, marker=" ", capsize=0, ls="None")

            tail += f"x{xwin_str}y{ywin_str}"

            # Fitting region
            ymin, ymax = ax.get_ylim()
            if not oneside:
                ax.vlines(-width_fit, ymin, ymax, ls=myls[2], color="black")
            ax.set_ylim([ymin, ymax])

        ax.grid(which="major", axis="both")
        ax.legend(fontsize=10)
        out = f"psf_radialprofile_{tail}.{args.outtype}"
        plt.savefig(out)
    # Plot averaged radial profile ============================================
