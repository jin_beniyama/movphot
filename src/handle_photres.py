#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Extract arbitary columns of an object.
"""
import numpy as np
import pandas as pd
import os 
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from argparse import ArgumentParser as ap


if __name__ == "__main__":
    parser = ap(description="Lighten the csv.")
    parser.add_argument(
        "csv", type=str, help="photometry_result.csv")
    parser.add_argument(
        "objID", type=int, help="objID")
    parser.add_argument(
        "column", type=str, help="column name")
    args = parser.parse_args()

    # Read csv
    df = pd.read_csv(args.csv, sep=" ")
    df_ref = df[df["objID"]==args.objID]
    print(df_ref[args.column])
    print(set(df_ref[args.column]))

