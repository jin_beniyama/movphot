#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Plot light curves of an object using a `reference ladder (aka refladder)`. 
This method is useful for data that have different field of view 
during observing time due to the non-sidereal tracking etc.
The target brightness is corrected with the `total flux` of the reference stars.
Faint objects are removed by `refmagmax` option.

Summary of Procedures
---------------------
(p1, p2, ... are usefull search words.)
  p1  Read photometric result
  p2  Clean the reference stars
  p3  Clean the target
  p4  Calculate distance from the edge in each frame
  p5  Check Nframe and remove Nframe < Nframe_min
  p6  Calculate time in second and save for periodic analysis
  p7  Plot raw and total flux light curves of stars
  p8  Plot relative lightcurves with correction 
  p9  Plot background count
  p10 Save data for the periodic analysis

Procedures
----------
First, create raw flux light curves of reference starts 
  using N > Nframe_min (minimum number of frames) objects.
    x axis : time in sec
    y axis : raw flux

Second, calculate a total flux of each frame.
    ex)
    frame 1 (starA, B, C): 1000+2000+2000 = 5000
    frame 2 (starA, B, C): 1010+2010+2000 = 5020
    frame 3 (starA, B, D): 1000+2010+4000 = 7010

    When stars are different from those in previous frame,
    total flux (total1) is corrected using the equation:
      total1 = C*total0 
    where, total0 is a total flux of previous frame.
    C is a coefficient indicates total flux of common stars and calulated as
      C = flux_total_common1 / flux_total_common0.
    Then relative flux is calculated.

Third, correct the target light curve by a relative flux
    x axis : time in sec
    y axis : object flux = flux_obj / relative_flux
Then the final magnitude light curve is obtained.


Output Examples
---------------
2021TY14_refladderlc_fmin1000f0_1000f0_1000f0_fmax1000000_1000000_1000000.png

ToDo
----
Too red/blue stars can also be used in the method.

"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from aspect_data import (
    add_aspect_data, time_correction, mag_correction, phase_correction)
from movphot.common import (
    mycolor, mymark, get_filename, adderr, adderr_series, 
    diverr, mulerr, log10err)
from movphot.prepro import (
    clean_photres, calc_d_from_edge, band4CTG, remove_suffix, remove_Nframe, refladder, plot_refladder,
    plot_bg)
from movphot.visualization import myfigure_ncol


# Functions finish ============================================================
if __name__ == "__main__":
    parser = ap(description="Plot relative lightcurves by refladder")
    parser.add_argument(
        "csv", type=str, help="photometry_result.csv")
    parser.add_argument(
        "obj", type=str, help="object name")
    parser.add_argument(
        "magtype", type=str, choices=["SDSS", "PS"],
        help="griz magnitude type of input csv")
    parser.add_argument(
        "--inst", type=str, default=None, help="instrumental name")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"], help="observed bands")
    parser.add_argument(
        "--catmag_min", type=float, default=13, help="minimum magnitude")
    parser.add_argument(
        "--catmag_max", type=float, default=19, help="maximum magnitude")
    parser.add_argument(
        "--catmagerr_max", type=float, default=0.1, 
        help="maximum catalog magnitude error")
    parser.add_argument(
        "--eflag_max", type=float, default=1, help="maximum eflag value")
    parser.add_argument(
        "--gr_min", type=float, default=0, 
        help="minimum g-r")
    parser.add_argument(
        "--gr_max", type=float, default=1.1, 
        help="maximum g-r")
    parser.add_argument(
        "--ri_min", type=float, default=0, 
        help="minimum g_r value for comparison stars")
    parser.add_argument(
        "--ri_max", type=float, default=0.8, 
        help="maximum g_r value for comparison stars")
    parser.add_argument(
        "--cntmfrac_max", type=float, default=0.05, 
        help="maximum contamination fraction")
    parser.add_argument(
        "--dedge_min", type=float, default=30, 
        help="maximum distance from the edge")
    parser.add_argument(
        "--nx", type=int, default=2160, help="number of pixels along x axis")
    parser.add_argument(
        "--ny", type=int, default=1280, help="number of pixels along y axis")
    parser.add_argument(
        "--key_x", type=str, default="x", help="keyword for x")
    parser.add_argument(
        "--key_y", type=str, default="y", help="keyword for y")
    parser.add_argument(
        "--title", type=str, default=None, help="title of the figure")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, 
        help="output messages")
    parser.add_argument(
        "--Nframe_min", type=int, default=10, 
        help="minimum frame numbers to be desired")
    parser.add_argument(
        "--yr", default=None, type=float, nargs=2, help="Y range in plot")
    parser.add_argument(
        "--JD0", default=2459514.5, type=float,
        help="time zero point in Juliand day (default is 2021-10-27)")
    parser.add_argument(
        "--loc", type=str, default="371",
        help="location of the observatory")
    parser.add_argument(
        "--stype", default="C", choices=["C", "S", "X", "D", "V"],
        help="spectral type(ex. C/S/X/D/V) for phase correction")
    args = parser.parse_args()

    # Save a figure in output directory
    outdir = "plot"
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    # Parse arguments
    # nx and ny are used to remove objects close to edge region
    if args.inst == "TriCCS":
        nx, ny = 2160, 1280
    else:
        nx, ny = args.nx, args.ny
    bands = args.bands

    # None by default
    # Do not remove objects with extreme colors
    gr_min, gr_max = args.gr_min, args.gr_max
    ri_min, ri_max = args.ri_min, args.ri_max
    catmag_min     = args.catmag_min
    catmag_max     = args.catmag_max
    catmagerr_max  = args.catmagerr_max
    cntmfrac_max   = args.cntmfrac_max
    dedge_min      = args.dedge_min
    key_x, key_y   = args.key_x, args.key_y
    # Selection criteria of reference stars
    Nframe_min    = args.Nframe_min
    # Selection criteria of the target
    eflag_max      = args.eflag_max
    verbose        = args.verbose
    # Number of bands
    N_band = len(bands)
    bands_str = "".join(bands)

    print("\nArguments")
    print(f"Select colors gr color          = {gr_min}--{gr_max}")
    print(f"              ri color          = {ri_min}--{ri_max}")
    print(f"              catalog mag       < {catmag_max}")
    print(f"              catalog mag error < {catmagerr_max}")
    print(f"              distance to edge  > {dedge_min}")
    print(f"              key_x, key_y      = {key_x}, {key_y}")
    print(f"              Nframe_min        = {Nframe_min}")
    print(f"              eflag             < {eflag_max}")


    print("\n================================================================")
    print("p1 Read photometric csv")
    print("==================================================================")
    df = pd.read_csv(args.csv, sep=" ")
    #df = df[0:10]
    print(f"  N_df = {len(df)} (original)")


    print("\n================================================================")
    print("p2 Clean the reference stars")
    print("==================================================================")
    # gr_min, gr_max are useless? 2022-04-30
    df, objID = clean_photres(
        df, bands, key_x, key_y, gr_min, gr_max, ri_min, ri_max, 
        catmag_min, catmag_max, catmagerr_max, verbose)


    print("\n================================================================")
    print("p3 Clean the target")
    print("==================================================================")
    # Select object by eflag
    N_bef = len(df)
    for b in bands:
        df = df[df[f"eflag_{b}"] < eflag_max]
    df = df.reset_index(drop=True)
    print(f"  N_df = {len(df)} (after cleaning)")
    
    # Select object by cntm
    for j,band in enumerate(bands):
        df = df[df[f"cntmfrac_{band}"] < cntmfrac_max]
    df = df.reset_index(drop=True)
    print(f"  N_df = {len(df)} (after cleaning)")

    # Check
    N_nan = np.sum(df.isnull().sum())
    assert N_nan==0, f"Nan N={N_nan}"

    print("\n================================================================")
    print("p4 Calculate distance from the edge in each frame")
    print("==================================================================")
    # Suppress PerformanceWarning
    df = df.copy()
    for b in bands:
        for obj in objID:
            df.loc[:,f"dedge_{obj}_{b}"] = calc_d_from_edge(
                df[f"{key_x}_{obj}_{b}"], df[f"{key_y}_{obj}_{b}"], 
                nx, ny)


    print("\n============================================================")
    print("p5 Check Nframe and remove Nframe < Nframe_min")
    print("==============================================================")
    
    print(f"N_objID = {len(objID)}")
    for b in bands:
        objID = remove_Nframe(
            df, objID, b, Nframe_min, eflag_max, dedge_min, cntmfrac_max)
    print(f"N_objID = {len(objID)} (after Nframe cleaning)")


    print("\n================================================================")
    print("p6 Calculate time in second and save for periodic analysis")
    print("==================================================================")
    for b in bands:
      # For periodic analysis 
      df.loc[:, f"t_sec_obs_{b}"] = df[f"t_mjd_{b}"]*24*3600.
      # Set the first time to zero
      df[f"t_sec_obs_{b}"] -= df.at[0, f"t_sec_obs_{b}"]


    print("\n==================================================================")
    print("p7 Plot raw and total flux light curves of stars")
    print("==================================================================")
    df_flux_list = []
    for b in bands:
        # Calculate 
        #   f_total_raw_{b}
        #   ferr_total_raw_{b}
        #   f_total_cor_{b}
        #   ferr_total_cor_{b}
        df_flux_band = refladder(
            df, objID, b, Nframe_min, eflag_max, dedge_min, cntmfrac_max)
        df_flux_band.loc[:, f"t_sec_obs_{b}"] = df[f"t_sec_obs_{b}"]
        df_flux_list.append(df_flux_band)
    df_flux_total = pd.concat(df_flux_list, axis=1)


    print("\n================================================================")
    print("p8 Plot relative lightcurves with correction")
    print("==================================================================")
    # Create axes depending on the number of bands
    # i.e. axes_flux      = [ax1, ax2, ax3] of [ax1, ax2] or [ax1]
    #      axes_templates = [ax4, ax5, ax6] of [ax3, ax4] or [ax2]
    #      axes_mag       = [ax7, ax8, ax9] of [ax5, ax6] or [ax3]
    #      axes_mag       = [ax10, ax11, ax12] of [ax7, ax8] or [ax4]
    df = plot_refladder(df, df_flux_total, bands, args.obj, outdir)

    print("\n================================================================")
    print("p9 Plot background count")
    print("==================================================================")
    # Create axes depending on the number of bands
    # i.e. axes_flux      = [ax1, ax2, ax3] of [ax1, ax2] or [ax1]
    #      axes_templates = [ax4, ax5, ax6] of [ax3, ax4] or [ax2]
    #      axes_mag       = [ax7, ax8, ax9] of [ax5, ax6] or [ax3]
    #      axes_mag       = [ax10, ax11, ax12] of [ax7, ax8] or [ax4]
    fig, axes = myfigure_ncol(n_band=len(bands), n_col=1)
    idx_c = int(np.floor(len(bands)/2))
    for idx, band in enumerate(bands):
        ax = axes[idx]
        plot_bg(df, ax, band, bands)
       
        # Plot ylabel for central axis
        if idx == idx_c:
            ax.set_ylabel(f"Background level [ADU]")
        if idx == (len(bands)-1):
            ax.set_xlabel(f"Elapsed time [s]")
        else:
            ax.axes.xaxis.set_visible(False)
    out =  f"{args.obj}_bg_N{len(df)}_{bands_str}.png"
    out = os.path.join(outdir, out)
    fig.savefig(out)
    plt.close()


    print("==================================================================")
    print("p10 Save data for the periodic analysis")
    print("==================================================================")
    # Correct time and brightness with JD0 (time zero point)
    df["obj"] = args.obj

    # Obtain aspect data
    # All col (mag, t_sec_obs, t_sec_ltcor, red_mag, red_mag_phase) have suffix 
    for b in bands:
        # r (asteroid-Sun), delta (asteroid-observer), alpha, PAB
        # Use t_jd_{band}
        df = add_aspect_data(df, args.loc, args.JD0, b)
        # Do time correction (t_sec_obj to t_sec_ltcor)
        df = time_correction(df, b)
        # Do brightness correction (delta, r)
        # Mag is reduced and magnitude (mag_red)
        df = mag_correction(df, suffix=b)
        # Do phase correction (alpha)
        df = phase_correction(df, args.stype, b)
    
    for b in bands:
      band_l, band_r = band4CTG(b, bands)
      
      # Save mag
      col_use_b = [
          f"t_sec_obs_{b}", f"t_sec_ltcor_{b}", f"t_jd_{b}", f"t_jd_ltcor_{b}", 
          f"flux_{b}", f"fluxerr_{b}", f"mag_{b}", f"mag_red_{b}", f"mag_red_phase_{b}", 
          f"magerr_{b}", f"eflag_{b}"]
      df_temp = df[col_use_b]
      # Remove band identical suffix for successive analyses
      # t_sec_ltcor -> t_sec_ltcor etc.
      df_temp = remove_suffix(df_temp, b)
      df_temp.to_csv(
          f"{args.obj}_refladder_mag_{b}.csv", sep=" ", index=False)
