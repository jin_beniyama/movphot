#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Lighten the photometry_result.txt.

Color criteria 0 < g-r < 1.1 and 0 < r-i < 0.8 are 
based on the investigation of Phaethon photomery by JB.

Use --wobaffle for old observations to remove edge regions
since wide regions are affected by stray light.
"""
from argparse import ArgumentParser as ap
import pandas as pd

from movphot.prepro import (
    lighten_photres, clean_photres_ref, clean_photres_target, set_baffle_TriCCS)


if __name__ == "__main__":
    parser = ap(description="Lighten the photometry_result.txt.")
    parser.add_argument(
        "res", type=str, help="photometry_result.txt")
    parser.add_argument(
        "--catmag_min", type=float, default=13.5,
        help="minimum magnitude")
    parser.add_argument(
        "--catmag_max", type=float, default=20,
        help="maximum magnitude")
    parser.add_argument(
        "--catmagerr_max", type=float, default=0.05, 
        help="maximum catalog magnitude error")
    parser.add_argument(
        "--eflag_max", type=float, default=1, 
        help="maximum eflag value for both object and reference stars")
    parser.add_argument(
        "--ignorewinpos", action="store_true", default=False, 
        help="Use objects with winposflag (1)")
    parser.add_argument(
        "--bands", nargs="*", default=["g", "r", "i"],
        help="bands")
    parser.add_argument(
        "--gr_min", type=float, default=0, 
        help="minimum g-r value for comparison stars")
    parser.add_argument(
        "--gr_max", type=float, default=1.1, 
        help="maximum g-r value for comparison stars")
    parser.add_argument(
        "--ri_min", type=float, default=0, 
        help="minimum r-i value for comparison stars")
    parser.add_argument(
        "--ri_max", type=float, default=0.8, 
        help="maximum r-i value for comparison stars")
    parser.add_argument(
        "--dedge_min", type=int, default=30, 
        help="Removed edge region in pixel")
    parser.add_argument(
        "--nx", type=int, default=2160, 
        help="number of pixels along x axis")
    parser.add_argument(
        "--ny", type=int, default=1280, 
        help="number of pixels along y axis")
    parser.add_argument(
        "--key_x", type=str, default="xwin", 
        help="keyword for x")
    parser.add_argument(
        "--key_y", type=str, default="ywin", 
        help="keyword for y")
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, 
        help="output messages")
    parser.add_argument(
        "--wobaffle", action="store_true", default=False, 
        help="Set when observations are done without baffles")
    args = parser.parse_args()

    
    # Parse arguments
    gr_min, gr_max = args.gr_min, args.gr_max
    ri_min, ri_max = args.ri_min, args.ri_max
    catmag_min     = args.catmag_min
    catmag_max     = args.catmag_max
    catmagerr_max  = args.catmagerr_max
    dedge_min      = args.dedge_min
    key_x, key_y   = args.key_x, args.key_y
    eflag_max      = args.eflag_max
    verbose        = args.verbose
    bands          = args.bands
    nx, ny         = args.nx, args.ny

    catmag_max_str    = "f".join(str(catmag_max).split("."))
    catmag_min_str    = "f".join(str(catmag_min).split("."))
    catmagerr_str = "f".join(str(catmagerr_max).split("."))
    dedge_str     = "f".join(str(dedge_min).split("."))
    eflag_str     = "f".join(str(eflag_max).split("."))

    filename_tail = (
        #f"mag{catmag_min_str}to{catmag_max_str}magerr{catmagerr_str}"
        f"magerr{catmagerr_str}"
        f"dedge{dedge_str}eflag{eflag_str}")

    print("\nArguments")
    print(f"Select colors gr color          = {gr_min}--{gr_max}")
    print(f"              ri color          = {ri_min}--{ri_max}")
    print(f"              catalog mag       > {catmag_min}")
    print(f"              catalog mag       < {catmag_max}")
    print(f"              catalog mag error < {catmagerr_max}")
    print(f"              distance to edge  > {dedge_min}")
    print(f"              key_x, key_y      = {key_x}, {key_y}")
    print(f"              eflag             < {eflag_max}")


    # Read result
    df = pd.read_csv(args.res, sep=" ")

    # Removed useless phrase to reduce the data
    # Update 2022-07-17
    phrase = [
        "radius", "qualityFlag", "raMean", "decMean",
        "nDetections", "nStackDetections", "raMeanErr", "decMeanErr",
        "QfPerfect", "p_scale", "inst", "index", "t_mjd"]
    df = lighten_photres(df, phrase)
    df = df.reset_index(drop=True)


    df_obj = df[df["objtype"] == "target"]
    df_ref = df[df["objtype"] == "ref"]


    # Remove object by winposflag
    #   Objects with bad winpos may have negative flux 
    #   or affected by nearby objects.
    #   Not valid for nowinpos data (with --nowinpos option in phot_color.py)
    if args.ignorewinpos:
        pass
    else:
        try:
            df_ref = df_ref[df_ref["winposflag"] == 0]
            df_obj = df_obj[df_obj["winposflag"] == 0]
        except:
            print("Skip selection with winposflag.")
            pass
    

    # Remove objects close to edge regions for old data (--2021/08 ?)
    # New data is ok since baffles are mounted.
    if args.wobaffle:
        df_ref = set_baffle_TriCCS(df_ref, width=250, nx=nx, ny=ny)

    # Clean the photometry result of reference stars
    try:
        df_ref  = clean_photres_ref(
            df_ref, bands, key_x, key_y, gr_min, gr_max, ri_min, ri_max, 
            catmag_min, catmag_max, catmagerr_max, dedge_min, nx, ny,
            eflag_max, verbose)
        out_ref = f"photres_ref_{filename_tail}.txt"
        df_ref = df_ref.reset_index(drop=True)
        df_ref.to_csv(out_ref, sep=" ", index=False)
    except:
        print("    Failed to get photres_ref...")

    if len(df_obj) > 0:
        try:
            # Clean the photometry result of the target
            df_obj  = clean_photres_target(
                df_obj, key_x, key_y, dedge_min, nx, ny, eflag_max, verbose)
            out_obj = f"photres_obj_{filename_tail}.txt"
            df_obj = df_obj.reset_index(drop=True)
            df_obj.to_csv(out_obj, sep=" ", index=False)
        except:
            print("    Failed to get photres_obj...")
