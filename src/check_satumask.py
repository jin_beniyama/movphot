#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Check saturation mask of input fits.
"""
import os 
from argparse import ArgumentParser as ap
import numpy as np
import matplotlib.pyplot as plt  
import sep
import astropy.io.fits as fits
from mpl_toolkits.mplot3d import axes3d
from matplotlib.ticker import AutoMinorLocator
from scipy.stats import sigmaclip

from myplot import mycolor, mymark, myls
import movphot as mp
from movphot.photfunc import obtain_winpos, calc_FWHM_fits, remove_background2d, create_saturation_mask, triccsgain
from movphot.common import extract1darr, fit_psf, radial_profile


if __name__ == "__main__":
    parser = ap(description="Plot color color diagrams.")
    parser.add_argument(
        "fits", type=str,  
        help="input fits file")
    parser.add_argument(
        "--inst", type=str, required=True, choices=["TriCCS", "murikabushi"],
        help="used instrument (for gain)")
    parser.add_argument(
        "--xr", type=int, nargs=2, default=None, 
         help="x range")
    parser.add_argument(
       "--yr", type=int, nargs=2, default=None, 
      help="y range")
    args = parser.parse_args()


    # fits source  (HDU0, header + image data)
    src = fits.open(args.fits)[0]
    # fits header
    hdr = src.header
    # 2-d image
    image = src.data.byteswap().newbyteorder()

    # Cut image
    if args.xr:
        xmin, xmax = args.xr
        ymin, ymax = args.yr
        image = image[int(ymin):int(ymax), int(xmin):int(xmax)]
        image  = image.copy(order='C')


    # Movphot Class
    ph = mp.Movphot(
        args.inst)
    image_satu = create_saturation_mask(image, ph.satu_count)
    N_mask = len(image_satu[image_satu==True])
    print(f"N_mask = {N_mask}")

    # BG subtraction
    image, _ = remove_background2d(image, mask=None)


    # Plot src image after 5-sigma clipping 
    sigma = 5
    _, vmin, vmax = sigmaclip(image, sigma, sigma)
    ny, nx = image.shape

    fig = plt.figure(figsize=(12,int(12*ny/nx)))
    ax = fig.add_subplot(111)
    ax.imshow(image, cmap='gray', vmin=vmin, vmax=vmax)

    # Plot mask
    ax.imshow(image_satu, alpha=0.2)
    ax.set_xlim([0, nx])
    ax.set_ylim([0, ny])
    ax.legend().get_frame().set_alpha(1.0)
    ax.invert_yaxis()
    plt.tight_layout()
    out = "satuimage.png"
    plt.savefig(out, dpi=200)
    plt.close()
