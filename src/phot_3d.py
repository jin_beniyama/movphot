#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Do Photometory for arbitrary x,y coordinates.
"""
import os
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import sep
import astropy.io.fits as fits

import matplotlib.pyplot as plt
from scipy.stats import sigmaclip

from calcerror import diverr
import movphot as mp
from movphot.common import get_filename, nowstr
from movphot.photfunc import obtain_winpos
from movphot.visualization import mycolor, add_circle_with_radius

plt.rcParams["font.family"] = "Arial"


if __name__ == "__main__":
    parser = ap(description="Point photometry")
    parser.add_argument(
        "fits", type=str,
        help="3-d fits file (Tomo-e or TriCCS)")
    parser.add_argument(
        "-x", type=int, nargs="*", 
        help="x in pixel coordinate")
    parser.add_argument(
        "-y", type=int, nargs="*", 
        help="y in pixel coordinate")
    parser.add_argument(
        "--obj", type=str, nargs="*", 
        help="object name")
    parser.add_argument(
        "--zr", type=int, nargs=2,  
        help="z range")
    parser.add_argument(
        "--inst", type=str, required=True, 
        choices=["TriCCS", "tomoe"],
        help="used instrument")
    parser.add_argument(
        "-r", "--radius", type=float, default=10, 
        help="aperture radius in unit of pixel")
    parser.add_argument(
        "--phottype",  type=str, default="app",
        help='app, ann (faint object), or iso (bright)')
    parser.add_argument(
        "--key_x", type=str, default="x", 
        help="keyworf of x")
    parser.add_argument(
        "--key_y", type=str, default="y", 
        help="keyworf of y")
    parser.add_argument(
        "--nowinpos", action="store_true", default=False,
        help="Do not use a winpos as an aperture location (for faint objects)")
    parser.add_argument(
        "--out", type=str, default=None, 
         help="output csv file")
    args = parser.parse_args()
    
    # Parse arguments
    radius = args.radius
    inst   = args.inst
    param_app = dict(
        radius=radius)
    param_iso = dict(
        r_disk=0, epsilon=0, sigma=0, minarea=0)

    if args.nowinpos:
        winpos = False
    else:
        winpos = True

    phottype = args.phottype
    key_x, key_y = args.key_x, args.key_y

    # Movphot Class
    # Target name == None
    # refmagmin, refmagmax are all None
    ph = mp.Movphot(
        inst, None, None, None, radius, param_app, param_iso)

    fitsID = get_filename(args.fits)
    now = nowstr()
    outdir = f"{now}_{fitsID}"
    if not os.path.isdir(outdir):
        os.makedirs(outdir, exist_ok=True)

    # Open fits
    src = fits.open(args.fits)[0]
    hdr = src.header
    # Obtain starting time (self.t_utc, self.t_mjd, and self.t_jd) in hdr
    ph.obtain_time_from_hdr(hdr)
    cube = src.data.byteswap().newbyteorder()
    assert len(cube.shape)==3, "Input should be 3-d."
    if args.zr:
        z0, z1 = args.tr[0], args.tr[1]
        cube = cube[z0:z1]
    nz, ny, nx = cube.shape
    ph.nx = nx
    ph.ny = ny
    print(f"Shape of the input fits (nx, ny, nz) = ({nx}, {ny}, {nz})")

    df_list = []
    for z in range(nz):
        # Extract image
        image = cube[z]

        # Backgroung calculation
        image = ph.remove_background(image, None)
        print(f"BG mean: {ph.bg_level:.2f} rms: {ph.bg_rms:.2f}")

        # Single exposure time
        t_frame = hdr[ph.hdr_kwd["exp"]]


        # Do Photometry
        for idx_obj, (x_obj, y_obj, obj) in enumerate(zip(args.x, args.y, args.obj)):
            # Winpos for plot
            x_obj, y_obj, _ = obtain_winpos(
                image, [x_obj], [y_obj], [radius], nx, ny)

            df_obj = ph.phot_loc(
                image, hdr, phottype, x_obj, y_obj, winpos)

            # Update time with time shift
            # Time is not common
            ts = calc_time_shift((nx, ny), x_obj, y_obj)
            print(f"Time shift = {ts}")
            ph.t_jd = ph.t_jd + (t_frame + ts)/3600./24.

            df_obj["nframe"] = z+1
            df_obj["obj"]    = obj
            ph.add_photometry_info(df_obj)
            df_list.append(df_obj)

    df = pd.concat(df_list)
    # Remove dummy columns
    df = df.drop(columns="t_utc")
    df = df.drop(columns="t_mjd")
    df = df.reset_index(drop=True)

    out = "photres.txt"
    out = os.path.join(outdir, out)
    df.to_csv(out, sep=" ", index=False)


    # Plot lcs ================================================================
    fig = plt.figure(figsize=(20, 20))
    
    # Last image
    ax_img = fig.add_axes([0.1, 0.55, 0.80, 0.4])
    ax_img.set_xlabel("x")
    ax_img.set_ylabel("y")
    ax_img.set_xlim([0, ph.nx])
    ax_img.set_ylim([0, ph.ny])
    sigma = 5
    _, vmin, vmax = sigmaclip(image, sigma, sigma)
    ax_img.imshow(image, cmap='gray', vmin=vmin, vmax=vmax)

    for idx_obj, obj in enumerate(args.obj):
        color = mycolor[idx_obj]
        ls    = "dashed"
        label = f"{obj} rad={radius}"
        df_temp = df[(df["obj"]==obj) & (df["nframe"]==nz)]
        add_circle_with_radius(
            ax_img, df_temp, key_x, key_y, radius, color, ls, label)

    # Raw lcs
    ax1 = fig.add_axes([0.15, 0.28, 0.80, 0.22])
    ax1.axes.xaxis.set_visible(False)
    ax1.set_ylabel("Raw Flux [ADU]")
    for idx_obj, obj in enumerate(args.obj):
        col = mycolor[idx_obj]
        # Extract object 
        df_temp = df[df["obj"]==obj]
        ax1.errorbar(
            df_temp["t_jd"], df_temp[f"flux"], df_temp[f"fluxerr"],
            marker=None, color=col, ls="None")
        ax1.scatter(
            df_temp["t_jd"], df_temp[f"flux"], label=f"{obj}", 
            ec=col, facecolor="None", s=300)
    
    if len(obj) > 1:
        obj1, obj2 = args.obj[0], args.obj[1]
        ax2 = fig.add_axes([0.15, 0.04, 0.80, 0.22])
        ax2.set_ylabel(f"Normalized flux of {obj1}")

        df1, df2 = df[(df["obj"]==obj1)], df[(df["obj"]==obj2)]
        flux_cor = [f1/f2 for f1, f2 in zip(df1["flux"], df2["flux"])]
        flux_cor_mean = np.mean(flux_cor)
        flux_cor_norm = flux_cor / flux_cor_mean
        fluxerr_cor = []
        for f, ferr, f_ref, ferr_ref in zip(
              df1["flux"], df1["fluxerr"], df2["flux"], df2["fluxerr"]):
            temperr = diverr(f, ferr, f_ref, ferr_ref)
            fluxerr_cor.append(temperr)
        fluxerr_cor_norm = [ferr / flux_cor_mean for ferr in fluxerr_cor]
          
        color = "black"
        # Use time of obj1
        ax2.errorbar(
            df1["t_jd"], flux_cor_norm, fluxerr_cor_norm, 
            marker=None, color=color, ls="None")
        ax2.scatter(
            df1["t_jd"], flux_cor_norm, ec=color, 
            facecolor="None", s=300, label=f"{obj1}/{obj2}")
 
    for ax in [ax_img, ax1, ax2]:
        ax.legend()

    x, y = -0.07, 0.5
    for ax in [ax1, ax2]:
        ax.yaxis.set_label_coords(x, y)

    ymin, ymax = -0.2, 1.3
    ax2.set_ylim([ymin, ymax])

    out = fitsID + "_lc.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    # Plot lcs ================================================================
