# Moving Object Photometry (movphot)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

[developer mail](mailto:beniyama@ioa.s.u-tokyo.ac.jp)

## Overview

Photometry of moving objects could be done in this repository.
Though optimized for Seimei/TriCCS data,
you can apply it for imaging data taken with
other telescopes (MITSuME, MuSaSHI, MuSCUT, etc.)

Successive analyses below are done using other scripts.
See README.md in each directory for detail.

- Periodic analysis
Use `package4peri`(in preparation).
- Visualization (Plot object light curve, comparison light curve, etc.)
and Spectral type estimation
Use `package4vis`(in preparation) to plot lightcurve, color-color diagrams and
reflectance on Bus-DeMeo templates.

## Procedure

1. Standard reduction
dark(bias) subtraction, flat fielding, split fits from 3d to 2d
Use `TriCCS Data Reduction (TDR)` ([bitbucket](https://bitbucket.org/jin_beniyama/triccs-data-reduction/src/master/)).
2. Create a comparison star catalog
3. Create standard location text (x, y, nframe)
4. Photometry

## Installing
```
# Install from PyPI
pip install movphot
```
or
```
# Install with `setup.py`
git clone https://jin_beniyama@bitbucket.org/jin_beniyama/movphot.git
python setup.py install
```

## Usage

Here 3-bands observation by Seimei/TriCCS is assumed.
So `psdb.py` is used to handling the database for comparison stars.
If you need other catalogs for data taken by other instruments,
you can also use `gaiadb.py` or other scripts.

### 0. Standard reduction

Input fits data should be 2-dimensional even taken with TriCCS.
Standard reduction and splitting fits could be done using scripts in
`TriCCS Data Reduction (TDR)` ([bitbucket](https://bitbucket.org/jin_beniyama/triccs-data-reduction/src/master/)).


### 1. Create a comparison star catalog

The first step before photometry is to construct a homemade catalog database.
When your fits file has almost correct WCS coordinates,
type commands below.

At first, create a catalog database you are interested in.
When you used Seimei/TriCCS(Pan-STARRS system), create
Pan-STARRS catalog database as `ps.db` in `~/db4movphot/` (hereinafter `db`).
Please specify a directory to change the path of the database
in `psdb.py` and in `phot_color` as optional arguments.
The database should be created manually until the automated function will be
implemented in near future.

```
[usage]
# Create database (once) in movphot/src
python psdb.py first
# Create new tables
psdb.py create --table (table name)
# Insert stars to database
psdb.py insert --tablename (table name) --ra (ra in degree) --dec (dec in degree)
--radius (fov radius in degree) --magmin (minimum magnitude) --magmax (maximum magnitude)
# Check stars in database
psdb.py extract --tablename (table name) --ra (ra in degree) --dec (dec in degree)
--radius (fov radius in degree) --magmin (minimum magnitude) --magmax (maximum magnitude)

[example]
# Create 2021DX1 table.
# Observed locations are below.
psdb.py create --table _2021DX1
psdb.py insert --table _2021DX1 --ra 208.87 --dec 44.68 --radius 0.2 --magmin 12 --magmax 21
```

### 2. Create standard location text

The next step is registering target locations.
Ephemerides of newly discovered fast-moving (v > a few arcsec/s) asteroids
are sometimes not perfect.
In this script, use some observed points of the target in the 2nd band and
interpolate and extrapolate them to determine photometry circle locations.
If the target is close to any stars, it is better to remove it as an outlier.
But `eflag` is useful to remove contaminations after photometry.

All information needed to create location text is x, y (pixel number) and
nframe (number of frames).
The format example
when moving object is at (100,200) and (150, 230) in 3rd and 13th frames.

```
[location_band2.txt]
x y nframe
100 200 3
150 230 13
```

### 3. Photometry

Photometry is done using `ps.db`, `location.txt` and some input parameters.
Typical FWHM is needed (by IRAF projection etc.)
and should be converted to pixel scale by the observer.
There are 2 photometry types:

1. app (circle aperture photometry)
2. iso (isophotal photometry, in prep.)

Before photometry, create a fits list, for example, by

```
cat *(band unique character)*.fits > fitslist_band.txt
```

and save as `fitslist_band1.txt`, `fitslist_band2.txt` and `fitslist_band3.txt`.

The photometry command examples are below.
If the database is not in the current directory, please specify the `db` path.
2 parameters, `refmagmax` and `refmagmin`, are used to

1. create a mask to calculate `eflag` values (both target and comparison stars)
2. do comparison star photometry
.

```
[usage]
phot_color.py --target (obj) --inst (instrumental) 
--flist (fitslist_band1.txt) (fitslist_band2.txt) (fitslist_band3.txt)
--fitsdir (fitsdir) 
--standard (location_band2.txt) --bands (band1) (band2) (band3) --catalog (catalogname)
--radius_ref (photometry radius of comparison stars) --radius_obj (photometry radius of object) 
--autorad (estimate radius from automatically calculated FWHM of PSF frame by frame)
--refphot (photmetry type of comparison stars) --tarphot (photmetry type of target)
--idx_standard (index of standard file) --phot (true to plot photometry region)
--refmagmax (maximum comparison magnitude, N is number of band)

[example]
phot_color.py --target 3200 --inst TriCCS 
--flist glist_w_408.txt rlist_w_408.txt ilist_w_408.txt 
--fitsdir /data/asteroid/TriCCS_2021/Phaethon2021/20211126/3200/wcs 
--standard standard_g_408.txt --bands g r i --catalog ps  
--refphot app --tarphot app --dbdir /home/neo/Script/catalog/ 
--idx_standard 0 --phot --refmagmax 21 21 22
```

The result is saved as `photometry_result.txt`.
Visualization and detail analyses can be done in other repositories in the overview.
Unfavorable objects (such as too redder or low Quality) should be removed
at that time.


## Visualization

### 0. Preprocessing
Extract photometric results (of both target and reference stars) in good conditions.

```
[usage]
cleaning_photres.py photometry_result.csv --catmag_max (maximum catalog manitude) 
--catmagerr_max (acceptable magnitude error) --catmag_min (minimum cactalog magnitude) 
--dedge_min (maximum distance from the edge of sensor)

[example]
cleaning_photres.py photometry_result.csv --catmag_max 17 --catmagerr_max 0.05 --catmag_min 13.5 --dedge_min 200
```

### 1. Plot light curves of object relative magnitude by `plot_refladder_total2.py`
Slope parameter G should be updated after all analysis (i.e. phase curve fitting).

```
[usage]
plot_refladder_total2.py  (preprocessed photometry result of target) 
(preprocessed photometry result of reference stars) --target (target name) 
--bands (bands of filters) --loc (MPC code) --rotP (rotational period in hour)  
-G (slope parameter) --Nframe_min (minimum number of frames) --overwrite

[example]
plot_refladder_total2.py  photres_obj* photres_ref*  --target 3200 
--bands g r i --loc 371 --rotP 3.6  -G 0.053 --Nframe_min 50 --overwrite
```

### 2. Derive colors by `derive_color2.py`

```
[usage]
derive_color2.py (preprocessed photometry result of target) (preprocessed photometry result of reference stars)
(catalog name) --bands (bands of filters) --Nobj_min (necessary number of stars in a frame)
--target (target name) --overwrite

[example]
derive_color2.py photres_obj* photres_ref* PS --bands g r i --Nobj_min 4 --target 3200 --overwrite
```

### 3. Derive magnitudes by `derive_mag2.py`
```
[usage]
derive_mag2.py (preprocessed photometry result of target) (preprocessed photometry result of reference stars)
(result of 2. Derive colors) (catalog name) --bands (bands of filters) --Nobj_min (necessary number of stars in a frame)
--target (target name) --overwrite

[example]
derive_mag2.py photres_obj* photres_ref*  COLTXT PS --bands g r i --Nobj_min 5 --target 3200 --overwrite
```



## Note
This script can ignore bad pixels close to the edge of the sensor 
(hereinafter edge region, see a cartoon below) 
both when photometry and when visualization.
```
[a cartoon of typical edge region (2021.03 ver.)]
# a means type 1 edge
# c means type 2 edge
  x -->

y aaaaaaaaaaaaaaaaaaaaaaaaaa
| acc                    cca
| acc                    cca
v a                        a
  a                        a
  a                        a
  acc                    cca
  acc                    cca
  aaaaaaaaaaaaaaaaaaaaaaaaaa
```

There are 2 types of bad pixels close to the edge region:
  
1. along the edge (a)
   This bad pixels could be specified when photmetry as `dead_pix` 
   in `movphot.movphot.Movphot.__init__`.
   The default value (width) is 30 pixels.

2. corner (c)
   This bad pixels could be specified when visualization (i.e. after photmetry) 
   in `remove_corner` function in `movphot.movphot.scripts.plot_colorterm.py`.
   The default corner is 50x50 box.

Please check and edit the script when your photometric result seems bad.

* Bad pixel mask is implemented. (2023-05-02)

## To Do
- Determine locations of the target using ephemeris from JPL/HORIZONS.
- Check keywords (UTC and UTC0)

## Dependencies

This library is depending on `NumPy`, `SciPy`, `SEP`, `Astropy` 
and `Astroquery`.
Scripts are developed on `Python 3.7.10`, `NumPy 1.19.2`, `SciPy 1.6.1`,
`SEP 1.0.3`, `Astropy 4.2` and `Astroquery 0.4.1`.

## LICENCE

This software is released under the MIT License.
