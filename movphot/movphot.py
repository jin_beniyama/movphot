#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Relative photometory class using Gaia/Pan-STARRS catalog.
This function tracks moving objects (e.g. asteorids etc.).

Dependence
----------
class Movhot ---- photfunc.py ----- [catalog object photmetry]
                     |               appphot_catalog, isophot_catalog
                     |
                     |
                     | ------------ [object photometry]
                                     apphot_loc, isophot_loc
"""
import os
import sys
import datetime 
import numpy as np
import pandas as pd
import sep
import astropy.io.fits as fits
from astropy.wcs import WCS as wcs
from astropy.time import Time

## Path to movphot.py
BASE = os.path.dirname(os.path.abspath(__file__))
## Path to DataBase
DBPATH = os.path.normpath(os.path.join(BASE, "../src"))

# Functions to handle database.
from .common import log10err, adderr_series, time_keeper
from .photfunc import *
from .psdb import extract_ps


class Movphot:
    def __init__(
        self, inst, obj=None, refmagmin=0, refmagmax=99, radius_mask=10, 
        param_app={"radius":10, "ann_gap":3, "ann_width":3}, 
        param_iso={"r_disk":10, "epsilon":30, "sigma":5, "minarea":5},
        bw=128, fw=3):
        """
        Moving object photometry

        To Do
        -----
            (ref:app and obj:iso) combination

        Parameters
        ----------
        inst : str
            instrument name to indentify header keyword
        obj : str
            object name (used to choice handmade database table)
        refmagmin/refmagmax : float
            minimum/maximum magnitude of stars used as reference
        radius_mask : float
            radius of various type of masks
        param_app_obj : dict
            apperture photometry parameter of an object (radius)
        param_iso : dict, optional
            isophotal photometry parameter (r_disk, epsilon, sigma, minarea)
        """


        # Define header keywords.
        if inst=="TriCCS":
            # Example
            # DATE-OBS= '2021-03-08'         / observation date
            # UTC     = '2021-03-08T15:46:18.152736' / exposure starting date and time
            # TFRAME  =           0.99646400 / [s] frame interval in seconds
            # GAINCNFG= 'x4      '           / sensor gain setting
            
            # 2022-04-13
            # GAIN keyword is implemented at least on 2022-11-26 as below
            # GAIN    =                  1.5 / Sensor inverse gain (electron/ADU)
            self.hdr_kwd = dict(
              datetime="UTC", date="DATE-OBS", time=None, 
              exp="TFRAME", gain="GAINCNFG")
            # 0.34 -> 0.350 arcsec/pixel (2022-04-28)
            self.p_scale = 0.350
            # Saturation count (2021.08.21. ver., conservative ?)
            self.satu_count = 10000.
            # Dead zone
            # not used region is dead_pix + radius from each edge
            # from 2021CC2, 2021/02/24
            self.dead_pix = 30
            self.nx = 2160
            self.ny = 1280

        if inst=="murikabushi":
            # Example
            # DATE-OBS= '2020-10-28'         /  [yyyy-mm-dd] Observation start date
            # UT      = '14:10:15.84'        / [HH:MM:SS.SS] Universal Time at start
            # EXPTIME =               30.000 / [sec] Exposure time
            # GAIN    =                 1.70 / [e-/ADU] CCD gain
            self.hdr_kwd = dict(
              datetime=None, date="DATE-OBS", time="UT", 
              exp="EXPTIME", gain="GAIN")
            # 0.72 arcsec/pixel
            self.p_scale = 0.72
            # check ?
            self.satu_count = 15000.
            self.dead_pix = 0

        if inst=="tomoe":
            # Example 
            # UTC     = '2020-10-27T17:34:43.937610' / exposure starting date and time
            # DATE-OBS= '2020-10-27'         / observation date
            # TIME-OBS= '17:34:43.937610'    / exposure starting time
            # EXPTIME1=             0.499904 / [s] exposure time per frame 
            self.hdr_kwd = dict(
                datetime="UTC", date="DATE-OBS", time="TIME-OBS", 
                exp="EXPTIME1", gain="GAIN")
            self.p_scale = 1.189
            self.satu_count = 25000.
            self.dead_pix = 0
            # For full frame !!
            # Please overwrite nx and ny for partial photmetry
            print("  !! nx and ny are set for full frame observations !!")
            self.nx = 2000
            self.ny = 1180

        if inst=="wfgs2":
            # 2023-01-29
            # Example
            # DATE-OBS= '2022-12-20'         / Start UTC date (devided from original)
            # UT-CEN  = '15:30:37.061'       / Central UTC (from DATE_OBS, EXPTIME)
            # EXPTIME =         3.000000E+02 / Total Exposure Time (s)
            # GAIN    =                 2.28 / CCD gain in e/ADU, ref: MINT wiki
            # SCALE   =                0.198 / Approx arcsecs per pixel
            self.hdr_kwd = dict(
                datetime=None, date="DATE-OBS", time="UT-CEN", 
                exp="EXPTIME", gain="GAIN")
            self.p_scale = 0.198
            # Saturation count. Im not sure.
            self.satu_count = 15000.
            self.nx = 800
            self.ny = 2000
      
        if inst=="MSI":
            # 2023-01-29
            # Example
            # DATE-OBS= '2022-12-21'         / [yyyy-mm-dd] UTC date at exposure
            # UT-STR  = '15:43:40.0'         / [hh:mm:ss.s] UTC at exposure start
            # EXPTIME =              180.000 / [s] Exposure time per frame
            # GAIN    =                 1.65 / [electrons/DN] Effective AD conversion factor

            self.hdr_kwd = dict(
                datetime=None, date="DATE-OBS", time="UT-STR", 
                exp="EXPTIME", gain="GAIN")
            # See Ishiguro+2022, MNRAS
            self.p_scale = 0.39
            # Saturation count. Im not sure.
            self.satu_count = 15000.
            self.nx = 512
            self.ny = 512

        self.inst = inst
        # For background subtraction
        self.bw = bw
        self.fw = fw

        ## Aperture photmetry parameter
        self.radius    = param_app["radius"]
        self.ann_gap   = param_app["ann_gap"]
        self.ann_width = param_app["ann_width"]

        ## Isophotal photmetry parameter
        self.r_disk  = param_iso["r_disk"]
        self.epsilon = param_iso["epsilon"]
        self.sigma   = param_iso["sigma"]
        self.minarea = param_iso["minarea"]
    
        # Catalog stars
        self.refmagmin   = refmagmin
        self.refmagmax   = refmagmax
        self.radius_mask = radius_mask
        self.obj         = obj
        self.table       = f"_{obj}"


    def set_catalog(self, catalog, dbdir):
        """Set catalog for reference star photmetry.

        Parameters
        ---------_
        catalog : str
            catalog name
        dbdir : str
            databse directory 
        """
        catalogs = ["gaia", "ps", "sdss"]
        assert catalog in catalogs, f"Invalid catalog : {catalog}."
        self.catalog = catalog

        if catalog=="gaia":
            self.db        = os.path.join(dbdir, "gaia.db")
            self.kwd_ra    = "ra"
            self.kwd_dec   = "dec"
            self.kwd_objID = "objID"
        if catalog=="ps":
            self.db        = os.path.join(dbdir, "ps.db")
            self.kwd_ra    = "raMean"
            self.kwd_dec   = "decMean"
            self.kwd_objID = "objID"
        if catalog=="sdss":
            self.db        = os.path.join(dbdir, "sdss.db")
            self.kwd_ra    = "ra"
            self.kwd_dec   = "dec"
            self.kwd_objID = "objID"


    #@time_keeper
    def remove_background(self, image, mask=None):
        """
        Subtract background and add its infomation.
        Original image is not background subtracted.

        Parameter
        ---------
        src : HDU object
          fits HDU 
        """

        bkg = sep.Background(
            image, mask=mask, bw=self.bw, bh=self.bw, fw=self.fw, fh=self.fw)
        image_bg = bkg.back()
        image_bgsub = image - image_bg
        
        bg_global = bkg.globalback
        bg_rms = bkg.globalrms
        bg_info = {"level": bg_global, "rms": bg_rms}

        # Save background info
        self.bg_level = bg_info["level"]
        self.bg_rms = bg_info["rms"]

        return image_bgsub


    def return_background(self, image, mask=None):
        """
        Return background image.

        Parameter
        ---------
        image : array-like
            input image

        Return
        ------
        image_bg : array-like
            background image
        """
        bkg = sep.Background(
            image, mask=mask, bw=self.bw, bh=self.bw, fw=self.fw, fh=self.fw)
        image_bg = bkg.back()
        return image_bg


    #@time_keeper
    def calc_background(self, src, mask):
        """
        Calculate background and add its infomation.

        Parameter
        ---------
        src : HDU object
          fits HDU 
        """
        image = src.data.byteswap().newbyteorder()
        image, bg_info = remove_background2d(image, mask=None)
        self.bg_level = bg_info["level"]
        self.bg_rms = bg_info["rms"]


    #@time_keeper
    def create_mask(self, src, band):
        """
        Create mask of reference stars using sep.extract.
        Combine saturation mask and catalog star mask.

        To do
        -----
            extended object (e.g. galaxy) mask ? (PS includes galaxies)

        Parameters
        ----------
        src : HDU object
            fits HDU 
        band : str
            band of objects in catalog
        """
        image = src.data.byteswap().newbyteorder()
        ny, nx = image.shape
        hdr = src.header
        w = wcs(header=hdr)

        # # Create mask of extracted objects.
        # ext_mask = create_ext_mask(image, err=self.bg_rms)

        # Create mask of saturated objects.
        satu_mask = create_saturation_mask(image, self.satu_count)

        # Create mask of catalog objects.
        ## Center of cordinate and radius of catalog search region.
        cra, cdec, fovradius = search_param(w, nx, ny)
        if self.catalog=="gaia":
            df_cat = extract_gaia(
                self.db, self.table, cra, cdec, fovradius, 
                self.refmagmin, self.refmagmax)
        elif self.catalog=="ps":
            df_cat = extract_ps(
                self.db, self.table, cra, cdec, fovradius, 
                band, self.refmagmin, self.refmagmax)
        elif self.catalog=="sdss":
            df_cat = extract_sdss(
                self.db, self.table, cra, cdec, fovradius, 
                self.refmagmin, self.refmagmax)
        x, y  = w.all_world2pix(df_cat[self.kwd_ra], df_cat[self.kwd_dec], 0)
        cat_mask = np.zeros(image.shape, dtype=np.bool_)
        # Create catalog star mask
        sep.mask_ellipse(cat_mask, x, y, a=1, b=1, theta=0, r=self.radius_mask)

        self.mask = satu_mask|cat_mask


    def obtain_time_from_hdr(self, hdr):
        """
        Obtain central observation time information from header.
    
        Parameter
        ---------
        hdr : header
          header
        """

        # Extract time
        ## Use 'datetime'
        if self.hdr_kwd["datetime"]:
            exp_start = hdr[self.hdr_kwd["datetime"]]
        ## Use 'date' and 'time'
        else:
            exp_start = f"{hdr[hdr_kwd['date']]}T{hdr[hdr_kwd['time']]}"
        ## Exposure time of a single frame
        exp_frame = hdr[self.hdr_kwd["exp"]]
        obs_start = datetime.datetime.strptime(
            exp_start, "%Y-%m-%dT%H:%M:%S.%f")
        ## Central observation time of a fits
        ## (i.e., mid-time of exposure)
        obs_center = obs_start + datetime.timedelta(seconds=exp_frame/2.0)
        obs_center = datetime.datetime.strftime(
            obs_center, "%Y-%m-%dT%H:%M:%S.%f")
        t = Time(str(obs_center), format='isot', scale='utc')
        self.t_utc = str(t)
        self.t_mjd = t.mjd
        self.t_jd  = t.jd


    def add_photometry_info(self, df):
        """
        Add common constant values for all star/object in a fits,
        bg_level, bg_rms and time, to input DataFrame.
        Gain is not added in this function since it is used when photometry.

        Parameter
        ---------
        df : pandas.DataFrame
          dataframe
        """

        # Add bg info.
        # Note: lenght of right term is 1 
        df["bg_level"] = np.round(self.bg_level, 2)
        df["bg_rms"]   = np.round(self.bg_rms, 2)
        df["t_utc"]    = self.t_utc
        df["t_jd"]     = self.t_jd
        df["t_mjd"]    = self.t_mjd


    def add_instrumental_info(self, df):
        """
        Add instrumental information to input DataFrame.

        Parameter
        ---------
        df : pandas.DataFrame
          dataframe
        """
        # Add pixel scale.
        df["p_scale"] = self.p_scale
        df["inst"]    = self.inst


    #@time_keeper
    def phot_catalog(self, image, hdr, phottype, band, winpos, mask=None):
        """Do photometry of stars in catalog.
   
        Parameters
        ----------
        src : HDU object
            fits source
        phottype : str
            "app" (apperture), "ann" (annulus), or "iso" (isophotal)
        band : str
            band of filter ("g", "r", "i", "z", "R", "I" etc.)
            g, r, i are common for sdss and ps
        winpos : bool
            whether use windowed positions (xwin, ywin)
        mask : numpy.ndarray
            mask for sep.sum_circle

        Return  
        ------
        df_ref : pandas.DataFrame
            DataFrame of photometric results of reference start
        """

        # Check whether the band is appropreate
        assert bandcheck(self.catalog, band), f"Check {self.catalog} {band}"
        
        if phottype != "ann":
            assert np.median(image) < 500, "Input image should be background subtracted." 

        ny, nx = image.shape
        w = wcs(header=hdr)
        # Center of cordinate and radius of catalog search region.
        cra, cdec, fovradius = search_param(w, nx, ny)
        
        # Obtain lots of objects using loose magnitude threshold 
        if self.catalog=="gaia":
            df_cat = extract_gaia(
                self.db, self.table, cra, cdec, fovradius, 
                self.refmagmin, self.refmagmax)
        elif self.catalog=="ps":
            df_cat = extract_ps(
                self.db, self.table, cra, cdec, fovradius, 
                band, self.refmagmin, self.refmagmax)
        elif self.catalog=="sdss":
            df_cat = extract_sdss(
                self.db, self.table, cra, cdec, fovradius, 
                self.refmagmin, self.refmagmax)
        if len(df_cat)==0:
            print("  No reference stars were detected. Retrun an empty df.")
            df_ref = pd.DataFrame()
            return df_ref
        
        # Calculate x, y of catalog stars (catalog ra, dec -> x, y)
        # Add `x` and `y`
        df_cat = calc_xy(df_cat, w)
        # Calculate winpos
        if winpos:
            # Search winpos (with bgsubtracted data!) 
            xwin, ywin, flag = obtain_winpos(
                image, df_cat["x"], df_cat["y"], self.radius, self.nx, self.ny)
            # Save original xwin and ywin as xwin0 and ywin0
            df_cat.loc[:, "xwin"] = xwin
            df_cat.loc[:, "ywin"] = ywin
            df_cat.loc[:, "winposflag"] = flag

            key_x, key_y = "xwin", "ywin"
        else:
            key_x, key_y = "x", "y"


        # Do photometery ==========================================================
        
        ## Isophotal photometry for elongated objects/stars
        if phottype=="iso":
            df_ref = isophot_catalog(
                image, df_cat, self.bg_rms, None, self.hdr_kwd, 
                self.r_disk, self.epsilon, self.minarea, self.sigma)

        ## Aperture photometry for normal objects/stars
        ## (<-> Sum counts in the aperture of stars in df_cat)
        else:
            if phottype=="app":
                df_ref = appphot_catalog( 
                    image, hdr, df_cat, self.bg_rms, mask, self.hdr_kwd, 
                    self.radius, key_x, key_y)
            elif phottype=="ann":
                df_ref = annphot_catalog( 
                    image, hdr, df_cat, mask, self.hdr_kwd, 
                    self.radius, self.ann_gap, self.ann_width, key_x, key_y)
        # Do photometery ==========================================================
        
        if len(df_ref)!=0:
            df_ref = df_ref.reset_index(drop=True)
        return df_ref


    #@time_keeper
    def phot_loc(self, image, hdr, phottype, x, y, winpos, mask=None):
        """
        Do 'forced' Photometry of a target object with pixel corrdinates.
        Use this function after barycentric correction.
     
        Parameters
        ----------
        src : HDU object
          fits source
        phottype : str
          "app" (apperture), "ann" (annulus), or "iso" (isophotal)
        winpos : bool
            whether use windowed positions (xwin, ywin)
        mask : numpy.ndarray
            mask for sep.sum_circle

        Return 
        ------
        df_obj : pandas.DataFrame
          photometry result dataframe
        """
        
        # To save x and y
        df_xy = pd.DataFrame(dict(x=x, y=y))

        # Calculate winpos
        if winpos:
            # Search winpos (with bgsubtracted data!) 
            xwin, ywin, flag = obtain_winpos(
                image, df_xy["x"], df_xy["y"], self.radius, self.nx, self.ny)
            df_xy["xwin"] = xwin
            df_xy["ywin"] = ywin
            df_xy["winposflag"] = flag
            key_x, key_y = "xwin", "ywin"
        else:
            key_x, key_y = "x", "y"


        # Isophotal photmetry
        if phottype=="iso":
            df_obj = isophot_loc(
                src, df_xy[key_x], df_xy[key_y], self.bg_rms, self.mask, 
                self.hdr_kwd, self.r_disk, self_epsilon, self.minarea, self.sigma)
        
        # Aperture photmetry
        else:
            if phottype=="app":
                df_obj = appphot_loc(
                    image, hdr, df_xy, self.bg_rms, mask, 
                    self.hdr_kwd, self.radius, key_x, key_y)
            if phottype=="ann":
                df_obj = annphot_loc(
                    image, hdr, df_xy, mask, 
                    self.hdr_kwd, self.radius, self.ann_gap, self.ann_width, 
                    key_x, key_y)
   
        return df_obj

