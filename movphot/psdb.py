#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Handmade Pan-STARRS catalog handling script using SQLite.

The options are 
  1. create
    create new table
  2. insert
    insert objects to database
  3. extract
    extract objects from database

Use photometric quality flag (ratio of weighted masked pixels) 
gQfPerfect etc. to select good quality data. 
This leads to avoid selection of saturated stars(?).

Note: known bug:
  When no object returned, `NameError: name 'warnings' is not defined` happens.

History:
  Column number is 24.(2021/03/10)
  Column number is 29.(2021/04/03) add gQfPerfect etc.
"""
from argparse import ArgumentParser as ap
import os
import sys
import sqlite3
from contextlib import closing
import time
from astroquery.mast import Catalogs
from astropy.coordinates import SkyCoord
from astropy import units as u
import numpy as np
import pandas as pd

# objInfoFlag =================================================================
# Based on https://outerspace.stsci.edu/display/PANSTARRS/PS1+Object+Flags
# Accessed in 2022-04-17
# By default, id is
# 0000_0000_0000_0000_0000_0000_0000_0000
# If object is IRCF quasar (4th column),
# 0000_0000_0000_0000_0000_0000_0000_0100.
# If The last column 32th is 1,
# 0100_0000_0000_0000_0000_0000_0000_0000.
# So, N=31 is enough with 0 padding.

# Length = 31, not 32
_info_flag = {
    # 0 to 30
    "n":[x for x in range(31)],
    # 2^0 to 2^30
    "decimal":[
        #0,
        1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 
        1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 
        1048576, 2097152, 4194304, 8388608, 16777216, 
        33554432, 67108864, 134217728, 268435456, 536870912,
        1073741824
    ],
    # 0 to 30 
    "disc": [
        # XX
        #"Initial value; resets all bits.",
        # 0
        "Used within relphot; skip star.",
        # 1
        "Used within relphot; skip star.",
        # 2
        "object IDed with known ICRF quasar"
        "(may have ICRF position measurement)",
        # 3
        "identified as likely QSO (Hernitschek et al 2015), P_QSO >= 0.60",
        # 4
        "identified as possible QSO (Hernitschek et al 2015), P_QSO >= 0.05",
        # 5
        "identified as likely RR Lyra (Hernitschek et al 2015),"
        "P_RRLyra >= 0.60",
        # 6
        "identified as possible RR Lyra (Hernitschek et al 2015),"
        "P_RRLyra >= 0.05",
        # 7
        "identified as a variable based on ChiSq (Hernitschek et al 2015)",
        # 8
        "identified as a non-periodic (stationary) transient",
        # 9
        "at least one detection identified with a known solar-system object "
        "(asteroid or other).",
        # 10
        "most detections identified with a known solar-system object "
        "(asteroid or other).",
        # 11
        "star with large proper motion",
        # 12
        "simple weighted average position was used (no IRLS fitting)",
        # 13
        "average position was fitted",
        # 14
        "proper motion model was fitted",
        # 15
        "parallax model was fitted",
        # 16
        "average position used (not PM or PAR)",
        # 17
        "proper motion used (not AVE or PAR)",
        # 18
        "parallax used (not AVE or PM)",
        # 19
        "mean astrometry could not be measured",
        # 20
        "stack position used for mean astrometry",
        # 21
        "mean astrometry used for stack position",
        # 22
        "failure to measure proper-motion model",
        # 23
        "extended in our data (eg, PS)",
        # 24
        "extended in external data (eg, 2MASS)",
        # 25
        "good-quality measurement in our data (eg,PS)",
        # 26
        "good-quality measurement in external data (eg, 2MASS)",
        # 27
        "good-quality object in the stack (> 1 good stack measurement)",
        # 28
        "the primary stack measurements are the best measurements",
        # 29
        "suspect object in the stack (no more than 1 good measurement,"
        # 30
        "2 or more suspect or good stack measurement)",
        "poor-quality stack object (no more than 1 good or suspect measurement)"
    ]
}

# Useful flags to remove
#   2 : CRF quasar,
#   4 : possible  QSO
#   6 : Possible RR Lyra
#   7 : variable based on ChiSq
#   8 : transient
#  23 : Extended in PS
#  24 : Extended in external data
PS_info_checked = [2,4,6,7,8,23,24]
# objInfoFlag =================================================================


# qualityFlag =================================================================
# qualityFlag =================================================================

# ObjectFilterFlags ===========================================================
# Length = 25, not 26
_filter_flag = {
    # 0 to 24
    "n":[x for x in range(25)],
    # 2^0 to 2^24
    "decimal":[
        #0,
        1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 
        1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 
        1048576, 2097152, 4194304, 8388608, 16777216
    ],
    # 0 to 24
    "disc": [
        # XX
        #"Initial value; resets all bits.",
        # 0
        "Used within relphot; skip star.",
        # 1
        "Used within relphot; skip star.",
        # 2
        "Synthetic photometry used in average measurement.",
        # 3
        "Ubercal photometry used in average measurement.",
        # 4
        "PS1 photometry used in average measurement.",
        # 5
        "PS1 stack photometry exists.",
        # 6
        "Tycho photometry used for synthetic magnitudes.",
        # 7
        "Synthetic magnitudes repaired with zeropoint map.",
        # 8
        "Average magnitude uses only rank 0 detections.",
        # 9
        "Average magnitude uses only rank 1 detections.",
        # 10
        "Average magnitude uses only rank 2 detections.",
        # 11
        "Average magnitude uses only rank 3 detections.",
        # 12
        "Average magnitude uses only rank 4 detections.",
        # 13
        "None",
        # 14
        "PS1 stack photometry comes from primary skycell.",
        # 15
        "PS1 stack best measurement is a detection (not forced).",
        # 16
        "PS1 stack primary measurement is a detection (not forced).",
        # 17
        "None",
        # 18
        "None",
        # 19
        "None",
        # 20
        "This photcode has SDSS photometry.",
        # 21
        "This photcode has HSC photometry.",
        # 22
        "This photcode has CFH photometry (mostly Megacam).",
        # 23
        "This photcode has DES photometry.",
        # 24
        "Extended in this band."
    ]
}
# Useful flags to remove
#  24 : Extended in this band
PS_filter_checked = [24]
# ObjectFilterFlags ===========================================================


def hmsdms2deg(ra, dec):
  """
  Convert hmsdms to degree.

  Parameters
  ----------
  ra : str
    right acsension in hms
  dec : str
    declination in dms
  
  Returns
  -------
  ra : str
    right acsension in degree
  dec : str
    declination in degree
  """
  radec = f"{ra} {dec}"
  c = SkyCoord(radec, unit=(u.hourangle, u.degree))

  radec = c.to_string("decimal")
  ra  = radec.split(" ")[0]
  dec = radec.split(" ")[1]
  return ra, dec


def create_table(db, table):
  """
  Create table in db.

  Parameters
  ----------
  db : str
      path of sqlite3 database
  table : str
      table name of sqlite3 database
  """

  with closing(sqlite3.connect(db)) as conn:
      c = conn.cursor()
      sql = (
          f"CREATE TABLE IF NOT EXISTS"\
          f" {table}("\
          f"objID int unique, objinfoFlag int, qualityFlag int,"\
          f"raMean real, decMean real, raMeanErr real, decMeanErr real,"\
          f"nStackDetections int, nDetections int,"\
          f"gQfPerfect real, gMeanPSFMag real, gMeanPSFMagErr real, gFlags int,"\
          f"rQfPerfect real, rMeanPSFMag real, rMeanPSFMagErr real, rFlags int,"\
          f"iQfPerfect real, iMeanPSFMag real, iMeanPSFMagErr real, iFlags int,"\
          f"zQfPerfect real, zMeanPSFMag real, zMeanPSFMagErr real, zFlags int,"\
          f"yQfPerfect real, yMeanPSFMag real, yMeanPSFMagErr real, yFlags int)"
      )
      c.execute(sql)
      conn.commit()


def query_ps(ra, dec, radius, magmin, magmax):
    """
    Query Pan-STARRS catalog and output as pandas.DataFrame.
    Extract all objects in catalog.
    Bad objects should be removed in post processes.

    Parameters
    ----------
    ra, dec : float
        right ascension, declination of field in degree
    radius : int
        radius of filed of view in degree
    magmin, magmax : float
        the brightest/faintest magnitude to be considered (g-band)

    Return
    ------
    df : pandas.DataFrame
        result DataFrame
    """
    
    # 2024-09-19
    # Does not work?
    # Should use Catalogs.query_region()?
    # like result = Catalogs.query_region("10.68 41.27", radius=0.02, catalog="Panstarrs")
    # No! it works wo/ column option.

    radec = SkyCoord(ra=ra, dec=dec, unit=(u.degree, u.degree))
    columns= [
        "objID", "objinfoFlag", "qualityFlag", 
        "raMean", "decMean", "raMeanErr", "decMeanErr",
        "nStackDetections", "nDetections",
        "gQfPerfect", "gMeanPSFMag", "gMeanPSFMagErr", "gFlags",
        "rQfPerfect", "rMeanPSFMag", "rMeanPSFMagErr", "rFlags",
        "iQfPerfect", "iMeanPSFMag", "iMeanPSFMagErr", "iFlags",
        "zQfPerfect", "zMeanPSFMag", "zMeanPSFMagErr", "zFlags",
        "yQfPerfect", "yMeanPSFMag", "yMeanPSFMagErr", "yFlags"]

    # Bad detections are removed by only magnitude

    dr = "dr2"
    tabletype = "mean"
    t0 = time.time()
    res = Catalogs.query_criteria(
        coordinates=radec, radius=radius, catalog="PANSTARRS", 
        gMeanPSFMag    =[("lte", magmax),("gte", magmin)], 
        rMeanPSFMag    =[("lte", magmax),("gte", magmin)],
        iMeanPSFMag    =[("lte", magmax),("gte", magmin)],
        table=tabletype, data_release=dr, columns=columns, timeout=6000) 
    t1 = time.time()
    print(f"query time : {t1-t0}s")

    # Avoid int64 error for objID
    ID_str = [str(res[i]["objID"]) for i in range(len(res))]
    res["objID"] = ID_str
    df = res.to_pandas()
    return df


def insert_ps(db, table, df):
    """Insert Pan-STARES catalog and output pandas.Data.

    Parameters
    ----------
    db : str
        path of sqlite3 database
    table : str
        table name of sqlite3 database
    df : pandas.DataFrame
        DataFrame to be inserted
    """

    n_col = 29
    with closing(sqlite3.connect(db)) as conn:
        c = conn.cursor()
        spots = ",".join(["?"]*n_col)
        sql = ( 
            f"INSERT OR IGNORE INTO {table}"\
            f"(objID, objinfoFlag, qualityFlag,"\
            f"raMean, decMean, raMeanErr, decMeanErr,"\
            f"nStackDetections, nDetections,"\
            f"gQfPerfect, gMeanPSFMag, gMeanPSFMagErr, gFlags,"\
            f"rQfPerfect, rMeanPSFMag, rMeanPSFMagErr, rFlags,"\
            f"iQfPerfect, iMeanPSFMag, iMeanPSFMagErr, iFlags,"\
            f"zQfPerfect, zMeanPSFMag, zMeanPSFMagErr, zFlags,"\
            f"yQfPerfect, yMeanPSFMag, yMeanPSFMagErr, yFlags) values({spots})"
        )
        for idx, row in df.iterrows():
            data = row.to_numpy()
            assert len(data)==n_col, "Invalid columns number!!" 
            #print(data)
            c.execute(sql, data)
        conn.commit()


def extract_ps(db, table, ra, dec, radius, band, magmin, magmax):
    """
    Extract Pan-STARES catalog data and return as pandas.DataFrame.

    Parameters
    ----------
    db : str
        path of sqlite3 database
    table : str
        table name of sqlite3 database
    ra, dec : float
        right ascension, declination of field in degree.
    radius : int
        radius of filed of view in degree
    band : str
        band of the filter 
    magmin, magmax : float
        the brightest/faintest magnitude in the band

    Return
    ------
    df : pandas.DataFrame
        extracted DataFrame
    """

    with closing(sqlite3.connect(db)) as conn:
        c = conn.cursor()
        sql = ( 
            f"SELECT "\
            f"objID, objinfoFlag, qualityFlag,"\
            f"raMean, decMean, raMeanErr, decMeanErr,"\
            f"nStackDetections, nDetections,"\
            f"gQfPerfect, gMeanPSFMag, gMeanPSFMagErr, gFlags,"\
            f"rQfPerfect, rMeanPSFMag, rMeanPSFMagErr, rFlags,"\
            f"iQfPerfect, iMeanPSFMag, iMeanPSFMagErr, iFlags,"\
            f"zQfPerfect, zMeanPSFMag, zMeanPSFMagErr, zFlags,"\
            f"yQfPerfect, yMeanPSFMag, yMeanPSFMagErr, yFlags from {table}"\
            f" WHERE raMean > {ra-radius} and raMean < {ra+radius}"\
            f" and decMean > {dec-radius} and decMean < {dec+radius}"\
            f" and {band}MeanPSFMag > {magmin} and {band}MeanPSFMag < {magmax}"
        )
        df = pd.read_sql_query(sql=sql, con=conn)
        conn.commit()
    magmin, magmax = np.min(df[f"{band}MeanPSFMag"]), np.max(df[f"{band}MeanPSFMag"]),
    print(f"  {band}-mag range {magmin:.2f}--{magmax:.2f} ")
    objid = df["objID"].values.tolist()
    objid = sorted(objid)
    #print(f"obj = {objid}")
    return df



def show_objinfo(flag, verbose=False):
    """
    Show object info. with objinfoFlag.

    Parameter
    ---------
    flag : int
        objinfoFlag
    """
    # 31 ! not 32 (see header)
    N_bit = 31
    # Remove "0b"
    b_flag =  bin(int(flag))[2:]
    # 0 padding
    b_flag = format(int(b_flag), f"031") 
    print(f"  objinfoFlag        : {flag}")
    print(f"  objinfoFlag in bin : {b_flag}")
    for n in range(N_bit):
        if b_flag[N_bit-(n+1)] == "1":
            if verbose:
                print(f"    {n:02d} : {_info_flag['disc'][n]}")


def check_objinfo(flag, n_checked=[2, 4, 6, 7, 8, 23, 24], verbose=False):
    """
    Check object info. with objinfoFlag.
    Return False if at least one content is flagged.
    
    
    Parameters
    ----------
    flag : int
        objinfoFlag
    n_flag : array-like
        flag value in decimal forms (from 0 to 31)
        By default, these flags are used.
            2 : CRF quasar,
            4 : possible  QSO
            6 : Possible RR Lyra
            7 : variable based on ChiSq
            8 : transient
            23 : Extended in PS
            24 : Extended in external data
    verbose : bool
        output messages

    Return
    ------
    True or False
    """
    # 31 ! not 32 (see header)
    N_bit = 31
    # Remove "0b"
    b_flag =  bin(int(flag))[2:]
    #print(f"flag : {flag}")
    #print(f"original length = {len(b_flag)}")
    #print(b_flag)
    # 0 padding
    b_flag = format(int(b_flag), f"031")
    #print(f"0 padded length = {len(b_flag)}")
    #print(b_flag)

    # PS_checked = [2,4,6,7,8,23,24]
    for n in range(N_bit):
        # From the last number
        # ex) n==0 (whether the last character==1 or not)
        #     
        if (b_flag[N_bit-(n+1)] == "1") & (n in n_checked):
            if verbose:
                print(f"    {n:02d} : {_info_flag['disc'][n]}")
            # Return False at least one content is flagged
            return False
    return True


def check_objfilter(flag, n_checked=[24], verbose=False):
    """
    Check object filter flag with e.g., gFlag.
    Return False if at least one content is flagged.
    
    Parameters
    ----------
    flag : int
        objinfoFlag
    n_flag : array-like
        flag value in decimal forms (from 0 to 24)
        By default, 24 is used.
            24 : Extended in this band
    verbose : bool
        output messages

    Return
    ------
    True or False
    """
    # 25 ! not 26 (see header)
    N_bit = 25
    # Remove "0b"
    b_flag =  bin(int(flag))[2:]
    #print(f"flag : {flag}")
    #print(f"original length = {len(b_flag)}")
    #print(b_flag)
    # 0 padding
    b_flag = format(int(b_flag), f"025")
    #print(f"0 padded length = {len(b_flag)}")
    #print(b_flag)

    for n in range(N_bit):
        # From the last number
        # ex) n==0 (whether the last character==1 or not)
        #     
        if (b_flag[N_bit-(n+1)] == "1") & (n in n_checked):
            if verbose:
                print(f"    {n:02d} : {_filter_flag['disc'][n]}")
            # Return False at least one content is flagged
            return False
    return True


if __name__=="__main__":
    parser = ap(description="handling Pan-STARRS database test")
    parser.add_argument(
        "action", choices=["dbstart", "insert", "extract", "first", "checkflag",
            "showflag"],  
        help="actions to be done")
    parser.add_argument(
        "--table", type=str, 
        help="table name")
    parser.add_argument(
        "--ra", default=120, type=float, 
        help="center of right ascention in degree")
    parser.add_argument(
        "--dec", default=0, type=float, 
        help="center of declination in degree")
    parser.add_argument(
        "--radius", default=0.05, type=float, 
        help="object search radius in degree")
    parser.add_argument(
        "--magmin", default=14., type=float, 
        help="minimum magnitude")
    parser.add_argument(
        "--magmax", default=17., type=float, 
        help="maximum magnitude")
    parser.add_argument(
        "--band", default="g", type=str, 
        help="band")
    parser.add_argument(
        "--dbdir",  type=str, default=".",
        help="database directory name")
    parser.add_argument(
        "--save",  type=str, default=None,
        help="Save the results")
    parser.add_argument(
        "--objflag",  type=str, default=None,
        help="object flag")
    parser.add_argument(
        "--verbose",  action="store_true", default=False,
        help="output messages")
    args = parser.parse_args()

    # Check PS flag
    if args.action=="checkflag":
        print(check_objinfo(args.objflag, PS_checked, args.verbose))
        sys.exit()
    # Show PS flag info.
    if args.action=="showflag":
        print(show_objinfo(args.objflag, args.verbose))
        sys.exit()


    if args.dbdir:
        db = os.path.join(args.dbdir, "ps.db")
    else:
        # Default database directory
        db4movphot = "~/db4movphot"
        os.makedirs(db4movphot, exist_ok=True)
        db = os.path.join(db4movphot, "ps.db")

    # Create database itself
    if args.action=="dbstart":
        if os.path.exists(db):
            print(f"Already exists {db}")
            sys.exit()
        else:
            conn = sqlite3.connect(db)
            conn.close()
            print(f"Database {db} creation successfully finished!")


    # Create new table
    if args.action=="create":
        table = args.table
        create_table(db, table)
        print(f"Successfully created '{db}/{table}' !")


    # Insert stars in a table
    if args.action=="insert":
        table = args.table
        df = query_ps(
            args.ra, args.dec, args.radius, args.magmin, args.magmax)
        #print("Query result:")
        #print(df)
        insert_ps(db, table, df)
        print(f"{len(df)} objects successfully inserted to '{db}/{table}' !")


    # Extract stars to check
    if args.action=="extract":
        table = args.table
        df = extract_ps(
            db, table, args.ra, args.dec, args.radius, 
            args.band, args.magmin, args.magmax)
        print(f"{len(df)} objects extracted from '{db}/{table}' !")
        print(df)
      
        if args.save:
            df.to_csv(args.save, sep=" ", index=False)

        assert False, 1
        df["b_qualityFlag"] = [format(x, "08b") for x in df["qualityFlag"]]
        df["b_objinfoFlag"] = [format(x, "031b") for x in df["objinfoFlag"]]
        print(df[["b_qualityFlag", "b_objinfoFlag"]])
       

        # object IDed with knwon quasar
        # xxxx1xx means the object is known quasar
        N = 31
        n = 3
        for i in range(len(df)):
            if df.at[i, "b_objinfoFlag"][N-n]=="1":
                print("known quasar")
       

        # object IDed with knwon quasar
        # bad data 3-12 characters (quasar, moving objects etc.)
        N = 31
        n_list = [i for i in range(3, 13)]
        for i in range(len(df)):
            for n in n_list:
                if df.at[i, "b_objinfoFlag"][N-n]=="1":
                    print("quasar or moving objects or ...")
                    print(
                        f"{i}th character, n_row={i+1}, "
                        f"objID={df.at[i, 'objID']}"
                        f"gmag={df.at[i, 'gMeanPSFMag']}")

        #  # good
        #  N = 31
        #  n = 27
        #  for i in range(len(df)):
        #    if df.at[i, "b_objinfoFlag"][N-n]=="1":
        #      print("GOOD")

     



