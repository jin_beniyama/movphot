#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
For preprocess afer photometry.
Extract good data from DataFrame.
"""
import os
import sys
import numpy as np
import pandas as pd
from scipy import stats
from scipy.stats import norm
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
# For colormap
from matplotlib.colors import LogNorm
from matplotlib import cm

from calcerror import round_error

from .common import (
    adderr, adderr_series, diverr, mulerr, log10err, 
    mycolor, mymark, myls, linear_fit, add_color_reverse, checknan,
    band4CTG)
from .visualization import myfigure_refladder, color_from_band


def prepro_CTG(df, band_l, band_r, Nobj_min, shift=True, target=None):
    """
    Preprocess for CTG calculations.

    Note:
      Sometimes 'OptimizeWarning: Covariance of the parameters could not be 
      estimated warnings.warn('Covariance of the parameters could not be 
      estimated' happens. This is warning and does not lead to series problems?

    Parameters
    ----------
    df : pandas.DataFrame
        dataframe of reference stars
    band_l, band_r : str
        left and right band
    Nobj_min : int
        minimum object number
    shift : bool, optional
        whether shift to mean of zero
    target : array-like, optional
        target reference star

    Returns
    -------
    lists : array-like
        8 lists (col_cat_list, col_caterr_list, col_inst_list, col_insterr_list, 
        y_list, t_list, CTG_frame_list, t_frame_list)
    """
    # Output target info
    if target is not None:
        x0_t, x1_t, y0_t, y1_t = target

    # Lists for fitting
    col_inst_list, col_cat_list       = [], []
    col_insterr_list, col_caterr_list = [], []
    # y and t_sec
    y_list, t_list                    = [], []
    # time of the frame (i.e., N_t_frame_list == N_frame)
    CTG_frame_list, t_frame_list      = [], []

    nframe_list = list(set(df["nframe"].values.tolist()))
    
    # Output target info
    # Xaxis : col_inst (inst0 - inst1)
    # Yaxis : col_cat (cat0 - cat1)
    if target is not None:
        # Select by catalog color
        df_t = df[
            (df[f"{band_l}MeanPSFMag"]-df[f"{band_r}MeanPSFMag"] > y0_t)
            & 
            (df[f"{band_l}MeanPSFMag"]-df[f"{band_r}MeanPSFMag"] < y1_t)
            ]
        df_t_l = df_t[df_t["band"]==band_l]
        df_t_r = df_t[df_t["band"]==band_r]
        for nframe in nframe_list:
            df_t_l_n = df_t_l[df_t_l["nframe"]==nframe]
            df_t_r_n = df_t_r[df_t_r["nframe"]==nframe]
            # Extract common objects
            obj_l = df_t_l_n["objID"].values.tolist()
            obj_r = df_t_r_n["objID"].values.tolist()
            obj_common = set(obj_l) & set(obj_r)
            df_t_n_l = df_t_l_n[df_t_l_n["objID"].isin(obj_common)].sort_values("objID")
            df_t_n_r = df_t_r_n[df_t_r_n["objID"].isin(obj_common)].sort_values("objID")
            
            for obj in obj_common:
                df_obj_l = df_t_l_n[df_t_l_n["objID"]==obj]
                df_obj_r = df_t_r_n[df_t_r_n["objID"]==obj]
                flux_l = df_obj_l[f"flux"].values.tolist()
                flux_r = df_obj_r[f"flux"].values.tolist()
                col_inst = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
                if x1_t > col_inst[0] > x0_t:
                    x_target    = df_obj_l["xwin"].values.tolist()[0]
                    y_target    = df_obj_l["ywin"].values.tolist()[0]
                    fits_target = df_obj_l["fits"].values.tolist()[0]
                    cntmfrac    = df_obj_l["cntmfrac"].values.tolist()[0]
                    print(f"    Target info. in {nframe}-th fits")
                    print(f"      fits         : {fits_target}")
                    print(f"      objID        : {obj}")
                    print(f"      x,y          : {x_target:.1f}, {y_target:.1f}")
                    print(f"      inst col     : {col_inst[0]:.3f}")
                    print(f"      cntm fra     : {cntmfrac:.3f}")


    for nframe in nframe_list:
        # Extract objects in a frame
        df_n = df[df["nframe"]==nframe]

        # Extract 2 bands data (ex. g and r)
        df_n_l = df_n[df_n["band"] == band_l]
        df_n_r = df_n[df_n["band"] == band_r]
        # Extract common objects
        obj_l = df_n_l["objID"].values.tolist()
        obj_r = df_n_r["objID"].values.tolist()
        obj_common = set(obj_l) & set(obj_r)


        df_n_l = df_n_l[df_n_l["objID"].isin(obj_common)].sort_values("objID")
        df_n_r = df_n_r[df_n_r["objID"].isin(obj_common)].sort_values("objID")
        assert len(df_n_l) == len(df_n_r), "Check"
        
        # Use "band_l" data
        y_n = df_n_l["y"].values.tolist()
        t_n = df_n_l["t_sec"].values.tolist()

        # Skip few object data
        if len(obj_common) < Nobj_min:
            continue

        y_list += y_n
        t_list += t_n

        catmag_l = df_n_l[f"{band_l}MeanPSFMag"].values.tolist()
        catmag_r = df_n_r[f"{band_r}MeanPSFMag"].values.tolist()
        catmagerr_l = df_n_l[f"{band_l}MeanPSFMagErr"].values.tolist()
        catmagerr_r = df_n_r[f"{band_r}MeanPSFMagErr"].values.tolist()

        flux_l = df_n_l[f"flux"].values.tolist()
        flux_r = df_n_r[f"flux"].values.tolist()

        #print("Flux_l")
        #print(flux_l)
        #print("Flux_r")
        #print(flux_r)

        # For debug ===========================================================
        eflag_r = df_n_r[f"eflag"].values.tolist()
        x_r = df_n_r[f"xwin"].values.tolist()
        y_r = df_n_r[f"ywin"].values.tolist()
        wf_r = df_n_r[f"winposflag"].values.tolist()
        x_l = df_n_l[f"xwin"].values.tolist()
        y_l = df_n_l[f"ywin"].values.tolist()

        # print(f"nframe,band_l,band_r={nframe},{band_l},{band_r}")
        # print(flux_l)
        # print(flux_r)
        # print(f"x and y in {band_l}-band")
        # print(x_l)
        # print(y_l)
        # print(f"x and y in {band_r}-band")
        # print(x_r)
        # print(y_r)
        # print(f"catmag in {band_l}-band")
        # print(catmag_l)
        # print(f"catmag in {band_r}-band")
        # print(catmag_r)
        # print(f"winposflag in {band_r}-band")
        # print(wf_r)
        # For debug ===========================================================

        fluxerr_l = df_n_l[f"fluxerr"].values.tolist()
        fluxerr_r = df_n_r[f"fluxerr"].values.tolist()
        instmagerr_l = [2.5*log10err(x, y) for x,y in zip(flux_l, fluxerr_l)]
        instmagerr_r = [2.5*log10err(x, y) for x,y in zip(flux_r, fluxerr_r)]

        col_inst_list_n = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
        col_insterr_list_n   = [adderr(x, y) for x, y in zip(instmagerr_l, instmagerr_r)]
        col_cat_list_n = [x-y for x, y in zip(catmag_l, catmag_r)]
        col_caterr_list_n = [adderr(x, y) for x, y in zip(catmagerr_l, catmagerr_r)]


        if shift:
            # Shifted
            # Subtract mean values to use all photometric points 
            # (even in different nights) at once
            # See Jackson et al.2021, PASP, 133, 075003
            col_inst_list_n = [x-np.mean(col_inst_list_n) for x in col_inst_list_n]
            col_cat_list_n = [x-np.mean(col_cat_list_n) for x in col_cat_list_n]

        col_inst_list += col_inst_list_n
        col_insterr_list += col_insterr_list_n
        col_cat_list += col_cat_list_n
        col_caterr_list += col_caterr_list_n
        
        # Fitting to estimate each CTG 
        param, cov_sample = curve_fit(
            linear_fit, col_inst_list_n, col_cat_list_n) 
        CTG_frame_list.append(param[0])
        t_frame_list.append(t_n[0])
        #print(f"nframe {nframe}")
        #print(f"  col_inst_list_n: {col_inst_list_n}")
        #print(f"  col_cat_list_n: {col_cat_list_n}")

    lists = [col_cat_list, col_caterr_list, col_inst_list, col_insterr_list, 
        y_list, t_list, CTG_frame_list, t_frame_list]
    return lists


def prepro_CTI(df, band_l, band_r, CTG, CTGerr, Nobj_min, N_mc, verbose=False):
    """
    Preprocess for CTI calculations.

    Parameters
    ----------
    df : pandas.DataFrame
        input dataframe with photometric results of reference stars
    band_l, band_r : str
        left/right observed band
    CTG, CTGerr : float
        CTG and its uncertainty
    Nobj_min : int
        minimum stars to be needed for CTI calculations
    N_mc : int
        number of trials to estimate uncertainties by Monte Carlo mothod
    verbose : bool
        whether output messages

    Return
    ------
    df_CTI : pandas.DataFrame
        dataframe with CTIs
          nframe, {band_l}_{band_r}_CTI_MC, {band_l}_{band_r}_CTIerr_MC,
          {band_l}_{band_r}_t_sec, and {band_l}_{band_r}_t_jd
    """
    # Lists for output 
    CTI_MC_list, CTIerr_MC_list = [], []
    CTG_MC_list, CTGerr_MC_list = [], []
    t_sec_list, t_jd_list = [], []
    
    nframe_list = list(set(df["nframe"].values.tolist()))
    nframe_CTI_list = []
    # Extract stars and calculate CTI with CTG in each frame ==================
    for nframe in nframe_list:
        col_inst_list, col_insterr_list = [], []
        col_cat_list, col_caterr_list   = [], []

        nframe_CTI_list.append(nframe)
        # Extract objects in a frame
        df_n = df[df["nframe"]==nframe]
        # Extract 2 bands data (ex. g and r)
        df_n_l = df_n[df_n["band"] == band_l]
        df_n_r = df_n[df_n["band"] == band_r]

        # Extract objects detected in both bands
        obj_l = df_n_l["objID"].values.tolist()
        obj_r = df_n_r["objID"].values.tolist()
        obj_common = set(obj_l) & set(obj_r)

        df_n_l = df_n_l[df_n_l["objID"].isin(obj_common)].sort_values("objID")
        df_n_r = df_n_r[df_n_r["objID"].isin(obj_common)].sort_values("objID")
        assert len(df_n_l) == len(df_n_r), "Check the code."

        # Pan-STARRS Catalog manitudes and uncertainties
        catmag_l    = df_n_l[f"{band_l}MeanPSFMag"].values.tolist()
        catmag_r    = df_n_r[f"{band_r}MeanPSFMag"].values.tolist()
        catmagerr_l = df_n_l[f"{band_l}MeanPSFMagErr"].values.tolist()
        catmagerr_r = df_n_r[f"{band_r}MeanPSFMagErr"].values.tolist()
        
        # Observed qualities
        flux_l       = df_n_l[f"flux"].values.tolist()
        flux_r       = df_n_r[f"flux"].values.tolist()
        fluxerr_l    = df_n_l[f"fluxerr"].values.tolist()
        fluxerr_r    = df_n_r[f"fluxerr"].values.tolist()
        instmagerr_l = [2.5*log10err(x, y) for x,y in zip(flux_l, fluxerr_l)]
        instmagerr_r = [2.5*log10err(x, y) for x,y in zip(flux_r, fluxerr_r)]
        
        # Most important part
        col_inst_list_n = [
            -2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
        col_insterr_list_n   = [
            adderr(x, y) for x, y in zip(instmagerr_l, instmagerr_r)]
        col_cat_list_n = [
            x-y for x, y in zip(catmag_l, catmag_r)]
        col_caterr_list_n = [
            adderr(x, y) for x, y in zip(catmagerr_l, catmagerr_r)]
       
        # Add into single lists
        col_inst_list    += col_inst_list_n
        col_insterr_list += col_insterr_list_n
        col_cat_list     += col_cat_list_n
        col_caterr_list  += col_caterr_list_n

        # If CTI is not estimated due to the lack of reference stars,
        # zero is added to the dataframe to keep the length of DataFrame.
        N_star = len(col_cat_list)
        if N_star < Nobj_min:
            CTI_MC_list.append(0)
            CTIerr_MC_list.append(0)
            CTG_MC_list.append(0)
            CTGerr_MC_list.append(0)
            t_sec_list.append(0)
            t_jd_list.append(0)

            print(
              f"      {band_l}-{band_r} CTI is not estimeted on {nframe}-th "
              f"frame due to the lack of frames Nframe={len(col_cat_list)}."
              f" Skip the frame."
            )
            continue

        # Use "band_l" info. (this choice is not critical)
        t_sec = df_n_l["t_sec"].values.tolist()[0]
        t_jd  = df_n_l["t_jd"].values.tolist()[0]
        t_sec_list.append(t_sec)
        t_jd_list.append(t_jd)

        # Estimate CTI and its uncertainties using ref stars in fov
        #  with col_cat_list, col_caterr_list, col_inst_list, col_insterr_list
        # Use mean and std of Monte Carlo results.
        # Set seed 0
        seed = 0
        np.random.seed(seed)

        
        # Temporally, the function is defined here to fix CTG =================
        # ToDo: Update. Remove. 
        # There seems to be a good way.
        # https://qiita.com/Syuparn/items/e0b1e878a84f223e9223
        def linear_fit_CTI(x, b):
            """Linear fit function for scipy.optimize.
            """
            return CTG*x + b
        # Temporally, the function is defined here to fix CTG =================

        CTI_temp_list = []
        for n in range(N_mc):
            # Initialization
            col_cat_sample = np.random.normal(
                col_cat_list, col_caterr_list, N_star)
            col_inst_sample = np.random.normal(
                col_inst_list, col_insterr_list, N_star)
            param_sample, cov_sample = curve_fit(
                linear_fit_CTI, col_inst_sample, col_cat_sample) 
            CTI_sample = param_sample[0]
            CTI_temp_list.append(CTI_sample)
        
        CTI_MC_mean = np.mean(CTI_temp_list)
        CTI_MC_std  = np.std(CTI_temp_list)
        if verbose:
            print(f"  MC mean, std = {CTI_MC_mean:.4f}, {CTI_MC_std:.4f}")

        CTI_MC_list.append(CTI_MC_mean)
        CTIerr_MC_list.append(CTI_MC_std)
    # Extract stars and calculate CTI with CTG in each frame ==================


    # Create dataframe of a frame
    df_CTI = pd.DataFrame({
        "nframe"                      : nframe_CTI_list,
        f"{band_l}_{band_r}_CTI_MC"   : CTI_MC_list,
        f"{band_l}_{band_r}_CTIerr_MC": CTIerr_MC_list,
        f"{band_l}_{band_r}_t_sec"    : t_sec_list,
        f"{band_l}_{band_r}_t_jd"     : t_jd_list
        })
    return df_CTI


def plot_CTGfit2(
    df, band, bands, Nobj_min, fig, ax, ax_res=None, yr_res=None,
    shift=True, ycolor=1, ny=1280, colormap=False, cbar=False, ax_cbar=None, 
    ax_CTG=None, N_mc=100, target=None, verbose=False):
    """
    Calculate, plot, and return CTGs.

    Parameters
    ----------
    df : DataFrame
        DataFrames of photometry results of reference stars
    band : str
        band
    bands : array-likes
        list of bands
    Nobj_min : int
        minimum object to be required to shift data
    fig : figure
       figure (for colorbar, cmap)
    ax : axis
       ax
    shift : bool
        shift to set mean zero
    ax_res : axis
       axis for residual plot
    yr_res : list of float
       y range of residual plot
    ycolor : int
        number of colors depending on y pixel values
    ny : int
        number of pixels along y axis
    colormap : book
        whether plot colormap like Fig.7 in Jackson+2021
    cbar : book
        whether use cbar to show time-series info.
    ax_cbar : axis
        axis for color var to show time info
    target : array-like
        info. of target
    verbose : bool
        output messages

    Return
    ------
    df_CTG : pandas.DataFrame
        DataFrame with CTGs
    """

    band_l, band_r = band4CTG(band, bands)
    for idx,x in enumerate(bands):
        if x == band:
            break
    
    if verbose:
        print(f"  {band}-band in {bands}")

    col_rawfit = "gray"
    col_weifit = "black"
    col_MCfit  = "red"
    col_list   = ["red", "blue", "green", "orange"]
    col_p      = mycolor[idx]
    mark_p     = mymark[idx]
    
    # Calculate CTGs
    # 8 Returns :
    #   col_cat_list, col_caterr_list, col_inst_list, col_insterr_list, 
    #   y_list, t_list, CTG_frame_list, t_frame_list
    lists = prepro_CTG(df, band_l, band_r, Nobj_min, shift, target)
    col_cat_list     = lists[0]
    col_caterr_list  = lists[1]
    col_inst_list    = lists[2]
    col_insterr_list = lists[3]
    y_list           = lists[4]
    t_list           = lists[5]
    CTG_frame_list   = lists[6]
    t_frame_list     = lists[7]



    
    if ax_CTG is not None:
        ax_CTG.scatter(
          t_frame_list, CTG_frame_list, 
          s=10, color=col_p, marker=mark_p, 
          label=f"{band_l}-{band_r} N={len(t_frame_list)}")
        ax_CTG.set_xlabel("Elapsed time [sec]")
        ax_CTG.set_ylabel("CTG")
        ax_CTG.legend(fontsize=10)


    # Plot at once in any cases ===============================================
    ## Create colormap
    if colormap:
        cm = "inferno"
        # 0.02 mag bin
        ## Extract range
        ymin, ymax = np.min(col_cat_list), np.max(col_cat_list)
        ## Set bin width
        xmin, xmax = np.min(col_inst_list), np.max(col_inst_list)
        wbin = 0.02
        bins = [np.arange(xmin, xmax, wbin), np.arange(ymin, ymax, wbin)]
        # Please optimize for each observation
        Nmax = 1e3
        H = ax.hist2d(col_inst_list, col_cat_list, bins=bins, cmap=cm,
                norm=LogNorm(vmin=1e0, vmax=Nmax))
        # Add colorbars
        fig.colorbar(H[3],ax=ax)

    ## Create cbar to show time-series info
    elif cbar:
        from matplotlib import cm
        mapp = ax.scatter(
            col_inst_list, col_cat_list, c=t_list,  cmap=cm.inferno, 
            s=30, lw=1, marker=mark_p, facecolor="None", zorder=2)
        cbar = fig.colorbar(mapp, ax_cbar)
        cbar.set_label("Elapsed time [s]")
        # Add errorbars
        ax.errorbar(
            col_inst_list, col_cat_list, 
            xerr=col_insterr_list, yerr=col_caterr_list, lw=0.5, ms=3, fmt="o", 
            marker=" ", capsize=0, zorder=1)

    ## Simply plot as points
    else:

        ax.errorbar(
            col_inst_list, col_cat_list, 
            xerr=col_insterr_list, yerr=col_caterr_list,
            ms=1, linewidth=0.5, color=col_p, marker=mark_p, ls="None")

        # Test 2023-04-10  ====================================================
        # Color object 
        # obj = 109321497452335551
        # if obj in df.objID.tolist():
        #     df_obj = df[df.objID==obj]
        #     lists_obj = prepro_CTG(df_obj, band_l, band_r, Nobj_min, shift, target)
        #     y     = lists_obj[0]
        #     yerr  = lists_obj[1]
        #     x     = lists_obj[2]
        #     xerr  = lists_obj[3]
        #     assert False, y
        #     ax.errorbar(
        #         x, y, xerr=xerr, yerr=yerr,
        #         ms=10, linewidth=3, color="Red", marker="*", ls="None", zorder=100)
        # Test 2023-04-10  ====================================================

    # Plot at once in any cases ===============================================


    # Set initial parameters to avoid an error in covarience calculation
    p0 = [1, 0.001]

    print(f"  N_data = {len(col_cat_list)}")
    # Fitting =================================================================
    ## Raw fit
    param_r, cov_r = curve_fit(
        linear_fit, col_inst_list, col_cat_list, p0=p0)
    paramerr_r = np.sqrt(np.diag(cov_r))
    CTG_r, CTI_r = param_r[0], param_r[1]
    CTGerr_r, CTIerr_r = paramerr_r[0], paramerr_r[1]
    if verbose:
        print(f"    CTG raw = {CTG_r:.4f}+-{CTGerr_r:.4f} (not used)")

    param_w, cov_w = curve_fit(
        linear_fit, col_inst_list, col_cat_list, p0=p0, 
        sigma=col_insterr_list, absolute_sigma=False)
    paramerr_w = np.sqrt(np.diag(cov_w))
    CTG_w, CTI_w = param_w[0], param_w[1]
    CTGerr_w, CTIerr_w = paramerr_w[0], paramerr_w[1]
    if verbose:
        print(f"    CTG wei = {CTG_w:.4f}+-{CTGerr_w:.4f} (not used)")
    
    ## Monte Carlo method to estimate errors
    ## Use mean and std of Monte Carlo results
    # Set seed 0
    seed = 0
    np.random.seed(seed)
    CTG_temp_list = []
    CTI_temp_list = [] 
    for n in range(N_mc):
        # Initialization
        if n==0:
           # Use original values
           col_inst_sample = col_inst_list
           col_cat_sample = col_cat_list
        else:
            col_inst_sample = np.random.normal(
                col_inst_list, col_insterr_list, len(col_cat_list))
            # In general col_caterr << col_insterr.
            # Thus this does not change the results a lot.
            col_cat_sample = np.random.normal(
                col_cat_list, col_caterr_list, len(col_cat_list))

        param_sample, cov_sample = curve_fit(
            linear_fit, col_inst_sample, col_cat_sample, p0=p0)
        CTG_sample = param_sample[0]
        CTI_sample = param_sample[1]
        CTG_temp_list.append(CTG_sample)
        CTI_temp_list.append(CTI_sample)
    
    CTG_MC_mean = np.mean(CTG_temp_list)
    CTG_MC_std = np.std(CTG_temp_list)
    CTI_MC_mean = np.mean(CTI_temp_list)
    CTI_MC_std = np.std(CTI_temp_list)
    if verbose:
        print(f"    CTG MC  = {CTG_MC_mean:.4f}+-{CTG_MC_std:.4f} (used)")
    # Fitting =================================================================
  
    ## Mean values
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    x = np.arange(xmin, xmax, 0.01)

    # ToDo Round error for the sake of the clarity

    ## Raw
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_raw = (
            f"Raw y={CTG_r:.2f}" + "$\pm$" + f"{CTGerr_r:.3f}x + {CTI_r:.2f}"
            + "$\pm$" + f"{CTIerr_r:.3f}")
    ax.plot(
        x, CTG_r*x+CTI_r, marker="", lw=1, ls=myls[1], color=col_rawfit,
        label=label_raw)
    ## Weighted
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_wei = (
            f"Weighted y={CTG_w:.2f}" + "$\pm$" + f"{CTGerr_w:.3f}x + {CTI_w:.2f}"
        + "$\pm$" + f"{CTIerr_w:.3f}")
    ax.plot(
        x, CTG_w*x+CTI_w, marker="", lw=1, ls=myls[2], color=col_weifit,
        label=label_wei)

    ## Monte Carlo Method
    label_MC = (
        f"Monte Carlo y={CTG_MC_mean:.2f}" + "$\pm$" 
        + f"{CTG_MC_std:.3f}x + {CTI_MC_mean:.3f}" + "$\pm$" + f"{CTI_MC_std:.3f}")
    ax.plot(
        x, CTG_MC_mean*x+CTI_MC_mean, marker="", lw=1, ls=myls[0], 
        color=col_MCfit, label=label_MC)
    
    if shift:
        # Use different colors 
        if ycolor >1:
            # Size of each chunk
            width_y = ny/ycolor
            for y in range(ycolor):
                ymin = width_y*y
                ymax = width_y*(y+1)

                ## Residuals (MCfit - obs.)
                label_res = "Residuals\n(MCfit - obs.)"
                res = [
                    y-CTG_MC_mean*x+CTI_MC_mean for (x,y,y_coord) 
                    in zip(col_inst_list, col_cat_list, y_list)
                    if (ycoord > ymin) & (ycoord < ymax)]
                ax_res.hist(
                    res, 
                    orientation="horizontal", histtype="step", color=col_list[y],
                    label=label_res)
        else:
            ## Residuals (MCfit - obs.)
            label_res = "Residuals\n(MCfit - obs.)"
            res = [
                y-CTG_MC_mean*x+CTI_MC_mean for (x,y) 
                in zip(col_inst_list, col_cat_list)]
            ax_res.hist(
                res, 
                orientation="horizontal", histtype="step", color=col_MCfit,
                label=label_res)

        ax_res.set_xlabel("N")
        ax_res.legend(fontsize=10)
        ymin, ymax = ax_res.get_ylim()
        if yr_res is not None:
            ax_res.set_ylim(yr_res[0], yr_res[1])
        elif abs(ymin) > abs(ymax):
            ax_res.set_ylim(-abs(ymin), abs(ymin))
        else:
            ax_res.set_ylim(-abs(ymax), abs(ymax))

    col_cat_label  = "$m_{cat," + band_l + "} - m_{cat," + band_r + "}$"
    col_inst_label = "$m_{inst," + band_l + "} - m_{inst," + band_r + "}$"
    ax.set_xlabel(col_inst_label)
    ax.set_ylabel(col_cat_label)
    xmin, xmax = ax.get_xlim()
    ax.set_xlim([xmin, xmax])
    ax.legend(fontsize=10)

    df_CTG = pd.DataFrame({
        f"{band_l}_{band_r}_CTG_r"    : [CTG_r],
        f"{band_l}_{band_r}_CTGerr_r" : [CTGerr_r],
        f"{band_l}_{band_r}_CTG_w"    : [CTG_w],
        f"{band_l}_{band_r}_CTGerr_w" : [CTGerr_w],
        f"{band_l}_{band_r}_CTG_MC"   : [CTG_MC_mean],
        f"{band_l}_{band_r}_CTGerr_MC": [CTG_MC_std]}
        )
    return df_CTG


#  def plot_CTG2(df, band, bands, ax, verbose=False):
#      """
#      Plot time-series of CTGs.
#  
#      Parameters
#      ----------
#      df_CTG_list : array-like
#          list of DataFrames of CTI
#      band : str
#          band
#      bands : array-like
#          list of bands
#      ax : axis
#         ax
#      verbose : bool
#          whether output messages
#      """
#      band_l, band_r = band4CTG(band, bands)
#      # Number of used frame (i.e. stars in frame >= Nobj_min)
#  
#      for idx,x in enumerate(bands):
#          if x == band:
#              break
#      if verbose:
#          print(f"  {band}-band in {bands}")
#      col_p = mycolor[idx]
#      mark_p = mymark[idx]
#      
#      # Each df in df_CTG_list is like below.
#      #   df_temp = pd.DataFrame(
#      #       {"col_cat":col_cat_list,
#      #       "col_caterr":col_caterr_list,
#      #       "col_inst":col_inst_list,
#      #       "t_sec"   : t_list,
#      #       "col_insterr":col_insterr_list})
#      
#      # Calculate CTG in each frame by fitting
#      CTG_list, t_list = [], []
#  
#      nframe_list = df["nframe"].values.tolist()
#      for nframe in nframe_list:
#          # Extract objects in a frame
#          df_n = df[df["nframe"]==nframe]
#          
#          # Extract 2 bands data (ex. g and r)
#          df_n_l = df_n[df_n["band"] == band_l]
#          df_n_r = df_n[df_n["band"] == band_r]
#          # Extract common objects
#          obj_l = df_n_l["objID"].values.tolist()
#          obj_r = df_n_r["objID"].values.tolist()
#          obj_common = set(obj_l) & set(obj_r)
#  
#          df_n_l = df_n_l[df_n_l["objID"].isin(obj_common)].sort_values("objID")
#          df_n_r = df_n_r[df_n_r["objID"].isin(obj_common)].sort_values("objID")
#          assert len(df_n_l) == len(df_n_r), "Check"
#          
#          catmag_l = df_n_l[f"{band_l}MeanPSFMag"].values.tolist()
#          catmag_r = df_n_r[f"{band_r}MeanPSFMag"].values.tolist()
#          catmagerr_l = df_n_l[f"{band_l}MeanPSFMagErr"].values.tolist()
#          catmagerr_r = df_n_r[f"{band_r}MeanPSFMagErr"].values.tolist()
#  
#          flux_l = df_n_l[f"flux"].values.tolist()
#          flux_r = df_n_r[f"flux"].values.tolist()
#          fluxerr_l = df_n_l[f"fluxerr"].values.tolist()
#          fluxerr_r = df_n_r[f"fluxerr"].values.tolist()
#          instmagerr_l = [2.5*log10err(x, y) for x,y in zip(flux_l, fluxerr_l)]
#          instmagerr_r = [2.5*log10err(x, y) for x,y in zip(flux_r, fluxerr_r)]
#  
#          col_inst_list_n = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
#          col_insterr_list_n   = [adderr(x, y) for x, y in zip(instmagerr_l, instmagerr_r)]
#          col_cat_list_n = [x-y for x, y in zip(catmag_l, catmag_r)]
#          col_caterr_list_n = [adderr(x, y) for x, y in zip(catmagerr_l, catmagerr_r)]
#  
#  
#      ax.scatter(
#        t_list, CTG_list, 
#        s=10, color=col_p, marker=mark_p, 
#        label=f"{band_l}-{band_r} N={len(nframe_list)}")
#      ax.set_xlabel("Elapsed time [sec]")
#      ax.set_ylabel("CTG")
#      ax.legend(fontsize=10)


def plot_CTIfit2(
    df, band, bands, CTG, CTGerr, Nobj_min, ax_CTI, N_mc=100, verbose=False):
    """
    Calculate CTIs with fixed CTGs.

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    band : str
        observed band
    bands : array-like
        list of bands
    CTG, CTGerr : float
        CTG and its uncertainty
    Nobj_min : int
        minimum number of object in a frame
    ax_CTI : matplotlib.axis
        axis for CTI
    N_mc : int
        number of trials to evaluate uncertainties
    verbose : bool
        whether output messages

    Return
    ------
    df_CTI : pandas.DataFrame
        DataFrame with CTIs
    """

    band_l, band_r = band4CTG(band, bands)
    
    for idx,x in enumerate(bands):
        if x == band:
            break

    col_p = mycolor[idx]
    mark_p = mymark[idx]
    
    # Calculate CTIs in each frame
    df_CTI = prepro_CTI(df, band_l, band_r, CTG, CTGerr, Nobj_min, N_mc)

    if ax_CTI is not None:
        df_good = df_CTI[df_CTI[f"{band_l}_{band_r}_CTI_MC"]!=0]
        df_bad  = df_CTI[df_CTI[f"{band_l}_{band_r}_CTI_MC"]==0]
        ax_CTI.errorbar(
            df_good[f"{band_l}_{band_r}_t_sec"].tolist(), 
            df_good[f"{band_l}_{band_r}_CTI_MC"].tolist(), 
            yerr=df_good[f"{band_l}_{band_r}_CTIerr_MC"].tolist(),
            linewidth=0.2, ls="None", ms=1, color=col_p, marker=mark_p, 
            label=f"{band_l}-{band_r} N={len(df_good)} (bad N={len(df_bad)})")
        ax_CTI.legend(fontsize=10)
    return df_CTI


def addCTGI(df, df_CTG, df_CTI, bands):
    """
    Add CTG, CTGerr, CTI, CTIerr to df.

    Insert CTG/CTI in left band result!!
    """
    # Initial values
    df["CTG"]    = 0
    df["CTGerr"] = 0
    df["CTI"]    = 0
    df["CTIerr"] = 0
    
    for b in bands:
        band_l, band_r = band4CTG(b, bands)
        # Extract CTG
        CTG    = df_CTG.at[0, f"{band_l}_{band_r}_CTG_MC"]
        CTGerr = df_CTG.at[0, f"{band_l}_{band_r}_CTGerr_MC"]
        # Insert CTG
        df.loc[(df["band"]==b), f"CTG"] = CTG
        df.loc[(df["band"]==b), f"CTGerr"] = CTGerr

        df_b = df[df["band"]==b]
        nframe_list = list(set(df_b["nframe"].values.tolist()))
        print(len(nframe_list))
        print(len(df_CTI))
        print(nframe_list)
        print(df_CTI)
        for nframe in df_CTI["nframe"]:
            # Extract CTI and err in a frame
            assert False, df_CTI["nframe"]
            df_CTI_n = df_CTI[df_CTI["nframe"] == nframe]
            print(df_CTI_n)
            CTI = df_CTI_n.at[df_CTI_n["band"]==b, f"{band_l}_{band_r}_CTI_MC"]
            #CTI = df_CTI.at[nframe, f"{band_l}_{band_r}_CTI_MC"]
            CTIerr = df_CTI.at[nframe, f"{band_l}_{band_r}_CTIerr_MC"]
            # Insert CTI and err in a frame
            df.loc[(df["band"]==b) & (df["nframe"]==nframe), f"CTI"] = CTI
            df.loc[(df["band"]==b) & (df["nframe"]==nframe), f"CTIerr"] = CTIerr
    return df


def calc_res_obj(df, bands, band):
    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)
    
    obj_list = []
    gr_list, y_list = [], []
    res_list, reserr_list = [], []

    # Extract 2 bands data (ex. g and r)
    df_l = df[df["band"] == band_l]
    df_r = df[df["band"] == band_r]

    # Use common objects in two bands
    objID_l = set(df_l["objID"].values.tolist())
    objID_r = set(df_r["objID"].values.tolist())
    objID = list(objID_l & objID_r)

    for obj in objID:
        # Frame numbers in which obj are detected
        df_obj_l = df_l[df_l["objID"]==obj]
        df_obj_r = df_r[df_r["objID"]==obj]
        nframe_l = set(df_obj_l["nframe"].values.tolist())
        nframe_r = set(df_obj_r["nframe"].values.tolist())
        nframe_common = list(nframe_l & nframe_r)
        df_obj_l = df_obj_l[df_obj_l["nframe"].isin(nframe_common)]
        df_obj_r = df_obj_r[df_obj_r["nframe"].isin(nframe_common)]


        # Calculate residuals of an object
        catmag_l = df_obj_l[f"{band_l}MeanPSFMag"]
        catmag_r = df_obj_l[f"{band_r}MeanPSFMag"]
        catmagerr_l = df_obj_l[f"{band_l}MeanPSFMagErr"]
        catmagerr_r = df_obj_l[f"{band_r}MeanPSFMagErr"]

        col_cat = catmag_l - catmag_r
        col_caterr = [adderr(x, y) for x, y in zip(catmagerr_l, catmagerr_r)]

        flux_l = df_obj_l[f"flux"]
        flux_r = df_obj_r[f"flux"]
        fluxerr_l = df_obj_l[f"fluxerr"]
        fluxerr_r = df_obj_r[f"fluxerr"]
        instmagerr_l = [2.5*log10err(x, y) for x,y in zip(flux_l, fluxerr_l)]
        instmagerr_r = [2.5*log10err(x, y) for x,y in zip(flux_r, fluxerr_r)]

        col_inst = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
        col_insterr = [adderr(x, y) for x, y in zip(instmagerr_l, instmagerr_r)]

        # For successive analyses 
        y_l = df_obj_l[f"ywin"]
        gr_cat = df_obj_l[f"gMeanPSFMag"] - df_obj_l[f"rMeanPSFMag"]

        # Refer CTG/CTI in left band !
        CTG = df_obj_l["CTG"]
        CTGerr = df_obj_l["CTGerr"]
        CTI = df_obj_l["CTI"]
        CTIerr = df_obj_l["CTIerr"]
        
        res = [a - (b*c + d) for a,b,c,d in zip(
            col_cat, col_inst, CTG, CTI)]

        
        # Skip when no common object exists
        if len(res)==0:
            continue
        # Update !
        # Uncertainty of CTG*col_inst
        #err2 = mulerr(
        #    CTG, CTGerr,
        #    col_inst, col_insterr)
        #reserr = adderr(col_caterr, err2, CTIerr)
        reserr = [0 for x in res]

         
        obj_list.append(obj)
        # meaningless "mean" (col_cat is constant)
        # Fixed to g-r
        gr_list.append(np.mean(gr_cat))
        # meaningful "mean" for Phaethon in 2021
        # since Phaethon was moving along iso-dec great circle
        y_list.append(y_l)
        res_list.append(res)
        reserr_list.append(reserr)
        #reserr_list.append(reserr)
        
    # obj   : [obj1, obj2, ..., objN]
    # color : [col1, col2, ..., colN]
    # y     : [[y1], [y2], ..., [yN]]
    # res   : [[res1], [res2], ..., [resN]]
    # Note: color are mean values
    res_dict = dict(
        obj=obj_list, gr=gr_list, y=y_list, 
        res=res_list, reserr=reserr_list)
            #zip(obj_list, res_list))
    return res_dict


def plot_res_obj(res_dict, ax):
    """
    Plot residuals
    """

    for idx,obj in enumerate(res_dict["obj"]):
        res = res_dict["res"][idx]
        col = mycolor[idx]
        ax.hist(res, color=col, label=f"Obj {idx+1} N={len(res)}")
    ax.legend(fontsize=4)


def plot_y_res(res_dict, ax, band):
    """
    Plot y vs. residuals 

    Parameters
    ----------
    res_dict : dict
        dictionary of residual related info.
    ax : axis
        axis to be uesd
    band : str
        band to select color
    """

    for idx,obj in enumerate(res_dict["obj"]):
        # Mean residual
        res = res_dict["res"][idx]
        res_mean = np.mean(res)

        # Mean Y value
        y = res_dict["y"][idx]
        y_mean = np.mean(y)
        y_diff = np.max(y) - np.min(y)
        col = color_from_band(band)
        label=f"Obj {idx+1} N={len(res)} dy={int(y_diff)}"
        ax.scatter(
            y_mean, res_mean, color=col, label=label)
    ax.legend(fontsize=4)


def plot_res_obj_y(res_dict, fig, ax, ax_cbar):
    """
    Plot residuals
    """
    
    # Set range of cbar
    # Why...multiple min and max...?
    try:
        res_min = np.min(np.min(res_dict["res"]))
        res_max = np.max(np.max(res_dict["res"]))
    # For N=1 (1 frame)?
    except:
        res_min = -0.3
        res_max = 0.3

    # for symmetricity
    val_max = np.max([abs(res_min), abs(res_max)])
    vmin = -val_max
    vmax = val_max
    for idx,obj in enumerate(res_dict["obj"]):
        # Mean residual
        res = res_dict["res"][idx]
        res_mean = np.mean(res)

        # Mean y
        y = res_dict["y"][idx]
        y_mean = np.mean(y)

        gr = res_dict["gr"][idx]
        # catalog color vs. y with colorbar of residuals
        mapp = ax.scatter(
          gr, y_mean, c=res_mean,  cmap=cm.bwr_r,
          s=150, lw=1, facecolor="None", zorder=2, 
          vmin=vmin, vmax=vmax, edgecolor="black")
        cbar = fig.colorbar(mapp, ax_cbar)
        cbar.set_label("Residuals [mag]")
    #ax.legend(fontsize=4)


def calc_objcolor(df, df_CTI, band, bands):
    """
    Calculate instumental and CTG/CTI corrected object colors.
    df_CTI includes CTG!

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    df_CTI : pandas.DataFrame
        CTI dataframe
    band : str
        band
    bands : str
        bands

    Return
    ------
    df : pandas.DataFrame
        object dataframe with instrumental and CTG/CTI corrected colors
    """
    band_l, band_r = band4CTG(band, bands)
    for idx,x in enumerate(bands):
        if x == band:
            break
    print(f"{idx}, {band}, {bands}")

    # 1. instrumental color
    s_color_r = df[f"mag_{band_l}_inst"] - df[f"mag_{band_r}_inst"]
    s_colorerr_r = adderr_series(
      df[f"magerr_{band_l}_inst"], df[f"magerr_{band_r}_inst"])
    df.insert(0, f"{band_l}_{band_r}_inst", s_color_r)
    df.insert(0, f"{band_l}_{band_r}err_inst", s_colorerr_r) 

    # 2. CTG, CTI corrected color
    # color_cat = color_inst*CTG + CTI
    #    color_cat = color_inst*CTG + CTI
    s_color_c = (
      (df[f"mag_{band_l}_inst"] - df[f"mag_{band_r}_inst"])*df_CTI[f"{band_l}_{band_r}_CTG_MC"]
      + df_CTI[f"{band_l}_{band_r}_CTI_MC"]
      )
    # Consider error of color_raw, CTG and CTI
    err1 = mulerr(s_color_r, s_colorerr_r, 
            df_CTI[f"{band_l}_{band_r}_CTG_MC"],
            df_CTI[f"{band_l}_{band_r}_CTGerr_MC"])
    s_colorerr_c = adderr_series(
        err1, df_CTI[f"{band_l}_{band_r}_CTIerr_MC"])
    df.insert(0, f"{band_l}_{band_r}_ccor", s_color_c)
    df.insert(0, f"{band_l}_{band_r}err_ccor", s_colorerr_c) 

    # Save CTI to remove some data when CTI==0
    df.insert(0, f"{band_l}_{band_r}_CTI_MC", df_CTI[f"{band_l}_{band_r}_CTI_MC"]) 
  
    return df


def calc_objcolor2(df, band, bands):
    """
    Calculate instumental colors and CTG/CTI corrected colors of an object.

    Parameters
    ----------
    df : pandas.DataFrame
        input dataframe
    band : str
        band
    bands : str
        bands

    Return
    ------
    df : pandas.DataFrame
        object dataframe with instrumental and CTG/CTI corrected colors
    """
    if len(bands)==3:
        b1, b2, b3 = bands

        df_1 = df[df["band"]==b1]
        df_2 = df[df["band"]==b2]
        df_3 = df[df["band"]==b3]

        nf_1 = set(df_1["nframe"].values.tolist())
        nf_2 = set(df_2["nframe"].values.tolist())
        nf_3 = set(df_3["nframe"].values.tolist())
        # Common frames
        nf_common = list(nf_1 & nf_2 & nf_3)
        
        df_1 = df_1[df_1["nframe"].isin(nf_common)]
        df_2 = df_2[df_2["nframe"].isin(nf_common)]
        df_3 = df_3[df_3["nframe"].isin(nf_common)]

        df_1 = df_1.reset_index(drop=True)
        df_2 = df_2.reset_index(drop=True)
        df_3 = df_3.reset_index(drop=True)
       
        # 1. instrumental color (band1-band2)
        s_color_12 = df_1[f"maginst"] - df_2[f"maginst"]
        s_colorerr_12 = adderr_series(df_1[f"maginsterr"], df_2[f"maginsterr"])
        s_color_23 = df_2[f"maginst"] - df_3[f"maginst"]
        s_colorerr_23 = adderr_series(df_2[f"maginsterr"], df_3[f"maginsterr"])
        s_color_31 = df_3[f"maginst"] - df_1[f"maginst"]
        s_colorerr_31 = adderr_series(df_3[f"maginsterr"], df_1[f"maginsterr"])

        # 2. CTG, CTI corrected color
        # color_cat = color_inst*CTG + CTI
        s_color_12_c = (
            (df_1[f"maginst"] - df_2[f"maginst"])*df_1[f"CTG"] + df_1[f"CTI"]
            )
        # Consider error of color_raw, CTG and CTI
        err1_12 = mulerr(
            s_color_12, s_colorerr_12, df_1[f"CTG"], df_1[f"CTGerr"])
        s_colorerr_12_c = adderr_series(err1_12, df_1[f"CTIerr"])

        s_color_23_c = (
            (df_2[f"maginst"] - df_3[f"maginst"])*df_2[f"CTG"] + df_2[f"CTI"]
            )
        # Consider error of color_raw, CTG and CTI
        err1_23 = mulerr(s_color_23, s_colorerr_23, df_2[f"CTG"], df_2[f"CTGerr"])
        s_colorerr_23_c = adderr_series(err1_23, df_2[f"CTIerr"])

        s_color_31_c = (
            (df_3[f"maginst"] - df_1[f"maginst"])*df_3[f"CTG"] + df_3[f"CTI"]
            )
        # Consider error of color_raw, CTG and CTI
        err1_31 = mulerr(s_color_31, s_colorerr_31, df_3[f"CTG"], df_3[f"CTGerr"])
        s_colorerr_31_c = adderr_series(err1_31, df_3[f"CTIerr"])

        df_c = pd.DataFrame({
            f"{b1}_{b2}_inst"   : s_color_12,
            f"{b1}_{b2}err_inst": s_colorerr_12,
            f"{b1}_{b2}_ccor"   : s_color_12_c,
            f"{b1}_{b2}err_ccor": s_colorerr_12_c,
            f"{b2}_{b3}_inst"   : s_color_23,
            f"{b2}_{b3}err_inst": s_colorerr_23,
            f"{b2}_{b3}_ccor"   : s_color_23_c,
            f"{b2}_{b3}err_ccor": s_colorerr_23_c,
            f"{b3}_{b1}_inst"   : s_color_31,
            f"{b3}_{b1}err_inst": s_colorerr_31,
            f"{b3}_{b1}_ccor"   : s_color_31_c,
            f"{b3}_{b1}err_ccor": s_colorerr_31_c,
            f"t_jd_{b1}"        : df_1["t_jd"],
            f"t_jd_{b2}"        : df_2["t_jd"],
            f"t_jd_{b3}"        : df_3["t_jd"],
            f"t_utc_{b1}"       : df_1["t_utc"],
            f"t_utc_{b2}"       : df_2["t_utc"],
            f"t_utc_{b3}"       : df_3["t_utc"],
            f"nframe"           : nf_common
            })
        try:
            # gMeanPSFMag etc. for reference stars
            col_mag = [
                "gMeanPSFMag", "gMeanPSFMagErr", 
                "rMeanPSFMag", "rMeanPSFMagErr", 
                "iMeanPSFMag", "iMeanPSFMagErr", 
                "zMeanPSFMag", "zMeanPSFMagErr"]
            df_use = df_1[col_mag]
            df_c = pd.concat([df_c, df_use], axis=1)
        except:
            print("No catalog manitude found.")
        return df_c

    if len(bands)==2:
        b1, b2 = bands

        df_1 = df[df["band"]==b1]
        df_2 = df[df["band"]==b2]

        nf_1 = set(df_1["nframe"].values.tolist())
        nf_2 = set(df_2["nframe"].values.tolist())
        nf_common = list(nf_1 & nf_2)
        
        df_1 = df_1[df_1["nframe"].isin(nf_common)]
        df_2 = df_2[df_2["nframe"].isin(nf_common)]

        df_1 = df_1.reset_index(drop=True)
        df_2 = df_2.reset_index(drop=True)


        # 1. instrumental color (band1-band2)
        s_color_12 = df_1[f"maginst"] - df_2[f"maginst"]
        s_colorerr_12 = adderr_series(df_1[f"maginsterr"], df_2[f"maginsterr"])

        # 2. CTG, CTI corrected color
        # color_cat = color_inst*CTG + CTI
        s_color_12_c = (
            (df_1[f"maginst"] - df_2[f"maginst"])*df_1[f"CTG"] + df_1[f"CTI"]
            )
        # Consider error of color_raw, CTG and CTI
        err1_12 = mulerr(
            s_color_12, s_colorerr_12, df_1[f"CTG"], df_1[f"CTGerr"])
        s_colorerr_12_c = adderr_series(err1_12, df_1[f"CTIerr"])

        df_c = pd.DataFrame({
            f"{b1}_{b2}_inst": s_color_12,
            f"{b1}_{b2}err_inst": s_colorerr_12,
            f"{b1}_{b2}_ccor": s_color_12_c,
            f"{b1}_{b2}err_ccor": s_colorerr_12_c,
            f"t_jd_{b1}": df_1["t_jd"],
            f"t_jd_{b2}": df_2["t_jd"],
            f"t_utc_{b1}": df_1["t_utc"],
            f"t_utc_{b2}": df_2["t_utc"]
            })
        df = pd.concat([df_1, df_2, df_c], axis=1)
    
    return df


def clean_color(df, inmagtype, band, bands):
    """
    Clean color results.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    inmagtype : sty
        input magnitude system
    """

    if inmagtype == "SDSS":
        # Save neo magnitude in SDSS system
        # Convert to SDSS system?
        key_sdss = {
            "g":"mag_g", "gerr":"magerr_g", 
            "r":"mag_g", "rerr":"magerr_r", 
            "i":"mag_g", "ierr":"magerr_i", 
            "z":"mag_g", "zerr":"magerr_z", 
            }
        assert False, 1
        df = PS2SDSS(df, key0=key_sdss)
        df = df.rename(
            columns={"neomag_g":"g_temp", "neomagerr_g":"gerr_temp", 
                     "neomag_r":"r_temp", "neomagerr_r":"rerr_temp", 
                     "neomag_i":"i_temp", "neomagerr_i":"ierr_temp", 
                     "neomag_z":"z_temp", "neomagerr_z":"zerr_temp"})
        df = df.rename(
            columns={"g_SDSS":"mag_g", "gerr_SDSS":"magerr_g", 
                     "r_SDSS":"mag_r", "rerr_SDSS":"magerr_r", 
                     "i_SDSS":"mag_i", "ierr_SDSS":"magerr_i", 
                     "z_SDSS":"mag_z", "zerr_SDSS":"magerr_z"})

        col_out = []
        for band in bands:
            band_l, band_r = band4CTG(band, bands)
            # Rename g_r_median, g_rerr_median to g_r, g_rerr etc
            df = df.rename(columns={f"{band_l}_{band_r}": f"{band_l}_{band_r}", 
                f"{band_l}_{band_r}err": f"{band_l}_{band_r}err"})
            col_out.append(f"{band_l}_{band_r}")
            col_out.append(f"{band_l}_{band_r}err")
            # Break this process when CTGs==1 and N_band==2
            if len(CTGs)==1:
                break

    # Inputs are Pan-STARRS system
    if inmagtype == "PS":
      # Final results are CTG, CTI corrected ones (g_r_ccor etc.)
      # Original df (g,r,i) has : 
      #  r_ierr_ccor', 'r_i_ccor', 'g_rerr_ccor', 'g_r_ccor'
      # Rename g_r_ccor to g_r
      # Use column in output csv
      #   color, color error, and time in jd
      col_use = []
      for band in bands:
          band_l, band_r = band4CTG(band, bands)
          try:
              df = df.rename(
                  columns={f"{band_l}_{band_r}_ccor": f"{band_l}_{band_r}",
                  f"{band_l}_{band_r}err_ccor": f"{band_l}_{band_r}err"})
              col_use.append(f"{band_l}_{band_r}")
              col_use.append(f"{band_r}_{band_l}")
              col_use.append(f"{band_l}_{band_r}err")
              col_use.append(f"{band_r}_{band_l}err")
              col_use.append(f"t_mjd_{band}")
              col_use.append(f"t_jd_{band}")
              col_use.append(f"t_sec_obs_{band}")
          except:
              pass

      # Add reverse color (ex. g_r, g_rerr from r_g, r_gerr)
      df = add_color_reverse(df, bands)
      df = df[col_use]
      # For reflectance plot
      df["g_g"], df["g_gerr"] = 0., 0.
      # Sort by index before saving
      df = df.sort_index(axis="index")
    return df


def clean_color2(df, magtype, bands):
    """
    Clean color results.
    Outputs are always Pan-STARRS system.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    magtype : sty
        magnitude system of input data
    bands : array-like
        bands

    Return
    ------
    df : pandas.DataFrame
        result data
    """

    if magtype == "SDSS":
        # Convert to SDSS system
        from Tonry2012 import PS2SDSS
        key_sdss = {
          "g":"mag_g", "gerr":"magerr_g", 
          "r":"mag_g", "rerr":"magerr_r", 
          "i":"mag_g", "ierr":"magerr_i", 
          "z":"mag_g", "zerr":"magerr_z", 
          }
        df = PS2SDSS(df, key0=key_sdss)
        df = df.rename(
          columns={"neomag_g":"g_temp", "neomagerr_g":"gerr_temp", 
                   "neomag_r":"r_temp", "neomagerr_r":"rerr_temp", 
                   "neomag_i":"i_temp", "neomagerr_i":"ierr_temp", 
                   "neomag_z":"z_temp", "neomagerr_z":"zerr_temp"})
        df = df.rename(
          columns={"g_SDSS":"mag_g", "gerr_SDSS":"magerr_g", 
                   "r_SDSS":"mag_r", "rerr_SDSS":"magerr_r", 
                   "i_SDSS":"mag_i", "ierr_SDSS":"magerr_i", 
                   "z_SDSS":"mag_z", "zerr_SDSS":"magerr_z"})

        col_out = []
        for band in bands:
          band_l, band_r = band4CTG(band, bands)
          # Rename g_r_median, g_rerr_median to g_r, g_rerr etc
          df = df.rename(
              columns={f"{band_l}_{band_r}": f"{band_l}_{band_r}", 
                       f"{band_l}_{band_r}err": f"{band_l}_{band_r}err"})
          col_out.append(f"{band_l}_{band_r}")
          col_out.append(f"{band_l}_{band_r}err")
          # Break this process when CTGs==1 and N_band==2
          if len(CTGs)==1:
            break


    if magtype == "PS":
        # Original df (g,r,i) has : 
        #   r_ierr_ccor', 'r_i_ccor', 'g_rerr_ccor', 'g_r_ccor'
        # Final results are CTG, CTI corrected ones (g_r_ccor etc.)
        # Rename g_r_ccor to g_r

        # Use column in output file
        #   color, color error, and time in jd
        col_use = []
        for band in bands:
            band_l, band_r = band4CTG(band, bands)
            try:
                # Do if column exists
                df = df.rename(
                    columns={
                        f"{band_l}_{band_r}_ccor": f"{band_l}_{band_r}",
                        f"{band_l}_{band_r}err_ccor": f"{band_l}_{band_r}err",
                    })
                col_use.append(f"{band_l}_{band_r}")
                col_use.append(f"{band_r}_{band_l}")
                col_use.append(f"{band_l}_{band_r}err")
                col_use.append(f"{band_r}_{band_l}err")
                col_use.append(f"t_jd_{band}")
                col_use.append(f"t_utc_{band}")
                col_use.append(f"nframe")
            except:
                pass

        # Add reverse color
        df = add_color_reverse(df, bands)
        # For reflectance plot
        df["g_g"], df["g_gerr"] = 0., 0.
        # Sort by index before saving
        df = df.sort_index(axis="index")
    return df


def plot_foldcolorlc(df, ax, key_jd, band, bands, magtype, JD0, rotP_hour):
    """
    Plot folded lightcurve.
    """
    
    # Hour to Day
    rotP = rotP_hour/24.

    band_l, band_r = band4CTG(band, bands)
    
    # For color selectino 
    for idx,x in enumerate(bands):
        if x == band:
            break
    col = mycolor[idx]


    # Remove invalid data ?
    #df = df[df["eflag_color"]==0]
    deltaT = np.max(df[key_jd]) - np.min(df[key_jd])
    # Rotational phase
    x = ((df[key_jd]-JD0)/rotP)%1

    # Mean and std
    c_mean = np.mean(df[f"{band_l}_{band_r}"])
    # Photometric uncertainty
    c_std_phot = adderr(df[f"{band_l}_{band_r}err"])/len(df)
    # Standard Deviation
    c_SD = np.std(df[f"{band_l}_{band_r}"])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df))
    # Total error 
    c_std = np.sqrt(c_std_phot**2 + c_SE**2)

    # Weighted mean
    c_w = 1/df[f"{band_l}_{band_r}err"]**2
    c_wmean = np.average(df[f"{band_l}_{band_r}"], weights=c_w)
    # Check !!!!!!
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(c_w))

    ## Round error
    c_mean_str, c_std_str = round_error(c_mean, c_std)
    c_wmean_str, c_wstd_str = round_error(c_wmean, c_wstd)
    label_c = (
        f"N={len(df)}\n{band_l}-{band_r} "
        f"${c_wmean_str}" + r"\pm" + f"{c_wstd_str}$"
        )
   
    # For open circle
    ax.scatter(
      x, df[f"{band_l}_{band_r}"], color=col, s=70, lw=1, marker="o", 
      facecolor="None", edgecolor=col, zorder=-1, label=label_c)

    ax.errorbar(
      x, df[f"{band_l}_{band_r}"], df[f"{band_l}_{band_r}err"], 
      color=col, lw=0.5, ms=3, marker=None, capsize=0, ls="None")

    # Mean and std
    x0, x1 = ax.get_xlim()
    ax.hlines(c_wmean, x0, x1, color=mycolor[1])
    ax.hlines(c_wmean+c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    ax.hlines(c_wmean-c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    ax.set_xlim([x0, x1])




def plot_colorlc(df, ax, key_jd, band, bands, magtype, JD0):
    """
    Plot lightcurve.
    """
    band_l, band_r = band4CTG(band, bands)
    
    # For color selectino 
    for idx,x in enumerate(bands):
        if x == band:
            break
    col = mycolor[idx]

    # Remove invalid data ?
    #df = df[df["eflag_color"]==0]
    # Time window
    deltaT = np.max(df[key_jd]) - np.min(df[key_jd])
    #print(f"  {idx} : Delta T = {deltaT:.2f}")

    # Mean and std
    c_mean = np.mean(df[f"{band_l}_{band_r}"])
    # Photometric uncertainty
    c_std_phot = adderr(df[f"{band_l}_{band_r}err"])/len(df)
    # Standard Deviation
    c_SD = np.std(df[f"{band_l}_{band_r}"])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df))
    # Total error 
    c_std = np.sqrt(c_std_phot**2 + c_SE**2)

    # Weighted mean
    c_w = 1/df[f"{band_l}_{band_r}err"]**2
    c_wmean = np.average(df[f"{band_l}_{band_r}"], weights=c_w)
    # Check !!!!!!
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(c_w))

    ## Round error
    c_mean_str, c_std_str = round_error(c_mean, c_std)
    c_wmean_str, c_wstd_str = round_error(c_wmean, c_wstd)
    label_c = (
        f"N={len(df)}\n{band_l}-{band_r} "
        f"${c_wmean_str}" + r"\pm" + f"{c_wstd_str}$"
        )
   
    # For open circle
    ax.scatter(
      df[key_jd]-JD0, df[f"{band_l}_{band_r}"], color=col, s=70, lw=1, 
      marker="o", facecolor="None", edgecolor=col, zorder=-1, label=label_c)

    ax.errorbar(
      df[key_jd]-JD0, df[f"{band_l}_{band_r}"], df[f"{band_l}_{band_r}err"], 
      color=col, lw=0.5, ms=3, marker=None, capsize=0, ls="None")

    # Mean and std
    x0, x1 = ax.get_xlim()
    ax.hlines(c_wmean, x0, x1, color=mycolor[1])
    ax.hlines(c_wmean+c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    ax.hlines(c_wmean-c_wstd, x0, x1, color=mycolor[1], ls=myls[1])
    ax.set_xlim([x0, x1])


