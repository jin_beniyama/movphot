#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
For preprocess afer photometry.
Extract good data from DataFrame.
"""
import os
import sys
import numpy as np
import pandas as pd
from scipy import stats
from scipy.stats import norm
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
# For colormap
from matplotlib.colors import LogNorm
from matplotlib import cm

from calcerror import round_error

from .common import (
    adderr, adderr_series, diverr, mulerr, log10err, 
    mycolor, mymark, myls, linear_fit, add_color_reverse, checknan,
    band4CT)
from .visualization import myfigure_refladder, color_from_band


def prepro_CT(df, band, bands, Nobj_min, shift=True, target=None):
    """
    Preprocess for CT calculations.
    x-axis is always g-r.

    Parameters
    ----------
    df : pandas.DataFrame
        dataframe of reference stars
    band : str
        band
    bands : str
        bands
    Nobj_min : int
        minimum object number
    shift : bool, optional
        whether shift to mean of zero
    target : array-like, optional
        target reference star

    Returns
    -------
    lists : array-like
        8 lists (col_cat_list, col_caterr_list, cat_sub_inst_list, cat_sub_insterr_list, 
        y_list, t_list, CT_frame_list, t_frame_list)
    """
    # Output target info
    if target is not None:
        x0_t, x1_t, y0_t, y1_t = target
    
    # Always g, r
    band_l, band_r = band4CT(band, bands)

    # Lists for fitting
    col_cat_list, cat_sub_inst_list       = [], []
    col_caterr_list, cat_sub_insterr_list = [], []
    # y and t_sec 
    y_list, t_list                        = [], []
    # time of the frame (i.e., N_t_frame_list == N_frame)
    CT_frame_list, t_frame_list           = [], []

    nframe_list = list(set(df["nframe"].values.tolist()))
    
    # Output target info
    # Xaxis : col_cat (cat0 - cat1)
    # Yaxis : col_sub_inst (cat0 - inst0)
    if target is not None:
        # Select by catalog color
        assert False, "Check"
        df_t = df[
            (df[f"{band_l}MeanPSFMag"]-df[f"{band_r}MeanPSFMag"] > y0_t)
            & 
            (df[f"{band_l}MeanPSFMag"]-df[f"{band_r}MeanPSFMag"] < y1_t)
            ]
        df_t_l = df_t[df_t["band"]==band_l]
        df_t_r = df_t[df_t["band"]==band_r]
        for nframe in nframe_list:
            df_t_l_n = df_t_l[df_t_l["nframe"]==nframe]
            df_t_r_n = df_t_r[df_t_r["nframe"]==nframe]
            # Extract common objects
            obj_l = df_t_l_n["objID"].values.tolist()
            obj_r = df_t_r_n["objID"].values.tolist()
            obj_common = set(obj_l) & set(obj_r)
            df_t_n_l = df_t_l_n[df_t_l_n["objID"].isin(obj_common)].sort_values("objID")
            df_t_n_r = df_t_r_n[df_t_r_n["objID"].isin(obj_common)].sort_values("objID")
            
            for obj in obj_common:
                df_obj_l = df_t_l_n[df_t_l_n["objID"]==obj]
                df_obj_r = df_t_r_n[df_t_r_n["objID"]==obj]
                flux_l = df_obj_l[f"flux"].values.tolist()
                flux_r = df_obj_r[f"flux"].values.tolist()
                col_inst = [-2.5*np.log10(x/y) for x, y in zip(flux_l, flux_r)]
                if x1_t > col_inst[0] > x0_t:
                    x_target    = df_obj_l["xwin"].values.tolist()[0]
                    y_target    = df_obj_l["ywin"].values.tolist()[0]
                    fits_target = df_obj_l["fits"].values.tolist()[0]
                    cntmfrac    = df_obj_l["cntmfrac"].values.tolist()[0]
                    print(f"    Target info. in {nframe}-th fits")
                    print(f"      fits         : {fits_target}")
                    print(f"      objID        : {obj}")
                    print(f"      x,y          : {x_target:.1f}, {y_target:.1f}")
                    print(f"      inst col     : {col_inst[0]:.3f}")
                    print(f"      cntm fra     : {cntmfrac:.3f}")


    for nframe in nframe_list:
        # Extract objects in a frame
        df_n_all = df[df["nframe"]==nframe]

        # Observed band
        df_n = df_n_all[df_n_all["band"] == band]
        y_n = df_n["y"].values.tolist()
        t_n = df_n["t_sec"].values.tolist()

        # Skip few object data
        if len(df_n) < Nobj_min:
            continue

        y_list += y_n
        t_list += t_n
    
        # Note: 
        # CTG
        #     col_inst vs. col_cat
        # CT
        #     col_cat vs. cat_sub_inst

        catmag = df_n[f"{band}MeanPSFMag"].values.tolist()
        catmagerr = df_n[f"{band}MeanPSFMagErr"].values.tolist()

        flux = df_n[f"flux"].values.tolist()
        fluxerr = df_n[f"fluxerr"].values.tolist()
        instmagerr = [2.5*log10err(x, y) for x,y in zip(flux, fluxerr)]

        # Always g and r
        catmag_l = df_n[f"{band_l}MeanPSFMag"].values.tolist()
        catmag_r = df_n[f"{band_r}MeanPSFMag"].values.tolist()
        catmagerr_l = df_n[f"{band_l}MeanPSFMagErr"].values.tolist()
        catmagerr_r = df_n[f"{band_r}MeanPSFMagErr"].values.tolist()

        col_cat_list_n = [x-y for x, y in zip(catmag_l, catmag_r)]
        col_caterr_list_n = [adderr(x, y) for x, y in zip(catmagerr_l, catmagerr_r)]
        cat_sub_inst_list_n = [x+2.5*np.log10(y) for x, y in zip(catmag, flux)]
        cat_sub_insterr_list_n   = [adderr(x, y) for x, y in zip(catmagerr, instmagerr)]

        if shift:
            # Shifted
            # Subtract mean values to use all photometric points 
            # (even in different nights) at once
            # See Jackson et al.2021, PASP, 133, 075003
            col_cat_list_n = [x-np.mean(col_cat_list_n) for x in col_cat_list_n]
            cat_sub_inst_list_n = [x-np.mean(cat_sub_inst_list_n) for x in cat_sub_inst_list_n]

        col_cat_list += col_cat_list_n
        col_caterr_list += col_caterr_list_n
        cat_sub_inst_list += cat_sub_inst_list_n
        cat_sub_insterr_list += cat_sub_insterr_list_n
        
        # Fitting to estimate each CT
        param, cov_sample = curve_fit(
            linear_fit, col_cat_list_n, cat_sub_inst_list_n) 
        CT_frame_list.append(param[0])
        t_frame_list.append(t_n[0])

    lists = [col_cat_list, col_caterr_list, cat_sub_inst_list, cat_sub_insterr_list, 
        y_list, t_list, CT_frame_list, t_frame_list]
    return lists


def plot_CTfit2(
    df, band, bands, Nobj_min, fig, ax, ax_res=None, yr_res=None,
    shift=True, ycolor=1, ny=1280, colormap=False, cbar=False, ax_cbar=None,
    ax_CT=None, N_mc=100, target=None, verbose=False):
    """
    Calculate, plot, and return CTs.

    Parameters
    ----------
    df : DataFrame
        DataFrames of photometry results of reference stars
    band : str
        band
    bands : array-likes
        list of bands
    Nobj_min : int
        minimum object to be required to shift data
    fig : figure
       figure (for colorbar, cmap)
    ax : axis
       ax
    shift : bool
        shift to set mean zero
    ax_res : axis
       axis for residual plot
    yr_res : list of float
       y range of residual plot
    ycolor : int
        number of colors depending on y pixel values
    ny : int
        number of pixels along y axis
    colormap : book
        whether plot colormap like Fig.7 in Jackson+2021
    cbar : book
        whether use cbar to show time-series info.
    ax_cbar : axis
        axis for color var to show time info
    target : array-like
        info. of target
    verbose : bool
        output messages

    Return
    ------
    df_CT : pandas.DataFrame
        DataFrame with CTs
    """

    # band_l, band_r = band4CTG(band, bands)
    # For g, r , i, [g-r, g-r, g-r] 
    band_l, band_r = band4CT(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)

    for idx,x in enumerate(bands):
        if x == band:
            break

    if verbose:
        print(f"  {band}-band in {bands}")

    col_rawfit = "gray"
    col_weifit = "black"
    col_MCfit  = "red"
    col_list   = ["red", "blue", "green", "orange"]
    col_p      = mycolor[idx]
    mark_p     = mymark[idx]

    # Calculate CTs
    # 8 Returns :
    # col_cat_list, col_caterr_list, cat_sub_inst_list, cat_sub_insterr_list, 
    # y_list, t_list, CT_frame_list, t_frame_list
    lists = prepro_CT(df, band, bands, Nobj_min, shift, target)
    col_cat_list         = lists[0]
    col_caterr_list      = lists[1]
    cat_sub_inst_list    = lists[2]
    cat_sub_insterr_list = lists[3]
    y_list               = lists[4]
    t_list               = lists[5]
    CT_frame_list       = lists[6]
    t_frame_list         = lists[7]
    
    if ax_CT is not None:
        ax_CT.scatter(
          t_frame_list, CT_frame_list, 
          s=10, color=col_p, marker=mark_p, 
          label=f"{band_l}-{band_r} N={len(t_frame_list)}")
        ax_CT.set_xlabel("Elapsed time [sec]")
        ax_CT.set_ylabel("CT")
        ax_CT.legend(fontsize=10)


    # Plot at once in both cases ==============================================
    ## Create colormap
    cm = "inferno"
    if colormap:
        # 0.02 mag bin
        ## Extract range
        ymin, ymax = np.min(cat_sub_inst_list), np.max(cat_sub_inst_list)
        ## Set bin width
        xmin, xmax = np.min(col_cat_list), np.max(col_cat_list)
        wbin = 0.02
        bins = [np.arange(xmin, xmax, wbin), np.arange(ymin, ymax, wbin)]
        # Please optimize for each observation
        Nmax = 1e3
        H = ax.hist2d(col_cat_list, cat_sub_inst_list, bins=bins, cmap=cm,
                norm=LogNorm(vmin=1e0, vmax=Nmax))
        # Add colorbars
        fig.colorbar(H[3],ax=ax)

    ## Create cbar to show time-series info
    elif cbar:
        from matplotlib import cm
        mapp = ax.scatter(
            col_cat_list, cat_sub_inst_list, c=t_list,  cmap=cm.inferno, 
            s=30, lw=1, marker=mark_p, facecolor="None", zorder=2)
        cbar = fig.colorbar(mapp, ax_cbar)
        cbar.set_label("Elapsed time [s]")
        # Add errorbars
        ax.errorbar(
            col_cat_list, cat_sub_inst_list,
            xerr=col_caterr_list, yerr=cat_sub_insterr_list, 
            lw=0.5, ms=3, fmt="o", marker=" ", capsize=0, zorder=1)
    ## Simply plot points
    else:
        ax.errorbar(
            col_cat_list, cat_sub_inst_list,
            xerr=col_caterr_list, yerr=cat_sub_insterr_list, 
            ms=1, linewidth=0.5, color=col_p, marker=mark_p, ls="None")
    # Plot at once in both cases ==============================================


    # Set initial parameters to avoid an error in covarience calculation
    p0 = [1, 0.001]

    # Fitting =================================================================
    ## Raw fit
    param_r, cov_r = curve_fit(
        linear_fit, col_cat_list, cat_sub_inst_list, p0=p0)
    paramerr_r = np.sqrt(np.diag(cov_r))
    CT_r, Z_r = param_r[0], param_r[1]
    CTerr_r, Zerr_r = paramerr_r[0], paramerr_r[1]
    if verbose:
        print(f"    CT raw = {CT_r:.4f}+-{CTerr_r:.4f} (not used)")

    param_w, cov_w = curve_fit(
        linear_fit, col_cat_list, cat_sub_inst_list, p0=p0, 
        sigma=cat_sub_insterr_list, absolute_sigma=False)
    paramerr_w = np.sqrt(np.diag(cov_w))
    CT_w, Z_w = param_w[0], param_w[1]
    CTerr_w, Zerr_w = paramerr_w[0], paramerr_w[1]
    if verbose:
        print(f"    CT wei = {CT_w:.4f}+-{Zerr_w:.4f} (not used)")
    
    ## Monte Carlo method to estimate errors
    ## Use mean and std of Monte Carlo results
    # Set seed 0
    seed = 0
    np.random.seed(seed)
    CT_temp_list = []
    Z_temp_list = []

    for n in range(N_mc):
        # Initialization
        cat_sub_inst_sample = np.random.normal(
          cat_sub_inst_list, cat_sub_insterr_list, len(cat_sub_inst_list))
        col_cat_sample = np.random.normal(
          col_cat_list, col_caterr_list, len(col_cat_list))

        param_sample, cov_sample = curve_fit(
          linear_fit, col_cat_sample, cat_sub_inst_sample)
        CT_sample = param_sample[0]
        Z_sample = param_sample[1]
        CT_temp_list.append(CT_sample)
        Z_temp_list.append(Z_sample)
    
    CT_MC_mean = np.mean(CT_temp_list)
    CT_MC_std = np.std(CT_temp_list)
    Z_MC_mean = np.mean(Z_temp_list)
    Z_MC_std = np.std(Z_temp_list)
    if verbose:
        print(f"    CT MC  = {CT_MC_mean:.4f}+-{CT_MC_std:.4f} (used)")
    # Fitting =================================================================


    ## Mean values
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    x = np.arange(xmin, xmax, 0.01)
 
    # ToDo Round error

    ## Raw
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_raw = (
            f"Raw y={CT_r:.2f}" + "$\pm$" + f"{CTerr_r:.3f}x + {Z_r:.2f}"
            + "$\pm$" + f"{Zerr_r:.3f}")
    ax.plot(
        x, CT_r*x+Z_r, marker="", lw=1, ls=myls[1], color=col_rawfit,
        label=label_raw)
    ## Weighted
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_wei = (
            f"Weighted y={CT_w:.2f}" + "$\pm$" + f"{CTerr_w:.3f}x + {Z_w:.2f}"
        + "$\pm$" + f"{Zerr_w:.3f}")
    ax.plot(
        x, CT_w*x+Z_w, marker="", lw=1, ls=myls[2], color=col_weifit,
        label=label_wei)

    ## Monte Carlo Method
    label_MC = (
        f"Monte Carlo y={CT_MC_mean:.2f}" + "$\pm$" 
        + f"{CT_MC_std:.3f}x + {Z_MC_mean:.3f}" + "$\pm$" + f"{Z_MC_std:.3f}")
    ax.plot(
        x, CT_MC_mean*x+Z_MC_mean, marker="", lw=1, ls=myls[0], 
        color=col_MCfit, label=label_MC)
    
    # Plot residuals only when shift == True
    # since its nonsense without shift (ask JB for the detail)
    if shift:
        # Use different colors 
        if ycolor >1:
            # Size of each chunk
            width_y = ny/ycolor
            for y in range(ycolor):
                ymin = width_y*y
                ymax = width_y*(y+1)

                ## Residuals (MCfit - obs.)
                label_res = "Residuals\n(MCfit - obs.)"
                res = [
                    CT_MC_mean*x+Z_MC_mean-y for (x,y,ycoord)
                    in zip(col_cat_list, cat_sub_inst_list, y_list) 
                    if (ycoord > ymin) & (ycoord < ymax)]
                ax_res.hist(
                    res, 
                    orientation="horizontal", histtype="step", color=col_list[y],
                    label=label_res)

        else:
            ## Residuals (MCfit - obs.)
            label_res = "Residuals\n(MCfit - obs.)"
            res = [
                CT_MC_mean*x+Z_MC_mean-y for x,y in zip(col_cat_list, cat_sub_inst_list)]
            ax_res.hist(
                res, 
                orientation="horizontal", histtype="step", color=col_MCfit,
                label=label_res)

        ax_res.set_xlabel("N")
        ax_res.legend(fontsize=10)
        ymin, ymax = ax_res.get_ylim()
        if abs(ymin) > abs(ymax):
            ax_res.set_ylim(-abs(ymin), abs(ymin))
        else:
            ax_res.set_ylim(-abs(ymax), abs(ymax))

    col_cat_label = "$m_{cat," + band_l + "} - m_{cat," + band_r + "}$"
    cat_sub_inst_label = "$m_{cat," + band + "} - m_{inst," + band + "}$"
    ax.set_xlabel(col_cat_label)
    ax.set_ylabel(cat_sub_inst_label)
    xmin, xmax = ax.get_xlim()
    ax.set_xlim([xmin, xmax])
    ax.legend(fontsize=10)

    df_CT = pd.DataFrame({
        f"{band}_CT_r"   : [CT_r],
        f"{band}_CTerr_r" : [CTerr_r],
        f"{band}_CT_w"    : [CT_w],
        f"{band}_CTerr_w" : [CTerr_w],
        f"{band}_CT_MC"   : [CT_MC_mean],
        f"{band}_CTerr_MC": [CT_MC_std]}
        )
    return df_CT


def plot_CTfit(
    CT_df_list, band, bands, fig, ax, ax_res=None, 
    shift=True, ycolor=1, ny=1280, colormap=False, verbose=False):
    """
    Plot CTs.

    Parameters
    ----------
    CT_df_list : list
        list of DataFrames of photometry results of reference stars
    band : str
        band
    bands : array-likes
        list of bands
    fig : figure
       figure (for colorbar)
    ax : axis
       ax
    shift : bool
        shift to set mean zero
    ax_res : axis
       axis for residual plot
    ycolor : int
        number of colors depending on y pixel values
    ny : int
        number of pixels along y axis
    colormap : book
        whether plot colormap like Fig.7 in Jackson+2021
    verbose : bool
        whether output messages

    Return
    ------
    df_CT : pandas.DataFrame
        DataFrame with CTs
    """

    # band_l, band_r = band4CTG(band, bands)
    # For g-r, g-r, i-r 
    band_l, band_r = band4CT(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)

    # Lists for fitting
    cat_sub_inst_list, col_cat_list       = [], []
    cat_sub_insterr_list, col_caterr_list = [], []
    y_list = []
    
    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    col_rawfit = "gray"
    col_weifit = "black"
    col_MCfit  = "red"
    col_list = ["red", "blue", "green", "orange"]
    col_p = mycolor[idx]
    mark_p = mymark[idx]
    
    for df in CT_df_list:
        if shift:
            # Shifted
            # Subtract mean values to use all photometric points 
            # (even in different nights) at once
            # See Jackson et al.2021, PASP, 133, 075003
            df["col_cat"] -= np.mean(df["col_cat"])
            df["cat_sub_inst"] -= np.mean(df["cat_sub_inst"])


        # Save all result for fitting
        col_cat_list   += df["col_cat"].values.tolist()
        cat_sub_inst_list  += df["cat_sub_inst"].values.tolist()
        col_caterr_list    += df["col_caterr"].values.tolist()
        cat_sub_insterr_list   += df["cat_sub_insterr"].values.tolist()
        y_list += df["y"].values.tolist()

    # Plot at once in both cases
    ## Create colormap
    cm = "inferno"
    if colormap:
        # 0.02 mag bin
        ## Extract range
        xmin, xmax = np.min(col_cat_list), np.max(col_cat_list)
        ymin, ymax = np.min(cat_sub_inst_list), np.max(cat_sub_inst_list)
        ## Set bin width
        xmin, xmax = np.min(col_cat_list), np.max(col_cat_list)
        wbin = 0.02
        bins = [np.arange(xmin, xmax, wbin), np.arange(ymin, ymax, wbin)]
        # Please optimize for each observation
        Nmax = 1e3
        H = ax.hist2d(col_cat_list, cat_sub_inst_list, bins=bins, cmap=cm,
                norm=LogNorm(vmin=1e0, vmax=Nmax))
        # Add colorbars
        fig.colorbar(H[3],ax=ax)
    ## Plot points
    else:
        ax.errorbar(
            col_cat_list, cat_sub_inst_list, 
            xerr=col_caterr_list, yerr=cat_sub_insterr_list,
            ms=1, linewidth=0.5, color=col_p, marker=mark_p, ls="None")



    # Set initial parameters to avoid an error in covarience calculation
    p0 = [1, 0.001]

    ## Raw fit
    param_r, cov_r = curve_fit(
      linear_fit, col_cat_list, cat_sub_inst_list, p0=p0)
    paramerr_r = np.sqrt(np.diag(cov_r))
    CT_r, Z_r = param_r[0], param_r[1]



    CTerr_r, Zerr_r = paramerr_r[0], paramerr_r[1]
    if verbose:
        print(f"  CT raw = {CT_r:.4f}+-{CTerr_r:.4f} (not used)")

    ## Weighted fit
    param_w, cov_w = curve_fit(
      linear_fit, col_cat_list, cat_sub_inst_list, p0=p0, 
      sigma=cat_sub_insterr_list, absolute_sigma=False)
    paramerr_w = np.sqrt(np.diag(cov_w))
    CT_w, Z_w = param_w[0], param_w[1]
    CTerr_w, Zerr_w = paramerr_w[0], paramerr_w[1]
    if verbose:
        print(f"  CT wei = {CT_w:.4f}+-{CTerr_w:.4f} (not used)")

    ## Monte Carlo method to estimate errors
    ## Use mean and std of Monte Carlo results
    N_mc = 100
    # Set seed 0
    seed = 0
    np.random.seed(seed)
    CT_temp_list = []
    Z_temp_list = []

    for n in range(N_mc):
        # Initialization
        col_cat_sample = np.random.normal(
          col_cat_list, col_caterr_list, len(col_cat_list))
        cat_sub_inst_sample = np.random.normal(
          cat_sub_inst_list, cat_sub_insterr_list, len(cat_sub_inst_list))

        param_sample, cov_sample = curve_fit(
          linear_fit, col_cat_sample, cat_sub_inst_sample)
        CT_sample = param_sample[0]
        Z_sample = param_sample[1]
        CT_temp_list.append(CT_sample)
        Z_temp_list.append(Z_sample)

    CT_MC_mean = np.mean(CT_temp_list)
    CT_MC_std = np.std(CT_temp_list)
    Z_MC_mean = np.mean(Z_temp_list)
    Z_MC_std = np.std(Z_temp_list)
    if verbose:
        print(f"  CT MC  = {CT_MC_mean:.4f}+-{CT_MC_std:.4f} (used)")

    ## Mean values
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    x = np.arange(xmin, xmax, 0.01)
 
    # ToDo Round error

    ## Raw
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_raw = (
            f"Raw y={CT_r:.2f}" + "$\pm$" + f"{CTerr_r:.3f}x + {Z_r:.2f}"
            + "$\pm$" + f"{Zerr_r:.3f}")
    ax.plot(
        x, CT_r*x+Z_r, marker="", lw=1, ls=myls[1], color=col_rawfit,
        label=label_raw)
    ## Weighted
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_wei = (
            f"Weighted y={CT_w:.2f}" + "$\pm$" + f"{CTerr_w:.3f}x + {Z_w:.2f}"
        + "$\pm$" + f"{Zerr_w:.3f}")
    ax.plot(
        x, CT_w*x+Z_w, marker="", lw=1, ls=myls[2], color=col_weifit,
        label=label_wei)

    ## Monte Carlo Method
    label_MC = (
        f"Monte Carlo y={CT_MC_mean:.2f}" + "$\pm$" 
        + f"{CT_MC_std:.3f}x + {Z_MC_mean:.3f}" + "$\pm$" + f"{Z_MC_std:.3f}")
    ax.plot(
        x, CT_MC_mean*x+Z_MC_mean, marker="", lw=1, ls=myls[0], 
        color=col_MCfit, label=label_MC)
    
    # Plot residuals only when shift == True
    # since its nonsense without shift (ask JB for the detail)
    if shift:

        # Use different colors 
        if ycolor >1:
            # Size of each chunk
            width_y = ny/ycolor
            for y in range(ycolor):
                ymin = width_y*y
                ymax = width_y*(y+1)

                label_res = f"Residuals {ymin:.1f} < y < {ymax:.1f}\n(MCfit - obs.)"
                res = [
                    CT_MC_mean*x+Z_MC_mean-y for (x,y,ycoord)
                    in zip(col_cat_list, cat_sub_inst_list, y_list) 
                    if (ycoord > ymin) & (ycoord < ymax)]
                ax_res.hist(
                    res, 
                    orientation="horizontal", histtype="step", color=col_list[y],
                    label=label_res)

        else:
            ## Residuals (MCfit - obs.)
            label_res = "Residuals\n(MCfit - obs.)"
            res = [
                CT_MC_mean*x+Z_MC_mean-y for x,y in zip(col_cat_list, cat_sub_inst_list)]
            ax_res.hist(
                res, 
                orientation="horizontal", histtype="step", color=col_MCfit,
                label=label_res)

        ax_res.set_xlabel("N")
        ax_res.legend(fontsize=10)
        ymin, ymax = ax_res.get_ylim()
        if abs(ymin) > abs(ymax):
            ax_res.set_ylim(-abs(ymin), abs(ymin))
        else:
            ax_res.set_ylim(-abs(ymax), abs(ymax))

    col_cat_label = "$m_{cat," + band_l + "} - m_{cat," + band_r + "}$"
    cat_sub_inst_label = "$m_{cat," + band + "} - m_{inst," + band + "}$"
    ax.set_xlabel(col_cat_label)
    ax.set_ylabel(cat_sub_inst_label)
    xmin, xmax = ax.get_xlim()
    ax.set_xlim([xmin, xmax])
    ax.legend(fontsize=10)

    # Use band for CT, not band_l_band_r as CTG
    df_CT = pd.DataFrame(
      {f"{band}_CT_r"   : [CT_r],
      f"{band}_CTerr_r" : [CTerr_r],
      f"{band}_CT_w"    : [CT_w],
      f"{band}_CTerr_w" : [CTerr_w],
      f"{band}_CT_MC"   : [CT_MC_mean],
      f"{band}_CTerr_MC": [CT_MC_std]}
      )
    return df_CT


def prepro_Z(df, band, bands, CT, CTerr, Nobj_min, N_mc, verbose=False):
    """
    Preprocess for Z calculations.
    """
    # Lists for fitting
    Z_MC_list, Zerr_MC_list   = [], []
    CT_MC_list, CTerr_MC_list = [], []
    t_list = []
    
    nframe_list = list(set(df["nframe"].values.tolist()))
    nframe_Z_list = []
    band_l, band_r = band4CT(band, bands)

    print(f"    band, band_l, band_r = {band}, {band_l}, {band_r}")

    for nframe in nframe_list:

        # Extract objects in a frame
        df_n_all = df[df["nframe"]==nframe]

        # Observed band
        df_n = df_n_all[df_n_all["band"] == band]

        col_cat_list, col_caterr_list           = [], []
        cat_sub_inst_list, cat_sub_insterr_list = [], []
         
        catmag      = df_n[f"{band}MeanPSFMag"].values.tolist()
        catmag_l    = df_n[f"{band_l}MeanPSFMag"].values.tolist()
        catmag_r    = df_n[f"{band_r}MeanPSFMag"].values.tolist()
        catmagerr   = df_n[f"{band}MeanPSFMagErr"].values.tolist()
        catmagerr_l = df_n[f"{band_l}MeanPSFMagErr"].values.tolist()
        catmagerr_r = df_n[f"{band_r}MeanPSFMagErr"].values.tolist()

        flux = df_n["flux"].values.tolist()
        fluxerr = df_n[f"fluxerr"].values.tolist()
        instmagerr = [2.5*log10err(x, y) for x,y in zip(flux, fluxerr)]

        col_cat_list_n = [x-y for x, y in zip(catmag_l, catmag_r)]
        col_caterr_list_n = [adderr(x, y) for x, y in zip(catmagerr_l, catmagerr_r)]
        cat_sub_inst_list_n = [x+2.5*np.log10(y) for x, y in zip(catmag, flux)]
        cat_sub_insterr_list_n   = [adderr(x, y) for x, y in zip(catmagerr, instmagerr)]

        col_cat_list += col_cat_list_n
        col_caterr_list += col_caterr_list_n
        cat_sub_inst_list += cat_sub_inst_list_n
        cat_sub_insterr_list += cat_sub_insterr_list_n

        nframe_Z_list.append(nframe)
        #if (flux_r < 0) or (flux_l < 0):
        #    print("Negative flux detected.")
        #    print(f"    flux_l, flux_r = {flux_l}, {flux_r}")
        #    sys.exit()

        # If Z is not estimated due to the lack of reference stars,
        # add 0 to the dataframe to keep the length of DataFrame
        if len(col_cat_list) < Nobj_min:
            # Temporally insert 0 to be removed later
            Z_MC_list.append(0)
            Zerr_MC_list.append(0)
            CT_MC_list.append(0)
            CTerr_MC_list.append(0)
            t_list.append(0)

            print(
              f"    {band} {nframe}-th frame Z is not "
              f"estimated due to the lack of frames Nframe={len(col_cat_list)}."
              f"\nskip the frame."
            )
            continue

        t_sec = df_n["t_sec"].values.tolist()[0]
        t_list.append(t_sec)

        # Estimate Z using ref stars in fov
        # Use col_cat_list, col_caterr_list, col_inst_list, col_insterr_list

        # Use mean and std of Monte Carlo results
        # Set seed 0
        seed = 0
        np.random.seed(seed)
        Z_temp_list = []

        Nstar = len(col_cat_list)

        def linear_fit_Z(x, b):
            """Linear fit function for scipy.optimize.
            """
            return CT*x + b

        for n in range(N_mc):
            # Initialization
            col_cat_sample = np.random.normal(
                col_cat_list, col_caterr_list, Nstar)
            cat_sub_inst_sample = np.random.normal(
                cat_sub_inst_list, cat_sub_insterr_list, Nstar)

            param_sample, cov_sample = curve_fit(
                linear_fit_Z, col_cat_sample, cat_sub_inst_sample) 
            Z_sample = param_sample[0]
            Z_temp_list.append(Z_sample)
        
        Z_MC_mean = np.mean(Z_temp_list)
        Z_MC_std = np.std(Z_temp_list)
        if verbose:
            print(f"  MC mean, std = {Z_MC_mean:.4f}, {Z_MC_std:.4f}")
        Z_MC_list.append(Z_MC_mean)
        Zerr_MC_list.append(Z_MC_std)

    # Create dataframe of a frame
    # Save CTs as well
    df_Z = pd.DataFrame({
      "nframe"                     : nframe_Z_list,
      f"{band}_Z_MC"    : Z_MC_list,
      f"{band}_Zerr_MC" : Zerr_MC_list,
      f"{band}_t_sec"   : t_list
      })
    return df_Z


def plot_Zfit2(
    df, band, bands, CT, CTerr, 
    Nobj_min, ax_Z, N_mc=100, verbose=False):
    """
    Calculate Zs with fixed CTs.

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    band : str
        band
    bands : array-likes
        list of bands
    CT : float
        CT
    CTerr : float
        uncertainty of CT
    Nobj_min : int
        minimum threshold of number of object in a frame
        (not contaminated, not on edge)
    ax_Z : matplotlib.axis
        axis for Z
    N_mc : int
        number of trials to evaluate uncertainties
    verbose : bool
        whether output messages

    Return
    ------
    df_CTI : pandas.DataFrame
        DataFrame with CTIs
   """

    for idx,x in enumerate(bands):
        if x == band:
            break

    col_p = mycolor[idx]
    mark_p = mymark[idx]
  
    df_Z = prepro_Z(df, band, bands, CT, CTerr, Nobj_min, N_mc)

    if ax_Z is not None:
        df_good = df_Z[df_Z[f"{band}_Z_MC"]!=0]
        df_bad  = df_Z[df_Z[f"{band}_Z_MC"]==0]
        ax_Z.errorbar(
          df_good[f"{band}_t_sec"], df_good[f"{band}_Z_MC"], 
          yerr=df_good[f"{band}_Zerr_MC"],
          linewidth=0.2, ls="None", ms=1, color=col_p, marker=mark_p, 
          label=f"{band} N={len(df_good)} (bad N={len(df_bad)})")
        ax_Z.legend(fontsize=10)
        ax_Z.invert_yaxis()

    return df_Z


def calc_objmag2(df, df_col, band, bands):
    """
    Calculate CT/Z corrected object magnitudes.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    df_col : pandas.DataFrame
        object color dataframe
    band : str
        band
    bands : str
        bands

    Return
    ------
    df : pandas.DataFrame
        object dataframe with instrumental and CT/Zcorrected colors
    """
    

    if len(bands)==3:
        b1, b2, b3 = bands

        # Common frame in df
        df_1 = df[df["band"]==b1]
        df_2 = df[df["band"]==b2]
        df_3 = df[df["band"]==b3]
        nf_1 = set(df_1["nframe"].values.tolist())
        nf_2 = set(df_2["nframe"].values.tolist())
        nf_3 = set(df_3["nframe"].values.tolist())

        # Common frame in df_col
        nf_col = set(df_col["nframe"].values.tolist())

        nf_common = list(nf_1 & nf_2 & nf_3 & nf_col)

        df_1 = df_1[df_1["nframe"].isin(nf_common)]
        df_2 = df_2[df_2["nframe"].isin(nf_common)]
        df_3 = df_3[df_3["nframe"].isin(nf_common)]
        df_col = df_col[df_col["nframe"].isin(nf_common)]

        df_1 = df_1.reset_index(drop=True)
        df_2 = df_2.reset_index(drop=True)
        df_3 = df_3.reset_index(drop=True)
        df_col = df_col.reset_index(drop=True)


        # CT, Z corrected magnitude
        #   m_cat = m_inst + (g-r)*CT + Z
        m1 = df_1["maginst"] + df_col["g_r"]*df_1["CT"] + df_1["Z"]
        # Consider error of maginst, g_r, CT and Z
        temperr = mulerr(df_col["g_r"], df_col["g_rerr"], df_1["CT"], df_1[f"CTerr"])
        m1err = adderr_series(df_1["maginsterr"], temperr, df_1[f"Zerr"])

        m2 = df_2["maginst"] + df_col["g_r"]*df_2["CT"] + df_2["Z"]
        # Consider error of maginst, g_r, CT and Z
        temperr = mulerr(df_col["g_r"], df_col["g_rerr"], df_2["CT"], df_2[f"CTerr"])
        m2err = adderr_series(df_2["maginsterr"], temperr, df_2[f"Zerr"])

        m3 = df_3["maginst"] + df_col["g_r"]*df_3["CT"] + df_3["Z"]
        # Consider error of maginst, g_r, CT and Z
        temperr = mulerr(df_col["g_r"], df_col["g_rerr"], df_3["CT"], df_3[f"CTerr"])
        m3err = adderr_series(df_3["maginsterr"], temperr, df_3[f"Zerr"])

        df = pd.DataFrame({
            f"{b1}mag": m1,
            f"{b1}magerr": m1err,
            f"{b2}mag": m2,
            f"{b2}magerr": m2err,
            f"{b3}mag": m3,
            f"{b3}magerr": m3err,
            f"t_jd_{b1}": df_1["t_jd"],
            f"t_jd_{b2}": df_2["t_jd"],
            f"t_jd_{b3}": df_3["t_jd"],
            f"t_utc_{b1}": df_1["t_utc"],
            f"t_utc_{b2}": df_2["t_utc"],
            f"t_utc_{b3}": df_3["t_utc"],
            f"nframe": nf_common
            })

    return df


def calc_refmag(df, band, bands):
    """
    Calculate CT/Z corrected magnitudes of reference stars.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    band : str
        band
    bands : str
        bands

    Return
    ------
    df : pandas.DataFrame
        object dataframe with instrumental and CT/Zcorrected magnitudes
    """
    
    objID = list(set(df["objID"].values.tolist()))
    b1, b2, b3 = bands
    df_list = []
    for obj in objID:
        df_obj = df[df["objID"] == obj]

        # Common frame in df
        df_1 = df_obj[df_obj["band"]==b1]
        df_2 = df_obj[df_obj["band"]==b2]
        df_3 = df_obj[df_obj["band"]==b3]
        nf_1 = set(df_1["nframe"].values.tolist())
        nf_2 = set(df_2["nframe"].values.tolist())
        nf_3 = set(df_3["nframe"].values.tolist())

        nf_common = list(nf_1 & nf_2 & nf_3)

        df_1 = df_1[df_1["nframe"].isin(nf_common)]
        df_2 = df_2[df_2["nframe"].isin(nf_common)]
        df_3 = df_3[df_3["nframe"].isin(nf_common)]

        # Skip unfavorable targets.
        # Use only reference stars detected in all three bands
        if len(df_1) == 0:
            continue

        df_1 = df_1.reset_index(drop=True)
        df_2 = df_2.reset_index(drop=True)
        df_3 = df_3.reset_index(drop=True)

        # Use g-r color
        g_r = df_1[f"gMeanPSFMag"]-df_1[f"rMeanPSFMag"]
        g_rerr = adderr_series(df_1[f"gMeanPSFMagErr"], df_1[f"rMeanPSFMagErr"])

        # CT, Z corrected magnitude
        #   m_cat = m_inst + (g-r)*CT + Z
        m1 = df_1["maginst"] + (g_r)*df_1["CT"] + df_1["Z"]
        # Consider error of maginst, g_r, CT and Z
        temperr = mulerr(g_r, g_rerr, df_1["CT"], df_1[f"CTerr"])
        m1err = adderr_series(df_1["maginsterr"], temperr, df_1[f"Zerr"])

        m2 = df_2["maginst"] + (g_r)*df_2["CT"] + df_2["Z"]
        # Consider error of maginst, g_r, CT and Z
        temperr = mulerr(g_r, g_rerr, df_2["CT"], df_2[f"CTerr"])
        m2err = adderr_series(df_2["maginsterr"], temperr, df_2[f"Zerr"])

        m3 = df_3["maginst"] + g_r*df_3["CT"] + df_3["Z"]
        # Consider error of maginst, g_r, CT and Z
        temperr = mulerr(g_r, g_rerr, df_3["CT"], df_3[f"CTerr"])
        m3err = adderr_series(df_3["maginsterr"], temperr, df_3[f"Zerr"])

        df_temp = pd.DataFrame({
            f"objID": obj,
            f"{b1}mag": m1,
            f"{b1}magerr": m1err,
            f"{b2}mag": m2,
            f"{b2}magerr": m2err,
            f"{b3}mag": m3,
            f"{b3}magerr": m3err,
            f"{b1}MeanPSFMag": df_1[f"{b1}MeanPSFMag"],
            f"{b1}MeanPSFMagErr": df_1[f"{b1}MeanPSFMagErr"],
            f"{b2}MeanPSFMag": df_1[f"{b2}MeanPSFMag"],
            f"{b2}MeanPSFMagErr": df_1[f"{b2}MeanPSFMagErr"],
            f"{b3}MeanPSFMag": df_1[f"{b3}MeanPSFMag"],
            f"{b3}MeanPSFMagErr": df_1[f"{b3}MeanPSFMagErr"],
            f"t_jd_{b1}": df_1["t_jd"],
            f"t_jd_{b2}": df_2["t_jd"],
            f"t_jd_{b3}": df_3["t_jd"],
            f"t_utc_{b1}": df_1["t_utc"],
            f"t_utc_{b2}": df_2["t_utc"],
            f"t_utc_{b3}": df_3["t_utc"],
            f"nframe": nf_common,
            })
        df_list.append(df_temp)
    df = pd.concat(df_list)
    return df
