#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Common functions for movinv object photometry.
  (error calculation, DataFrame handling, object orbit prediction etc.)
"""
import os
import datetime
from functools import wraps
import time
import pandas as pd
from scipy import interpolate
from scipy.spatial import KDTree
import astropy.io.fits as fits
from astropy.time import Time
from astropy.wcs import WCS as wcs
import numpy as np
import sep
from scipy.optimize import curve_fit
from astroquery.jplhorizons import Horizons
from astropy import units as u

from fitting import gauss_fit, gauss_fit_oneside, moffat_fit, moffat_fit_oneside


mycolor = ["#AD002D", "#1e50a2", "#006e54", "#ffd900", "#EFAEA1", 
           "#69821b", "#ec6800", "#afafb0", "#0095b9", "#89c3eb"]*100

mymark = ["o", "^", "x", "D", "+", "v", "<", ">", "h", "H"]*100

myls = ["solid", "dashed", "dashdot", "dotted", 
        (0, (5, 3, 1, 3, 1, 3)), (0, (4,2,1,2,1,2,1,2))]*100


def band4CTG(band, bands):
  """
  Return 2 bands for CTG determination.
  For CTG ! 
  if g,r,i -> g-r, r-i, (i-g)
  if g,r,z -> g-r, r-z, (z-g)
  
  Parameters
  ----------
  band : str
    band of fits
  bands : str
    3 bands

  Returns
  -------
  mag_l, mag_r : float
    used magnitude
  """

  if bands == ["g", "r", "i"]:

    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "i"
    elif band=="i":
      mag_l, mag_r = "i", "g"

  elif bands == ["g", "r", "z"]:
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "r", "z"
    elif band=="z":
      mag_l, mag_r = "z", "g"

  elif bands == ["r", "z"]:
    mag_l, mag_r = "r", "z"

  return mag_l, mag_r


def band4CT(band, bands):
  """
  Return 2 bands for CT determination.
  For CT ! 
  if g,r,i -> g-r, g-r, (g-r)
  if g,r,z -> g-r, g-r, (g-r)
  
  Parameters
  ----------
  band : str
      band of fits
  bands : str
      3 bands

  Returns
  -------
  mag_l, mag_r : float
      used magnitude
  """

  if bands == ["g", "r", "i"]:
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "g", "r"
    elif band=="i":
      mag_l, mag_r = "g", "r"

  if bands == ["g", "r", "z"]:
    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "g", "r"
    elif band=="z":
      mag_l, mag_r = "g", "r"
  return mag_l, mag_r


def checknan(df):
    """
    Check whether df contains NaN.
    """
    N_nan = np.sum(df.isnull().sum())
    if N_nan > 0:
        idx_nan = df[df.isnull().any(axis=1)].index.tolist()
        print(f"idx_nan = {idx_nan}")
        print(f"  N_nan =  {N_nan}")
    assert N_nan==0, "Nan detected."



def add_color_reverse(df, bands):
    """
    Add reversed color to input DataFrame.
    (ex. from g_r, g_rerr to r_g, r_gerr)

    Parameters
    ----------
    df : pandas.DataFrame
        input dataframe
    bands : str
        bands

    Return
    ------
    df : pandas.DataFrame
        output dataframe with reverse colors
    """
    col = df.columns.tolist()
    N = len(bands)
    for i in range(N):
        idx_band1 = i%N
        idx_band2 = (i+1)%N
        band1 = bands[idx_band1]
        band2 = bands[idx_band2]
        try:
          df[f"{band1}_{band2}"]    = -df[f"{band2}_{band1}"]
          df[f"{band1}_{band2}err"] = df[f"{band2}_{band1}err"]
        except:
          pass
        try:
          df[f"{band2}_{band1}"]    = -df[f"{band1}_{band2}"]
          df[f"{band2}_{band1}err"] = df[f"{band1}_{band2}err"]
        except:
          pass
    return df


def linear_fit(x, a, b):
  """Linear fit function for scipy.optimize.
  """
  return a*x + b


def linear_fit_Z(x, b):
  """Linear fit function for scipy.optimize.
  """
  return _CT*x + b


def linear_fit_Z_fixslope(x, b):
  """Linear fit function for scipy.optimize with the slope=1.
  """
  return x + b


def dir_from_arg(args):
    """
    Create identical output directory from arguments.

    Parameter
    ---------
    args : parser

    Return
    ------
    outdir : str
      output directory
        {now}{photref}{radref}{photobj}{radref}{refstar}
        {winpos}{band}{filename}{nraneg})
    """
    # Time
    now = nowstr()

    # photmetry method
    if args.refphot == "app":
        rad_ref = "f".join(str(args.rad_ref).split("."))
        photref = f"refapp_r{rad_ref}"
    elif args.refphot == "ann":
        rad_ref = "f".join(str(args.rad_ref).split("."))
        photref = f"refann_r{rad_ref}"
    # ToDo : Update arguments for ref and obj
    elif args.refphot == "iso":
        photref = (
            f"refiso_rdisk{args.r_disk}sigma{args.sigma}"
            f"eps{args.epsilon}ma{args.minarea}")


    if args.tarphot == "app":
        rad_tar = "f".join(str(args.rad_tar).split("."))
        phottar = f"tarapp_r{rad_tar}"
    elif args.tarphot == "ann":
        rad_tar = "f".join(str(args.rad_tar).split("."))
        phottar = f"tarann_r{rad_tar}"
    elif args.tarphot == "iso":
        phottar = (
            f"tariso_rdisk{args.r_disk}sigma{args.sigma}"
            f"eps{args.epsilon}ma{args.minarea}")

    photpart = f"{photref}_{phottar}"

    # Override
    if args.autorad:
        autoradfac = "f".join(str(args.autoradfac).split("."))
        photpart = f"ref{args.refphot}tar{args.tarphot}autorad{autoradfac}"
    
    # ref star info.
    if len(args.refmagmax)==1:
        rmmax = "f".join(str(args.refmagmax[0]).split("."))
    else:
        # [18.0, 19.0] -> [18f0, 19f0]
        refmagmax = ["f".join(x.split(".")) for x in args.refmagmax]
        # -> 18f0_19f0
        rmmax = "_".join(refmagmax)
    refstar = f"rmto{rmmax}"


    # winpos
    if args.nowinpos:
        winpos_str = "nowin"
    else:
        winpos_str = "win"

    # band
    band_str = "".join(args.bands)

    # filename 
    if args.standard:
        filename = get_filename(args.standard)
        filename_str = f"{filename}"
    # Photometry only reference stars
    else:
        filename_str = "noMO"

    # n range 
    if args.N1:
        N0 = args.N0
        N1 = args.N1
        nrange_str = f"N{N0}to{N1}"
    elif args.Nana:
        N0 = args.N0
        N1 = args.N0 + args.Nana-1
        nrange_str = f"N{N0}to{N1}"
    else:
        nrange_str = "all"

    outdir = (
        f"{now}_{photpart}_{refstar}"
        f"_{winpos_str}_{band_str}_{filename_str}_{nrange_str}")

    return outdir




def extract_identical(df_base, df_spec, d_th, key_x="x", key_y="y"):
  """
  Extract identical elements from df_special

  
  Parameters
  ----------
  df_base : pandas.DataFrame
    base object DataFrame
  df_spec : pandas.DataFrame
    special object DataFrame
  d_th : float
    distance threshold in pixel
  key_x, key_y : str
    keys for x and y

  Return
  ------
  df_identical : pandas.DataFrame
    identical object DataFrame or empty DataFrame
  """

  # ToDo:
  # Update this part before the function.
  # Return empty dataframe if df_spec is empty
  if df_spec.empty:
    return pd.DataFrame()
  x_base, y_base = df_base[key_x], df_base[key_y]

  tree_base = KDTree(list(zip(x_base, y_base)), leafsize=10)

  idx_identical = []
  df_identical = []
  for idx, row in df_spec.iterrows():
    # Search whether close object exists in df_base
    res = tree_base.query_ball_point((row[key_x], row[key_y]), d_th)
    if len(res) == 0:

        #print(
        #  "Identical object is detected in df_spec "
        #  "since no object is detected in df_base."
        #  )

        idx_identical.append(idx)
        df_identical.append(df_spec.loc[idx:idx])
    if len(res) > 0:
        # Judge the object in df_spec is the same with an object in df_base
        pass

  if len(df_identical) == 0:
    return pd.DataFrame()
  #print(f"  All df_spec N = {len(df_spec)}")
  #print(f"  identical   N = {len(idx_identical)}")
  #print(f"  identical object index: {idx_identical}")
  df_identical = pd.concat(df_identical)
  df_identical = df_identical.reset_index(drop=True)
  return df_identical
  


#def check_mask(df, mask, key_x="x", key_y="y"):
#    """
#    Check whether objects in df are in mask.
# 
#    Parameters
#    ----------
#    df : pandas.DataFrame
#        object to be checked 
#    mask : numpy.ndarray
#        mask
#    key_x, key_y : str
#        keyword for location
#
#    Return
#    ------
#    df : pandas.DataFrame
#        dataframe or empty DataFrame
#    """



def check_cntm(df, cntm, cntmrad, key_x="x", key_y="y"):
    """
    Check amount of contamination in df.
 
    Parameters
    ----------
    df : pandas.DataFrame
      object to be checked whether comtaminated
    cntm : array-like
      sources of contamination
    cntmrad : array-like
      distance threshold of the same object identification

    Return
    ------
    df : pandas.DataFrame
      dataframe or empty DataFrame
    """

    # Return empty dataframe
    if df.empty:
        return df

    idx_cntm_all, n_cntm_all, flux_cntm_all = [], [], []

    # Remove empty DataFrame
    cntm = [x for x in cntm if not x.empty]

    for idx, df_cntm in enumerate(cntm):
      #print(f"    Search contamination from {idx}-th cntm") 
      x_cntm, y_cntm = df_cntm[key_x], df_cntm[key_y]
      tree_cntm = KDTree(list(zip(x_cntm, y_cntm)), leafsize=10)

      idx_cntm_list, n_cntm_list, flux_cntm_list = [], [], []
      # Resister comtaninated object, amount of contaminated flux
      for idx_obj, row in df.iterrows():
        # Search whether any contamination exists in df
        res = tree_cntm.query_ball_point((row[key_x], row[key_y]), cntmrad[idx])
        N_cntm = len(res)
        if N_cntm == 0:
          #print("      Not contaminated.")
          pass
        if N_cntm > 0:
          flux_cntm = 0
          for idx_cntm in res:
             flux_cntm += df_cntm.loc[idx_cntm]["flux"]
          idx_cntm_list.append(idx_obj)
          n_cntm_list.append(N_cntm)
          flux_cntm_list.append(flux_cntm)
          #print(f"      Contaminated as n={N_cntm}, idx={idx_cntm}, flux={flux_cntm}")
          pass

      idx_cntm_all.append(idx_cntm_list)
      n_cntm_all.append(n_cntm_list)
      flux_cntm_all.append(flux_cntm_list)
    
    #print(f"  idx_cntm_all : {idx_cntm_all}")
    #print(f"  n_cntm_all : {n_cntm_all}")
    #print(f"  flux_cntm_all : {flux_cntm_all}")

    # Sum up all contaminations using idx_cntm
    for n in range(len(cntm)):
      idx_cntm_temp = idx_cntm_all[n]
      n_cntm_temp = n_cntm_all[n]
      flux_cntm_temp = flux_cntm_all[n]

      # Resister in df["cntmn"] df["cntmflux"] if df do not have columns
      col_df = df.columns.tolist()
      if ("cntmn" in col_df) and ("cntmflux" in col_df):
        pass
      else:
        df["cntmn"] = 0
        df["cntmflux"] = 0
      
      for idx, n, flux in zip(idx_cntm_temp, n_cntm_temp, flux_cntm_temp):
        df.loc[idx, "cntmn"] += n
        df.loc[idx, "cntmflux"] += flux

    # Contamination fraction
    # If df["flux"] == 0, set 0
    s_flux_ok = df["flux"] > 0
    df.loc[s_flux_ok, "cntmfrac"] = df[s_flux_ok]["cntmflux"]/df[s_flux_ok]["flux"]
    df.loc[~s_flux_ok, "cntmfrac"] = 0
    return df


def self_cntm(df, cntmrad, key_x="x", key_y="y"):
    """
    Check amount of contamination in df itself.
 
    Parameters
    ----------
    df : pandas.DataFrame
      object to be checked whether comtaminated
    cntmrad : float
      distance threshold of the same object identification

    Return
    ------
    df : pandas.DataFrame
      dataframe with objects added the eflag==256
    """

    # Return empty dataframe
    if df.empty:
        return df

    assert ~np.isnan(cntmrad), "NaN cntmrad!"

    idx_cntm_all, n_cntm_all, flux_cntm_all = [], [], []

    #print(f"    Search contamination inside the df") 
    x_cntm, y_cntm = df[key_x], df[key_y]
    tree_cntm = KDTree(list(zip(x_cntm, y_cntm)), leafsize=10)

    idx_cntm_list, n_cntm_list, flux_cntm_list = [], [], []
    # Resister comtaninated object, amount of contaminated flux
    for idx_obj, row in df.iterrows():
      # Search whether any contamination exists in df
      res = tree_cntm.query_ball_point((row[key_x], row[key_y]), cntmrad)

      # Remove index of the object itself
      res.remove(idx_obj)
      
      # Threshold to identify itself in pix
      rad_self = 0.001
      res_self = tree_cntm.query_ball_point((row[key_x], row[key_y]), rad_self)
      # Remove index of bad identified objects
      # (e.g., due to the mistake of barycenter determination).
      # Remove removed idx_obj
      res_self.remove(idx_obj)
      for i in res_self:
        res.remove(i)
        print(f"   Remove self object {i}====================================")

      N_cntm = len(res)

      if N_cntm == 0:
          #print("      Not contaminated. (only the same object is detected)")
          pass
      if N_cntm > 0:
        flux_cntm = 0
        for idx_cntm in res:
           flux_cntm += df.loc[idx_cntm]["flux"]
        idx_cntm_list.append(idx_obj)
        n_cntm_list.append(N_cntm)
        flux_cntm_list.append(flux_cntm)
        #print(f"      Contaminated as n={N_cntm}, idx={idx_cntm}, flux={flux_cntm}")
        pass

    # Resister in df["cntm_n"] df["cntm_flux"] if df do not have columns
    col_df = df.columns.tolist()
    if ("cntmn" in col_df) and ("cntmflux" in col_df):
      pass
    else:
      df["cntmn"] = 0
      df["cntmflux"] = 0
    
    for idx, n, flux in zip(idx_cntm_list, n_cntm_list, flux_cntm_list):
      df.loc[idx, "cntmn"] += n
      df.loc[idx, "cntmflux"] += flux

    # Contamination fraction
    # If df["flux"] == 0, set 0
    s_flux_ok = df["flux"] > 0
    df.loc[s_flux_ok, "cntmfrac"] = df[s_flux_ok]["cntmflux"]/df[s_flux_ok]["flux"]
    df.loc[~s_flux_ok, "cntmfrac"] = 0

    return df


def check_mask(df, df_mask, radius_th, d_th, fr_th):
  """
  Check the objects in df are whether in mask (df_mask).
 
  Parameters
  ----------
  df : pandas.DataFrame
    dataframe with objects
  df_mask : pandas.DataFrame
    dataframe with masks
  radius_th : float
    threshold of mask detection
  d_th : float
    threshold of the same object identification
  fr_th : float
    threshold of the same object identification

  Return
  ------
  df : pandas.DataFrame
    dataframe with objects added the eflag==256
  """
  #print(f"Columns of df_mask: {df_mask.columns.tolist()}")

  masks = list(zip(df_mask["x"], df_mask["y"]))
  tree = KDTree(masks, leafsize=10)
  # Distance threshold is 2*radius
  r_th = 2*radius_th

  # If target is detected near other objects (d < r_th),
  # eflag==256.
  # Or, target and sep detection mask are not close (d > d_th),
  # eflag==256.
  
  # Mask should have flux and ab (a/b)
  key_flux = "flux"

  for idx, row in df.iterrows():
    # Search masks whose distance < radius
    x_obj, y_obj = row["x"], row["y"]
    res = tree.query_ball_point((x_obj, y_obj), r_th)
    
    # Number of close detections
    df.loc[idx, "N_closedet"] = len(res)

    # This should not happen
    if len(res)==0:
      pass
    # Only one objects are detected (often catalog object itself)
    elif len(res)==1:
      idx_mask = res[0]
      x_mask, y_mask = masks[idx_mask]
      d = ((x_mask-x_obj)**2 + (y_mask-y_obj)**2)**0.5
      
      # Add flux ratio (ideally close to the unity)
      flux_mask = df_mask.loc[idx_mask, key_flux]
      df.loc[idx, "fr"] = flux_mask/df.loc[idx, key_flux]

      # Do not add eflag to the same obj
      if d < d_th:


        # Temporally! 2022-04-14
        df_mask['ab'] = 2
        if df_mask.loc[idx_mask, "ab"] > 1.5:
          df.loc[idx, "eflag"] = 128
        else:
          # Invalid ! --2022-02-07T12:21 (JST)
          # df["fr"] = 1
          pass

      # Add eflag when the to the same obj
      else:
        #print("Objects in mask detected! Set eflag=256.")
        df.loc[idx, "eflag"] = 256

    # Add eflag when the more than two objects are detected in mask
    # and the flux ratio flux_ref/flux_obj > fr_th 
    else:
      #print("Objects in mask detected! Set eflag=512.")
      df.loc[idx, "eflag"] = 512
      flux_mask = 0
      for idx_mask in res:
        flux_mask += df_mask.loc[idx_mask, key_flux]
      df.loc[idx, "fr"] = flux_mask/df.loc[idx, key_flux]


  return df


def nowstr():
  """Return time in string.
  """
  now = datetime.datetime.now()
  now = datetime.datetime.strftime(now, "%Y%m%dT%H%M%S")
  return now


def get_filename(filepath):
  """
  Get filename from the filepath.

  Parameter
  ---------
  filepath : str
    arbitraty string
  
  Return
  ------
  filename : str
    filename of the filepath
  """
  return filepath.split("/")[-1].split(".")[0]


def adderr(*args):
  """Calculate additional error.

  Parameters
  ----------
  args : array-like
    list of values

  Return
  ------
  err : float
    calculated error
  """
  return np.sqrt(np.sum(np.square(args)))


def adderr_series(*args):
  """Add error of multiple pandas.Series.

  Parameters
  ----------
  args : array-like
    list of pandas.Series 

  Return
  ------
  err_s : pandas.Series
    single pandas.Series of calculated error
  """ 
  for i,x in enumerate(args):
    assert type(x) is pd.core.frame.Series, "Input should be Series."
    #assert type(x)==type(pd.Series()), "Sould be Series"
    if i==0:
      temp = x.map(np.square)
    else:
      temp += x.map(np.square)
  err_s = temp.map(np.sqrt)
  return err_s


def diverr(val1, err1, val2, err2):
  """Calculate error for division.
  
  Parameters
  ----------
  val1 : float or pandas.Series 
    value 1
  err1 : float or pandas.Series 
    error 1
  val2 : float or pandas.Series 
    value 2
  err2 : float or pandas.Series 
    error 2
  """
  return np.sqrt((err1/val2)**2 + (err2*val1/val2**2)**2)


def mulerr(val1, err1, val2, err2):
   return np.sqrt((val2*err1)**2 + (val1*err2)**2)


def log10err(val, err):
  """Calculate log10 error.

  Parameter
  ---------
  val : float
    value of the parameter
  err : float
    error of the parameter

  Return
  ------
  errlog10 : float 
    calculated log10 error
  """
  errlog10 = err/val/np.log(10)
  return errlog10


def calc_xy(df, w, kwd_ra="raMean", kwd_dec="decMean"):
  """
  Calculate pixel x and y from ra and dec.

  Parameters
  ----------
  df : pandas.DataFrame
    input DataFrame which should have ra and dec
  w : astropy.wcs.WCS
    wcs information
  kwd_ra, kwd_dec : str, optional
    keyword of ra, dec in input df (optimaized for PS catalog)

  Return
  ------
  df : pandas.DataFrame
    output DataFrame which have x and y
  """

  x_list, y_list = [], []
  for idx, row in df.iterrows():
    x, y  = w.all_world2pix(row[kwd_ra], row[kwd_dec], 0)
    x_list.append(x)
    y_list.append(y)
  df_coo = pd.DataFrame(dict(x=x_list, y=y_list), dtype=float)
  df_con = pd.concat([df, df_coo], axis=1)
  return df_con


def calc_radec(df, w):
  """Calculate ra and dec using wcs information.

  Parameters
  ----------
  df : pandas.DataFrame
    input DataFrame which should have x and y
  w : astropy.wcs.WCS
    wcs information

  Return
  ------
  df : pandas.DataFrame
    output DataFrame which have ra and dec
  """
  ra_list, dec_list = [], []
  for idx, row in df.iterrows():
    cra, cdec  = w.all_pix2world(row["x"], row["y"], 0)
    ra_list.append(cra)
    dec_list.append(cdec)
  
  df["ra"] = ra_list
  df["dec"] = dec_list
  return df
 

def remove_xy(df, nx, ny):
  """Remove objects close to edge.

  Parameters
  ----------
  df : pandas.DataFrame
    input DataFrame which contains x, y
  nx, ny : int
    number of pixels

  Return
  ------
  df : pandas.DataFrame
    close objects removed DataFrame
  """

  # Remove objects in edge region.
  df = df[(df["x"] > 0) & (df["x"] < nx)]
  df = df[(df["y"] > 0) & (df["y"] < ny)]
  df = df.reset_index(drop=True)
  return df


def remove_edge(df, radius, nx, ny):
  """Remove objects close to edge.

  Parameters
  ----------
  df : pandas.DataFrame
    input DataFrame which contains x, y
  radius : float
    removal threshold in pixel
  nx, ny : int
    number of pixels

  Return
  ------
  df : pandas.DataFrame
    close objects removed DataFrame
  """

  # Remove objects in edge region.
  df = df[(df["x"] > radius) & (df["x"] < (nx - radius))]
  df = df[(df["y"] > radius) & (df["y"] < (ny - radius))]
  df = df.reset_index(drop=True)
  return df


def remove_close(df, radius):
  """Remove close objects.

  Parameters
  ----------
  df : pandas.DataFrame
    input DataFrame which contains x, y
  radius : float
    removal threshold in pixel

  Return
  ------
  df : pandas.DataFrame
    close objects removed DataFrame
  """

  # Remove close objects.
  data = list(zip(df["x"], df["y"]))
  tree = KDTree(data, leafsize=10)
  res = tree.query_pairs(radius)
  idx_rm = [elem for inner in res for elem in inner]
  idx_rm = list(set(idx_rm))
  return df.drop(index=df.index[idx_rm])


def modify_ref(df_ref, key_objID):
  """
  Modify reference dataframe to 1 liner style using objID.
  Extract flux and fluxerr of each objects.
  [objID_ra, objID_dec, objID_flux, objID_fluxerr ...]

  Note for future update
  ----------------------
  Do not use _ in any column name like x_hoge other than objID

  """
  col_use = ["flux", "fluxerr", "eflag", "x", "y", "xwin", "ywin",
             "cntmn", "cntmflux", "cntmfrac",
             "objinfoFlag", "qualityFlag", 
             "raMean", "decMean", "raMeanErr", "decMeanErr",
             "nStackDetections", "nDetections",
             "gQfPerfect", "gMeanPSFMag", "gMeanPSFMagErr", 
             "rQfPerfect", "rMeanPSFMag", "rMeanPSFMagErr", 
             "iQfPerfect", "iMeanPSFMag", "iMeanPSFMagErr", 
             "zQfPerfect", "zMeanPSFMag", "zMeanPSFMagErr", 
             "yQfPerfect", "yMeanPSFMag", "yMeanPSFMagErr", ]
  
  # Check no "_" in col_use
  bad_col = [x for x in col_use if "_" in x]
  assert len(bad_col)==0, "Check and remove '_' in any column name!"

  df_ref["objID"] = df_ref["objID"].astype(str)

  temp_list = []
  for idx,row in df_ref.iterrows():
    objID = row[key_objID]
    s_temp = df_ref.loc[idx, col_use]
    df_temp = pd.DataFrame([s_temp])
    df_temp = df_temp.add_suffix(f"_{objID}")
    df_temp = df_temp.reset_index(drop=True)
    temp_list.append(df_temp)
  df_mod = pd.concat(temp_list, axis=1)
  return df_mod 


def concat_allband(res_allband, bands):
  """
  Concatenate multi-bands DataFrame 
  and remove common raws (catalog ra, dec, magnitude etc.)

  Parameters
  ----------
  res_allband : list
    all bands photometry result
  bands : list
    all bands used as suffix like ["g", "r", "i"]
  """
  res_all = []
  for df,band in zip(res_allband,bands):
    df_temp = df.add_suffix(f"_{band}")
    res_all.append(df_temp)
  df = pd.concat(res_all, axis=1)
  return df


def extract_obj(df_all, bands):
  """Extract object data.

  Parameters
  ----------
  df_all : pandas.DataFrame
    all photometry result
  bands : list
    3 band used as suffix like ["g", "r", "i"]

  Return
  ------
  df_obj : pandas.DataFrame
    object DataFrame
  """

  # 18 is objID length (Pan-STARRS catalog)
  columns = df_all.columns.tolist()
  col_obj = [col for col in columns if (len(col)<18)]
  df_obj = df_all[col_obj]
  return df_obj


def time_keeper(func):
  """Decorator to measure time.
  """
  # To take over docstring etc.
  @wraps(func)
  def wrapper(*args, **kargs):
    t0 = time.time()
    result = func(*args, **kargs)
    t1 = time.time()
    t_elapse = t1 - t0
    print(f"[time keeper] t_elapse = {t_elapse:.03f} s (func :{func.__name__})")
    return result
  return wrapper


## Orbit prediction start =====================================================
class Orbit:
  """
  Time should be in days.
  Ra and Dec should be in degree.
  """
  def __init__(self, **kargs):
    self.t = kargs["t"]
    self.ra = kargs["ra"]
    self.dec = kargs["dec"]
    self.f_ra = interpolate.interp1d(
            self.t, self.ra, kind='linear', fill_value='extrapolate')
    self.f_dec = interpolate.interp1d(
            self.t, self.dec, kind='linear', fill_value='extrapolate')


  def calcloc(self, obstime):
    # ra, dec in degree
    return self.f_ra(obstime), self.f_dec(obstime)


def obtain_objorb(hdr_kwd, flist, fitsdir, slist):
    """
    Predict orbit of moving object by fitting.
    The function is for 2-d fits and optimized for murikabushi and TriCCS.

    Parameters
    ----------
    hdr_kwd : dict
        header keyword
    flist : str
        path of fits file list
    fitsdir : str
        path of fits file directory
    slist : str
        path of standard file (x, y, nfits, nframe) 

    Return
    ------
    orb : neoorb class
        neo orbit
    """

    t_standard, ra_standard, dec_standard = [], [], []
    x_s, y_s, nframe = [], [], []
    
    df = pd.read_csv(slist, sep=" ")
    col = df.columns.tolist()
    print(col)

    # Use 'fits' 
    if "fits" in col:
        x_s = df.x.tolist()
        y_s = df.y.tolist()
        fits_s = df.fits.tolist()
        # Obtain nframe from fits name
        nframe = []
        with open(flist, "r") as f:
            lines = f.readlines()
            fitslist = [x.split("\n")[0] for x in lines]
            for x in fits_s:
                idx = fitslist.index(x)
                # +1 is important
                nframe.append(idx+1)
    # Use 'nframe' 
    else:
        x_s = df.x.tolist()
        y_s = df.y.tolist()
        nframe = df.nfits.tolist()

    with open(flist, "r") as f:
        lines = f.readlines()
        for (x,y,i) in zip(x_s,y_s,nframe):
              
            tfits = lines[i-1].split("\n")[0]
            tfits = os.path.join(fitsdir, tfits)
            src = fits.open(tfits)[0]
            hdr = src.header
              
            if hdr_kwd["datetime"]:
              exp_start = hdr[hdr_kwd["datetime"]]
            else:
              exp_start = f"{hdr[hdr_kwd['date']]}T{hdr[hdr_kwd['time']]}"

            exp_frame = hdr[hdr_kwd["exp"]]
            obs_start = datetime.datetime.strptime(
              exp_start, "%Y-%m-%dT%H:%M:%S.%f")
            obs_center = obs_start + datetime.timedelta(seconds=exp_frame/2.0)
            obs_center = datetime.datetime.strftime(
              obs_center, "%Y-%m-%dT%H:%M:%S.%f")
            t = Time(str(obs_center), format='isot', scale='utc')
            t = t.mjd
            w = wcs(tfits)
            cra, cdec  = w.all_pix2world(x, y, 0)
            t_standard.append(t)
            ra_standard.append(cra)
            dec_standard.append(cdec)

    # For only one detection
    if len(t_standard) == 1:
        t_standard.append(t+1e-9)
        ra_standard.append(cra+1e-9)
        dec_standard.append(cdec+1e-9)
        
    orb = Orbit(t=t_standard, ra=ra_standard, dec=dec_standard)
    return orb


## Orbit prediction end =======================================================

## PSF related stuff
def extract1darr(image, x, y, theta, width):
    """
    Extract y fixed array.
    
    Parameters
    ----------
    image : array-like
        2-d image
    x, y : float
        x and y of the object
    theta : float
       theta = 0 for x extraction, theta = 90 for y extraction
       0 < theta < 180
    width : int
        width of extraction
    
    Return
    ------
    pix : array-like
        pixel value
    val : array-like
        value of the pixel
    """
    assert 0 <= theta <= 180, "Check the value of theta."

    # x direction array (y fixed)
    # Arrays in fitting
    xmin, xmax = x - width, x + width
    ymin, ymax = y - width, y + width
    pix = np.arange(int(xmin), int(xmax), 1)
    
    if theta == 0:
        # y fixed
        val = image[int(y):int(y)+1, int(xmin):int(xmax)][0]
    if theta == 90:
        # y fixed
        val = image[int(ymin):int(ymax)+1, int(x):int(x)][0]

    return pix, val


def fit_psf(x, y, yerr=None, fittype="Gauss", oneside=False):
    """
    Fit and return parameter of psf.

    Parameters
    ----------
    x, y : array-like
        1-d array
    yerr : array-like
        1-d array for uncertainty
    fittype : str
        Gauss or Moffat
    oneside : bool
        Set True for radial profile fitting

    Return
    ------
    param : array-like
        fitting parameters
    """
     
    if fittype == "Gauss":
        if oneside:
            func_fit = gauss_fit_oneside
            # Initial values
            p0 = [np.max(y), 1]
        else:
            func_fit = gauss_fit
            # Initial values
            p0 = [np.max(y), np.mean(x), 1]
    elif fittype == "Moffat":
        if oneside:
            func_fit = moffat_fit_oneside
            p0 = [np.max(y), 1, 2]
        else:
            func_fit = moffat_fit
            p0 = [np.max(y), np.mean(x), 1, 2]
    
    param, cov = curve_fit(func_fit, x, y, sigma=yerr, p0=p0)
    return param


def radial_profile(image, center, gain):
    """
    Create radial prifile from the center with certain width.

    Parameters
    ----------
    image : 2d array-like
        2d array
    center : tuple
        x and y coordinates
    gain : float
        inverse gain e/AUD

    Return
    ------
    y : array-like
        radial profile
    yerr : array-like
        error of y
    """

    # Create grid
    x, y   = np.indices((image.shape))
    nx, ny = image.shape
    print(f"Shape of cut image: {nx} x {ny}")
    c_x, c_y =  center
    print(f"Object center ({c_x}, {c_y})")
    # Calculate distance
    r = np.sqrt((x - c_x)**2 + (y - c_y)**2)
    r = r.astype(int)
    tbin = np.bincount(r.ravel(), image.ravel())
    # ex. nr=3 (number of pixel in the range), tbin=4000 (total in the bin),
    #     -> nr = 4000/3
    nr = np.bincount(r.ravel())
    y = tbin / nr
    # Poisson error for elevtron, not AUD
    # F (ADU) * gain (e/AUD) -> F (e)
    # Ferr (e) = (F (e) )**0.5
    # Ferr (ADU) = Ferr (e) / gain (e/ADU)
    yerr = [np.sqrt(abs(v)*gain)/gain for v in y]
    return y, yerr


def add_aspect_data(df, loc, suffix=None, step="1m"):
    """
    Add aspect data with JPL/Horizons.

    Parameters
    ----------
    df : pandas.core.series.DataFrame
        pandas.core.series.DataFrame of the object
    loc : str
        location of the observatory (MPC code)
    suffix : str, optional
        suffix (band etc.) for time keywords (ex. "g")

    Return
    ------
    df : pandas.core.series.DataFrame
        pandas.core.series.DataFrame with aspect data
    """
    if suffix:
      suffix = f"_{suffix}"
    else:
      suffix = ""

    # Extract initial info.
    obj = df.at[0, "obj"]
    t0  = df.at[0, f"t_utc{suffix}"]
    # For 1 detection
    if len(df)==1:
        t0_dt = datetime.datetime.strptime(t0, "%Y-%m-%dT%H:%M:%S.%f")
        t1_dt = t0_dt + datetime.timedelta(minutes=1)
        t1 = datetime.datetime.strftime(t1_dt, "%Y-%m-%dT%H:%M:%S.%f")
    else:
        t1  = df.at[len(df)-1, f"t_utc{suffix}"]
    #print(f"  obj, t0, t1 = {obj}, {t0}, {t1}")

    # Create template in minute
    # time format :
    #   2004-11-13T04:10:44.918

    # For unnumbered objects
    try:
        objid = f"{obj[0:4]} {obj[4:]}"
        jpl = Horizons(id=objid, location=loc,
            epochs={'start':t0, 'stop':t1, 'step':step})
    # Numbered objects
    except:
        objid = obj
        jpl = Horizons(id=objid, location=loc,
            epochs={'start':t0, 'stop':t1, 'step':step})
            # To speed up for test
            #epochs={'start':t0, 'stop':t1, 'step':"1h"})
    print(f"  objid used in add_aspect data: {objid}")
    
    t0_dt = datetime.datetime.strptime(t0, "%Y-%m-%dT%H:%M:%S.%f")
    t1_dt = datetime.datetime.strptime(t1, "%Y-%m-%dT%H:%M:%S.%f")
    dt_dt = t1_dt - t0_dt
    dt_sec = dt_dt.total_seconds()
    # Check observation arc
    #if dt_sec < 3600*24:
    #  print(f"    Arc (t0-t1): {dt_sec} sec")
    #else:
    #  print(f"    Arc (t0-t1): {dt_dt.days} day and {dt_dt.seconds} sec")


    # Note: arc/step should smaller than 90024
    eph = jpl.ephemerides()
    # If observations are shorter than 1 min, use the same info.
    if len(eph)==1:
        df["r"]      = eph[0]["r"]
        df["delta"]  = eph[0]["delta"]
        df["alpha"]  = eph[0]["alpha"]
        df["PABLon"] = eph[0]["PABLon"]
        df["PABLat"] = eph[0]["PABLat"]

    else:
        ## time in jd
        jpl_t_jd = list(eph["datetime_jd"])
        jpl_alpha = list(eph["alpha"])
        # from JPL : 
        #     Apparent range ("delta", light-time aberrated) 
        #     and range-rate ("delta-dot") of the target center relative 
        #     to the observer.
        jpl_r = list(eph["r"])
        jpl_delta = list(eph["delta"])
        jpl_PABLon = list(eph["PABLon"])
        jpl_PABLat = list(eph["PABLat"])

        # Interpolate template to observational results
        f_r = interpolate.interp1d(
          jpl_t_jd, jpl_r, 
          kind='linear', fill_value="extrapolate")
        f_delta = interpolate.interp1d(
          jpl_t_jd, jpl_delta, 
          kind='linear', fill_value="extrapolate")
        f_alpha = interpolate.interp1d(
          jpl_t_jd, jpl_alpha, 
          kind='linear', fill_value="extrapolate")
        f_PABLon = interpolate.interp1d(
          jpl_t_jd, jpl_PABLon, 
          kind='linear', fill_value="extrapolate")
        f_PABLat = interpolate.interp1d(
          jpl_t_jd, jpl_PABLat, 
          kind='linear', fill_value="extrapolate")
        
        # Obtain aspect data with t_jd
        df["r"]      = df[f"t_jd{suffix}"].map(f_r)
        df["delta"]  = df[f"t_jd{suffix}"].map(f_delta)
        df["alpha"]  = df[f"t_jd{suffix}"].map(f_alpha)
        df["PABLon"] = df[f"t_jd{suffix}"].map(f_PABLon)
        df["PABLat"] = df[f"t_jd{suffix}"].map(f_PABLat)

     
    # Light-traveling time
    # Light speed 3e5 km/s
    c = 3e5
    # Astronomical unit
    au = 1*u.au
    # in km
    au2km = au.to("km").value

    # Calculation is consistent with https://alcdef.org/docs/ALCDEF_Standard.pdf
    df[f"lt_sec{suffix}"] = df["delta"]*au2km/c
    sec2day = 24.*3600.
    df[f"lt_day{suffix}"] = df["delta"]*au2km/c/sec2day
    return df


def time_correction(df, suffix=None):
    """
    Do time correction using light traveling time (lt_sec and lt_day).

    Parameters
    ----------
    df : pandas.core.series.DataFrame
        pandas.core.series.DataFrame of the object
    suffix : str, optional
        suffix (band etc.) for time keywords

    Return
    ------
    df : pandas.core.series.DataFrame
        light traveing time corrected data 
    """
    if suffix:
        suffix = f"_{suffix}"
    else:
        suffix = ""
    
    # Useless?
    try:
        df[f"t_sec_ltcor{suffix}"] = (
            df[f"t_sec_obs{suffix}"] - df[f"lt_sec{suffix}"]
            )
        print("  Time correction was done (t_sec -> t_sec_ltcor)")
    except:
        print("  Time correction failed   (t_sec -> t_sec_ltcor)")
        pass

    try:
        df[f"t_jd_ltcor{suffix}"] = df[f"t_jd{suffix}"] - df[f"lt_day{suffix}"]
        print("  Time correction was done (t_jd -> t_jd_ltcor)")
    except:
        print("  Time correction failed   (t_jd -> t_jd_ltcor)")
        pass

    return df


def mag_correction(df, key_mag="mag", suffix=None):
    """
    Do brightness correction using heliocentric distance (r) and 
    geocentric distance (delta).

    Parameters
    ----------
    df : pandas.core.series.DataFrame
        pandas.core.series.DataFrame of the object
    key_mag : str
        keyword for magnitude
    suffix : str, optional
        suffix (band etc.) for time keywords

    Return
    ------
    df : pandas.core.series.DataFrame
        pandas.core.series.DataFrame after correction
    """
    if suffix:
        suffix = f"_{suffix}"
    else:
        suffix = ""

    # Calculate a reduced magnitude at a phase angle of alpha
    df[f"mag_red{suffix}"] = (
        df[f"{key_mag}{suffix}"] - 5*np.log10(df["r"]*df["delta"])
        )
    return df
