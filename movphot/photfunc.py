#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Color photmetry main functions
"""
import pandas as pd
import numpy as np
from scipy.ndimage import median_filter
from skimage.morphology import binary_dilation, disk
#from sklearn.cluster import DBSCAN as dbscan_engine
from scipy.stats import sigmaclip
import os
import sep
import astropy.io.fits as fits
from astropy.wcs import WCS as wcs
import astropy.units as u

from .common import calc_xy, calc_radec, remove_close, remove_edge


kernel_7x7 = np.array([
 [1., 3., 5., 6., 5., 3., 1.],
 [3., 7.,13.,15.,13., 7., 3.],
 [5.,13.,21.,25.,21.,13., 5.],
 [6.,15.,25.,30.,25.,15., 6.],
 [5.,13.,21.,25.,21.,13., 5.],
 [3., 7.,13.,15.,13., 7., 3.],
 [1., 3., 5., 6., 5., 3., 1.],
])


def calc_FWHM_fits(infits, radius0):
    # Open fits
    # fits source  (HDU0, header + image data)
    src = fits.open(infits)[0]
    
    # Use bright stars
    minarea_det = 5
    sigma_det = 10
    src.data = src.data.byteswap().newbyteorder()
    # src.data are bgsubtracted !
    data_bgsub, bginfo =  remove_background2d(src.data, mask=None)

    objects = sep.extract(
        data_bgsub, sigma_det, err=bginfo["rms"], minarea=minarea_det)

    # Radius should be large enough to obtain total flux
    # wpos_param: constant to convert `0.5*FWHM` to `sigma` (FWHM = 2.35*sigma) 
    # 0.5*FWHM*wpos_param = 0.5*FWHM*2/2.35 = sigma
    wpos_param  = 2.0/2.35
    # Half flux radius
    frad_frac   = 0.5
    frad_subpix = 5
    frad_ratio  = 5.0
    flux, fluxerr, eflag = sep.sum_circle(
        data_bgsub, objects["x"], objects["y"], r=radius0)
    radius0_arr = np.full_like(objects["x"], radius0)
    # Not bgsub ?
    #r, flag = sep.flux_radius(
    #  src.data, objects["x"], objects["y"], radius0_arr, frad_frac,
    #  normflux=flux, subpix=frad_subpix)

    r, flag = sep.flux_radius(
        data_bgsub, objects["x"], objects["y"], radius0_arr, frad_frac,
        normflux=flux, subpix=frad_subpix)


    # r (0.5*FWHM) to FWHM (see above)
    fwhm = 2*r

    # 3-sigmaclip
    sigma = 3
    fwhm, _, _ = sigmaclip(fwhm, sigma, sigma)
    fwhm_mean, fwhm_std = np.mean(fwhm), np.std(fwhm)

    return fwhm_mean, fwhm_std


def calc_FWHM(f, fdir, radius0):
    """
    Calculate FWHM of the PSF using bright objects.

    Parameters
    ----------
    f : array-like
      fits list
    fdir : str
      fits directory
    radius0 : str
      first try photometry radius in pix
    
    Returns
    -------
    df : pandas.DataFrame
      nframe, fwhm, fwhmerr, and radius
    """
    
    fwhm_list, fwhmerr_list = [], []
    nframe_list = []
    for idx, line in enumerate(f):
        # Open fits
        infits = line.split("\n")[0]
        infits = os.path.join(fdir, infits)
        # fits source  (HDU0, header + image data)
        src = fits.open(infits)[0]
        
        # Use bright stars
        minarea_det = 5
        sigma_det = 10
        src.data = src.data.byteswap().newbyteorder()
        # src.data are bgsubtracted !
        data_bgsub, bginfo =  remove_background2d(src.data, mask=None)

        objects = sep.extract(
            data_bgsub, sigma_det, err=bginfo["rms"], minarea=minarea_det)

        # Radius should be large enough to obtain total flux
        # wpos_param: constant to convert `0.5*FWHM` to `sigma` (FWHM = 2.35*sigma) 
        # 0.5*FWHM*wpos_param = 0.5*FWHM*2/2.35 = sigma
        wpos_param  = 2.0/2.35
        # Half flux radius
        frad_frac   = 0.5
        frad_subpix = 5
        frad_ratio  = 5.0
        flux, fluxerr, eflag = sep.sum_circle(
            data_bgsub, objects["x"], objects["y"], r=radius0)
        radius0_arr = np.full_like(objects["x"], radius0)
        # Not bgsub ?
        #r, flag = sep.flux_radius(
        #  src.data, objects["x"], objects["y"], radius0_arr, frad_frac,
        #  normflux=flux, subpix=frad_subpix)

        r, flag = sep.flux_radius(
            data_bgsub, objects["x"], objects["y"], radius0_arr, frad_frac,
            normflux=flux, subpix=frad_subpix)


        # r (0.5*FWHM) to FWHM (see above)
        fwhm = 2*r

        # 3-sigmaclip
        sigma = 3
        fwhm, _, _ = sigmaclip(fwhm, sigma, sigma)
        fwhm_mean, fwhm_std = np.mean(fwhm), np.std(fwhm)

        nframe_list.append(idx+1)
        fwhm_list.append(np.round(fwhm_mean, 2 ))
        fwhmerr_list.append(fwhm_std)
        print(f"  FWHM, FWHMerr = {fwhm_mean:.2f}, {fwhm_std:.2f}")

        df = pd.DataFrame(
            dict(nframe=nframe_list, fwhm=fwhm_list, fwhmerr=fwhmerr_list))
    df["radius"] = radius0
    return df


def triccsgain(gain_kwd):
    """
    Obtain inverse gain [e-/ADU] from header keyword

    Parameter
    ---------
    gain_kwd : str
        header keyword about gain ("x4" etc.)
    
    Return
    ------
    gain : float
        inverse gain in e-/ADU
    """
    gain_kwd = gain_kwd.strip()
    dic = dict(x32=0.19, x16=0.38, x8=0.76, x4=1.5, x2=3, x1=6)
    return dic[gain_kwd]


def bandcheck(catalog, band):
    """
    Check whether the band in catalog.

    Parameters
    ----------
    catalog : str
      catalog of reference stars 
    band : str
      band

    Return
    ------
    boolian
      Whether exists or not

    """
    if catalog=="gaia":
      bands = ["G"]
    elif catalog=="ps":
      bands = ["u", "g", "r", "i", "z"]
    elif catalog=="sdss":
      bands = ["g", "r", "i", "z", "y"]
    elif catalog=="usnob":
      bands = ["R", "I"]
    
    if band in bands:
      return True
    else:
      return False


def search_param(w, nx, ny):
  """
  Obtain catalog search parameters ra, dec, and fov radius.

  Parameters
  ----------
  w : astropy.wcs.WCS
    wcs information of the fits
  nx, ny : float
    number of pixels 

  Returns 
  -------
  cra, cdec : float
    ra and dec of field of view center
  fovradius : float
    radius of field of view
  """

  # Ra and Dec of center 
  cra, cdec  = w.all_pix2world(nx/2., ny/2., 0)
  # Width of search region.
  edge_1 =  w.all_pix2world(0, 0, 0)
  edge_2 =  w.all_pix2world(nx, ny, 0)
  width_ra = abs(edge_1[0]-edge_2[0])/2.
  width_dec = abs(edge_1[1]-edge_2[1])/2.
  fovradius = max(width_ra, width_dec)
  return cra, cdec, fovradius


def create_saturation_mask(
    image, satu_count, r_dilation=10.0):
    """ 
    Create a 2D saturation mask image.

    Parameters
    ----------
    image : array-like
        input image
    satu_count : float
        saturation count
    r_dilation : float
        dilation radius
    """
    minimum_detection_area = 1.0
    mask_radius            = 15.0

    mask = np.zeros(image.shape, dtype=np.bool_)
    obj = sep.extract(
        image, satu_count, filter_kernel=kernel_7x7, 
        minarea=minimum_detection_area, err=None, mask=None)
    sep.mask_ellipse(
        mask, obj['x'], obj['y'],
        obj['a'], obj['b'], obj['theta'], r=mask_radius)
    mask = binary_dilation(mask, disk(r_dilation))
    return mask


def create_badpixel_mask(
    inst, band, nx, ny):
    """ 
    Create a 2D bad pixel mask.

    Parameters
    ----------
    inst : str
        instrument
    band : str
        filter
    nx, ny : int
        shape of image
    """
    mask = np.zeros((ny, nx), dtype=np.bool_)
    # TriCCS i-band 
    if (inst == "TriCCS") and (band=="i"):
        mask[241, 263] = 1
    if (inst == "TriCCS") and (band=="z"):
        mask[241, 263] = 1
    return mask


def remove_background2d(image, mask, bw=128, fw=3):
    """
    Remove background from 2D FITS

    Parameters
    ----------
    image : array-like
        input image to be background subtracted
    mask : array-like
        mask array 
    bw : int
        box width 
    fw : int
        filter width 

    Returns
    -------
    image : array like
        background subtracted image
    bg_info : dict
        background info.
    """
    bkg = sep.Background(
        image, mask=mask, bw=bw, bh=bw, fw=fw, fh=fw)
    image_bg = bkg.back()
    image_bgsub = image - image_bg

    bg_global = bkg.globalback
    bg_rms = bkg.globalrms
    bg_info = {'level': bg_global, 'rms': bg_rms}
    return image_bgsub, bg_info


def obtain_winpos(data, x, y, radius, nx, ny):
    """ 
    Obtain windowed centroid xwin and ywin.
    Note: x and y should be numpy.ndarray

    Parameters
    ----------
    data : numpy.ndarray
      2-d image data
    x, y : numpy.ndarray
      location(s) of object(s)
    radius : float
      scale related to the area where searched the centroid
    """

    # Radius should be large enough to obtain total flux
    # wpos_param: constant to convert `0.5*FWHM` to `sigma` (FWHM = 2.35*sigma) 
    # 0.5*FWHM*wpos_param = 0.5*FWHM*2/2.35 = sigma
    wpos_param  = 2.0/2.35
    # Half flux radius
    frad_frac   = 0.5
    frad_subpix = 5
    frad_ratio  = 5.0

    # Do photometry to obtain all flux
    # Note1: err and gain are needless for flux estimation
    # Note2: Sky background should be subtracted (?) 

    # Must need
    flux,fluxerr,eflag = sep.sum_circle(data, x, y, r=radius)

    # Use only objects with nonzero eflag (?)
    # Create array like [radius, radius, ... , radius]
    # Note: radius is not used in flux_radius when normflux is used (?)
    # r : flux radius, i.e., `0.5*fwhm!`
    radius = np.full_like(x, radius)
    r, flag = sep.flux_radius(
        data, x, y, radius, frad_frac, normflux=flux, subpix=frad_subpix)
    # r (0.5*FWHM) to sigma (see above)
    sigma      = wpos_param*r
    sigma_mean = np.mean(sigma)
    sigma_std  = np.std(sigma)
    #print(f"  N={len(sigma)}, calculated in obtain_winpos")
    #print(f"  sigma: {sigma_mean:.1f}+-{sigma_std:.1f}")
    #print(f"  FWHM : {2.35*sigma_mean:.1f} (sigma times 2.35)")

    # wflag is always 0 if mask=None
    # Search winpos with estimated sigma

    # Search narrow region
    # sigma = 0.5*sigma
    # Dramatically works bad for faint objects
    xwin, ywin, wflag = sep.winpos(data, x, y, sigma)

    
    # If the differences of coordinates are larger than ratio_diff*radius,
    # for objects more than ratio_obj*N_obj,
    # print a warning message.
    # original values as xwin and ywin with eflag_win = 1
    ratio_diff = 0.3
    ratio_obj  = 0.5
    diff = np.sqrt((xwin-x)**2 + (ywin-y)**2)
    # 1 for large diff, 0 for small diff
    flag = np.where(diff > ratio_diff*radius, 1, 0)
    ratio_large_diff= np.sum(flag)/flag.size 
    if ratio_large_diff > ratio_obj:
        print(f"      Large winpos correct ratio detected :{ratio_large_diff:.1f}")
        print(f"      This is just a caution. Please check wcs information etc.")

    # Insert original value when xwin and ywin are outside of FoV
    xwin = [x if (x < nx) and (x > 0) and (y < ny) and (y > 0) else x0 for x,y,x0 in zip(xwin,ywin,x)]
    ywin = [y if (x < nx) and (x > 0) and (y < ny) and (y > 0) else y0 for x,y,y0 in zip(xwin,ywin,y)]

    return xwin, ywin, flag


def photloc_xy(
    image, df, radius, gain, err, mask=None, key_x="xwin", key_y="ywin"):
    """
    Photometry using x and y.
    Save original x/y as x0/y0 and used x/y as x1/y1.
    Create pandas.DataFrame which contains both 
    catalog magnitude and photometric result.

    Parameters
    ----------
    image : array-like
        input data
    df : pandas.DataFrame
        input DataFrame
    radius : float
        aperture radius
    gain : float
        inverse gain in e-/ADU
    err : float
        typical photometric error in ADU
    mask : array-like, optional
        boolian mask
    key_x, key_y : str
        keywords for x and y


    Return
    ------
    df : pandas.DataFrame
      result DataFrame
    """

    ny, nx = image.shape
    
    # Photometry without annulus 
    bkgann = None
    flux, fluxerr, eflag = sep.sum_circle(
        image, df[key_x], df[key_y], r=radius, 
        err=err, mask=mask, gain=gain, bkgann=bkgann)
    if len(flux)==1:
        x, y = df[key_x][0], df[key_y][0]
        print(f"      x, y, flux, fluxerr, eflag = {x:.1f}, {y:.1f}, {flux}, {fluxerr}, {eflag}")

    # If mask is not None, objects in masked pixel have flux/fluxerr==nan
    # Zero padding 
    flux = [0 if np.isnan(x) else x for x in flux]
    fluxerr = [0 if np.isnan(x) else x for x in fluxerr]

    # Add results to the DataFrame
    df.loc[:, "flux"]    = flux
    df.loc[:, "fluxerr"] = fluxerr
    df.loc[:, "eflag"]   = eflag
    # Add constant value
    df.loc[:, "radius"]  = radius
    df = df.reset_index(drop=True)
    return df


def photloc_xy_ann(
    image, df, radius, ann_gap, ann_width, gain, mask=None, 
    key_x="xwin", key_y="ywin"):
    """
    Photometry using x and y.
    Save original x/y as x0/y0 and updated x/y as x1/y1.
    Create pandas.DataFrame which contains both 
    catalog magnitude and photometric result.

    Parameters
    ----------
    image : array-like
        input data
    df : pandas.DataFrame
        input DataFrame
    radius : float
        aperture radius
    ann_gap : float
        gap between aperture and annulus
    ann_width : float
        width of annulus
    gain : float
        inverse gain in e-/ADU
    mask : array-like, optional
        boolian mask
    key_x, key_y : str
      keywords for x and y

    Return
    ------
    df : pandas.DataFrame
        photometric result
    """
    ny, nx = image.shape
    
    # Photometry with annulus 
    # Note: Background should be not subtracted 
    #       to estimate background noise with "high accuracy". 
    #       If after background subtraction, noise may be underestimated.
    
    # Original number of target
    N0 = len(df)
    # Define annulus
    ann_in  = radius + ann_gap
    ann_out = radius + ann_gap + ann_width
    print("Define aperture parameters:")
    print(f"  rad, ann_in, ann_out = {radius:.1f}, {ann_in:.1f}, {ann_out:.1f}")

    # Do not use edge regions to avoid `ZeroDivisionError: float division` 
    # (see below)
    # We use 3*ann_out as threshold since sometimes ann_out is not enough.
    # A constant threshold is easy to understand ?
    df = df[
        (3*ann_out < df[key_x]) & (df[key_x] < nx-3*ann_out) 
        & (3*ann_out < df[key_y]) & (df[key_y] < ny-3*ann_out)]
    # Just to aboid error arise from Chained Indexing
    df = df.copy()
    # Number of target
    N1 = len(df)
    if N1 < N0:
        print(f"    Remove objects close to the edge to avoid ZeroDivisionError. ")
        print(f"      N0, N1 = {N0}, {N1}")

    #  Photmetry area is related to a background noise.
    #  Large annulus and small area is better, but difficult in real reality.
    #  (due to such as contaminations of sources.)
    # Aperture area
    S_app = np.pi*radius**2
    # Annulus
    S_ann = np.pi*(ann_out**2 - ann_in**2)
    #  A characteric index is annulus : area = 1 : 1 (?)
    print("Apperture and annulus size:")
    print(f"  (S_app, S_ann) = {S_app:.1f}, {S_ann:.1f} in pix")

    bkgann  = (ann_in, ann_out)
    # Set error per pixel to 0 since local background error is calculated
    # in sep.sum_circle.
    # Read sep document p16:
    #  Pixels in the background annulus are not subsampled and any masked 
    #  pixels in the annulus are completely igored rather than corrected. 
    #  The inner and outer radii can also be arrays. The error in the 
    #  background is included in the reported error.
    err = None

    # When there is a bright object around the target,
    # sometimes "ZeroDivisionError: float division" happens.
    # To avoid the error, use try and except here.
    try:

        # 1. Estimate of background 'noise' using global image:
        bkg = sep.Background(image)
        bgerr_pix = bkg.globalrms

        # 2. Background error in aperture with local background subtraction with gain = None
        #    i.e., reported error is simply (apreture area)**2 x bkgerr_pix
        #    Note: If you use sum_circle w/ bkgann, the reported error is 
        #          sum of (apreture area)**2 x bkgerr_pix and some error 
        #          even when gain is None......?
        #          I mean the function as below is inappropriate.
        #            flux, fluxerr, eflag = sep.sum_circle(
        #                image, df[key_x], df[key_y], r=radius, 
        #                err=err, mask=mask, gain=gain, bkgann=bkgann)
        #          It seems that the Poission error of the mean background is 
        #          added if we use the function above. 
        #          However, in some cases, this is not appropriate.
        #          For example, when we are using mean-stacked (not summed) images,
        #          the Poisson error of the mean background does not represent
        #          typical background error. It works for single image, I think.
        _, fluxerr, _ = sep.sum_circle(
            image, df[key_x], df[key_y], r=radius, err=bgerr_pix, gain=None)

        # 3. Aperture flux with local background subtraction with gain = None
        flux, _, _ = sep.sum_circle(
            image, df[key_x], df[key_y], r=radius, err=bgerr_pix, 
            bkgann=bkgann, gain=None)

        # A. Background levels in annuli (not used)
        # fann, _, _ = sep.sum_circann(
        #    image, df[key_x], df[key_y], ann_in, ann_out, err=None, gain=None)
        # fann = float(fann)
        
        # 4. Add poisson error of the target by hand
        Perr_target = [(f*gain)**0.5/gain for f in flux]
        fluxerr = [(fe**2 + Pe**2)**0.5 for (fe, Pe) in zip(fluxerr, Perr_target)]

    except ZeroDivisionError as e:
        print(
            f"Zero division error possibly due to there is a bright "
            f"object around the target in annulus photometry.")
        # For photometry of the target
        if len(df)==1:
            flux    = [0]
            fluxerr = [0]
            eflag   = [1]
        # For photometry of catalog stars
        else:
        # If len(df) > 1 (i.e., for reference stars), not implemented.
            assert False, "Not implemented."

    # If mask is not None, objects in masked pixel have flux/fluxerr==nan
    # Zero padding to avoid this
    flux    = [0 if np.isnan(x) else x for x in flux]
    fluxerr = [0 if np.isnan(x) else x for x in fluxerr]
    # Here I assume that possible contaminations are removed by visual inspections.
    # i.e., this function is mainly for single object. Otherwise do not fix eflag == 0!
    eflag_fix = 0

    print(f"Photometry done: N={len(flux)}")
    if len(flux)==1:
        x, y = df[key_x][0], df[key_y][0]
        f, fe = flux[0], fluxerr[0]
        snr = f/fe
        print(f"  x, y,              = {x:.1f}, {y:.1f}")
        print(f"  flux, fluxerr, S/N = {f:.2f}, {fe:.2f}, {snr:.1f}")
    else:
        pass

    # Add results to the DataFrame
    df.loc[:, "flux"]    = flux
    df.loc[:, "fluxerr"] = fluxerr
    df.loc[:, "eflag"]   = eflag_fix
    # Add constant value
    df.loc[:, "radius"]  = radius
    df = df.reset_index(drop=True)
    return df


# Check =======================================================================
sum_keywords  = 'npix','tnpix','cflux','flux'
max_keywords  = 'cpeak','peak','xmax','ymax'
min_keywords  = 'xmin','ymin'
peak_keywords = 'xcpeak','ycpeak','xpeak','ypeak'
or_keywords   = 'flag'
from functools import reduce
# Check =======================================================================


def merge_objects(objects, segmap, epsilon, min_samples=2):
  ''' Merge separately-detected objects by DBSCAN

  Paramters:
    objects (ndarray): structured array generated by SExtractor
    segmap (ndarray): segmentation map generated by SExtractor
    epsilon (float): range parameter for clustering
    min_samples (int): minimum cluster size

  Returns:
    objects (ndarray): array where separated objects are merged
    segmap (ndarray): updated segmentation map
  '''
  #engine = dbscan_engine(eps=epsilon, min_samples=min_samples)
  xy = np.array((objects['x'],objects['y'])).copy().T
  clusters = engine.fit(xy)
  labels = clusters.labels_
  merged,done,newmap = list(), list(), np.zeros_like(segmap)
  for n,idx in enumerate(labels):
    obj = objects[n]
    if idx in done:
      continue
    if idx != -1:
      done.append(idx)
      objs = objects[idx==labels]
      for key in obj.dtype.names:
        if key in sum_keywords:
          obj[key] = objs[key].sum()
        elif key in max_keywords:
          obj[key] = objs[key].max()
        elif key in min_keywords:
          obj[key] = objs[key].min()
        elif key in peak_keywords:
          obj[key] = objs[key][objs['peak'].argmax()]
        elif key in or_keywords:
          obj[key] = reduce(np.bitwise_or, objs[key].astype('int'))
        else:
          obj[key] = objs[key].mean()
          tmpmap = reduce(np.logical_or,
              [segmap==(s+1) for s in np.argwhere(labels==idx)])
    else:
      tmpmap = segmap==(n+1)
    merged.append(obj.copy())
    newmap += (len(merged))*tmpmap
  return np.array(merged),newmap


def sum_isophot(image,segmap,r,err,mask=None,gain=None):
  ''' Measure source fluxes by isophotal photometry
  Return fluxerr=0 for minus flux data.

  Parameters:
    image (ndarray): image to be measured
    segmap (ndarray): segmentation map generated by SExtractor
    r (int): disk radius for mask dilation
    err (float): standard error per pixel
    mask (ndarray): bad pixel mask
    gain (float): sensor gain (e-/ADU)

  Returns:
    flux (ndarray): measured flux
    fluxerr (ndarray): uncertainty
    isomap (ndarray): area map in photometary
  '''
  ny,nx = image.shape
  nz = segmap.max()
  isomap = np.zeros((nz,ny,nx))
  flux,fluxerr = np.zeros(nz),np.zeros(nz)
  for n in range(nz):
    isomask = binary_dilation(segmap==(n+1), disk(r))
    if mask is not None: isomask = isomask & (~mask)
    flux[n] = image[isomask].sum()
    fluxerr[n] = err*np.sqrt(isomask.sum())
    if gain: 
      if flux[n] <= 0:
        fluxerr[n] = -1
      else:   
        fluxerr[n] += np.sqrt(flux[n]/gain)
    isomap[n] = isomask
  return flux,fluxerr,isomap



def photiso(
  image, stddev, sigma=3, r_disk=2, epsilon=20, minarea=3, gain=None):
  """
  Parameters
  ----------
  image : array-like
    2-d image
  stddev : float
    typical flux standard deviation
    use stddev times sigma for extraction
  sigma : int
    extraction threshold in  object-merging process
    use stddev times sigma for extraction
  r_disk : float
    disk size of isophotal photometry in pixel
  epsilon : int
    range parameter in object-merging process in pixel
  minarea : int
    minimum area in object-merging process in pixel
  gain : float, optional
    inverse gain in e-/ADU

  Return
  ------
  df : pandas.DataFrame
    resuls DataFrame
  objects : sep.extract
    extracted objects 
  isomap : array-like
    area map in photometary
  """
  
  # radius for aperture photometry in pixel
  radius = 10
  
  # set segmentation_map=True to create a segmentation map
  objects, segmap = sep.extract(
    image, sigma, err=stddev,
    minarea=minarea, filter_kernel=kernel_7x7,
    segmentation_map=True)

  # merge separated objects by DBSCAN
  objects, segmap = merge_objects(objects, segmap, epsilon)

  # measure flux and flux error by circle apertures
  aper_flux, aper_fluxerr, flag = sep.sum_circle(
    image, x=objects['x'], y=objects['y'], r=radius, err=stddev, gain=gain)

  # measure flux and flux error by isophotal apertures
  iso_flux, iso_fluxerr, isomap = sum_isophot(
    image, segmap, r=r_disk, err=stddev, gain=gain)
  
  df = pd.DataFrame({
      "x": objects["x"], "y": objects["y"], 
      "iso_flux": iso_flux, "iso_fluxerr": iso_fluxerr, 
      "aper_flux": aper_flux, "aper_fluxerr": aper_fluxerr,
      })
  return df, objects, isomap


def merge_photres_xy(df_phot, df_obj, radius=10):
  """Merge photometory result and object(s) using x,y.

  Parameters
  ----------
  df_phot : pandas.DataFrame
    input DataFrame which should have x and y
  df_obj : pandas.DataFrame
    input DataFrame which should have x and y
  radius : float
    matching radius in x,y 

  Return
  ------
  df : pandas.DataFrame
    merged DataFrame
  """
  res = []
  for idx,row_phot in df_phot.iterrows():
    df_temp_obj = (
      df_obj[((row_phot["x"]-df_obj["x"])**2 
      + (row_phot["y"]-df_obj["y"])**2)**0.5 < radius])
    if len(df_temp_obj)==1:
      df_temp_obj["flux"] = row_phot["iso_flux"]
      df_temp_obj["fluxerr"] = row_phot["iso_fluxerr"]
      df_temp_obj["x1"] = row_phot["x"]
      df_temp_obj["y1"] = row_phot["y"]
      # Check
      df_temp_obj["eflag"] = 0
      res.append(df_temp_obj)
    else:
      pass
  
  if len(res)!=0:
    df = pd.concat(res, axis=0)
    df = df.reset_index(drop=False)
    return df
  else:
    # Return empty DataFrame
    return pd.DataFrame()


def appphot_loc(
    image, hdr, df_obj, err, mask, hdr_kwd, radius, key_x="x", key_y="y"):
      
    """Do photometry for specific object.

    Parameters
    ----------
    src : str
        target fits
    df_obj : 
        object dataframe with x, y or xwin, ywin
    x,y : float
        photometry coordinates
    err : float
        background rms
    mask : array-like
        photometry mask
    hdr_kwd : dict
        header keyword
    radius : float
        aperture radius in pixel

    Return
    ------
    df_obj : pandas.DataFrame
        photometory result DataFrame
    """

    # Obtain gain value.
    gain = hdr[hdr_kwd["gain"]]
    if hdr_kwd["gain"]=="GAINCNFG":
        # Convert str(ex. x4) to float value(1.5)
        gain = triccsgain(gain)

    # Do photometry using x and y.  (add flux and fluxerr to the DataFrame)
    df_obj = photloc_xy(
        image, df_obj, radius, gain, err, mask, key_x=key_x, key_y=key_y)
    df_obj = df_obj.reset_index(drop=True)
    return df_obj


def annphot_loc(
    image, hdr, df_obj, mask, hdr_kwd, radius, ann_gap, ann_width, 
    key_x="x", key_y="y"):
      
    """Do photometry for specific object.

    Parameters
    ----------
    src : str
        target fits
    df_obj : 
        object dataframe with x, y or xwin, ywin
    x,y : float
        photometry coordinates
    err : float
        background rms
    mask : array-like
        photometry mask
    hdr_kwd : dict
        header keyword
    radius : float
        aperture radius in pixel
    ann_gap : float
        gap between aperture and annulus
    ann_width : float
        width of annulus

    Return
    ------
    df_obj : pandas.DataFrame
        photometory result DataFrame
    """

    # Obtain gain value.
    gain = hdr[hdr_kwd["gain"]]
    if hdr_kwd["gain"]=="GAINCNFG":
        # Convert str(ex. x4) to float value(1.5)
        gain = triccsgain(gain)

    # Do photometry using x and y.  (add flux and fluxerr to the DataFrame)
    df_obj = photloc_xy_ann(
        image, df_obj, radius, ann_gap, ann_width, 
        gain, mask, key_x=key_x, key_y=key_y)

    df_obj = df_obj.reset_index(drop=True)
    return df_obj


def isophot_loc(
    src, err, hdr_kwd, orb, radius):
  """Do isophotla photometry for specific object.

  Parameters
  ----------
  src : str
    target fits
  err : float
    background rms
  hdr_kwd : dict
    header keyword
  orb : neoorb class
    neo orbit
  radius : float
    aperture radius in pixel
  ann : bool, optional
    whether use annulus to estimate background noise of the source

  Return
  ------
  df_obj : pandas.DataFrame
    photometory result DataFrame
  return df
  """
  image = src.data.byteswap().newbyteorder()
  hdr = src.header
  w = wcs(header=hdr)

  # Obtain gain value.
  gain = hdr[hdr_kwd["gain"]]
  if hdr_kwd["gain"]=="GAINCNFG":
    # Convert str(ex. x4) to float value(1.5)
    gain = triccsgain(gain)

  # Create DataFrame
  df_obj = pd.DataFrame(dict(x=x, y=y))
  # Round values
  df_obj = df_obj.round(1)

  # Do isophotal photometry (add flux and fluxerr to the DataFrame)
  ## Extract and do photometry of all objects.
  df_iso, objects, isomap = photiso(
    image, err, sigma, r_disk, epsilon, minarea, gain)
  ## Merge df_iso (isophot result) and df_obj (object) using x, y.
  df = merge_photres_xy(df_iso, df_obj, radius=10)


  df_obj = df_obj.reset_index(drop=True)
  return df_obj


def appphot_catalog(
    image, hdr, df_cat, err, mask, hdr_kwd, radius, key_x="xwin", key_y="ywin"):
    """
    Do aperture photometry of stars in catalog.
    Assume all data are already background subtracted
    and not mask saturated stars.

    If ann==True, annulus is used to estimate background noise.

    Parameters
    ----------
    src : HDU object
      HDU object of target fits
    df_cat : pandas.DataFrame
      catalog DataFrame
    err : float or array-like
      standard deviation of sky background
    mask : array-like
      mask array when photometry
    hdr_kwd : dict
      header keyword
    radius : float
      photometry radius in pixel
    key_x, key_y : str
      keywords for x and y

    Return
    ------
    df_cat : pandas.DataFrame
        not in edge photometry result added DataFrame
    """
    
    w = wcs(header=hdr)
    ny, nx = image.shape
    
    gain = hdr[hdr_kwd["gain"]]
    if hdr_kwd["gain"]=="GAINCNFG":
        # Convert str(ex. x4) to float value(1.5)
        gain = triccsgain(gain)

    # Do photometry using x and y.  (add flux and fluxerr to the DataFrame)
    # Use annulus when ann=True
    df_cat = photloc_xy(
        image, df_cat, radius, gain, err, mask, key_x=key_x, key_y=key_y)
    df_cat = df_cat.reset_index(drop=True)
    return df_cat


def annphot_catalog(
    image, hdr, df_cat, mask, hdr_kwd, radius, ann_gap, ann_width, 
    key_x="xwin", key_y="ywin"):
    """
    Do aperture photometry of stars in catalog.
    Assume all data are already background subtracted
    and not mask saturated stars.

    If ann==True, annulus is used to estimate background noise.

    Parameters
    ----------
    src : HDU object
        HDU object of target fits
    df_cat : pandas.DataFrame
        catalog DataFrame
    mask : array-like
        mask array when photometry
    hdr_kwd : dict
        header keyword
    radius : float
        photometry radius in pixel
    ann_gap : float
        gap between aperture and annulus
    ann_width : float
        width of annulus
    key_x, key_y : str
        keywords for x and y

    Return
    ------
    df_cat : pandas.DataFrame
        not in edge photometry result added DataFrame
    """
    
    w = wcs(header=hdr)
    ny, nx = image.shape
    
    gain = hdr[hdr_kwd["gain"]]
    if hdr_kwd["gain"]=="GAINCNFG":
        # Convert str(ex. x4) to float value(1.5)
        gain = triccsgain(gain)

    # Do photometry using x and y.  (add flux and fluxerr to the DataFrame)
    # Use annulus when ann=True
    df_cat = photloc_xy_ann(
        image, df_cat, radius, ann_gap, ann_width, 
        gain, mask, key_x=key_x, key_y=key_y)
    df_cat = df_cat.reset_index(drop=True)
    return df_cat


def isophot_catalog(
    src, df_cat, err, mask, hdr_kwd, r_disk, epsilon, minarea, sigma):
  """Do photometry for reference stars.

  Define header keyword for murikabushi.
  Do not mask saturated stars.

  Parameters
  ----------
  src : str
    target fits
  err :
    ho
  band : str
    g, r, i or z band of target fits
  magmin, magmax : float
    magnitude to be searched
  r_disk : float
    disk size of isophotal photometry in pixel
  epsilon : float
    range parameter in object-merging process')
  minarea : int
    minimum area in object-merging process in pixel
  sigma : int
    extraction threshold in  object-merging process
    use stddev times sigma for extraction

  Return
  ------
  df_cat : pandas.DataFrame
    photometry result added DataFrame
  """

  image = src.data.byteswap().newbyteorder()
  hdr = src.header
  w = wcs(header=hdr)
  ny, nx = image.shape


  gain = hdr[hdr_kwd["gain"]]
  if hdr_kwd["gain"]=="GAINCNFG":
    # Convert str(ex. x4) to float value(1.5)
    gain = triccsgain(gain)



  # Center of cordinate and radius of catalog search region.
  cra, cdec, fovradius = search_param(w, nx, ny)

  # apply median filter before exract (drop cosmic rays and bad pixels)
  image = median_filter(image, size=3)

  # Do isophotal photometry (add flux and fluxerr to the DataFrame)
  ## Extract and do photometry of all objects.
  df_iso, objects, isomap = photiso(
    image, err, sigma, r_disk, epsilon, minarea, gain)
  ## calculate objects radec from x, y
  df_iso = calc_radec(df_iso, w)
  ## Merge df_iso (isophot result) and df_cat (catalog stars) using x, y.
  # Check
  r_merge = 20
  df = merge_photres_xy(df_iso, df_cat, radius=r_merge)
  if len(df)==0:
    # Return a empty DataFrame
    return pd.DataFrame()

  # Remove close objects and objects in edge region.
  # Check
  r_close = 50
  df = remove_close(df, r_close)
  df = df.reset_index(drop=True)
  if len(df)==0:
    # Return a empty DataFrame
    return pd.DataFrame()

  return df


def calc_time_shift(shape, x, y):
    """
    Calculate time-shift from the (1,1) pixel for Tomo-e.
    (Only for 2 fps Tomo-e reduced data)

    Parameters
    ----------
    shape : array-like
        (nx,ny) of the image
    x : float
        NAXIS1 coordinate of the target
    y : float
        NAXIS2 coordinate of the target

    Return
    -------
    ts : float
        the timeshift value in units of second
    """
    nx, ny = shape
    if nx==2000 and ny==1128:
        ts = (930+2160+1000+np.floor(nx/4)*25)*np.floor(y/4)*100e-9
    else:
        ts = (930+2160+975+np.floor(nx/4)*25)*np.floor(y/4)*100e-9
    return ts
