#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
For preprocess afer photometry.
Extract good data from DataFrame.
"""
import os
import sys
import numpy as np
import pandas as pd
from scipy import stats
from scipy.stats import norm
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
# For colormap
from matplotlib.colors import LogNorm
from matplotlib import cm

from calcerror import round_error

from .psdb import check_objinfo, check_objfilter
from .common import (
    adderr, adderr_series, diverr, mulerr, log10err, 
    mycolor, mymark, myls, linear_fit, add_color_reverse, checknan,
    band4CTG)
from .visualization import myfigure_refladder


def lighten_photres(df, phrase):
    """
    Remove useless columns.

    Parameters
    ----------
    df : pandas.DataFrame
        photometry result
    phrase : array-like
        removed phrases
    """
    
    # Remove by phrases
    col_use = df.columns.tolist()
    print(f"N_col0 = {len(col_use)}")
    for ph in phrase:
        col_use = [x for x in col_use if not ph in x]
        print(f"N_col  = {len(col_use)}")
    df = df[col_use]

    return df


def gaussian(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))


def calc_rescolor(
    df, objID, band, bands, CTG, CTGerr, df_CTI,
    Nobj_min, eflag_max, dedge_min, cntmfrac_max, verbose=False):
    """
    Calculate color residuals of reference stars with fixed CTG and CTI.
      'g_r_res' and 'g_r_reserr' etc. are saved in output dataframe.


    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    band : str
        band
    bands : array-likes
        list of bands
    CTG : float
        CTG
    CTGerr : float
        uncertainty of CTG
    df_CTI : float
        DataFrame with g_r_CTI_MC and g_r_CTIerr_MC etc.
    Nobj_min : int
        minimum threshold of number of object in a frame
        (not contaminated, not on edge)
    dedge_min : int
        acceptable distance to edge region
    cntmfrac_max : int
        acceptable contamination fraction
    ax_raw, ax_wei : axis
       axes for raw fit and weighted fit, respectively
    key_x, key_y : str
        keywords of x and y
    verbose : bool
        whether output messages

    Return
    ------
    df_res : pandas.DataFrame
        DataFrame with color residuals of reference stars
   """

    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)
    
    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    
    res_list, reserr_list = [], []
    # List of index to identify object
    # List of objID to identify object
    idx_list, objID_list = [], []
    
    for idx_row, row in df.iterrows():
        col_inst_list, col_insterr_list = [], []
        col_cat_list, col_caterr_list   = [], []

        # CTI and err of a frame
        CTI = df_CTI.at[idx_row, f"{band_l}_{band_r}_CTI_MC"]
        CTIerr = df_CTI.at[idx_row, f"{band_l}_{band_r}_CTIerr_MC"]

        # Use flux, mag_cat, mag_cat_l, mag_cat_r (+ errors), and eflag
        for obj in objID:
            eflag_l    = row[f"eflag_{obj}_{band_l}"]
            eflag_r    = row[f"eflag_{obj}_{band_r}"]
            dedge_l    = row[f"dedge_{obj}_{band_l}"]
            dedge_r    = row[f"dedge_{obj}_{band_r}"]
            cntmfrac_l = row[f"cntmfrac_{obj}_{band_l}"]
            cntmfrac_r = row[f"cntmfrac_{obj}_{band_r}"]
            
            # Good condition
            flux_l      = row[f"flux_{obj}_{band_l}"]
            flux_r      = row[f"flux_{obj}_{band_r}"]
            if ((dedge_l > dedge_min) 
                & (dedge_r > dedge_min) 
                & (cntmfrac_l < cntmfrac_max)
                & (cntmfrac_r < cntmfrac_max)
                & (eflag_l < eflag_max) 
                & (flux_l > 0) 
                & (flux_r > 0) 
              and (eflag_r < eflag_max)):

                flux_l      = row[f"flux_{obj}_{band_l}"]
                flux_r      = row[f"flux_{obj}_{band_r}"]
                fluxerr_l   = row[f"fluxerr_{obj}_{band_l}"]
                fluxerr_r   = row[f"fluxerr_{obj}_{band_r}"]
                catmag_l    = row[f"{band_l}MeanPSFMag_{obj}_{band_l}"]
                catmag_r    = row[f"{band_r}MeanPSFMag_{obj}_{band_r}"]
                catmagerr_l = row[f"{band_l}MeanPSFMagErr_{obj}_{band_l}"]
                catmagerr_r = row[f"{band_r}MeanPSFMagErr_{obj}_{band_r}"]
                
                if (flux_r < 0) or (flux_l < 0):
                    print("Negative flux detected.")
                    print(f"    flux_l, flux_r = {flux_l}, {flux_r}")
                    sys.exit()

                # col_inst (mag_l - mag_r)
                col_inst = -2.5*np.log10(flux_l/flux_r)
                instmagerr_l = 2.5*log10err(flux_l, fluxerr_l)
                instmagerr_r = 2.5*log10err(flux_r, fluxerr_r)
                col_insterr = adderr(instmagerr_l, instmagerr_r)
                ##col_inst_list.append(col_inst)
                ##col_insterr_list.append(col_insterr)

                # col_cat (mag_l - mag_r)
                col_cat = catmag_l - catmag_r
                col_caterr = adderr(catmagerr_l, catmagerr_r)
                ##col_cat_list.append(col_cat)
                ##col_caterr_list.append(col_caterr)


                # Residual
                res = col_cat - (CTG*col_inst + CTI)
                # Uncertainty of CTG*col_inst
                err2 = mulerr(
                    CTG, CTGerr,
                    col_inst, col_insterr)
                reserr = adderr(col_caterr, err2, CTIerr)
                
                res_list.append(res)
                reserr_list.append(reserr)
                # To identify objects
                idx_list.append(idx)
                objID_list.append(obj)

        # If res is not estimated due to the lack of reference stars,
        # add 0 to the dataframe to keep the length of DataFrame
        if len(res_list)==0:
            res_list.append(0)
            reserr_list.append(0)
            idx_list.append(0)
            objID_list.append(0)
            print(
              f"    {band}-band, {idx_row+1}-th frame res is not estimated and "
              f"skip the frame."
            )
            continue
        
    
    # Create dataframe of a frame
    # Save res and reserr as well
    df_res = pd.DataFrame(
      {f"{band_l}_{band_r}_res"   : res_list,
      f"{band_l}_{band_r}_reserr"   : reserr_list,
      f"{band_l}_{band_r}_idx"   : idx_list,
      f"{band_l}_{band_r}_objID"   : objID_list})
    return df_res


def plot_res(
    df_res, band, bands, ax, norm=False, mag=False, same=False, verbose=False):
    """
    Plot residuals of color/magnitude.

    # ToDo
    ------
    not fixed keywords

    Parameters
    ----------
    df_res : list
        DataFrames of residuals color/magnitude of reference stars
    band : str
        band
    bands : array-likes
        list of bands
    ax : axis
       ax
    norm : bool
        whether normalized with error
    mag : bool
        whether use magnitude, not color
    same : bool
        whether use the same color for all objects
    verbose : bool
        whether output messages
    """
    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)

    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    col_p = mycolor[idx]
    
    # For mag
    if mag:
        key_res    = f"{band}_res"
        key_reserr = f"{band}_reserr"
        key_objID  = f"{band}_objID"
    # For Color
    else:
        key_res    = f"{band_l}_{band_r}_res"
        key_reserr = f"{band_l}_{band_r}_reserr"
        key_objID  = f"{band_l}_{band_r}_objID"

    # Do not use value == 0 (which are added to keep the length of data)
    df_res = df_res[df_res[key_res]!=0]
    df_bad = df_res[df_res[key_res]==0]

    # Plot histogram and gaussian fitting curve
    # data, bins, range
    bins0 = 100
    
    if norm:
        data = [x/sigma for x,sigma in zip(df_res[key_res], df_res[key_reserr])]
    else:
        data = df_res[key_res]

    # Use the same color for all objects
    if same:

        xmin, xmax = np.min(data), np.max(data)
        data_range = (xmin, xmax)
        # Use -3 to 3 sigma
        #data_range = [-3, 3]
        width_data = xmax-xmin

        hist, bins = np.histogram(data, bins0, data_range)
        bins = bins[:-1]
        # ?
        param_ini = [len(data)/2, 0, 1]
        popt, pcov = curve_fit(gaussian, bins, hist, p0=param_ini)
        fitting = gaussian(bins, popt[0], popt[1], popt[2])

        # KS test with mu and sigma
        normed_data=(data-popt[1])/popt[2]
        D, p_value = stats.kstest(normed_data, "norm")

        if mag:
            label_fit = (
                f"{band} (A, mu, sigma) = "
                f"({popt[0]:.3f}, {popt[1]:.3f}, {popt[2]:.3f})\n"
                f"(KS statistic, p) = ({D:.3f},{p_value:.3f})")
        else:
            label_fit = (
                f"{band_l}-{band_r} (A, mu, sigma) = "
                f"({popt[0]:.3f}, {popt[1]:.3f}, {popt[2]:.3f})\n"
                f"(KS statistic, p) = ({D:.3f},{p_value:.3f})")
        ax.bar(
            bins, hist, width=width_data/len(bins), 
            alpha=0.5, color=col_p, align='edge')
        ax.plot(
            bins, fitting, 'k', label=label_fit)

    # Use different colors for all objects (use objID)
    else:

        xmin, xmax = np.min(data), np.max(data)
        data_range = (xmin, xmax)
        # Use -3 to 3 sigma
        #data_range = [-3, 3]
        width_data = xmax-xmin
        
        objID = list(set(df_res[key_objID].values.tolist()))
        print(f"  N_obj: {len(objID)}")
        for obj in objID:
            res    = df_res[df_res[key_objID]==obj][key_res].values.tolist()
            reserr = df_res[df_res[key_objID]==obj][key_reserr].values.tolist()

            if norm:
                data = [x/sigma for x,sigma in zip(res, reserr)]
            else:
                data = res
            print(f"  {obj} : N_obj={len(data)}")
            hist, bins = np.histogram(data, bins0, data_range)
            bins = bins[:-1]
            # ?
            param_ini = [len(data)/2, 0, 1]
            try:
                popt, pcov = curve_fit(gaussian, bins, hist, p0=param_ini)
                fitting = gaussian(bins, popt[0], popt[1], popt[2])

                # KS test with mu and sigma
                normed_data=(data-popt[1])/popt[2]
                D, p_value = stats.kstest(normed_data, "norm")

                if mag:
                    label_fit = (
                        f"{band} (A, mu, sigma) = "
                        f"({popt[0]:.3f}, {popt[1]:.3f}, {popt[2]:.3f})\n"
                        f"(KS statistic, p) = ({D:.3f},{p_value:.3f})")
                else:
                    label_fit = (
                        f"{band_l}-{band_r} (A, mu, sigma) = "
                        f"({popt[0]:.3f}, {popt[1]:.3f}, {popt[2]:.3f})\n"
                        f"(KS statistic, p) = ({D:.3f},{p_value:.3f})")
                ax.plot(
                    bins, fitting, 'k', label=label_fit)
            except:
                print("Failed gaissian fitting and skip to fit.")
            ax.bar(
                bins, hist, width=width_data/len(bins), 
                alpha=0.5, color=col_p, align='edge')

    ax.legend(fontsize=10)


def calc_CT(
        df, objID, band, bands, Nobj_min, eflag_max, dedge_min, cntmfrac_max,
        target=None, verbose=False):
    """
    Calculate CT.

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    band : str
        band
    bands : array-likes
        list of bands
    Nobj_min : int
         minimum threshold of number of object in a frame
         (not contaminated, not on edge)
    dedge_min : int
        acceptable distance to edge region
    cntmfrac_max : int
        acceptable contamination fraction
    target : array-like, optional
       target info 
       [min(cat0-cat1), max(cat0-cat1), min(cat0-inst0), max(cat0-inst0)]
    verbose : book
       output messages


    Return
    ------
    CT_df_list : list
        list of DataFrames of photometry results of reference stars
    """

    band_l, band_r = band4CT(band, bands)

    for idx,x in enumerate(bands):
        if x == band:
            break

    #assert band==band_l, "Check the code!!"

    # Output target info
    if target is not None:
        x0_t, x1_t, y0_t, y1_t = target
             

    column = df.columns.tolist()
    CT_df_list = []
    for idx_row, row in df.iterrows():
        cat_sub_inst_list    = []
        cat_sub_insterr_list = []
        col_cat_list         = []
        col_caterr_list      = []
        t_sec_list           = []
        y_list               = []
        # Use flux, mag_cat, mag_cat_l, mag_cat_r (+ errors), and eflag
        for obj in objID:
            eflag_l    = row[f"eflag_{obj}_{band_l}"]
            eflag_r    = row[f"eflag_{obj}_{band_r}"]
            dedge_l    = row[f"dedge_{obj}_{band_l}"]
            dedge_r    = row[f"dedge_{obj}_{band_r}"]
            cntmfrac_l = row[f"cntmfrac_{obj}_{band_l}"]
            cntmfrac_r = row[f"cntmfrac_{obj}_{band_r}"]

            # Good condition
            flux_l      = row[f"flux_{obj}_{band_l}"]
            flux_r      = row[f"flux_{obj}_{band_r}"]
            if ((dedge_l > dedge_min) 
                & (dedge_r > dedge_min) 
                & (cntmfrac_l < cntmfrac_max)
                & (cntmfrac_r < cntmfrac_max)
                & (eflag_l < eflag_max) 
                & (flux_l > 0) 
                & (flux_r > 0) 
              and (eflag_r < eflag_max)):

                flux_l      = row[f"flux_{obj}_{band_l}"]
                flux_r      = row[f"flux_{obj}_{band_r}"]
                fluxerr_l   = row[f"fluxerr_{obj}_{band_l}"]
                fluxerr_r   = row[f"fluxerr_{obj}_{band_r}"]
                catmag_l    = row[f"{band_l}MeanPSFMag_{obj}_{band_l}"]
                catmag_r    = row[f"{band_r}MeanPSFMag_{obj}_{band_r}"]
                catmagerr_l = row[f"{band_l}MeanPSFMagErr_{obj}_{band_l}"]
                catmagerr_r = row[f"{band_r}MeanPSFMagErr_{obj}_{band_r}"]
                
                if (flux_r < 0) or (flux_l < 0):
                    print("Negative flux detected.")
                    print(f"    flux_l, flux_r = {flux_l}, {flux_r}")
                    sys.exit()

                # cat_sub_inst (m_cat - m_inst)
                instmag_l = -2.5*np.log10(flux_l)
                cat_sub_inst = catmag_l - instmag_l
                instmagerr_l = 2.5*log10err(flux_l, fluxerr_l)
                cat_sub_insterr = adderr(catmagerr_l, instmagerr_l)

                cat_sub_inst_list.append(cat_sub_inst)
                cat_sub_insterr_list.append(cat_sub_insterr)

                # col_cat (mag_l - mag_r)
                col_cat = catmag_l - catmag_r
                col_caterr = adderr(catmagerr_l, catmagerr_r)
                col_cat_list.append(col_cat)
                col_caterr_list.append(col_caterr)

                # y coordinate
                # To investiagete y_coord dependence of photometric results
                y_coord    = row[f"y_{obj}_{band}"]
                y_list.append(y_coord)

                # Output target info
                # Xaxis : col_cat      (cat0 - cat1)
                # Yaxis : cat_sub_inst (cat0 - inst0)
                if target is not None:
                    if (x0_t < col_cat < x1_t) & (y0_t < cat_sub_inst < y1_t):
                        x_target    = row[f"x_{obj}_{band}"]
                        y_target    = row[f"y_{obj}_{band}"]
                        fits_target = row[f"fits_{band}"]
                        objinfoFlag = row[f"objinfoFlag_{obj}_{band}"]
                        print(f"    Target info. in {idx_row+1}-th row")
                        print(f"      fits         : {fits_target}")
                        print(f"      objID        : {obj}")
                        print(f"      objinfoFlag  : {objinfoFlag}")
                        print(f"      x,y          : {x_target:.1f}, {y_target:.1f}")
                        print(f"      dedge_l,r    : {dedge_l:.1f}, {dedge_r:.1f}")
                        print(f"      cat mag      : {catmag_l:.3f}")
                        print(f"      cat col      : {col_cat:.3f}")
                        print(f"      cat colerr   : {col_caterr:.3f}")
                        print(f"      cat_sub_inst : {cat_sub_inst:.3f}")
                        print(f"      inst mag     : {instmag_l:.3f}")


        Nobj_frame = len(col_cat_list)
        if Nobj_frame >= Nobj_min:
            if verbose:
                print(f"{band}-band, {idx_row+1}-th frame objects N={Nobj_frame}")
            t_sec = row[f"t_sec_obs_{band}"]

            # Create dataframe of a frame
            df_temp = pd.DataFrame(
              {"col_cat":col_cat_list,
              "col_caterr":col_caterr_list,
              # Should have the same dimension
              "t_sec"   : [t_sec]*len(col_cat_list),
              # Should have the same dimension
              # To investigate y coord dependence
              "y"   : y_list,
              "cat_sub_inst":cat_sub_inst_list,
              "cat_sub_insterr":cat_sub_insterr_list})

            CT_df_list.append(df_temp)

        else:
            print(f"{band}-band, {idx_row+1}-th frame N={Nobj_frame} skip")
            pass

    if target is not None:
        print("Finish analyses.")
        sys.exit()

    return CT_df_list


def plot_objmag(df, band, bands, ax_inst, ax_cor):
    """
    Plot instumental and CT/Z corrected object colors.
    Remove outliers in the function.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    band : str
        band
    bands : str
        bands
    ax_inst : axis
        axis for instumental magnitude
    ax_cor : axis
        axis for corrected magnitude
    """
    # First index
    idx_eff = df.index.values
    idx0 = idx_eff[0]


    band_l, band_r = band4CTG(band, bands)
    for idx,x in enumerate(bands):
        if x == band:
            break
    print(f"{idx}, {band}, {bands}")
    col_p = mycolor[idx]
    mark_p = mymark[idx]

    N_det = len(df)

    # 1. instrumental magnitude

    ## g_mean_inst, g_std_inst
    #c_mean = df.at[idx0, f"{band_l}_{band_r}_mean_inst"]
    #c_std = df.at[idx0, f"{band_l}_{band_r}_std_inst"]
    ## g_r_wmean_inst, g_r_wstd_inst
    #c_wmean = df.at[idx0, f"{band_l}_{band_r}_wmean_inst"]
    #c_wstd = df.at[idx0, f"{band_l}_{band_r}_wstd_inst"]
    label_inst = (
      f"{band} N={N_det}\n"
      #f"  (mu,std)=({c_mean:.4f}+-{c_std:.4f})\n"
      #f"  (mu_wei,std_wei)=({c_wmean:.4f}+-{c_wstd:.4f})"
      )
    ax_inst.errorbar(
      df[f"t_sec_obs_{band}"], df[f"{band}_inst"], 
      df[f"{band}err_inst"], label=label_inst,
      ms=3, lw=1, ls="None", marker=mark_p, color=col_p)

    # 2. CT corrected 
    # g_mean, g_std
    c_mean = df.at[idx0, f"{band}_mean"]
    c_std = df.at[idx0, f"{band}_std"]
    # g_wmean, g_wstd
    c_wmean = df.at[idx0, f"{band}_wmean"]
    c_wstd = df.at[idx0, f"{band}_wstd"]
    label_ccor = (
      f"{band} N={N_det}\n"
      f"  (mu,std)=({c_mean:.4f}+-{c_std:.4f})\n"
      f"  (mu_wei,std_wei)=({c_wmean:.4f}+-{c_wstd:.4f})"
      )
    ax_cor.errorbar(
      df[f"t_sec_obs_{band}"], df[f"{band}_ccor"], 
      df[f"{band}err_ccor"], label=label_ccor,
      ms=3, lw=1, ls="None", marker=mark_p, color=col_p)


def plot_objcolor(df, band, bands, ax_inst, ax_cor):
    """
    Plot instumental and CTG/CTI corrected object colors.
    Remove outliers in the function.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    band : str
        band
    bands : str
        bands
    ax_inst : axis
        axis for instumental color
    ax_cor : axis
        axis for corrected color
    """
    # First index
    idx_eff = df.index.values
    idx0 = idx_eff[0]


    band_l, band_r = band4CTG(band, bands)
    for idx,x in enumerate(bands):
        if x == band:
            break
    print(f"{idx}, {band}, {bands}")
    col_p = mycolor[idx]
    mark_p = mymark[idx]

    N_det = len(df)

    # 1. instrumental color 
    # g_r_mean_inst, g_r_std_inst
    c_mean = df.at[idx0, f"{band_l}_{band_r}_mean_inst"]
    c_std = df.at[idx0, f"{band_l}_{band_r}_std_inst"]
    # g_r_wmean_inst, g_r_wstd_inst
    c_wmean = df.at[idx0, f"{band_l}_{band_r}_wmean_inst"]
    c_wstd = df.at[idx0, f"{band_l}_{band_r}_wstd_inst"]
    label_inst = (
      f"{band_l}-{band_r} N={N_det}\n"
      f"  (mu,std)=({c_mean:.4f}+-{c_std:.4f})\n"
      f"  (mu_wei,std_wei)=({c_wmean:.4f}+-{c_wstd:.4f})"
      )
    ax_inst.errorbar(
      df[f"t_sec_obs_{band}"], df[f"{band_l}_{band_r}_inst"], 
      df[f"{band_l}_{band_r}err_inst"], label=label_inst,
      ms=3, lw=1, ls="None", marker=mark_p, color=col_p)

    # 2. CTG corrected 
    # g_r_mean, g_r_std
    c_mean = df.at[idx0, f"{band_l}_{band_r}_mean"]
    c_std = df.at[idx0, f"{band_l}_{band_r}_std"]
    # g_r_wmean, g_r_wstd
    c_wmean = df.at[idx0, f"{band_l}_{band_r}_wmean"]
    c_wstd = df.at[idx0, f"{band_l}_{band_r}_wstd"]
    label_ccor = (
      f"{band_l}-{band_r} N={N_det}\n"
      f"  (mu,std)=({c_mean:.4f}+-{c_std:.4f})\n"
      f"  (mu_wei,std_wei)=({c_wmean:.4f}+-{c_wstd:.4f})"
      )
    ax_cor.errorbar(
      df[f"t_sec_obs_{band}"], df[f"{band_l}_{band_r}_ccor"], 
      df[f"{band_l}_{band_r}err_ccor"], label=label_ccor,
      ms=3, lw=1, ls="None", marker=mark_p, color=col_p)


def calc_objmag(df, df_Z, df_col, band, bands):
    """
    Calculate CT/Z corrected object magnitudes.
    df_Z includes CT!

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    df_CTG : pandas.DataFrame
        CTG dataframe
    df_col : pandas.DataFrame
        mean color
    band : str
        band
    bands : str
        bands

    Return
    ------
    df : pandas.DataFrame
        object dataframe with instrumental and CTG/CTI corrected colors
    """
    band_l, band_r = band4CTG(band, bands)
    for idx,x in enumerate(bands):
        if x == band:
            break
    print(f"{idx}, {band}, {bands}")

    # Error of instrumental magnitude
    s_magerr_r = df[f"{band}err_inst"]

    # Refer 
    #  {band_l}_{band_r}_wmean 
    #  {band_l}_{band_r}_wstd

    # CT/Z corrected magnitude
    #    m_cat - m_inst = color_cat*CT + Z
    #    m_cat          = m_inst + color_cat*CT + Z
    s_mag_c = (
      df[f"{band}_inst"] 
      + df_col[f"{band_l}_{band_r}_wmean"]*df_Z[f"{band}_CT_MC"] 
      + df_Z[f"{band}_Z_MC"]
      )
    err1 = mulerr(
        df_col[f"{band_l}_{band_r}_wmean"], 
        df_col[f"{band_l}_{band_r}_wstd"], 
        df_Z[f"{band}_CT_MC"], 
        df_Z[f"{band}_CTerr_MC"]
        )
    # Consider error of mag_inst, mul and Z
    s_magerr_c = adderr_series(
      s_magerr_r, err1, df_Z[f"{band}_Zerr_MC"])
    df.insert(0, f"{band}_ccor", s_mag_c)
    df.insert(0, f"{band}err_ccor", s_magerr_c) 

    return df



def calc_meanmag(df, band, verbose=False):
    """
    Calculate mean magnitude.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    band : str
        band
    verbose : bool
        whether output messages

    Return
    ------
    df : pandas.DataFrame
        dataframe with mean colors
    """
    # Calculate 2 types of mean
    # 1. normal mean
    #    mu = (C_1 + C_2 + ...)/N,
    #    err_total^2 = err_phot^2 + SE^2,
    #  where
    #    err_phot^2 = err_phot_1^2 + err_phot_2^2 + ...
    #    and SE = SD/sqrt(N)
    # 2. weighted mean
    #    mu = (C_1*w_1 + C_2*w_2 + ...)/w_sum,
    #    err_total^2 = 1/w_sum
    #  where
    #    w_i = 1/err_phot_i^2
    #    and w_sum = 1/sum(err_phot_1^2 + err_phot^2 + ...)


    # Inst. =================================================================
    c_std = np.std(df[f"{band}_inst"])
    w = 1/df[f"{band}err_inst"]**2
    c_wmean = np.average(df[f"{band}_inst"], weights=w)
    c_wstd = np.sqrt(1/np.sum(w))

    # Average
    c_mean = np.mean(df[f"{band}_inst"])
    # Photometric uncertainty
    c_std_phot = adderr(df[f"{band}err_inst"])/len(df)
    # Standard Deviation
    c_SD = np.std(df[f"{band}_inst"])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df))
    # Total error
    c_std_total = np.sqrt(c_std_phot**2 + c_SE**2)
    # Weighted mean
    w = 1/df[f"{band}err_inst"]**2
    c_wmean = np.average(df[f"{band}_inst"], weights=w)
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(w))

    # Add stacked info 
    df[f"{band}_mean_inst"] = c_mean
    df[f"{band}_std_inst"] = c_std_total
    df[f"{band}_wmean_inst"] = c_wmean
    df[f"{band}_wstd_inst"] = c_wstd
    df[f"{band}_std_phot_inst"] = c_std_phot
    df[f"{band}_SD_inst"] = c_SD
    df[f"{band}_SE_inst"] = c_SE
    # Inst. finish ==========================================================


    # CT corrected ==========================================================
    # Average
    c_mean = np.mean(df[f"{band}_ccor"])
    # Photometric uncertainty
    c_std_phot = adderr(df[f"{band}err_ccor"])/len(df)
    # Standard Deviation
    c_SD = np.std(df[f"{band}_ccor"])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df))
    # Total error
    c_std_total = np.sqrt(c_std_phot**2 + c_SE**2)

    # Weighted mean
    w = 1/df[f"{band}err_ccor"]**2
    c_wmean = np.average(df[f"{band}_ccor"], weights=w)
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(w))

    # Add stacked info 
    df[f"{band}_mean"] = c_mean
    df[f"{band}_std"] = c_std_total
    df[f"{band}_wmean"] = c_wmean
    df[f"{band}_wstd"] = c_wstd
    df[f"{band}_std_phot"] = c_std_phot
    df[f"{band}_SD"] = c_SD
    df[f"{band}_SE"] = c_SE
    # CT corrected finish ===================================================
    
    if verbose:
        print(
            f"{band_l}-{band_r}\n"
            f"  Normal mean : {c_mean:.4f}+-{c_std_total:.4f}\n"
            f"    (std^2 = std_phot^2 + SE^2," 
            f"{c_std_total:.4f}^2={c_std_phot:.4f}^2+{c_SE}^2,\n"
            f"    standard deviation(SD) : {c_SD:.4f})\n"
            f"  Weighted mean : {c_wmean:.4f}+-{c_wstd:.4f}"
            )
    return df


def calc_simplemeancolor(df, band, bands, verbose=False):
    """
    Calculate simple mean colors.
    Input should have  g_r, g_rerr etc.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    band : str
        band
    bands : str
        bands
    verbose : bool
        whether output messages

    Return
    ------
    df : pandas.DataFrame
        dataframe with mean colors
    """
    # Calculate 2 types of mean
    # 1. normal mean
    #    mu = (C_1 + C_2 + ...)/N,
    #    err_total^2 = err_phot^2 + SE^2,
    #  where
    #    err_phot^2 = err_phot_1^2 + err_phot_2^2 + ...
    #    and SE = SD/sqrt(N)
    # 2. weighted mean
    #    mu = (C_1*w_1 + C_2*w_2 + ...)/w_sum,
    #    err_total^2 = 1/w_sum
    #  where
    #    w_i = 1/err_phot_i^2
    #    and w_sum = 1/sum(err_phot_1^2 + err_phot^2 + ...)

    band_l, band_r = band4CTG(band, bands)
    for idx,x in enumerate(bands):
        if x == band:
            break
    print(f"{idx}, {band}, {bands}")

    # Average
    c_mean = np.mean(df[f"{band_l}_{band_r}"])
    # Photometric uncertainty
    c_std_phot = adderr(df[f"{band_l}_{band_r}err"])/len(df)
    # Standard Deviation
    c_SD = np.std(df[f"{band_l}_{band_r}"])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df))
    # Total error
    c_std_total = np.sqrt(c_std_phot**2 + c_SE**2)

    # Weighted mean
    w = 1/df[f"{band_l}_{band_r}err"]**2
    c_wmean = np.average(df[f"{band_l}_{band_r}"], weights=w)
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(w))

    # Add stacked info 
    df[f"{band_l}_{band_r}_mean"] = c_mean
    df[f"{band_l}_{band_r}_std"] = c_std_total
    df[f"{band_l}_{band_r}_wmean"] = c_wmean
    df[f"{band_l}_{band_r}_wstd"] = c_wstd
    df[f"{band_l}_{band_r}_std_phot"] = c_std_phot
    df[f"{band_l}_{band_r}_SD"] = c_SD
    df[f"{band_l}_{band_r}_SE"] = c_SE
    # CT corrected finish ===================================================
    
    if verbose:
        print(
            f"{band_l}-{band_r}\n"
            f"  Normal mean : {c_mean:.4f}+-{c_std_total:.4f}\n"
            f"    (std^2 = std_phot^2 + SE^2," 
            f"{c_std_total:.4f}^2={c_std_phot:.4f}^2+{c_SE}^2,\n"
            f"    standard deviation(SD) : {c_SD:.4f})\n"
            f"  Weighted mean : {c_wmean:.4f}+-{c_wstd:.4f}"
            )
    return df


def calc_meancolor(df, band, bands, verbose=False):
    """
    Calculate mean colors.
    Please refer to calc_simplemeancolor for simle mean.

    Parameters
    ----------
    df : pandas.DataFrame
        object dataframe
    band : str
        band
    bands : str
        bands
    verbose : bool
        whether output messages

    Return
    ------
    df : pandas.DataFrame
        dataframe with mean colors
    """
    # Calculate 2 types of mean
    # 1. normal mean
    #    mu = (C_1 + C_2 + ...)/N,
    #    err_total^2 = err_phot^2 + SE^2,
    #  where
    #    err_phot^2 = err_phot_1^2 + err_phot_2^2 + ...
    #    and SE = SD/sqrt(N)
    # 2. weighted mean
    #    mu = (C_1*w_1 + C_2*w_2 + ...)/w_sum,
    #    err_total^2 = 1/w_sum
    #  where
    #    w_i = 1/err_phot_i^2
    #    and w_sum = 1/sum(err_phot_1^2 + err_phot^2 + ...)

    band_l, band_r = band4CTG(band, bands)
    for idx,x in enumerate(bands):
        if x == band:
            break
    print(f"{idx}, {band}, {bands}")

    # Inst. =================================================================
    c_std = np.std(df[f"{band_l}_{band_r}_inst"])
    w = 1/df[f"{band_l}_{band_r}err_inst"]**2
    c_wmean = np.average(df[f"{band_l}_{band_r}_inst"], weights=w)
    c_wstd = np.sqrt(1/np.sum(w))

    # Average
    c_mean = np.mean(df[f"{band_l}_{band_r}_inst"])
    # Photometric uncertainty
    c_std_phot = adderr(df[f"{band_l}_{band_r}err_inst"])/len(df)
    # Standard Deviation
    c_SD = np.std(df[f"{band_l}_{band_r}_inst"])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df))
    # Total error
    c_std_total = np.sqrt(c_std_phot**2 + c_SE**2)
    # Weighted mean
    w = 1/df[f"{band_l}_{band_r}err_inst"]**2
    c_wmean = np.average(df[f"{band_l}_{band_r}_inst"], weights=w)
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(w))

    # Add stacked info 
    df[f"{band_l}_{band_r}_mean_inst"] = c_mean
    df[f"{band_l}_{band_r}_std_inst"] = c_std_total
    df[f"{band_l}_{band_r}_wmean_inst"] = c_wmean
    df[f"{band_l}_{band_r}_wstd_inst"] = c_wstd
    df[f"{band_l}_{band_r}_std_phot_inst"] = c_std_phot
    df[f"{band_l}_{band_r}_SD_inst"] = c_SD
    df[f"{band_l}_{band_r}_SE_inst"] = c_SE
    # Inst. finish ==========================================================
    

    # CT corrected ==========================================================
    # Average
    c_mean = np.mean(df[f"{band_l}_{band_r}_ccor"])
    # Photometric uncertainty
    c_std_phot = adderr(df[f"{band_l}_{band_r}err_ccor"])/len(df)
    # Standard Deviation
    c_SD = np.std(df[f"{band_l}_{band_r}_ccor"])
    # Standard Error
    c_SE = c_SD/np.sqrt(len(df))
    # Total error
    c_std_total = np.sqrt(c_std_phot**2 + c_SE**2)


    # Weighted mean
    w = 1/df[f"{band_l}_{band_r}err_ccor"]**2
    c_wmean = np.average(df[f"{band_l}_{band_r}_ccor"], weights=w)
    # SD of weighted mean
    c_wstd = np.sqrt(1/np.sum(w))


    # Add stacked info 
    df[f"{band_l}_{band_r}_mean"] = c_mean
    checknan(df)
    df[f"{band_l}_{band_r}_std"] = c_std_total
    checknan(df)
    # BAD!!
    df[f"{band_l}_{band_r}_wmean"] = c_wmean
    checknan(df)
    df[f"{band_l}_{band_r}_wstd"] = c_wstd
    df[f"{band_l}_{band_r}_std_phot"] = c_std_phot
    df[f"{band_l}_{band_r}_SD"] = c_SD
    df[f"{band_l}_{band_r}_SE"] = c_SE
    # CT corrected finish ===================================================
    
    if verbose:
        print(
            f"{band_l}-{band_r}\n"
            f"  Normal mean : {c_mean:.4f}+-{c_std_total:.4f}\n"
            f"    (std^2 = std_phot^2 + SE^2," 
            f"{c_std_total:.4f}^2={c_std_phot:.4f}^2+{c_SE}^2,\n"
            f"    standard deviation(SD) : {c_SD:.4f})\n"
            f"  Weighted mean : {c_wmean:.4f}+-{c_wstd:.4f}"
            )
    return df


def calc_Z(
    df, objID, band, bands, CT, CTerr, 
    Nobj_min, eflag_max, dedge_min, cntmfrac_max, verbose=False):
    """
    Calculate CTIs with fixed CTGs.

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    band : str
        band
    bands : array-likes
        list of bands
    CT : float
        CT
    CTerr : float
        uncertainty of CT
    Nobj_min : int
        minimum threshold of number of object in a frame
        (not contaminated, not on edge)
    dedge_min : int
        acceptable distance to edge region
    cntmfrac_max : int
        acceptable contamination fraction
    ax_raw, ax_wei : axis
       axes for raw fit and weighted fit, respectively
    key_x, key_y : str
        keywords of x and y
    verbose : bool
        whether output messages

    Return
    ------
    df_Z : pandas.DataFrame
        DataFrame with Zs
   """
    
    # Use band4CTG ! not CT
    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)
    
    for idx,x in enumerate(bands):
        if x == band:
            break

    # Lists for fitting
    Z_MC_list, Zerr_MC_list = [], []
    CT_MC_list, CTerr_MC_list = [], []
    t_sec_list = []
    
    for idx_row, row in df.iterrows():
        cat_sub_inst_list, cat_sub_insterr_list = [], []
        col_cat_list, col_caterr_list = [], []
        # Use flux, mag_cat, mag_cat_l, mag_cat_r (+ errors), and eflag
        for obj in objID:
            eflag_l    = row[f"eflag_{obj}_{band_l}"]
            eflag_r    = row[f"eflag_{obj}_{band_r}"]
            dedge_l    = row[f"dedge_{obj}_{band_l}"]
            dedge_r    = row[f"dedge_{obj}_{band_r}"]
            cntmfrac_l = row[f"cntmfrac_{obj}_{band_l}"]
            cntmfrac_r = row[f"cntmfrac_{obj}_{band_r}"]
            
            # Good condition
            flux_l      = row[f"flux_{obj}_{band_l}"]
            flux_r      = row[f"flux_{obj}_{band_r}"]
            if ((dedge_l > dedge_min) 
                & (dedge_r > dedge_min) 
                & (cntmfrac_l < cntmfrac_max)
                & (cntmfrac_r < cntmfrac_max)
                & (eflag_l < eflag_max) 
                & (flux_l > 0) 
                & (flux_r > 0) 
              and (eflag_r < eflag_max)):

                flux_l      = row[f"flux_{obj}_{band_l}"]
                flux_r      = row[f"flux_{obj}_{band_r}"]
                fluxerr_l   = row[f"fluxerr_{obj}_{band_l}"]
                fluxerr_r   = row[f"fluxerr_{obj}_{band_r}"]
                catmag_l    = row[f"{band_l}MeanPSFMag_{obj}_{band_l}"]
                catmag_r    = row[f"{band_r}MeanPSFMag_{obj}_{band_r}"]
                catmagerr_l = row[f"{band_l}MeanPSFMagErr_{obj}_{band_l}"]
                catmagerr_r = row[f"{band_r}MeanPSFMagErr_{obj}_{band_r}"]
                
                if (flux_r < 0) or (flux_l < 0):
                    print("Negative flux detected.")
                    print(f"    flux_l, flux_r = {flux_l}, {flux_r}")
                    sys.exit()

                # cat_sub_inst (m_cat - m_inst)
                cat_sub_inst = catmag_l - (-2.5*np.log10(flux_l))
                instmagerr_l = 2.5*log10err(flux_l, fluxerr_l)
                cat_sub_insterr = adderr(catmagerr_l, instmagerr_l)
                cat_sub_inst_list.append(cat_sub_inst)
                cat_sub_insterr_list.append(cat_sub_insterr)

                # col_cat (mag_l - mag_r)
                col_cat = catmag_l - catmag_r
                col_caterr = adderr(catmagerr_l, catmagerr_r)
                col_cat_list.append(col_cat)
                col_caterr_list.append(col_caterr)

        t_sec = row[f"t_sec_obs_{band}"]
        t_sec_list.append(t_sec)

        # If CTI is not estimated due to the lack of reference stars,
        # add 0 to the dataframe to keep the length of DataFrame
        if len(col_cat_list)==0:
            Z_MC_list.append(0)
            Zerr_MC_list.append(0)
            CT_MC_list.append(0)
            CTerr_MC_list.append(0)
            print(
              f"    {band}-band, {idx_row+1}-th frame Z is not estimated and "
              f"skip the frame."
            )
            continue
        
        # Use mean and std of Monte Carlo results
        N_mc = 100
        # Set seed 0
        seed = 0
        np.random.seed(seed)
        Z_temp_list = []

        Nstar = len(col_cat_list)
 
        def linear_fit_Z(x, b):
            """Linear fit function for scipy.optimize.
            """
            return CT*x + b

        for n in range(N_mc):
            # Initialization
            col_cat_sample = np.random.normal(
                col_cat_list, col_caterr_list, Nstar)
            cat_sub_inst_sample = np.random.normal(
                cat_sub_inst_list, cat_sub_insterr_list, Nstar)

            param_sample, cov_sample = curve_fit(
                linear_fit_Z, col_cat_sample, cat_sub_inst_sample) 
            Z_sample = param_sample[0]
            Z_temp_list.append(Z_sample)
        
        Z_MC_mean = np.mean(Z_temp_list)
        Z_MC_std = np.std(Z_temp_list)
        if verbose:
            print(f"  MC mean, std = {Z_MC_mean:.4f}, {Z_MC_std:.4f}")
        Z_MC_list.append(Z_MC_mean)
        Zerr_MC_list.append(Z_MC_std)
        CT_MC_list.append(CT)
        CTerr_MC_list.append(CTerr)
        
    
    # Create dataframe of a frame
    # Save CTs as well
    df_Z = pd.DataFrame(
      {f"{band}_CT_MC"   : CT_MC_list,
      f"{band}_CTerr_MC"   : CTerr_MC_list,
      f"{band}_Z_MC"   : Z_MC_list,
      f"{band}_t_sec"   : t_sec_list,
      f"{band}_Zerr_MC"   : Zerr_MC_list})
    return df_Z


def calc_CTI(
    df, objID, band, bands, CTG, CTGerr, 
    Nobj_min, eflag_max, dedge_min, cntmfrac_max, verbose=False):
    """
    Calculate CTIs with fixed CTGs.

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    band : str
        band
    bands : array-likes
        list of bands
    CTG : float
        CTG
    CTGerr : float
        uncertainty of CTG
    Nobj_min : int
        minimum threshold of number of object in a frame
        (not contaminated, not on edge)
    dedge_min : int
        acceptable distance to edge region
    cntmfrac_max : int
        acceptable contamination fraction
    verbose : bool
        whether output messages

    Return
    ------
    df_CTI : pandas.DataFrame
        DataFrame with CTIs
   """

    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)
    
    for idx,x in enumerate(bands):
        if x == band:
            break

    # Lists for fitting
    CTI_MC_list, CTIerr_MC_list = [], []
    CTG_MC_list, CTGerr_MC_list = [], []
    t_sec_list = []
    
    for idx_row, row in df.iterrows():
        col_inst_list, col_insterr_list = [], []
        col_cat_list, col_caterr_list = [], []
        # Use flux, mag_cat, mag_cat_l, mag_cat_r (+ errors), and eflag
        for obj in objID:
            eflag_l    = row[f"eflag_{obj}_{band_l}"]
            eflag_r    = row[f"eflag_{obj}_{band_r}"]
            dedge_l    = row[f"dedge_{obj}_{band_l}"]
            dedge_r    = row[f"dedge_{obj}_{band_r}"]
            cntmfrac_l = row[f"cntmfrac_{obj}_{band_l}"]
            cntmfrac_r = row[f"cntmfrac_{obj}_{band_r}"]
            
            # Good condition
            flux_l      = row[f"flux_{obj}_{band_l}"]
            flux_r      = row[f"flux_{obj}_{band_r}"]
            if ((dedge_l > dedge_min) 
                & (dedge_r > dedge_min) 
                & (cntmfrac_l < cntmfrac_max)
                & (cntmfrac_r < cntmfrac_max)
                & (eflag_l < eflag_max) 
                & (flux_l > 0) 
                & (flux_r > 0) 
              and (eflag_r < eflag_max)):

                flux_l      = row[f"flux_{obj}_{band_l}"]
                flux_r      = row[f"flux_{obj}_{band_r}"]
                fluxerr_l   = row[f"fluxerr_{obj}_{band_l}"]
                fluxerr_r   = row[f"fluxerr_{obj}_{band_r}"]
                catmag_l    = row[f"{band_l}MeanPSFMag_{obj}_{band_l}"]
                catmag_r    = row[f"{band_r}MeanPSFMag_{obj}_{band_r}"]
                catmagerr_l = row[f"{band_l}MeanPSFMagErr_{obj}_{band_l}"]
                catmagerr_r = row[f"{band_r}MeanPSFMagErr_{obj}_{band_r}"]
                
                if (flux_r < 0) or (flux_l < 0):
                    print("Negative flux detected.")
                    print(f"    flux_l, flux_r = {flux_l}, {flux_r}")
                    sys.exit()

                # col_inst (mag_l - mag_r)
                col_inst = -2.5*np.log10(flux_l/flux_r)
                instmagerr_l = 2.5*log10err(flux_l, fluxerr_l)
                instmagerr_r = 2.5*log10err(flux_r, fluxerr_r)
                col_insterr = adderr(instmagerr_l, instmagerr_r)
                col_inst_list.append(col_inst)
                col_insterr_list.append(col_insterr)

                # col_cat (mag_l - mag_r)
                col_cat = catmag_l - catmag_r
                col_caterr = adderr(catmagerr_l, catmagerr_r)
                col_cat_list.append(col_cat)
                col_caterr_list.append(col_caterr)

        t_sec = row[f"t_sec_obs_{band}"]
        t_sec_list.append(t_sec)

        # If CTI is not estimated due to the lack of reference stars,
        # add 0 to the dataframe to keep the length of DataFrame
        if len(col_cat_list)==0:
            CTI_MC_list.append(0)
            CTIerr_MC_list.append(0)
            CTG_MC_list.append(0)
            CTGerr_MC_list.append(0)
            print(
              f"    {band}-band, {idx_row+1}-th frame CTI is not estimated and "
              f"skip the frame."
            )
            continue

        # Estimate CTI using ref stars in fov
        # Use col_cat_list, col_caterr_list, col_inst_list, col_insterr_list

        #param_w, cov_w = curve_fit(
        #  linear_fit_CTI, col_cat_list, col_inst_list, 
        #  sigma=col_insterr_list, absolute_sigma=True)
        #paramerr_w = np.sqrt(np.diag(cov_w))
        #CTI_w = param_w[0]
        #CTIerr_w = paramerr_w[0]
        #print(
        #  f"    {band}-band, {n}-th frame CTI wei = {CTI_w:.2f}+-{CTIerr_w:.2f}")

        # Use mean and std of Monte Carlo results
        N_mc = 100
        # Set seed 0
        seed = 0
        np.random.seed(seed)
        CTI_temp_list = []

        Nstar = len(col_cat_list)
 
        def linear_fit_CTI(x, b):
            """Linear fit function for scipy.optimize.
            """
            return CTG*x + b

        for n in range(N_mc):
            # Initialization
            col_cat_sample = np.random.normal(
                col_cat_list, col_caterr_list, Nstar)
            col_inst_sample = np.random.normal(
                col_inst_list, col_insterr_list, Nstar)

            param_sample, cov_sample = curve_fit(
                linear_fit_CTI, col_inst_sample, col_cat_sample) 
            CTI_sample = param_sample[0]
            CTI_temp_list.append(CTI_sample)
        
        CTI_MC_mean = np.mean(CTI_temp_list)
        CTI_MC_std = np.std(CTI_temp_list)
        if verbose:
            print(f"  MC mean, std = {CTI_MC_mean:.4f}, {CTI_MC_std:.4f}")
        CTI_MC_list.append(CTI_MC_mean)
        CTIerr_MC_list.append(CTI_MC_std)
        CTG_MC_list.append(CTG)
        CTGerr_MC_list.append(CTGerr)
        
       
    
    # Create dataframe of a frame
    # Save CTGs as well
    df_CTI = pd.DataFrame(
      {f"{band_l}_{band_r}_CTG_MC"   : CTG_MC_list,
      f"{band_l}_{band_r}_CTGerr_MC"   : CTGerr_MC_list,
      f"{band_l}_{band_r}_CTI_MC"   : CTI_MC_list,
      f"{band_l}_{band_r}_t_sec"   : t_sec_list,
      f"{band_l}_{band_r}_CTIerr_MC"   : CTIerr_MC_list})
    return df_CTI


def plot_CT(df_CT_list, band, bands, ax, verbose=False):
    """
    Plot time-series of CTs.

    Parameters
    ----------
    df_CT_list : array-like
        list of DataFrames of CT
    band : str
        band
    bands : array-like
        list of bands
    ax : axis
       ax
    verbose : bool
        whether output messages
    """
    band_l, band_r = band4CT(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)

    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    col_p = mycolor[idx]
    mark_p = mymark[idx]
    
    # Each df in df_CT_list is like below.
    #   df_temp = pd.DataFrame(
    #       {"col_cat":col_cat_list,
    #       "col_caterr":col_caterr_list,
    #       "col_inst":col_inst_list,
    #       "t_sec"   : [t_s,
    #       "col_insterr":col_insterr_list})

    #       {"col_cat":col_cat_list,
    #       "col_caterr":col_caterr_list,
    #       "cat_sub_inst":cat_sub_inst_list,
    #       "t_sec"   : t_list,
    #       "cat_sub_insterr":cat_sub_insterr_list})
    
    # Calculate CT in each frame by fitting
    CT_list, CTerr_list = [], []
    t_list = []
    for df in df_CT_list:
        param, cov_sample = curve_fit(
            linear_fit, df["col_cat"], df["cat_sub_inst"]) 
        # Extract slope
        CT_list.append(param[0])
        # Temporarily
        CTerr_list.append(0.05)

        # all "t_sec" are the same ()
        t_list.append(df.at[0, "t_sec"])

    ax.scatter(
      t_list, CT_list, 
      s=10, color=col_p, marker=mark_p, 
      label=f"{band_l}-{band_r} N={len(df_CT_list)}")
    ax.set_xlabel("Elapsed time [sec]")
    ax.set_ylabel("CT")


def plot_CTG(df_CTG_list, band, bands, ax, verbose=False):
    """
    Plot time-series of CTGs.

    Parameters
    ----------
    df_CTG_list : array-like
        list of DataFrames of CTI
    band : str
        band
    bands : array-like
        list of bands
    ax : axis
       ax
    verbose : bool
        whether output messages
    """
    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)

    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    col_p = mycolor[idx]
    mark_p = mymark[idx]
    
    # Each df in df_CTG_list is like below.
    #   df_temp = pd.DataFrame(
    #       {"col_cat":col_cat_list,
    #       "col_caterr":col_caterr_list,
    #       "col_inst":col_inst_list,
    #       "t_sec"   : t_list,
    #       "col_insterr":col_insterr_list})
    
    # Calculate CTG in each frame by fitting
    CTG_list, CTGerr_list = [], []
    t_list = []
    for df in df_CTG_list:
        param, cov_sample = curve_fit(
            linear_fit, df["col_inst"], df["col_cat"]) 
        # Extract slope
        CTG_list.append(param[0])
        # Temporarily
        CTGerr_list.append(0.05)

        # all "t_sec" are the same ()
        t_list.append(df.at[0, "t_sec"])

    ax.scatter(
      t_list, CTG_list, 
      s=10, color=col_p, marker=mark_p, 
      label=f"{band_l}-{band_r} N={len(df_CTG_list)}")
    ax.set_xlabel("Elapsed time [sec]")
    ax.set_ylabel("CTG")
    ax.legend(fontsize=10)


def plot_bg(df, ax, band, bands, verbose=False):
    """
    Plot background counts (bg_level_g etc.).

    Parameters
    ----------
    df : list
        DataFrames of photometry results
    ax : axis
       ax
    band : str
        band
    bands : str
        bands
    verbose : bool
        whether output messages
    """
    # Number of used frame (i.e. stars in frame >= Nobj_min)
    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    col_p = mycolor[idx]
    mark_p = mymark[idx]

    ax.errorbar(
      df[f"t_sec_obs_{band}"], df[f"bg_level_{band}"], yerr=df[f"bg_rms_{band}"], 
      linewidth=0.2, ls="None", ms=1, color=col_p, marker=mark_p, 
      label=f"{band} N={len(df)}")
    ax.legend(fontsize=10)


def plot_Z(df_Z, band, bands, ax, verbose=False):
    """
    Plot Zs.

    Parameters
    ----------
    df_Z : list
        DataFrames of Z
    band : str
        band
    bands : str
        bands
    ax : axis
       ax
    verbose : bool
        whether output messages
    """
    # Number of used frame (i.e. stars in frame >= Nobj_min)
    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    col_p = mycolor[idx]
    mark_p = mymark[idx]
    

    df_Z = df_Z[df_Z[f"{band}_Z_MC"]!=0]
    df_bad = df_Z[df_Z[f"{band}_Z_MC"]==0]

    ax.errorbar(
      df_Z[f"{band}_t_sec"], df_Z[f"{band}_Z_MC"], 
      yerr=df_Z[f"{band}_Zerr_MC"],
      linewidth=0.2, ls="None", ms=1, color=col_p, marker=mark_p, label=f"{band} N={len(df_Z)}")
    ax.errorbar(
      df_bad[f"{band}_t_sec"], df_bad[f"{band}_Z_MC"], 
      yerr=df_bad[f"{band}_Zerr_MC"],
      linewidth=0.2, ls="None", ms=1, color="gray", marker=mark_p, label=f"{band} Bad N={len(df_bad)}")
    ax.legend(fontsize=10)


def plot_CTI(df_CTI, band, bands, ax, verbose=False):
    """
    Plot CTIs.

    Parameters
    ----------
    df_CTI : list
        DataFrames of CTI
    band : str
        band
    bands : array-likes
        list of bands
    ax : axis
       ax
    verbose : bool
        whether output messages
    """
    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)

    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")
    col_p = mycolor[idx]
    mark_p = mymark[idx]
    

    df_CTI = df_CTI[df_CTI[f"{band_l}_{band_r}_CTI_MC"]!=0]
    df_bad = df_CTI[df_CTI[f"{band_l}_{band_r}_CTI_MC"]==0]

    ax.errorbar(
      df_CTI[f"{band_l}_{band_r}_t_sec"], df_CTI[f"{band_l}_{band_r}_CTI_MC"], 
      yerr=df_CTI[f"{band_l}_{band_r}_CTIerr_MC"],
      linewidth=0.2, ls="None", ms=1, color=col_p, marker=mark_p, label=f"{band_l}-{band_r} N={len(df_CTI)}")
    ax.errorbar(
      df_bad[f"{band_l}_{band_r}_t_sec"], df_bad[f"{band_l}_{band_r}_CTI_MC"], 
      yerr=df_bad[f"{band_l}_{band_r}_CTIerr_MC"],
      linewidth=0.2, ls="None", ms=1, color="gray", marker=mark_p, label=f"{band_l}-{band_r} Bad N={len(df_bad)}")
    ax.legend(fontsize=10)



def calc_CTG(
        df, objID, band, bands, Nobj_min, eflag_max, 
        dedge_min, cntmfrac_max, target=None, verbose=False):
    """
    Calculate CTG.

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    band : str
        band
    bands : array-likes
        list of bands
    Nobj_min : int
         minimum threshold of number of object in a frame
         (not contaminated, not on edge)
    dedge_min : int
        acceptable distance to edge region
    cntmfrac_max : int
        acceptable contamination fraction
    target : array-like, optional
       target info 
       [min(cat0-cat1), max(cat0-cat1), min(cat0-inst0), max(cat0-inst0)]
    verbose : bool
        output messages

    Return
    ------
    CTG_df_list : list
        list of DataFrames of photometry results of reference stars
    """
    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)
    
    for idx,x in enumerate(bands):
        if x == band:
            break
    if verbose:
        print(f"  {band}-band in {bands}")

    # Output target info
    if target is not None:
        x0_t, x1_t, y0_t, y1_t = target


    column = df.columns.tolist()
    CTG_df_list = []
    for idx_row, row in df.iterrows():
        col_inst_list    = []
        col_insterr_list = []
        col_cat_list     = []
        col_caterr_list  = []
        t_sec_list       = []
        y_list           = []
        # Use flux, mag_cat, mag_cat_l, mag_cat_r (+ errors), and eflag
        for obj in objID:
            eflag_l    = row[f"eflag_{obj}_{band_l}"]
            eflag_r    = row[f"eflag_{obj}_{band_r}"]
            dedge_l    = row[f"dedge_{obj}_{band_l}"]
            dedge_r    = row[f"dedge_{obj}_{band_r}"]
            cntmfrac_l = row[f"cntmfrac_{obj}_{band_l}"]
            cntmfrac_r = row[f"cntmfrac_{obj}_{band_r}"]

            #print(f"    dedge_l, dedge_r = {dedge_l:.1f}, {dedge_r:.1f}")
            #if dedge_l < dedge_min:
            #    print(f"  obj {obj}")
            #    print(f"    small dedge_l, dedge_r = {dedge_l:.1f}, {dedge_r:.1f}")
            
            # # Good condition
            # if ((dedge_l > dedge_min) 
            #     & (dedge_r > dedge_min) 
            #     & (cntmfrac_l < cntmfrac_max)
            #     & (cntmfrac_r < cntmfrac_max)
            #     & (eflag_l < eflag_max) 
            #   and (eflag_r < eflag_max)):

            # Use flux as well
            flux_l      = row[f"flux_{obj}_{band_l}"]
            flux_r      = row[f"flux_{obj}_{band_r}"]
            if ((dedge_l > dedge_min) 
                & (dedge_r > dedge_min) 
                & (cntmfrac_l < cntmfrac_max)
                & (cntmfrac_r < cntmfrac_max)
                & (eflag_l < eflag_max) 
                & (flux_l > 0) 
                & (flux_r > 0) 
              and (eflag_r < eflag_max)):
                flux_l      = row[f"flux_{obj}_{band_l}"]
                flux_r      = row[f"flux_{obj}_{band_r}"]
                fluxerr_l   = row[f"fluxerr_{obj}_{band_l}"]
                fluxerr_r   = row[f"fluxerr_{obj}_{band_r}"]
                catmag_l    = row[f"{band_l}MeanPSFMag_{obj}_{band_l}"]
                catmag_r    = row[f"{band_r}MeanPSFMag_{obj}_{band_r}"]
                catmagerr_l = row[f"{band_l}MeanPSFMagErr_{obj}_{band_l}"]
                catmagerr_r = row[f"{band_r}MeanPSFMagErr_{obj}_{band_r}"]
                
                if (flux_r < 0) or (flux_l < 0):
                    print("Negative flux detected.")
                    print(f"    flux_l, flux_r = {flux_l}, {flux_r}")
                    sys.exit()

                # col_inst (mag_l - mag_r)
                col_inst = -2.5*np.log10(flux_l/flux_r)
                instmagerr_l = 2.5*log10err(flux_l, fluxerr_l)
                instmagerr_r = 2.5*log10err(flux_r, fluxerr_r)
                col_insterr = adderr(instmagerr_l, instmagerr_r)
                col_inst_list.append(col_inst)
                col_insterr_list.append(col_insterr)

                # col_cat (mag_l - mag_r)
                col_cat = catmag_l - catmag_r
                col_caterr = adderr(catmagerr_l, catmagerr_r)
                col_cat_list.append(col_cat)
                col_caterr_list.append(col_caterr)

                # y coordinate
                # To investiagete y_coord dependence of photometric results
                y_coord    = row[f"y_{obj}_{band}"]
                y_list.append(y_coord)

                # Output target info
                # Xaxis : col_inst (inst0 - inst1)
                # Yaxis : col_cat (cat0 - cat1)
                #print(f"col_inst, col_cat = {col_inst:.1f}, {col_cat:.1f}")
                if target is not None:
                    if (x0_t < col_inst < x1_t) & (y0_t < col_cat < y1_t):
                        x_target    = row[f"x_{obj}_{band}"]
                        y_target    = row[f"y_{obj}_{band}"]
                        fits_target = row[f"fits_{band}"]
                        objinfoFlag = row[f"objinfoFlag_{obj}_{band}"]
                        print(f"    Target info. in {idx_row+1}-th row")
                        print(f"      fits         : {fits_target}")
                        print(f"      objID        : {obj}")
                        print(f"      objinfoFlag  : {objinfoFlag}")
                        print(f"      x,y          : {x_target:.1f}, {y_target:.1f}")
                        print(f"      dedge_l,r    : {dedge_l:.1f}, {dedge_r:.1f}")
                        print(f"      cat mag      : {catmag_l:.3f}")
                        print(f"      cat col      : {col_cat:.3f}")
                        print(f"      cat colerr   : {col_caterr:.3f}")
                        print(f"      inst col     : {col_inst:.3f}")


        Nobj_frame = len(col_cat_list)
        if Nobj_frame >= Nobj_min:
            if verbose:
                print(f"{band}-band, {idx_row+1}-th frame objects N={Nobj_frame}")
            
            t_sec = row[f"t_sec_obs_{band}"]
            # Create dataframe of a frame
            df_temp = pd.DataFrame(
              {"col_cat":col_cat_list,
              "col_caterr":col_caterr_list,
              "col_inst":col_inst_list,
              # Should have the same dimension
              "t_sec"   : [t_sec]*len(col_cat_list),
              "y"   : y_list,
              "col_insterr":col_insterr_list})

            CTG_df_list.append(df_temp)

        else:
            print(f"{band}-band, {idx_row+1}-th frame N={Nobj_frame} skip")
            pass

    if target is not None:
        print("Finish analyses.")
        sys.exit()

    return CTG_df_list


def plot_CTGfit(
    CTG_df_list, band, bands, fig, ax, ax_res=None, yr_res=None,
    shift=True, ycolor=1, ny=1280, colormap=False, cbar=False, ax_cbar=None, 
    verbose=False):
    """
    Plot CTGs.

    Parameters
    ----------
    CTG_df_list : list
        list of DataFrames of photometry results of reference stars
    band : str
        band
    bands : array-likes
        list of bands
    fig : figure
       figure (for colorbar, cmap)
    ax : axis
       ax
    shift : bool
        shift to set mean zero
    ax_res : axis
       axis for residual plot
    yr_res : list of float
       y range of residual plot
    ycolor : int
        number of colors depending on y pixel values
    ny : int
        number of pixels along y axis
    colormap : book
        whether plot colormap like Fig.7 in Jackson+2021
    cbar : book
        whether use cbar to show time-series info.
    ax_cbar : axis
       axis for color var to show time info
    verbose : bool
        output messages

    Return
    ------
    df_CTG : pandas.DataFrame
        DataFrame with CTGs
    """

    band_l, band_r = band4CTG(band, bands)
    # Number of used frame (i.e. stars in frame >= Nobj_min)

    # Lists for fitting
    col_inst_list, col_cat_list       = [], []
    col_insterr_list, col_caterr_list = [], []
    y_list = []
    t_list = []
    
    for idx,x in enumerate(bands):
        if x == band:
            break

    if verbose:
        print(f"  {band}-band in {bands}")
    col_rawfit = "gray"
    col_weifit = "black"
    col_MCfit  = "red"
    col_list = ["red", "blue", "green", "orange"]
    col_p = mycolor[idx]
    mark_p = mymark[idx]
    
    for df in CTG_df_list:
        if shift:
            # Shifted
            # Subtract mean values to use all photometric points 
            # (even in different nights) at once
            # See Jackson et al.2021, PASP, 133, 075003
            df["col_cat"] -= np.mean(df["col_cat"])
            df["col_inst"] -= np.mean(df["col_inst"])

        # Save all result for fitting
        col_cat_list   += df["col_cat"].values.tolist()
        col_inst_list  += df["col_inst"].values.tolist()
        col_caterr_list    += df["col_caterr"].values.tolist()
        col_insterr_list   += df["col_insterr"].values.tolist()
        y_list += df["y"].values.tolist()
        t_list += df["t_sec"].values.tolist()

    # Plot at once in both cases
    ## Create colormap
    cm = "inferno"
    if colormap:
        # 0.02 mag bin
        ## Extract range
        ymin, ymax = np.min(col_inst_list), np.max(col_inst_list)
        ymin, ymax = np.min(col_cat_list), np.max(col_cat_list)
        ## Set bin width
        xmin, xmax = np.min(col_inst_list), np.max(col_inst_list)
        wbin = 0.02
        bins = [np.arange(xmin, xmax, wbin), np.arange(ymin, ymax, wbin)]
        # Please optimize for each observation
        Nmax = 1e3
        H = ax.hist2d(col_inst_list, col_cat_list, bins=bins, cmap=cm,
                norm=LogNorm(vmin=1e0, vmax=Nmax))
        # Add colorbars
        fig.colorbar(H[3],ax=ax)

    ## Create cbar to show time-series info
    elif cbar:
        from matplotlib import cm
        mapp = ax.scatter(
            col_inst_list, col_cat_list, c=t_list,  cmap=cm.inferno, 
            s=30, lw=1, marker=mark_p, facecolor="None", zorder=2)
        cbar = fig.colorbar(mapp, ax_cbar)
        cbar.set_label("Elapsed time [s]")
        # Add errorbars
        ax.errorbar(
            col_inst_list, col_cat_list, 
            xerr=col_insterr_list, yerr=col_caterr_list, lw=0.5, ms=3, fmt="o", 
            marker=" ", capsize=0, zorder=1)
    ## Simply plot points
    else:
        ax.errorbar(
            col_inst_list, col_cat_list, 
            xerr=col_insterr_list, yerr=col_caterr_list,
            ms=1, linewidth=0.5, color=col_p, marker=mark_p, ls="None")

    # Set initial parameters to avoid an error in covarience calculation
    p0 = [1, 0.001]

    # Raw =====================================================================
    ## Raw fit
    param_r, cov_r = curve_fit(
        linear_fit, col_inst_list, col_cat_list, p0=p0)
    paramerr_r = np.sqrt(np.diag(cov_r))
    CTG_r, CTI_r = param_r[0], param_r[1]
    CTGerr_r, CTIerr_r = paramerr_r[0], paramerr_r[1]
    if verbose:
        print(f"    CTG raw = {CTG_r:.4f}+-{CTGerr_r:.4f} (not used)")

    ## Weighted fit
    ## Use larger error as weights 
    param_w, cov_w = curve_fit(
        linear_fit, col_inst_list, col_cat_list, p0=p0, 
        sigma=col_insterr_list, absolute_sigma=False)
      #sigma=col_insterr_list_all, absolute_sigma=False)
    paramerr_w = np.sqrt(np.diag(cov_w))
    CTG_w, CTI_w = param_w[0], param_w[1]
    CTGerr_w, CTIerr_w = paramerr_w[0], paramerr_w[1]
    if verbose:
        print(f"    CTG wei = {CTG_w:.4f}+-{CTGerr_w:.4f} (not used)")
    
    ## Monte Carlo method to estimate errors
    ## Use mean and std of Monte Carlo results
    N_mc = 100
    # Set seed 0
    seed = 0
    np.random.seed(seed)
    CTG_temp_list = []
    CTI_temp_list = []

    for n in range(N_mc):
        # Initialization
        col_cat_sample = np.random.normal(
          col_cat_list, col_caterr_list, len(col_cat_list))
        col_inst_sample = np.random.normal(
          col_inst_list, col_insterr_list, len(col_cat_list))

        param_sample, cov_sample = curve_fit(
          linear_fit, col_inst_sample, col_cat_sample)
        CTG_sample = param_sample[0]
        CTI_sample = param_sample[1]
        CTG_temp_list.append(CTG_sample)
        CTI_temp_list.append(CTI_sample)
    
    CTG_MC_mean = np.mean(CTG_temp_list)
    CTG_MC_std = np.std(CTG_temp_list)
    CTI_MC_mean = np.mean(CTI_temp_list)
    CTI_MC_std = np.std(CTI_temp_list)
    if verbose:
        print(f"    CTG MC  = {CTG_MC_mean:.4f}+-{CTG_MC_std:.4f} (used)")
    # Raw =====================================================================
  
    ## Mean values
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    x = np.arange(xmin, xmax, 0.01)

    # ToDo Round error

    ## Raw
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_raw = (
            f"Raw y={CTG_r:.2f}" + "$\pm$" + f"{CTGerr_r:.3f}x + {CTI_r:.2f}"
            + "$\pm$" + f"{CTIerr_r:.3f}")
    ax.plot(
        x, CTG_r*x+CTI_r, marker="", lw=1, ls=myls[1], color=col_rawfit,
        label=label_raw)
    ## Weighted
    # Error1: return of scipy (do not consider xerr (col_cat error)!)
    label_wei = (
            f"Weighted y={CTG_w:.2f}" + "$\pm$" + f"{CTGerr_w:.3f}x + {CTI_w:.2f}"
        + "$\pm$" + f"{CTIerr_w:.3f}")
    ax.plot(
        x, CTG_w*x+CTI_w, marker="", lw=1, ls=myls[2], color=col_weifit,
        label=label_wei)

    ## Monte Carlo Method
    label_MC = (
        f"Monte Carlo y={CTG_MC_mean:.2f}" + "$\pm$" 
        + f"{CTG_MC_std:.3f}x + {CTI_MC_mean:.3f}" + "$\pm$" + f"{CTI_MC_std:.3f}")
    ax.plot(
        x, CTG_MC_mean*x+CTI_MC_mean, marker="", lw=1, ls=myls[0], 
        color=col_MCfit, label=label_MC)
    
    if shift:

        # Use different colors 
        if ycolor >1:
            # Size of each chunk
            width_y = ny/ycolor
            for y in range(ycolor):
                ymin = width_y*y
                ymax = width_y*(y+1)

                ## Residuals (MCfit - obs.)
                label_res = "Residuals\n(MCfit - obs.)"
                res = [
                    y-CTG_MC_mean*x+CTI_MC_mean for (x,y,y_coord) 
                    in zip(col_inst_list, col_cat_list, y_list)
                    if (ycoord > ymin) & (ycoord < ymax)]
                ax_res.hist(
                    res, 
                    orientation="horizontal", histtype="step", color=col_list[y],
                    label=label_res)
        else:
            ## Residuals (MCfit - obs.)
            label_res = "Residuals\n(MCfit - obs.)"
            res = [
                y-CTG_MC_mean*x+CTI_MC_mean for (x,y) 
                in zip(col_inst_list, col_cat_list)]
            ax_res.hist(
                res, 
                orientation="horizontal", histtype="step", color=col_MCfit,
                label=label_res)

        ax_res.set_xlabel("N")
        ax_res.legend(fontsize=10)
        ymin, ymax = ax_res.get_ylim()
        if yr_res is not None:
            ax_res.set_ylim(yr_res[0], yr_res[1])
        elif abs(ymin) > abs(ymax):
            ax_res.set_ylim(-abs(ymin), abs(ymin))
        else:
            ax_res.set_ylim(-abs(ymax), abs(ymax))

    col_cat_label = "$m_{cat," + band_l + "} - m_{cat," + band_r + "}$"
    col_inst_label = "$m_{inst," + band_l + "} - m_{inst," + band_r + "}$"
    ax.set_xlabel(col_inst_label)
    ax.set_ylabel(col_cat_label)
    xmin, xmax = ax.get_xlim()
    ax.set_xlim([xmin, xmax])
    ax.legend(fontsize=10)

    df_CTG = pd.DataFrame(
      {f"{band_l}_{band_r}_CTG_r"   : [CTG_r],
      f"{band_l}_{band_r}_CTGerr_r" : [CTGerr_r],
      f"{band_l}_{band_r}_CTG_w"    : [CTG_w],
      f"{band_l}_{band_r}_CTGerr_w" : [CTGerr_w],
      f"{band_l}_{band_r}_CTG_MC"   : [CTG_MC_mean],
      f"{band_l}_{band_r}_CTGerr_MC": [CTG_MC_std]}
      )
    return df_CTG



def plot_refladder(df, df_flux, bands, obj, outdir=".", out=None):
    """
    Plot lightcurves using refladder templates.

    Parameters
    ----------
    df : pandas.DataFrame
        dataframe with object flux, fluxerr etc.
    df_flux : pandas.DataFrame
        dataframe with flux templates
    bands : array-like
        bands of filters
    obj : str
        object name

    Return
    ------
    df : pandas.DataFrame
        dataframe with magnitude
    """
    N_band = len(bands)
    fig, axes_flux, axes_template, axes_mag, axes_col = myfigure_refladder(
        N_band)
    # Log scale
    for ax in axes_flux:
        ax.set_yscale("log")

    # List for y axis scaling
    ymin_list_mag, ymax_list_mag = [], []
    ymin_list_template, ymax_list_template = [], []
    ymin_list_col, ymax_list_col = [], []
    for idx,band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)
        ax_flux = axes_flux[idx]
        ax_template = axes_template[idx]
        ax_mag = axes_mag[idx]
        color = mycolor[idx]
        mark = mymark[idx]


        # Flux template =======================================================
        flux_total = df_flux[f"f_total_cor_{band}"]
        fluxerr_total = df_flux[f"ferr_total_cor_{band}"]
        flux_total_raw = df_flux[f"f_total_raw_{band}"]
        ax_template.errorbar(
          df_flux[f"t_sec_obs_{band}"], flux_total, yerr=fluxerr_total,
          fmt="o", linewidth=0.1, ms=1, color="gray", label="Corrected total flux")
        ax_template.errorbar(
          df_flux[f"t_sec_obs_{band}"], flux_total_raw,
          fmt="^", linewidth=0.1, ms=1, color="black", label="Raw total flux")
        ax_template.legend()
        # Flux template finish ================================================


        # Obj mag =============================================================
        flux_obj = df[f"flux_{band}"]
        fluxerr_obj = df[f"fluxerr_{band}"]
        ax_flux.errorbar(
          df[f"t_sec_obs_{band}"], flux_obj, yerr=fluxerr_obj, ls="None",
          linewidth=0.2, ms=3, color=color, marker=mark, 
          label=f"{obj} raw flux")

        # Normalized mangnitude
        ratio = flux_obj/flux_total
        ratioerr = diverr(flux_obj, fluxerr_obj, flux_total, fluxerr_total)
        # Remove 0 or negative
        idx_use = ratio > 0
        ratio = ratio[idx_use]
        ratioerr = ratioerr[idx_use]

        df = df[idx_use]

        mag = -2.5*np.log10(ratio)
        mag = mag - np.mean(mag)
        magerr = 2.5*log10err(ratio, ratioerr)

        # Save for output
        df.insert(0, f"mag_{band}", mag)
        df.insert(0, f"magerr_{band}", magerr)

        ax_mag.errorbar(
          df[f"t_sec_obs_{band}"], mag, yerr=magerr,ls="None",
          linewidth=0.2, ms=3, color=color, marker=mark,
          label=f"{obj} {band}-band magnitude")
        ax_mag.invert_yaxis()

        # Save magnitude y range
        ymin_mag, ymax_mag = ax_mag.get_ylim()
        ymin_list_mag.append(ymin_mag)
        ymax_list_mag.append(ymax_mag)

        ax_flux.set_ylabel("Flux")
        ax_mag.set_ylabel("Relative magnitude")
        ax_flux.legend()
        ax_mag.legend()
        # Obj mag =============================================================

    
    # After magnitude insertioin !! (need calculated mags above)
    for idx,band in enumerate(bands):
        band_l, band_r = band4CTG(band, bands)
        ax_col = axes_col[idx]
        color = mycolor[idx]
        mark = mymark[idx]
        # Obj color ===========================================================
        col = df[f"mag_{band_l}"] - df[f"mag_{band_r}"]
        colerr = adderr_series(
          df[f"magerr_{band_l}"], df[f"magerr_{band_r}"])
        
        # Average (c_mean, c_std)
        c_mean = np.mean(col)
        # Photometric uncertainty
        c_std_phot = np.mean(col)
        # Standard Deviation
        c_SD = np.std(col)
        # Standard Error
        c_SE = c_SD/np.sqrt(len(col))
        # Total error
        c_std = np.sqrt(c_std_phot**2 + c_SE**2)

        # Weighted mean (c_wmean, c_wstd)
        w = 1/colerr**2
        c_wmean = np.average(col, weights=w)
        # SD of weighted mean
        c_wstd = np.sqrt(1/np.sum(w))

        label_col = (
          f"{band_l}-{band_r}\n"
          f"  (mu,std)=({c_mean:.4f}+-{c_std:.4f})\n"
          f"  (mu_wei,std_wei)=({c_wmean:.4f}+-{c_wstd:.4f})"
          )
        ax_col.errorbar(
          df[f"t_sec_obs_{band}"], col, yerr=colerr, ls="None",
          linewidth=0.2, ms=3, color=color, marker=mark,
          label=label_col)
        ax_col.invert_yaxis()

        # Save color y range
        ymin_col, ymax_col = ax_col.get_ylim()
        ymin_list_col.append(ymin_col)
        ymax_list_col.append(ymax_col)

        ax_col.set_ylabel("Relative magnitude")
        ax_col.legend()

        # Save for output
        try:
          df.insert(0, f"{band_l}_{band_r}", col)
          df.insert(0, f"{band_l}_{band_r}err", colerr)
        except:
          # Pass if already exists
          pass
        # Obj color finish ====================================================

    # Set ylim ================================================================
    ## Mag
    ymin_mag = np.min(ymin_list_mag)
    ymax_mag = np.max(ymax_list_mag)
    # Margin for errorbars
    ymin_mag = ymin_mag*1.3
    ymax_mag = ymax_mag*1.3
    print(f"  Scaling y-axes of magnitude to {ymax_mag:.2f}--{ymin_mag:.2f}")
    for ax in axes_mag:
      ax.set_ylim(ymax_mag, ymin_mag)
      ax.invert_yaxis()

    ## Color
    ymin_col = np.min(ymin_list_col)
    ymax_col = np.max(ymax_list_col)
    # Margin for errorbars
    ymin_col = ymin_col*1.3
    ymax_col = ymax_col*1.3
    print(f"  Scaling y-axes of color to {ymax_col:.2f}--{ymin_col:.2f}")
    for ax in axes_col:
      ax.set_ylim(ymax_col, ymin_col)
      ax.invert_yaxis()
    # Set ylim finish =========================================================

    # Set labels ==============================================================
    ## Remove useless xlables of template
    for ax in axes_template[:-1]:
      ax.axes.xaxis.set_visible(False)

    axes_template[-1].set_xlabel("Time [s]")
    axes_mag[-1].set_xlabel("Time [s]")
    axes_col[-1].set_xlabel("Time [s]")
    # Set labels finish =======================================================


    band_str = "".join(bands)
    if out is None:
      out =  f"{obj}_refladderlc_N{len(df)}_{band_str}.png"
    out = os.path.join(outdir, out)
    plt.savefig(out, dpi=200)
    plt.close()
    return df



def refladder(
        df, objID, suf, Nframe_min, eflag_max, dedge_min, cntmfrac_max,
        key_x="xwin", key_y="ywin"):
    """
    Calculate total flux with reference ladder.

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    suf : str
        column name is like "HOGE_objID_suffix" 
    Nframe_min : int
         minimum threshold of number of frames
         (not contaminated, not on edge)
    key_x, key_y : str
        keywords of x and y
    dedge_min : int
        acceptable distance to edge region
    cntmfrac_max : int
        acceptable contamination fraction
        
    Return
    ------
    df_flux : pandas.DataFrame
         dataframe with calculated flux
    """
    # Save results
    f_total_raw_list, ferr_total_raw_list, = [], []
    f_total_cor_list, ferr_total_cor_list, = [], []

    # Loop for frame
    #   0 : previous frame
    #   1 : current frame
    for idx_row, row1 in df.iterrows():
        print(f"  Frame {idx_row+1}")
        # Define parameters for the 1st frame
        if idx_row == 0:
          obj0 = []
          f_total0_raw, ferr_total0_raw = 0, 0
          f_total0_cor, ferr_total0_cor = 0, 0
          C_common     = 1
          Cerr_common  = 0

        # Sum up Objects 
        #  small eflag, not on edge regions, and small cntmfrac
        #    do not have bad PS flag
        obj1, flux_temp, fluxerr_temp = [], [], []
        for obj in objID:
          eflag_obj    = row1[f"eflag_{obj}_{suf}"]
          dedge_obj    = row1[f"dedge_{obj}_{suf}"]
          cntmfrac_obj = row1[f"cntmfrac_{obj}_{suf}"]
        
          # Temporally
          if row1[f"flux_{obj}_{suf}"] > 1000000:
            x = row1[f"xwin_{obj}_{suf}"]
            y = row1[f"ywin_{obj}_{suf}"]
            fits = row1[f"fits_{suf}"]
            mag = row1[f"{suf}MeanPSFMag_{obj}_{suf}"]
            print(f"{x}, {y}, {obj}, {fits}, {mag}")
            continue

          # Good condition
          if ((dedge_obj > dedge_min) & (eflag_obj < eflag_max) 
              & (cntmfrac_obj < cntmfrac_max)):
            obj1.append(obj)
            flux_temp.append(row1[f"flux_{obj}_{suf}"])
            fluxerr_temp.append(row1[f"fluxerr_{obj}_{suf}"])
          # Bad condition
          else:
            # Do not use object
            pass
        
        # Raw total flux of effective objects in a frame
        f_total1_raw = np.sum(flux_temp)
        ferr_total1_raw = adderr(fluxerr_temp)
        print(f"    raw f_total : {f_total0_raw:.1f}, {f_total1_raw:.1f}")

        # Whether objects are common or not
        # 1. First frame (cor == raw)
        if idx_row==0:
          print(f"    Initial objects N={len(obj1)}")
          f_total1_cor = f_total1_raw
          ferr_total1_cor = ferr_total1_raw

        # 2. Common objects are detected
        #    -> go ahead but with C_common
        elif obj0 == obj1:
          # Use common coefficient for idx != 0 with raw flux0 and flux1
          C_common = f_total1_raw / f_total0_raw
          print(f"    Common objects N={len(obj1)}, C={C_common:.1f}")
          Cerr_common = diverr(
            f_total1_raw, ferr_total1_raw,
            f_total0_raw, ferr_total0_raw)
          f_total1_cor = C_common*f_total0_cor
          ferr_total1_cor = mulerr(
              C_common, Cerr_common,
              f_total0_cor, ferr_total0_cor)
          print(f"    cor f_total : {f_total0_cor:.1f}, {f_total1_cor:.1f}")

        # 3. objects are changed 
        else:
          obj_common = list(set(obj0) & set(obj1)) 
          print(f"    Different Objects N0, N1 = {len(obj0)}, {len(obj1)}")  
          print(f"    Use common objects N_common={len(obj_common)}")
          # Recalculate total flux of common objects
          f_total_common0, ferr_total_common0 = [], []
          f_total_common1, ferr_total_common1 = [], []
          for obj in obj_common:
            f_total_common0.append(row0[f"flux_{obj}_{suf}"])
            f_total_common1.append(row1[f"flux_{obj}_{suf}"])
            ferr_total_common0.append(row0[f"fluxerr_{obj}_{suf}"])
            ferr_total_common1.append(row1[f"fluxerr_{obj}_{suf}"])
          f_total_common0 = np.sum(f_total_common0)
          f_total_common1 = np.sum(f_total_common1)
          ferr_total_common0 = adderr(ferr_total_common0)
          ferr_total_common1 = adderr(ferr_total_common1)
          
          # Ratio 
          C_common = f_total_common1 / f_total_common0
          Cerr_common = diverr(
            f_total_common1, ferr_total_common1,
            f_total_common0, ferr_total_common0)
          f_total1_cor = C_common*f_total0_cor
          err_total1_cor = mulerr(
              C_common, Cerr_common,
              f_total0_cor, ferr_total0_cor)
          print(f"    cor f_total : {f_total0_cor:.1f}, {f_total1_cor:.1f}")

        # Save result
        f_total_raw_list.append(f_total1_raw)
        f_total_cor_list.append(f_total1_cor)
        ferr_total_raw_list.append(ferr_total1_raw)
        ferr_total_cor_list.append(ferr_total1_cor)

        # Update row for next iterration
        row0            = row1
        f_total0_raw    = f_total1_raw
        ferr_total0_raw = ferr_total1_raw
        f_total0_cor    = f_total1_cor
        ferr_total0_cor = ferr_total1_cor
        obj0            = obj1

    df_flux = pd.DataFrame(
      {f"f_total_raw_{suf}"    :f_total_raw_list,
       f"f_total_cor_{suf}"    :f_total_cor_list,
       f"ferr_total_raw_{suf}" :ferr_total_raw_list,
       f"ferr_total_cor_{suf}" :ferr_total_cor_list})
    return df_flux


def refladder2(df, objID, verbose=False):
    """
    Calculate total flux with reference ladder.

    ToDo check Cerr. (temporalily set to zero.)

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID with single band
    objID : array-like
        list of objects under good condition
        
    Return
    ------
    df_flux : pandas.DataFrame
         dataframe with calculated flux
    """
    # List to save results
    f_total_raw_list, ferr_total_raw_list, = [], []
    f_total_cor_list, ferr_total_cor_list, = [], []
    nframe_use_list = []

    nframe_list = list(set(df["nframe"].values.tolist()))
    
    # Initial setting
    # List of objID
    # 0 : previous frame
    # 1 : current frame
    C_common     = 1
    Cerr_common  = 0
    jd_list = []

    # Loop for frame
    # frame0 : current frame
    # frame1 : next frame
    for idx_frame,nframe in enumerate(nframe_list):
        if verbose:
            print(f"\nFrame {nframe}")
        df_1 = df[df["nframe"]==nframe]

        # Obtain flux, fluxerr, and objID of frame1
        flux_temp = df_1.flux.values.tolist()
        fluxerr_temp = df_1.fluxerr.values.tolist()
        obj1 = df_1.objID.values.tolist()
        obj1 = sorted(obj1)

        # Raw total flux of objects in frame1
        f_total1_raw = np.sum(flux_temp)
        ferr_total1_raw = adderr(fluxerr_temp)
        if verbose:
            print(
                f"    raw f_total : {f_total0_raw:.1f}+-{ferr_total0_raw:.1f}, "
                f"{f_total1_raw:.1f}+-{ferr_total1_raw:.1f}")

        # Whether objects are common or not
        # 1. First frame (cor == raw)
        # Note: idx_frame==0 means the first frame, not second!
        if idx_frame==0:
            if verbose:
                print(f"    Initial objects N={len(obj1)}")
            f_total1_cor = f_total1_raw
            ferr_total1_cor = ferr_total1_raw
            if verbose:
                print(
                    f"    ini f_total : {f_total1_cor:.1f}+-{ferr_total1_cor:.1f}")

        # 2. After 2nd frame. Common objects are detected
        #    -> go ahead but with C_common
        elif obj0 == obj1:
            # Use common coefficient with raw flux0 and flux1
            C_common = f_total1_raw / f_total0_raw
            # This error should be small.
            # If > 0.05, relative error is larger than typical photometric error.
            # i.e., bright stars are needed.
            Cerr_common = diverr(
                f_total1_raw, ferr_total1_raw, f_total0_raw, ferr_total0_raw)
            # ToDo check
            Cerr_common = 0
            if verbose:
                print(
                    f"    Common objects N={len(obj1)},"
                    f" C={C_common:.3f}+-{Cerr_common:.3f}")
            f_total1_cor = C_common*f_total0_cor
            ferr_total1_cor = mulerr(
                C_common, Cerr_common, f_total0_cor, ferr_total0_cor)

            #f_total1_cor = C_common*f_total1_cor
            #ferr_total1_cor = mulerr(
            #    C_common, Cerr_common, f_total1_cor, ferr_total1_cor)
            if verbose:
                print(
                    f"    cor f_total : {f_total0_cor:.1f}+-{ferr_total0_cor:.1f},"
                    f" {f_total1_cor:.1f}+-{ferr_total1_cor:.1f}")

        # 3. After 2nd frame. objects are changed 
        else:
            obj_common = list(set(obj0) & set(obj1)) 

            if len(obj_common)==0:
                if verbose:
                    print("No common objects detected. Skip the frame")
                df_0 = df_1
                obj0 = obj1
                f_total0_raw    = f_total1_raw
                ferr_total0_raw = ferr_total1_raw
                continue

            if verbose:
                print(f"    Different Objects N0, N1 = {len(obj0)}, {len(obj1)}")  
            # Recalculate total flux of common objects
            f_total_common0, ferr_total_common0 = [], []
            f_total_common1, ferr_total_common1 = [], []
            for obj in obj_common:
                f0_obj    = df_0[df_0["objID"]==obj]["flux"].values.tolist()[0]
                f0err_obj = df_0[df_0["objID"]==obj]["fluxerr"].values.tolist()[0]
                f1_obj    = df_1[df_1["objID"]==obj]["flux"].values.tolist()[0]
                f1err_obj = df_1[df_1["objID"]==obj]["fluxerr"].values.tolist()[0]

                f_total_common0.append(f0_obj)
                f_total_common1.append(f1_obj)
                ferr_total_common0.append(f0err_obj)
                ferr_total_common1.append(f1err_obj)
            f_total_common0 = np.sum(f_total_common0)
            f_total_common1 = np.sum(f_total_common1)
            ferr_total_common0 = adderr(ferr_total_common0)
            ferr_total_common1 = adderr(ferr_total_common1)
            if verbose:
                print(
                    f"    raw f_common_total : {f_total_common0:.1f}+-{ferr_total_common0:.1f},"
                    f" {f_total_common1:.1f}+-{ferr_total_common1:.1f}"
                    )

            
            # Ratio 
            C_common = f_total_common1 / f_total_common0
            Cerr_common = diverr(
                f_total_common1, ferr_total_common1,
                f_total_common0, ferr_total_common0)
            # ToDo check
            Cerr_common = 0
            if verbose:
                print(
                    f"    Common objects N={len(obj_common)},"
                    f" C={C_common:.3f}+-{Cerr_common:.3f}")

            f_total1_cor = C_common*f_total0_cor
            ferr_total1_cor = mulerr(
                C_common, Cerr_common,
                f_total0_cor, ferr_total0_cor)
            if verbose:
                print(
                    f"    cor f_total : {f_total0_cor:.1f}+-{ferr_total0_cor:.1f},"
                    f" {f_total1_cor:.1f}+-{ferr_total1_cor:.1f}")
            #if len(obj0) < len(obj1):
            #    assert False,1

        # Save result
        f_total_raw_list.append(f_total1_raw)
        f_total_cor_list.append(f_total1_cor)
        ferr_total_raw_list.append(ferr_total1_raw)
        ferr_total_cor_list.append(ferr_total1_cor)
        # Do not add 0 common object frame
        nframe_use_list.append(nframe)

        jd = df_1["t_jd"].values.tolist()[0]
        jd_list.append(jd)

        # Update row for next iterration
        df_0            = df_1
        obj0            = obj1

        f_total0_raw    = f_total1_raw
        ferr_total0_raw = ferr_total1_raw
        f_total0_cor    = f_total1_cor
        ferr_total0_cor = ferr_total1_cor

    df_flux = pd.DataFrame(
        {f"nframe"         :nframe_use_list,
         f"t_jd"    :jd_list,
         f"f_total_raw"    :f_total_raw_list,
         f"f_total_cor"    :f_total_cor_list,
         f"ferr_total_raw" :ferr_total_raw_list,
         f"ferr_total_cor" :ferr_total_cor_list})
    return df_flux


def refladder3(df, objID, verbose=False):
    """
    Calculate trend of the brightness.

    F_cor = F_obs/C.
    If nframe is 0,
      C = F_refall.
    Else,
      C = F_refall*(F_common_1/F_common_0).


    TODO
    ----
    Count the number of change of ref stars.
    Check Cerr. (temporalily set to zero.)

    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID with single band
    objID : array-like
        list of objects under good condition
        
    Return
    ------
    df_flux : pandas.DataFrame
         dataframe with calculated flux
    """
    # List to save results
    f_total_raw_list, ferr_total_raw_list, = [], []
    f_total_cor_list, ferr_total_cor_list, = [], []
    nframe_use_list = []

    nframe_list = list(set(df["nframe"].values.tolist()))
    
    jd_list = []
    corfac_list, corfacerr_list = [], []

    # number of change of field stars
    N_change = 0

    # Loop for frame
    # frame0 : current frame
    # frame1 : next frame
    for idx_frame,nframe in enumerate(nframe_list):
        df_1 = df[df["nframe"]==nframe]

        # Obtain flux, fluxerr, and objID of frame1
        flux_temp = df_1.flux.values.tolist()
        fluxerr_temp = df_1.fluxerr.values.tolist()
        obj1 = df_1.objID.values.tolist()
        obj1 = sorted(obj1)

        # Whether objects are common or not
        # 1. First frame
        if idx_frame==0:
            # Calculate total flux from all reference stars: F_refall_0
            F_refall_0 = np.sum(flux_temp)
            Ferr_refall_0 = adderr(fluxerr_temp)
            if verbose:
                print(
                    f"    Frame 0 F_refall_0 : {F_refall_0:.1f}+-{Ferr_refall_0:.1f}")

            corfac = F_refall_0
            corfacerr = Ferr_refall_0

        if idx_frame!=0:
            # Common object in the frame and previous frame
            N0 = len(obj0)
            N1 = len(obj1)
            obj_common = list(set(obj0) & set(obj1)) 
            N_common = len(obj_common)
            
            # Count the number of combination of field stars
            if N_common == N0 == N1:
                pass
            else:
                N_change += 1

            if len(obj_common)==0:
                if verbose:
                    print("No common objects Finish the process!!!")
                sys.exit()

            if verbose:
                print(f"    Frame {idx_frame} # refstars N0, N1 = {len(obj0)}, {len(obj1)}")  
            # Recalculate total flux of common objects
            f_total_common0, ferr_total_common0 = [], []
            f_total_common1, ferr_total_common1 = [], []
            for obj in obj_common:
                f0_obj    = df_0[df_0["objID"]==obj]["flux"].values.tolist()[0]
                f0err_obj = df_0[df_0["objID"]==obj]["fluxerr"].values.tolist()[0]
                f1_obj    = df_1[df_1["objID"]==obj]["flux"].values.tolist()[0]
                f1err_obj = df_1[df_1["objID"]==obj]["fluxerr"].values.tolist()[0]

                f_total_common0.append(f0_obj)
                f_total_common1.append(f1_obj)
                ferr_total_common0.append(f0err_obj)
                ferr_total_common1.append(f1err_obj)

            # Total flux of common stars
            f_total_common0 = np.sum(f_total_common0)
            f_total_common1 = np.sum(f_total_common1)
            ferr_total_common0 = adderr(ferr_total_common0)
            ferr_total_common1 = adderr(ferr_total_common1)
       
            print(f"{f_total_common0}, {ferr_total_common0}")
            print(f"{f_total_common1}, {ferr_total_common1}")

            # Value and uncertainty
            # This process is useless when all stars are common?
            tempval = f_total_common1/f_total_common0
            temperr = diverr(
                f_total_common1, ferr_total_common1,
                f_total_common0, ferr_total_common0)
            # Previous error and error of the coefficient
            # corfac1 = corfac0 * tempval
            corfacerr = mulerr(corfac, corfacerr, tempval, temperr)
            #corfacerr = 0.01
            corfac *= tempval
            if verbose:
                print(f"    Err of the frame {temperr/tempval*100.:.3f}%. Total err {corfacerr/corfac*100.:.3f}%.")  

        # Save result
        corfac_list.append(corfac)
        corfacerr_list.append(corfacerr)
        assert corfacerr/corfac*100 < 50, "Large corfac!"
        # Do not add 0 common object frame
        nframe_use_list.append(nframe)
        jd = df_1["t_jd"].values.tolist()[0]
        jd_list.append(jd)

        # Update dataframe and object
        df_0 = df_1
        obj0 = obj1


    df_flux = pd.DataFrame(
        {f"nframe"         :nframe_use_list,
         f"t_jd"    :jd_list,
         # Correction factor
         f"corfac"    :corfac_list,
         f"corfacerr" :corfacerr_list})
    if verbose:
        print(f"    Number of change of the combination of ref stars: {N_change}")  
    return df_flux


def remove_Nframe(
        df, objID, suffix, Nframe_min, eflag_max, dedge_min, cntmfrac_max,
        key_x="xwin", key_y="ywin"):
    """
    Claculate the number of detection of an object in a video.
    
    Parameters
    ----------
    df : pandas.DataFrame
        input list with objID
    objID : array-like
        list of objects under good condition
    suffix : str
        column name is like "HOGE_objID_suffix" 
    Nframe_min : int
         minimum threshold of number of frames
         (not contaminated, not on edge)
    key_x, key_y : str
        keywords of x and y
    dedge_min : int
        acceptable distance to edge region
    cntmfrac_max : int
        acceptable contamination fraction
        
    Return
    ------
    objID : array-like
         list of objects detected > Nframe_min
    """

    # For output
    # the frame number where the object detected at first
    detect1_list = []
    # coordinates of the first detection
    x1_list, y1_list = [], []

    # the total number of detections of an object 
    N_frame_obj_list = []
    # Magnitude list of an object
    mag_list = []
    # Remove object whose N_frame < N_Frame_min
    objID_rm = []

    column = df.columns.tolist()
    for obj in objID:
        # Search 1-band ref star
        col_obj = [x for x in column if obj in x]
        # Necessary to suppress `SettingWithCopyWarning`
        df_obj = df[col_obj].copy()

        # Select objects satisfy
        # Check the object 
        # 1. with small eflag, 2. not on edge regions, and 3. with small cntm.
        # (these are not common in frames)
        df_obj = df_obj[
          (df_obj[f"eflag_{obj}_{suffix}"] < eflag_max)
          & (df_obj[f"cntmfrac_{obj}_{suffix}"] < cntmfrac_max)
          & (df_obj[f"dedge_{obj}_{suffix}"] > dedge_min)
          ]

        # Number of detections of the object in a video
        N_frame_obj = len(df_obj)
        if N_frame_obj >= Nframe_min:
            # Use the object which were detected in enough frames
            # Add effective frame numbers
            N_frame_obj_list.append(N_frame_obj)
            # Add the frame number where the object detected at first
            idx_1st = df_obj.index.tolist()[0]
            detect1_list.append(idx_1st)
            # Add 1st frame coordinates
            x1_list.append(df_obj.at[idx_1st, f"{key_x}_{obj}_{suffix}"])
            y1_list.append(df_obj.at[idx_1st, f"{key_y}_{obj}_{suffix}"])
            # For PS catalog
            try:
                # Add catalog magnitude
                mag_list.append(
                    df_obj.at[idx_1st, f"{suffix}MeanPSFMag_{obj}_{suffix}"])
            except:
                mag_list.append(0)
        else:
            # Do not use the object which were not detected in enough frames
            objID_rm.append(obj)

    # Remove small detection objects (N_frame_obj < N_min_frame)
    for obj in objID_rm:
        objID.remove(obj)

    # Number of output lines
    N_line = 3
    N_iter = int(np.ceil(len(objID)/N_line))
    print(f"  objID (N_frame, 1stframe, x, y, {suffix} mag)")
    for n in range(N_iter):
        objs = objID[n*N_line:(n+1)*N_line]
        N_frame = N_frame_obj_list[n*N_line:(n+1)*N_line]
        mags = mag_list[n*N_line:(n+1)*N_line]
        detect1st = detect1_list[n*N_line:(n+1)*N_line]
        x1st = x1_list[n*N_line:(n+1)*N_line]
        y1st = y1_list[n*N_line:(n+1)*N_line]
        if len(objs)==1:
            print(
              f"    {objs[0]} ({N_frame[0]:4d}, {detect1st[0]:4d}, "
              f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f})")
        elif len(objs)==2:
            print(
              f"    {objs[0]} ({N_frame[0]:4d}, {detect1st[0]:4d}, " 
              f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]}"
              f"{objs[1]} ({N_frame[1]:4d}, {detect1st[1]:4d}, "
              f"{x1st[1]:5.0f}, {y1st[1]:5.0f}, {mags[1]:.1f})"
              )
        else:
            print(
              f"    {objs[0]} ({N_frame[0]:4d}, {detect1st[0]:4d}, " 
              f"{x1st[0]:5.0f}, {y1st[0]:5.0f}, {mags[0]:.1f}) "
              f"{objs[1]} ({N_frame[1]:4d}, {detect1st[1]:4d}, "
              f"{x1st[1]:5.0f}, {y1st[1]:5.0f}, {mags[1]:.1f}) "
              f"{objs[2]} ({N_frame[2]:4d}, {detect1st[2]:4d}, "
              f"{x1st[2]:5.0f}, {y1st[2]:5.0f}, {mags[2]:.1f})")
    return objID


def remove_suffix(df, suffix):
    """
    Remove suffix from the column name.

    Parameter
    ---------
    df : pandas.DataFrame
        dataframe

    Return
    ------
    df : pandas.DaraFrame
        suffix removed DataFrame
    """
    col0 = df.columns.tolist()
    idx_suf = -(len(suffix)+1)
    col_to_be_renamed = [x for x in col0 if x[idx_suf:]==f"_{suffix}" in x]
    # Rename HOGE_suffix -> HOGE
    for col in col_to_be_renamed:
        col_renamed = col[:idx_suf]
        df = df.rename(columns={col:col_renamed})
    return df


def band4CT(band, bands):
  """
  Return 2 bands for CT determination.
  For CT ! 
  if g,r,i -> g-r, g-r, r-i
  
  Parameters
  ----------
  band : str
    band of fits
  bands : str
    3 bands

  Returns
  -------
  mag_l, mag_r : float
    used magnitude
  """

  if bands == ["g", "r", "i"]:

    if band=="g":
      mag_l, mag_r = "g", "r"
    elif band=="r":
      mag_l, mag_r = "g", "r"
    elif band=="i":
      mag_l, mag_r = "r", "i"
  return mag_l, mag_r


def calc_d_from_edge(x, y, nx, ny):
    """
    Calculate distance from the edge

    Parameters
    ----------
    x, y : array-like
        x and y in pixel
    nx, ny : int
        number of pixels along each axis

    Return
    ------
    d : array-like
        distance fron the closest edge
    """
    
    # Distance from 0 and nx
    d_x1 = [abs(nx-x) for x in x]
    d_x2 = [abs(x) for x in x]
    
    # Distance from 0 and ny
    d_y1 = [abs(ny-y) for y in y]
    d_y2 = [abs(y) for y in y]
    
    # Closer distances
    d_x_small = [np.min([a,b]) for (a,b) in zip(d_x1,d_x2)]
    d_y_small = [np.min([a,b]) for (a,b) in zip(d_y1,d_y2)]
    
    d = [np.min([d_x, d_y]) for (d_x, d_y) in zip(d_x_small, d_y_small)]
    return d

def set_baffle_TriCCS(
        df, width=250, nx=2160, ny=1280, key_x="xwin", key_y="ywin"):
    """
    Clean data frame with baffles.

    Parameters
    ----------
    df : pandas.DataFrame
        input DataFrame
    width : float
        width of square region
    key_x : str
        keyword for x
    key_y : str
        keyword for y

    Return
    ------
    df : pandas.DataFrame
        baffle removed DataFrame
    """
    edge1 = (df[key_x] < width) & (df[key_y] < width)
    edge2 = (df[key_x] < width) & (ny-df[key_y] < width)
    edge3 = (nx-df[key_x] < width) & (ny-df[key_y] < width)
    edge4 = (nx-df[key_x] < width) & (df[key_y] < width)
    df = df[~(edge1|edge2|edge3|edge4)]

    return df


def clean_photres_ref(
    df, bands, key_x, key_y, gr_min=0, gr_max=1.1, ri_min=0, ri_max=0.8, 
    catmag_min=12, catmag_max=20, catmagerr_max=0.05, 
    dedge_min=0, nx=2160, ny=1180, eflag_max=1, verbose=False):
    """
    Clean the photometry result of reference stars.
 
    p1 Remove reference stars with extreme colors
    p2 Remove faint/bright stars
    p3 Remove stars with large magnitude error
    p4 Remove stars by PS objinfoflag
    p5 Remove stars by PS objfilterflag (e.g., gFlags)
    p6 Remove stars by dedge
    p7 Remove stars by eflag

    ToDo
    ----
    2-band data analysis (2022-07-11)

    Parameters
    ----------
    df : pandas.DataFrame
        initial photoemtric result
    bands : array-like
        bands of filters
    key_x, key_y : str
        keywords for x and y coordinates
    gr_min, gr_max, ri_min, ri_max : float
        thresholds of colors
    catmag_min, catmag_max : float
        catalog magnitude threshold
    catmagerr_max : float
        catalog magnitude error threshold
    dedge_min : float
        distance threshold from the edge
    nx, ny : int
        number of pixels toward x/y directions
    eflag_max : float
        maximum eflag to be permitted
    verbose : bool
        output messages
    
    Return
    ------
    df : pandas.DataFrame
        cleaned DataFrame
    """
    
    # To avoid simple mistake target/ref
    col_objtype = list(set(df["objtype"]))
    assert col_objtype == ["ref"], "Check the input"
   
    # g, r, i are always used to select by colors
    # These values are used for selection
    g_mag, g_magerr = f"gMeanPSFMag", f"gMeanPSFMagErr"
    r_mag, r_magerr = f"rMeanPSFMag", f"rMeanPSFMagErr"
    i_mag, i_magerr = f"iMeanPSFMag", f"iMeanPSFMagErr"

    # Obtain common objID from input DataFrame
    objID_all = set(df["objID"])
    if verbose:
        print(f"N_refstar (len(objID)) = {len(objID_all)}")
        print(f"N_df      (len(df)) = {len(df)}")
    
    # Select by color (gr_min, gr_max, ri_min, and ri_max)
    if ((gr_min is not None) and (gr_max is not None) 
        and (ri_min is not None) and (ri_max is not None)):
        if verbose:
            print("\n==========================================================")
            print("p1 Remove reference stars with extreme colors")
            print("============================================================")
            print(f"  Remove g-r > {gr_max} and g-r < {gr_min}")
            print(f"     and r-i > {ri_max} and r-i < {ri_min}")

        # Extract extreme color objects 
        df = df[
            ((df[g_mag]-df[r_mag]) < gr_max) 
            & ((df[g_mag]-df[r_mag]) > gr_min)
            & ((df[r_mag]-df[i_mag]) < ri_max)
            & ((df[r_mag]-df[i_mag]) > ri_min)]
    objID_all = set(df["objID"])
    if verbose:
        print(f"N_refstar (len(objID)) = {len(objID_all)}")
        print(f"N_df      (len(df)) = {len(df)}")
        print(f"   (after extreme color star removal)")


    # Useless?
    # if verbose:
    #     print("\n============================================================")
    #     print("p2 Remove faint/bright stars")
    #     print("==============================================================")
    #     print(f"  Remove mag > {catmag_max} and mag < {catmag_min}")

    # # Use common values !!
    # for b in bands:
    #     df = df[
    #         ((df[f"{b}MeanPSFMag"]) < catmag_max) 
    #         & ((df[f"{b}MeanPSFMag"]) > catmag_min)]
    # objID_all = set(df["objID"])
    # if verbose:
    #     print(f"N_refstar (len(objID)) = {len(objID_all)}")
    #     print(f"N_df      (len(df)) = {len(df)}")
    #     print(f"   (after faint/bright star removal)")
    

    if verbose:
        print("\n============================================================")
        print("p3 Remove stars with large magnitude error")
        print("==============================================================")
        print(f"  Remove magerr > {catmagerr_max}")
    # Use common values !!
    for b in bands:
        df = df[
            ((df[f"{b}MeanPSFMagErr"]) < catmagerr_max)]
    objID_all = set(df["objID"])
    if verbose:
        print(f"N_refstar (len(objID)) = {len(objID_all)}")
        print(f"N_df      (len(df)) = {len(df)}")
        print(f"   (after large magerr star removal)")
    

    if verbose:
        print("\n============================================================")
        print("p4 Remove stars by PS objinfoflag")
        print("==============================================================")
    objflag_all = set(df["objinfoFlag"])
    for objflag in objflag_all:
        if not check_objinfo(objflag):
            if verbose:
                print(
                    f"  Remove by PS flag : {objflag}")
            df = df[df["objinfoFlag"] != objflag]
    objID_all = set(df["objID"])
    if verbose:
        print(f"N_refstar (len(objID)) = {len(objID_all)}")
        print(f"N_df      (len(df)) = {len(df)}")
        print(f"   (after objinfo removal)")


    if verbose:
        print("\n============================================================")
        print("p5 Remove stars by PS objfilterflag (e.g., gFlags)")
        print("==============================================================")
    for b in bands:
        objflag_all = set(df[f"{b}Flags"])
        for objflag in objflag_all:
            if not check_objfilter(objflag):
                if verbose:
                    print(
                        f"  Remove by PS flag : {objflag}")
                df = df[df[f"{b}Flags"] != objflag]
        objID_all = set(df["objID"])
        if verbose:
            print(f"N_refstar (len(objID)) = {len(objID_all)}")
            print(f"N_df      (len(df)) = {len(df)}")
            print(f"   (after {b}Flags removal)")


    if verbose:
        print("\n============================================================")
        print("p6 Remove stars by dedge")
        print("==============================================================")
        print(f"  Remove dedge <= {dedge_min}")
    # Calculate dedge
    df.loc[:,f"dedge"] = calc_d_from_edge(
        df[f"{key_x}"], df[f"{key_y}"], nx, ny)
    # Remove small dedge objects
    df = df[df["dedge"] > dedge_min]
    objID_all = set(df["objID"])
    if verbose:
        print(f"N_refstar (len(objID)) = {len(objID_all)}")
        print(f"N_df      (len(df)) = {len(df)}")
        print(f"   (after small dedge removal)")


    if verbose:
        print("\n============================================================")
        print("p7 Remove stars by eflag")
        print("==============================================================")
        print(f"  Remove eflag >= {eflag_max}")
    df = df[df["eflag"] < eflag_max]
    objID_all = set(df["objID"])
    if verbose:
        print(f"N_refstar (len(objID)) = {len(objID_all)}")
        print(f"N_df      (len(df)) = {len(df)}")
        print(f"   (after large eflag removal)")

    return df


def clean_photres_target(
    df, key_x, key_y, dedge_min=0, nx=2160, ny=1128, eflag_max=1, 
    verbose=False):
    """
    Clean the photometry result for reference stars (for new format).
 
    p1 Remove stars by dedge
    p2 Remove stars by eflag

    Note
    ----
    bad objects (large eflag etc.) are also removed in this function.

    ToDo
    ----
    Update for other band data

    Parameters
    ----------
    df : pandas.DataFrame
        initial photoemtric result
    key_x, key_y : str
        keywords for x and y
    dedge_min : float
        distance threshold from the edge
    nx, ny : int
        number of pixels toward x/y directions
    eflag_max : float
        maximum eflag to be permitted

    verbose : bool
        output messages
    
    Return
    ------
    df : pandas.DataFrame
        cleaned DataFrame
    """
    
    # To avoid simple mistake target/ref
    col_objtype = list(set(df["objtype"]))
    assert col_objtype == ["target"], "Check the input"


    if verbose:
        print("\n============================================================")
        print("p1 Remove stars by dedge")
        print("==============================================================")
        print(f"  Remove dedge <= {dedge_min}")
    # Calculate dedge
    df.loc[:,f"dedge"] = calc_d_from_edge(
        df[f"{key_x}"], df[f"{key_y}"], nx, ny)
    # Remove small dedge objects
    df = df[df["dedge"] > dedge_min]
    if verbose:
        print(f"N_df      (len(df)) = {len(df)}")
        print(f"   (after small dedge removal)")


    if verbose:
        print("\n============================================================")
        print("p2 Remove stars by eflag")
        print("==============================================================")
        print(f"  Remove eflag >= {eflag_max}")
    # Remove large cntmfrac objects
    df = df[df["eflag"] < eflag_max]
    if verbose:
        print(f"N_df      (len(df)) = {len(df)}")
        print(f"   (after large eflag removal)")

    return df


def clean_photres(
    df, bands, key_x, key_y, gr_min=-1, gr_max=1, ri_min=-1, ri_max=1, 
    catmag_min=12, catmag_max=20, catmagerr_max=0.1, verbose=False):
    """
    Clean the photometry result.
 
      p1 Obtain common objID from input DataFrame
      p2 Remove reference stars with extreme colors
      p3 Remove faint stars
      p4 Remove large catalog magnitude error stars
      p5 Remove PS flagged stars


    Note
    ----
    bad objects (large eflag etc.) are not removed in this function,
    but in successive processes.

    ToDo
    ----
    Update for other band data


    Parameters
    ----------
    df_phot : pandas.DataFrame
        initial photoemtric result
    bands : array-like
        bands of filter
    key_x, key_y : str
        keywords for x and y
    gr_min, gr_max, ri_min, ri_max : float
        thresholds of colors
    catmag_min : float
        catalog magnitude threshold
    catmag_max : float
        catalog magnitude threshold
    catmagerr_max : float
        catalog magnitude error threshold
    verbose : bool
        output messages
    
    Return
    ------
    df : pandas.DataFrame
        cleaned DataFrame
    """
    

    assert bands==["g", "r", "i"]
    N_band = len(bands)
    b1 = bands[0]


    if verbose:
        print("\n============================================================")
        print("p1 Obtain common objID from input DataFrame")
        print("==============================================================")
    # Length of objID in Pan-STARRS catalog is 18
    # objID is extracted from XX__(objid)__YY
    # such as `flux_(objid)_g`
    col0 = df.columns.tolist()

    # List of objID in all bands
    objID_all = []
    for band in bands:
        # Removethe target related columns if exsits
        # such as `flux_g`
        col_band = [col for col in col0 if (len(col))>18 and f"_{band}" in col]
        # Extract objID from X_(objid)_Y
        objID_band = [col.split("_")[1] for col in col_band]
        # Extract unique objID set
        objID_band = set(objID_band)
        objID_all.append(objID_band)

    objID = objID_all[0]
    # Extract common objID 
    for n in range(N_band-1):
        objID = objID & objID_all[n+1]
    # Set to list
    objID = list(objID)
    # For the sake of the clarity when debug
    objID = sorted(objID)
    if verbose:
        print(f"N_refstar (len(objID))= {len(objID)} (common)")
    
    
    # Need gr_min, gr_max, ri_min, ri_max
    if ((gr_min is not None) and (gr_max is not None) 
        and (ri_min is not None) and (ri_max is not None)):
      if verbose:
          print("\n==========================================================")
          print("p2 Remove reference stars with extreme colors")
          print("============================================================")
          print(f"  Remove g-r > {gr_max} and g-r < {gr_min}")
          print(f"     and r-i > {ri_max} and r-i < {ri_min}")
      rm_list = []
      col = df.columns.tolist()
      for obj in objID:
          g_col = f"gMeanPSFMag_{obj}_{b1}"
          r_col = f"rMeanPSFMag_{obj}_{b1}"
          i_col = f"iMeanPSFMag_{obj}_{b1}"
          # Extract extreme color objects 
          df_extcol = df[
              ((df[g_col]-df[r_col]) > gr_max) 
              | ((df[g_col]-df[r_col]) < gr_min)
              | ((df[r_col]-df[i_col]) > ri_max)
              | ((df[r_col]-df[i_col]) < ri_min)]

          if len(df_extcol) != 0:
              # Output extreme color object info. of first detection
              idx_1st = df_extcol.index[0]
              x = df_extcol.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
              y = df_extcol.at[idx_1st, f"{key_y}_{obj}_{b1}"]
              if verbose:
                  print(
                      f"  Remove an object with extreme color:"
                      f" {obj} (x,y) = ({x:.1f},{y:.1f})")
              # Remove red object in df
              col_extcol = [x for x in col if obj in x]
              df = df.drop(col_extcol, axis=1)
              rm_list.append(obj)
      # Remove extreme color object in objID
      for x in rm_list:
          objID.remove(x)
      if verbose:
          print(f"N_ref (len(objID))={len(objID)} (after removal of extcol")

    
    if verbose:
        print("\n============================================================")
        print("p3 Remove faint stars")
        print("==============================================================")
    col = df.columns.tolist()
    for b in bands:
        rm_list = []
        for obj in objID:
            # cat mag has 'm_cat' or '0',
            # so remove 0 padding results
            df_mag = df[df[f"{b}MeanPSFMag_{obj}_{b1}"]!=0]
            df_mag = df_mag[
              (df_mag[f"{b}MeanPSFMag_{obj}_{b1}"] > catmag_max)
              | (df_mag[f"{b}MeanPSFMag_{obj}_{b1}"] < catmag_min)]
    
            if len(df_mag) != 0:
                # Output extreme color object info. of first detection
                idx_1st = df_mag.index[0]
                x = df_mag.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
                y = df_mag.at[idx_1st, f"{key_y}_{obj}_{b1}"]
                if verbose:
                    print(
                        f"  Remove faint objec:"
                        f" {obj} (x,y) = ({x:.1f},{y:.1f})")
                # Remove large magnitude error object in df
                col_mag = [x for x in col if obj in x]
                df = df.drop(col_mag, axis=1)
                rm_list.append(obj)
        # Remove large magnitude error object in objID
        for x in set(rm_list):
            objID.remove(x)
    if verbose:
        print(f"N_refstar (len(objID))= {len(objID)} (after faint removal)")
    
    assert len(objID) > 0, "N_ref star = 0 ! Check the script."


    if verbose:
        print("\n============================================================")
        print("p4 Remove large catalog magnitude error stars")
        print("==============================================================")
    col = df.columns.tolist()
    for b in bands:
        rm_list = []
        for obj in objID:
            df_magerr = df[df[f"{b}MeanPSFMagErr_{obj}_{b1}"] > catmagerr_max]
    
            if len(df_magerr) != 0:
                # Output extreme color object info. of first detection
                idx_1st = df_magerr.index[0]
                x = df_magerr.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
                y = df_magerr.at[idx_1st, f"{key_y}_{obj}_{b1}"]
                if verbose:
                    print(
                        f"  Remove large error object:"
                        f" {obj} (x,y) = ({x:.1f},{y:.1f})")
                # Remove large magnitude error object in df
                col_magerr = [x for x in col if obj in x]
                df = df.drop(col_magerr, axis=1)
                rm_list.append(obj)
        # Remove large magnitude error object in objID
        for x in set(rm_list):
            objID.remove(x)
    if verbose:
        print(f"N_refstar (len(objID))= {len(objID)} (after magerr removal)")


    if verbose:
        print("\n============================================================")
        print("p5 Remove stars by PS flag")
        print("==============================================================")
    col = df.columns.tolist()
    rm_list = []
    for obj in objID:
        idx_1st = df.index[0]
        objflag = df.loc[idx_1st, f"objinfoFlag_{obj}_{b1}"]
        if not check_objinfo(objflag, PS_checked):
            x = df.at[idx_1st, f"{key_x}_{obj}_{b1}"] 
            y = df.at[idx_1st, f"{key_y}_{obj}_{b1}"]
            if verbose:
                print(
                    f"  Remove PS flagged object:"
                    f" {obj} (x,y) = ({x:.1f},{y:.1f})")
            # Remove large magnitude error object in df
            col_PSflag = [x for x in col if obj in x]
            df = df.drop(col_PSflag, axis=1)
            rm_list.append(obj)
        else:
            pass
    # Remove large magnitude error object in objID
    for x in set(rm_list):
        objID.remove(x)
    if verbose:
        print(f"N_refstar (len(objID))= {len(objID)} (after PSflag removal)")


    return df, objID


