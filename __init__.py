#!/usr/bin/env python
# -*- coding: utf-8 -*-
__copyright__    = "Copyright (C) 2021 Jin Beniyama"
__version__      = "0.0.1"
__license__      = "MIT"
__author__       = "Jin BENIYAMA"
__author_email__ = "beniyama@ioa.s.u-tokyo.ac.jp"
__url__          = "https://bitbucket.org/jin_beniyama/movphot/src/master/"
