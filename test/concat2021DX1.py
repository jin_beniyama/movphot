#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Concat gri and grz data.
"""
import os 
import sys
from argparse import ArgumentParser as ap
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  

from colphotcommon import stackcolor


if __name__ == "__main__":
  parser = ap(description="Colorterm determination")
  parser.add_argument(
    "gri", type=str, help="photres")
  parser.add_argument(
    "grz", type=str, help="photres")
  args = parser.parse_args()


  df_gri = pd.read_csv(args.gri, sep=" ")
  df_gri = df_gri[["i_g", "i_gerr", "g_i", "g_ierr", "r_i", "r_ierr"]]
  df_gri["obj"] = "2021DX1"
  df_gri = stackcolor(df_gri)

  df_grz = pd.read_csv(args.grz, sep=" ")
  df_grz["obj"] = "2021DX1"
  df_grz = stackcolor(df_grz)

  df_con = pd.concat([df_gri, df_grz], axis=1)
  df_con.to_csv("2021DX1_magall_griz.csv", sep=" ", index=False)
