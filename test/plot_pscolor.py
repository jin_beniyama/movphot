#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Plot color term from g,r,i photometric data.
Do `ls standardr30refg_00* > glist.tx` etc. in /ref to create flist.

Example
-------
plot_pscolor.py glist.txt rlist.txt ilist.txt . . . i
"""
import os 
from argparse import ArgumentParser as ap
from argparse import ArgumentDefaultsHelpFormatter
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  
from scipy.optimize import curve_fit

from calcerror import adderr, adderr_series, log10err
from myio import get_filename, float2str
from myplot import mycolor, myls
from plot_limmag import nonlinear_fit, calc_intersec


if __name__ == "__main__":
  parser = ap(description="Plot color term",
    formatter_class=ArgumentDefaultsHelpFormatter)
  parser.add_argument(
    "flist", nargs=3, 
    help='g, r, i or z reference star csv list')
  parser.add_argument(
    "csvdir", nargs=3, 
    help='g, r, i or z csv directory')
  parser.add_argument(
    "band3", type=str, choices=["i", "z"],
    help="3rd band of data obtained with the TriCCS")
  parser.add_argument(
    "--th_snr", type=float, default=30, 
    help="snr threshold value to calculate lim. mag.")
  args = parser.parse_args()

  outdir = "plot"
  os.makedirs(outdir, exist_ok=True)
  commons = []
  ID_use = []
  band3 = args.band3

  with open(args.flist[0], "r") as f_g,\
       open(args.flist[1], "r") as f_r,\
       open(args.flist[2], "r") as f_3:
    f_g = f_g.readlines()
    f_r = f_r.readlines()
    f_3 = f_3.readlines()
    for n, (csv_g, csv_r, csv_3) in enumerate(zip(f_g, f_r, f_3)):
      csv_g = csv_g.split("\n")[0]
      csv_r = csv_r.split("\n")[0]
      csv_3 = csv_3.split("\n")[0]

      df_g = pd.read_csv(csv_g, sep=" ")
      df_r = pd.read_csv(csv_r, sep=" ")
      df_3 = pd.read_csv(csv_3, sep=" ")
      ID_g = df_g["objID_g"].to_list()
      ID_r = df_r["objID_r"].to_list()
      ID_3 = df_3[f"objID_{band3}"].to_list()

      ID_common = set(ID_g) & set(ID_r) & set(ID_3)
      
      df_g = df_g[df_g["objID_g"].isin(ID_common)]
      df_r= df_r[df_r["objID_r"].isin(ID_common)]
      df_3 = df_3[df_3[f"objID_{band3}"].isin(ID_common)]
      df_g = df_g.reset_index(drop=True)
      df_r = df_r.reset_index(drop=True)
      df_3 = df_3.reset_index(drop=True)
      df_common = pd.concat([df_g, df_r, df_3], axis=1)
      df_common["frame"] = n+1
      commons.append(df_common)
      ID_use =  set(ID_use) | set(ID_common)
  print("Used reference stars' objID")
  print(ID_use)
  # all frame reference stars 
  df_all = pd.concat(commons)
  df_all["inst_g"] = 25.-2.5*np.log10(df_all["flux_g"])
  df_all["inst_r"] = 25.-2.5*np.log10(df_all["flux_r"])
  df_all[f"inst_{band3}"] = 25.-2.5*np.log10(df_all[f"flux_{band3}"])

  label_g = f"g-band N={len(df_all)}"
  label_r = f"r-band N={len(df_all)}"
  label_3 = f"{band3}-band N={len(df_all)}"
  fig = plt.figure(figsize=(20, 15))  

  ax1 = fig.add_subplot(221)
  ax1.set_xlabel("mag inst")
  ax1.set_ylabel("mag catalog")
  ax1.errorbar(
    df_all["inst_g"], df_all["mag_g"], 
    yerr=df_all["magerr_g"],
    fmt="o", label=label_g, 
    c="green", markersize=1)
  ax1.errorbar(
    df_all["inst_r"], df_all["mag_r"], 
    yerr=df_all["magerr_r"],
    fmt="o", label=label_r, 
    c="red", markersize=1)
  ax1.errorbar(
    df_all[f"inst_{band3}"], df_all[f"mag_{band3}"], 
    yerr=df_all[f"magerr_{band3}"],
    fmt="o", label=label_3, 
    c="magenta", markersize=1)
  x = np.arange(np.min(df_all["inst_g"]), np.max(df_all["inst_g"]), 0.1)
  y = x
  ax1.plot(x, y, label="m_inst = m_catalog", c=mycolor[3], linestyle=myls[1])
  ax1.invert_xaxis()
  ax1.invert_yaxis()
  ax1.legend()

  ax2 = fig.add_subplot(222)
  ax2.set_xlabel("inst color")
  ax2.set_ylabel("catalog color")
  ax2.scatter(
    df_all["inst_g"]-df_all["inst_r"], df_all["mag_g"]-df_all["mag_r"], 
    label="g-r", c="green", s=1)
  ax2.scatter(
    df_all["inst_r"]-df_all[f"inst_{band3}"], df_all["mag_r"]-df_all[f"mag_{band3}"], 
    label=f"r-{band3}", c="red", s=1)
  ax2.scatter(
    df_all[f"inst_{band3}"]-df_all["inst_g"], df_all[f"mag_{band3}"]-df_all["mag_g"], 
    label=f"{band3}-g", c="magenta", s=1)
  ax2.legend()

  ax3 = fig.add_subplot(223)
  ax3.set_xlabel("mag catalog")
  ax3.set_ylabel("SNR")
  ax3.scatter(
    df_all["mag_g"], df_all["snr_g"], 
    label="g", c="green", s=1)
  ax3.scatter(
    df_all["mag_r"], df_all["snr_r"], 
    label="r", c="red", s=1)
  ax3.scatter(
    df_all[f"mag_{band3}"], df_all[f"snr_{band3}"], 
    label=f"{band3}", c="magenta", s=1)
  print(f"minimum snr_g {np.min(df_all['snr_g'])}")
  print(f"minimum snr_r {np.min(df_all['snr_r'])}")
  print(f"minimum snr_{band3} {np.min(df_all[f'snr_{band3}'])}")
  ax3.legend()

  # NOTE : threshold is inly for g-band
  th_magmin = 14
  th_magmax = 19
  df_fit = df_all[(df_all["mag_g"] > th_magmin) & (df_all["mag_g"] < th_magmax)]
  ax4 = fig.add_subplot(224)
  ax4.set_xlabel("mag catalog")
  ax4.set_ylabel("SNR")
  ax4.set_xlim([th_magmin, th_magmax])
  ax4.set_ylim([0, np.max(df_fit["snr_g"])*1.1])
  ax4.scatter(
    df_fit["mag_g"], df_fit["snr_g"], 
    label="g", c="green", s=1)
  ax4.scatter(
    df_fit["mag_r"], df_fit["snr_r"], 
    label="r", c="red", s=1)
  ax4.scatter(
    df_fit[f"mag_{band3}"], df_fit[f"snr_{band3}"], 
    label="i", c="magenta", s=1)


  param_g, cov_g = curve_fit(nonlinear_fit, df_fit["mag_g"], df_fit["snr_g"])
  param_r, cov_r = curve_fit(nonlinear_fit, df_fit["mag_r"], df_fit["snr_r"])
  param_i, cov_i = curve_fit(nonlinear_fit, df_fit[f"mag_{band3}"], df_fit[f"snr_{band3}"])
  mag_g = np.arange(np.min(df_fit["mag_g"]), np.max(df_fit["mag_g"]), 0.01)
  mag_r = np.arange(np.min(df_fit["mag_r"]), np.max(df_fit["mag_r"]), 0.01)
  mag_3 = np.arange(np.min(df_fit[f"mag_{band3}"]), np.max(df_fit[f"mag_{band3}"]), 0.01)
  snr_fit_g = param_g[0]*10**(-0.4*mag_g)
  snr_fit_r = param_r[0]*10**(-0.4*mag_r)
  snr_fit_3 = param_i[0]*10**(-0.4*mag_3)
  th_snr = args.th_snr
  intersec_g = calc_intersec(x=mag_g, y=snr_fit_g, th=th_snr)
  intersec_r = calc_intersec(x=mag_r, y=snr_fit_r, th=th_snr)
  intersec_3 = calc_intersec(x=mag_3, y=snr_fit_3, th=th_snr)
  ax4.plot(
    mag_g, snr_fit_g, 
    label=(f"fitting\n(use {th_magmin}<g mag<{th_magmax})\n"
    f"intersection w/S/N={th_snr}: {intersec_g:.2f}"), 
    c="green", linewidth=1, linestyle="dotted")
  ax4.plot(
    mag_r, snr_fit_r, 
    label=(f"fitting\n(use {th_magmin}<g mag<{th_magmax})\n"
    f"intersection w/S/N={th_snr}: {intersec_r:.2f}"), 
    c="red", linewidth=1, linestyle="dotted")
  ax4.plot(
    mag_3, snr_fit_3, 
    label=(f"fitting\n(use {th_magmin}<g mag<{th_magmax})\n"
    f"intersection w/S/N={th_snr}: {intersec_3:.2f}"), 
    c="magenta", linewidth=1, linestyle="dotted")
  xmin, xmax = ax4.get_xlim()
  ax4.hlines(
    th_snr, xmin, xmax, color="gray", linestyle="dotted", lw=1, label=f"S/N={th_snr}")

  ax4.legend()
  
  snr = float2str(th_snr)
  out = f"inst_cat_comp_snr{snr}.png"
  out = os.path.join(outdir, out)
  plt.savefig(out)
